package com.mart.martonlinevendor.AddNewProduct;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Download_CSV.DownloadReceiver;
import com.mart.martonlinevendor.Download_CSV.DownloadService;
import com.mart.martonlinevendor.FileUtils;
import com.mart.martonlinevendor.Model.Add_Product_Model;
import com.mart.martonlinevendor.Model.Country_List_Model;
import com.mart.martonlinevendor.Model.Delete_Image_Model;
import com.mart.martonlinevendor.Model.Get_Sub_Category_Model;
import com.mart.martonlinevendor.Model.Product_Detail_Model;
import com.mart.martonlinevendor.Model.Product_Search_Model;
import com.mart.martonlinevendor.Model.Size_Model;
import com.mart.martonlinevendor.Model.Size_id_Model;
import com.mart.martonlinevendor.Model.Vendor_Category_Model;
import com.mart.martonlinevendor.NetwrokCheck.NoInternetDialog;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.awen.camera.model.PermissionsModel;
import com.mart.martonlinevendor.awen.camera.view.TakePhotoActivity;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.martonlinevendor.FileUtils.isDownloadsDocument;
import static com.mart.martonlinevendor.FileUtils.isExternalStorageDocument;

public class AddNewProduct extends AppCompatActivity implements View.OnClickListener, BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {

    private static final String TAG = "kjnkjnk";
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recycler_product_images;
    private EditText et_Actual_Price;
    private EditText et_Discounted_Price;
    private EditText et_Quantity;
    private EditText et_Category;
    private EditText et_Sub_Category;
    private TextView tv_passport_name;
    private TextView tv_upload_photo;
    private RelativeLayout rl_upload_photo;
    private TextView tvdownload_demo;
    private Button btn_signin;
    // ProductImagesAdapter productImagesAdapter;
    private Spinner choose_category;
    private Spinner choose_size;
    private Spinner choose_sub_category;
    List<Vendor_Category_Model.Response> array_categories_list;
    List<Get_Sub_Category_Model.Response> array__sub_categories_list;
    List<Size_Model.Response> array_size_list;
    String tagg = "0";
    String shop_image = "";
    UserSessionManager session;
    HashMap<String, String> user;
    private LinearLayout photo_rel;
    List<String> lists;
    ImageAdapter imageAdapter;
    File file, filecsv;
    List<Size_id_Model> array_count_id;
    String id = "";
    EditText et_product_title, et_product_description;
    String catgory_id = "", Subcategory_id = "";
    EditText et_product_size;
    RecyclerView size_list;
    Sub_Category listAdapter;
    Country countryadapter;
    String size_id = "";
    private final static int FILE_REQUEST_CODE = 1;
    // private FileListAdapter fileListAdapter;
    String categoryname, Subcategoryname, sizename;
    String CSV_file;
    String type = "";
    SeekBar progressBarOne;
    NoInternetDialog noInternetDialog;
    private TextView text;
    ProductImagesAdapter productImagesAdapter;
    private RecyclerView recycler_product_images1;
    String product_id = "";
    private ImageView search;
    List<Product_Search_Model.Response> array_search_list;
    String Product_Size_id = "";
    String subcate_id = "";
    String maincat_id = "";
    String newproductId = "";
    List<Product_Search_Model.Images> array_search_image_list;
    RecyclerView recycler_product_images2;
    String imageids = "";
    RecyclerView search_list;
    String flag="1";
    TextView search_not_found;
    EditText size_quantity;
    Spinner choose_country;
    List<Country_List_Model.Response>array_country_list;
    String country_name;
    String country;
    String tag="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_product);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(this).build();
        product_id = getIntent().getStringExtra("product_id");
        Product_Size_id = getIntent().getStringExtra("size_id");
        subcate_id = getIntent().getStringExtra("subcat_id");
        maincat_id = getIntent().getStringExtra("mainid");
        country = getIntent().getStringExtra("country");
        // Log.d("ddddddddddd",Product_Size_id);
        initView();
    }

    private void initView() {
        lists = new ArrayList<>();
        array_count_id = new ArrayList<>();
        array_search_image_list=new ArrayList<>();
        iv_menu = findViewById(R.id.iv_menu);
        size_quantity = findViewById(R.id.size_quantity);
        choose_country = findViewById(R.id.choose_country);
        search_list = findViewById(R.id.search_list);
        search = findViewById(R.id.search);
        search_not_found = findViewById(R.id.search_not_found);
        recycler_product_images1 = findViewById(R.id.recycler_product_images1);
        recycler_product_images2 = findViewById(R.id.recycler_product_images2);
        text = findViewById(R.id.text);
        progressBarOne = findViewById(R.id.progressBarOne);
        iv_send = findViewById(R.id.iv_send);
        size_list = findViewById(R.id.size_list);
        et_search = findViewById(R.id.et_search);
        et_product_title = findViewById(R.id.et_product_title);
        et_product_description = findViewById(R.id.et_product_description);
        iv_voice = findViewById(R.id.iv_voice);
        et_product_size = findViewById(R.id.et_product_size);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        recycler_product_images = findViewById(R.id.recycler_product_images);
        et_Actual_Price = findViewById(R.id.et_mrp);
        et_Discounted_Price = findViewById(R.id.et_offer_Price);
        photo_rel = (LinearLayout) findViewById(R.id.photo_rel);
        et_Quantity = findViewById(R.id.et_Quantity);
        et_Category = findViewById(R.id.et_Category);
        // et_Sub_Category = findViewById(R.id.et_Sub_Category);
        tv_passport_name = findViewById(R.id.tv_passport_name);
        tv_upload_photo = findViewById(R.id.tv_upload_photo);
        rl_upload_photo = findViewById(R.id.rl_upload_photo);
        tvdownload_demo = findViewById(R.id.tvdownload_demo);
        btn_signin = findViewById(R.id.btn_signin);

//        productImagesAdapter = new ProductImagesAdapter(AddNewProduct.this);
//        recycler_product_images.setAdapter(productImagesAdapter);
        imageAdapter = new ImageAdapter(AddNewProduct.this, lists);
        ///  recycler_product_images.setVisibility(View.VISIBLE);
        recycler_product_images.setAdapter(imageAdapter);


        btn_signin.setOnClickListener(this);
        choose_category = (Spinner) findViewById(R.id.choose_category);
//        choose_category.setOnClickListener(this);
        choose_size = (Spinner) findViewById(R.id.choose_size);
        // choose_size.setOnClickListener(this);
        choose_sub_category = (Spinner) findViewById(R.id.choose_sub_category);
        //  choose_sub_category.setOnClickListener(this);


        Map<String, String> map = new HashMap<>();
        map.put("method", "VendorCategory");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        GetCategories(map);


        choose_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoryname = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();
                catgory_id = array_categories_list.get(i).getCatId();
                Map<String, String> map = new HashMap<>();
                map.put("method", "getSubCategory");
                map.put("catId", array_categories_list.get(i).getCatId());
                Get_Sub_Categories(map);
                Map<String, String> map1 = new HashMap<>();
                map1.put("method", "getSizeOncid");
                //map1.put("catId", array_categories_list.get(i).getCatId());
                Product_Size(map1);
                //category_id = array_categories_list.get(i).getCatId();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        choose_sub_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Subcategoryname = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();
                    Subcategory_id = array__sub_categories_list.get(i).getCatId();
                } catch (Exception e) {
                    e.getStackTrace();
                }

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        choose_size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sizename = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();
                size_id = array_size_list.get(i).getId();
                //Toast.makeText(AddNewProduct.this,size_id+"",Toast.LENGTH_LONG).show();

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        choose_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //  sizename = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();
                country_name = array_country_list.get(i).getSize();
                //Toast.makeText(AddNewProduct.this,size_id+"",Toast.LENGTH_LONG).show();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
             return;
            }
        });
        photo_rel.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        et_product_size.setOnClickListener(this);
        tv_upload_photo.setOnClickListener(this);
        tvdownload_demo.setOnClickListener(this);
        et_search.setOnClickListener(this);
        iv_bellnotification.setOnClickListener(this);
        search.setOnClickListener(this);

        if (getIntent().getStringExtra("tag").equals("edit_product")) {
            et_Actual_Price.setText(getIntent().getStringExtra("mrp"));
            et_Discounted_Price.setText(getIntent().getStringExtra("offerprize"));
            et_product_title.setText(getIntent().getStringExtra("product_title"));
            et_product_description.setText(getIntent().getStringExtra("description"));
            et_Quantity.setText(getIntent().getStringExtra("quantity"));
            size_quantity.setText(getIntent().getStringExtra("Size_value"));
            btn_signin.setText("Submit");
            text.setVisibility(View.GONE);
            rl_upload_photo.setVisibility(View.GONE);
            tvdownload_demo.setVisibility(View.GONE);
            Map<String, String> map1 = new HashMap<>();
            map1.put("method", "Product_Details");
            map1.put("ProductId", getIntent().getStringExtra("product_id"));
            //user.get(UserSessionManager.KEY_ID)
            getProductDetail(map1);
        } else {
            text.setVisibility(View.GONE);
            rl_upload_photo.setVisibility(View.GONE);
            tvdownload_demo.setVisibility(View.GONE);
            et_product_title.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (flag.equals("1")){
                        if (et_product_title.getText().toString().length()>0){
                            Map<String, String> map1 = new HashMap<>();
                            map1.put("method", "SearchNew");
                            map1.put("key", et_product_title.getText().toString());
                            SearchProduct(map1);
                        }else {
                            search_list.setVisibility(View.GONE);
                            search_not_found.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }
        et_product_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag="1";
            }
        });



    }


    public static String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent, "Open CSV"), FILE_REQUEST_CODE);
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(intent, "Open CSV"), FILE_REQUEST_CODE);

            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
            if (tag.equals("1")){
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent, "Open CSV"), FILE_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                submit();
                break;
            case R.id.search:
                array_search_image_list.clear();
                if (et_product_title.getText().toString().length()>0){
                    //showdialog();
                    Map<String, String> map1 = new HashMap<>();
                    map1.put("method", "SearchNew");
                    map1.put("key", et_product_title.getText().toString());
                    //SearchProduct(map1);
                }else {
                    Custom_Toast_Activity.makeText(AddNewProduct.this, "Please Enter Product Title", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
                break;
            case R.id.et_search:
                startActivity(new Intent(AddNewProduct.this, SearchProduct.class));
                break;
            case R.id.iv_bellnotification:
                startActivity(new Intent(AddNewProduct.this, Notification_Screen.class));
                break;
            case R.id.tvdownload_demo:
                progressBarOne.setVisibility(View.VISIBLE);
                Intent intent1 = new Intent(AddNewProduct.this, DownloadService.class);
                intent1.putExtra("url", Apis.CSV_URL);
                intent1.putExtra("receiver", new DownloadReceiver(new Handler(), progressBarOne, AddNewProduct.this, Apis.CSV_URL));
                startService(intent1);
                break;
            case R.id.tv_upload_photo:
                tag="1";
                isStoragePermissionGranted();

                break;
            case R.id.photo_rel:
                final int i;
                if (lists != null) {
                    i = 6 - lists.size();
                } else {
                    i = 6;
                }
                if (lists.size() < 6) {
                    showoption();
                   /* BSImagePicker multiSelectionPicker = new BSImagePicker.Builder("com.mart.martonlinevendor")
                            .isMultiSelect()//Set this if you want to use multi selection mode.
                            .setMinimumMultiSelectCount(1) //Default: 1.
                            .setMaximumMultiSelectCount(1)//Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                            .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                            .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                            .setMultiSelectDoneTextColor(R.color.colorAccent) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                            .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                            .disableOverSelectionMessage() //You can also decide not to show this over select message.
                            .build();
                    multiSelectionPicker.show(getSupportFragmentManager(), "picker");*/

                } else {
                    Snackbar.make(btn_signin, "You Can't Upload More Then 6 Photos.",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    AddNewProduct.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                            .show();
                }

                break;
            case R.id.iv_menu:
                onBackPressed();
                break;
        }
    }
    public void showoption(){
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddNewProduct.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);

        sweetAlertDialog.setTitleText("MartOline");
        sweetAlertDialog.setContentText("Choose Anyone");
        sweetAlertDialog.setConfirmText("OK");
        sweetAlertDialog.setCancelable(false);

        sweetAlertDialog.showCancelButton(true);
        sweetAlertDialog.setCancelText("Gallery");
        sweetAlertDialog.setConfirmText("Camera");
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sweetAlertDialog.cancel();
                PermissionsModel permissionsModel = new PermissionsModel(AddNewProduct.this);
                permissionsModel.checkCameraPermission(new PermissionsModel.PermissionListener() {
                    @Override
                    public void onPermission(boolean isPermission) {
                        if (isPermission) {
                            Intent intent = new Intent(AddNewProduct.this, TakePhotoActivity.class);
                            startActivityForResult(intent, TakePhotoActivity.REQUEST_CAPTRUE_CODE);
                        }
                    }
                });
            }
        });
        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sweetAlertDialog.cancel();
                BSImagePicker pickerDialog1 = new BSImagePicker.Builder("com.martoline.fileprovider").hideCameraTile()
                        .build();
                pickerDialog1.show(getSupportFragmentManager(), "picker");
            }
        });

        if (sweetAlertDialog.isShowing()) {

        } else {
            sweetAlertDialog.show();
        }

    }

    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    public static String getPath(Context context, Uri uri) throws URISyntaxException {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";

        // ExternalStorageProvider
        if (isExternalStorageDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            final String type = split[0];

            if ("primary".equalsIgnoreCase(type)) {
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else {

                if (Build.VERSION.SDK_INT > 20) {
                    //getExternalMediaDirs() added in API 21
                    File extenal[] = context.getExternalMediaDirs();
                    for (File f : extenal) {
                        filePath = f.getAbsolutePath();
                        if (filePath.contains(type)) {
                            int endIndex = filePath.indexOf("Android");
                            filePath = filePath.substring(0, endIndex) + split[1];
                        }
                    }
                }else{
                    filePath = "/storage/" + type + "/" + split[1];
                }
                return filePath;
            }

        } else if (isDownloadsDocument(uri)) {
            // DownloadsProvider
            final String id = DocumentsContract.getDocumentId(uri);
            //final Uri contentUri = ContentUris.withAppendedId(
            // Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

            Cursor cursor = null;
            final String column = "_data";
            final String[] projection = {column};

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    String result = cursor.getString(index);
                    cursor.close();
                    return result;
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } else if (DocumentsContract.isDocumentUri(context, uri)) {
            // MediaProvider
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String[] ids = wholeID.split(":");
            String id;
            String type;
            if (ids.length > 1) {
                id = ids[1];
                type = ids[0];
            } else {
                id = ids[0];
                type = ids[0];
            }

            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }

            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{id};
            final String column = "_data";
            final String[] projection = {column};
            Cursor cursor = context.getContentResolver().query(contentUri,
                    projection, selection, selectionArgs, null);

            if (cursor != null) {
                int columnIndex = cursor.getColumnIndex(column);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            return filePath;
        } else {
            String[] proj = {MediaStore.Audio.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                if (cursor.moveToFirst())
                    filePath = cursor.getString(column_index);
                cursor.close();
            }


            return filePath;
        }
        return null;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TakePhotoActivity.REQUEST_CAPTRUE_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                String path = data.getStringExtra(TakePhotoActivity.RESULT_PHOTO_PATH);
                Log.d("ghfgh", path + "___");
                Uri uri = Uri.fromFile(new File(path));
                lists.add(compressImage(uri.toString()));
                imageAdapter.notifyDataSetChanged();
            }
        }else {
            if (resultCode == RESULT_OK) {
                String filePath = "";
                if (requestCode == FILE_REQUEST_CODE) {

                    filePath = getRealPathFromURI_API19(AddNewProduct.this, Objects.requireNonNull(data.getData()));

                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(filePath.toString());
                    String extension = "";
                    if (data.getData().getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                        //If scheme is a content
                        final MimeTypeMap mime = MimeTypeMap.getSingleton();
                        extension = mime.getExtensionFromMimeType(AddNewProduct.this.getContentResolver().getType(data.getData()));
                    } else {
                        //If scheme is a File
                        //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
                        extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(data.getData().getPath())).toString());

                    }
                    Log.d("vfhhlhlgj", extension + "");

                    if (extension.equals("csv")) {
                        Log.d("vfhgj", filePath + "");
                        CSV_file = filePath;
                        type = "csv";
                        String filename = CSV_file.substring(CSV_file.lastIndexOf("/") + 1);
                        tv_passport_name.setText(filename);
                        //Check uri format to avoid null

                    } else if (extension.equals("CSV")) {
                        Log.d("vfhgj", filePath + "");
                        CSV_file = filePath;
                        type = "csv";
                        String filename = CSV_file.substring(CSV_file.lastIndexOf("/") + 1);
                        tv_passport_name.setText(filename);
                    } else {
                        Snackbar.make(btn_signin, "You can upload only csv file",
                                Snackbar.LENGTH_SHORT)
                                .setActionTextColor(ContextCompat.getColor(
                                        AddNewProduct.this, R.color.colorWhite))
                                .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                                .show();

                    }

                }

            }
        }
    }
    // }
    @Override
    public void onBackPressed() {
        finish();
    }

    private void submit() {
        // validate
        imageids = "";
        for (int i = 0; i < array_count_id.size(); i++) {
            imageids = imageids + array_count_id.get(i).getId() + "" + ",";
        }
        if (type.equals("csv")) {
            HashMap<String, String> hashmap = new HashMap<>();
            hashmap.put("method", "Add_Product");
            hashmap.put("MRP", "");
            hashmap.put("OfferPrize", "");
            hashmap.put("quantity", "");
            hashmap.put("Producttitle", "");
            hashmap.put("userId", user.get(UserSessionManager.KEY_ID));
            hashmap.put("Description", "");
            hashmap.put("Category_id", "");
            hashmap.put("Subcategory_id", "");
            hashmap.put("size_id", "");
            hashmap.put("type", type);
            hashmap.put("newproductId", "");
            hashmap.put("imageids", "");
            hashmap.put("size_value", "");
            hashmap.put("country", "");
            UploadImage1(hashmap);

        } else {
            if (getIntent().getStringExtra("tag").equals("edit_product")) {

            } else {
                if (array_search_image_list.isEmpty()) {
                    if (lists.isEmpty()) {
                        Snackbar.make(btn_signin, "Select Product Image ",
                                Snackbar.LENGTH_SHORT)
                                .setActionTextColor(ContextCompat.getColor(
                                        AddNewProduct.this, R.color.colorWhite))
                                .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                                .show();
                        return;
                    }

                }else {
                    if (imageids.equals("")){
                        Snackbar.make(btn_signin, "Please Select Product Image in List",
                                Snackbar.LENGTH_SHORT)
                                .setActionTextColor(ContextCompat.getColor(
                                        AddNewProduct.this, R.color.colorWhite))
                                .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                                .show();
                        return;
                    }
                }

            }
            String Price = et_Actual_Price.getText().toString().trim();
            if (TextUtils.isEmpty(Price)) {
                //Toast.makeText(this, "Actual Price", Toast.LENGTH_SHORT).show();

                Snackbar.make(btn_signin, "Enter MRP ",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }

            String Discounted_Price = et_Discounted_Price.getText().toString().trim();
            if (TextUtils.isEmpty(Discounted_Price)) {
                Snackbar.make(btn_signin, "Enter Offer Prize ",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                // Toast.makeText(this, "Discounted Price", Toast.LENGTH_SHORT).show();
                return;
            }

            String Quantity = et_Quantity.getText().toString().trim();
            if (TextUtils.isEmpty(Quantity)) {
                // Toast.makeText(this, "Quantity", Toast.LENGTH_SHORT).show();
                Snackbar.make(btn_signin, "Enter Product Quantity ",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }
            String producttitle = et_product_title.getText().toString().trim();
            if (TextUtils.isEmpty(producttitle)) {
                // Toast.makeText(this, "Quantity", Toast.LENGTH_SHORT).show();
                Snackbar.make(btn_signin, "Enter Product Title ",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }

            String productdes = et_product_description.getText().toString().trim();
//            if (TextUtils.isEmpty(productdes)) {
//                // Toast.makeText(this, "Quantity", Toast.LENGTH_SHORT).show();
//                Snackbar.make(btn_signin, "Enter Product Description ",
//                        Snackbar.LENGTH_SHORT)
//                        .setActionTextColor(ContextCompat.getColor(
//                                AddNewProduct.this, R.color.colorWhite))
//                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
//                        .show();
//                return;
//            }
            if (categoryname.equals("Choose Category")) {
                Snackbar.make(btn_signin, "Choose  Category  Name",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }
//            if (sizename.equals("Choose Product Size")) {
//                Snackbar.make(btn_signin, "Choose Product Size",
//                        Snackbar.LENGTH_SHORT)
//                        .setActionTextColor(ContextCompat.getColor(
//                                AddNewProduct.this, R.color.colorWhite))
//                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
//                        .show();
//                return;
//            }
//            if (sizename.equals("Product Size Not Found")) {
//                Snackbar.make(btn_signin, "Product Size Not Avialable for this Category Choose New Category",
//                        Snackbar.LENGTH_SHORT)
//                        .setActionTextColor(ContextCompat.getColor(
//                                AddNewProduct.this, R.color.colorWhite))
//                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
//                        .show();
//                return;
//            }
            if (Subcategoryname.equals("Choose Sub Category")) {
                Snackbar.make(btn_signin, "Choose Sub Category",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }
            if (Subcategoryname.equals("Sub Category Not Found")) {
                Snackbar.make(btn_signin, "Sub Category Not Available for this Category You Add Your Category",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
                return;
            }

            if (getIntent().getStringExtra("tag").equals("edit_product")) {
                // TODO validate success, do something
                HashMap<String, String> hashmap = new HashMap<>();
                hashmap.put("method", "Add_Product");
                hashmap.put("MRP", et_Actual_Price.getText().toString());
                hashmap.put("OfferPrize", et_Discounted_Price.getText().toString());
                hashmap.put("quantity", et_Quantity.getText().toString());
                hashmap.put("Producttitle", et_product_title.getText().toString());
                hashmap.put("userId", user.get(UserSessionManager.KEY_ID));
                hashmap.put("Description", et_product_description.getText().toString());
                hashmap.put("Category_id", catgory_id);
                hashmap.put("Subcategory_id", Subcategory_id);
                hashmap.put("size_id", size_id);
                hashmap.put("type", "");
                hashmap.put("newproductId", newproductId);
                hashmap.put("imageids", removeLastCharacter(imageids));
                hashmap.put("size_value", size_quantity.getText().toString());
                if(country_name.equals("Choose Country")){
                    hashmap.put("country", "");
                }else{
                    hashmap.put("country", country_name);
                }
                hashmap.put("product_id", product_id);
                if (CSV_file != null) {
                    hashmap.put("csvfile", CSV_file);
                } else {
                    hashmap.put("csvfile", "");
                }
                UploadImage(hashmap);
            } else {
                // TODO validate success, do something
                HashMap<String, String> hashmap = new HashMap<>();
                hashmap.put("method", "Add_Product");
                hashmap.put("MRP", et_Actual_Price.getText().toString());
                hashmap.put("OfferPrize", et_Discounted_Price.getText().toString());
                hashmap.put("quantity", et_Quantity.getText().toString());
                hashmap.put("Producttitle", et_product_title.getText().toString());
                hashmap.put("userId", user.get(UserSessionManager.KEY_ID));
                hashmap.put("Description", et_product_description.getText().toString());
                hashmap.put("Category_id", catgory_id);
                hashmap.put("Subcategory_id", Subcategory_id);
                hashmap.put("size_id", size_id);
                hashmap.put("type", "");
                hashmap.put("newproductId", newproductId);
                hashmap.put("imageids", removeLastCharacter(imageids));
                hashmap.put("size_value", size_quantity.getText().toString());
                if(country_name.equals("Choose Country")){
                    hashmap.put("country", "");
                }else{
                    hashmap.put("country", country_name);
                }

                if (CSV_file != null) {
                    hashmap.put("csvfile", CSV_file);
                } else {
                    hashmap.put("csvfile", "");
                }
                UploadImage1(hashmap);
            }

        }
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(AddNewProduct.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
      /*  Float fileSizeInKB = getImageSize(AddNewProduct.this,uriList.get(0)) ;
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        Float fileSizeInMB = fileSizeInKB / 1024;
        Log.d("asdfasdf",fileSizeInKB+"_________"+fileSizeInMB);
        if (fileSizeInKB>50){
            Custom_Toast_Activity.makeText(AddNewProduct.this, "Please Select Image less than 50 KB", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
        }else {
            lists.addAll(uriList);
            imageAdapter.notifyDataSetChanged();
        }*/
    }
    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
//        selectedImagePath = uri.getPath();
        lists.add(compressImage(uri.toString()));
        imageAdapter.notifyDataSetChanged();
    }

    public static float getImageSize(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            cursor.moveToFirst();
            float imageSize = cursor.getLong(sizeIndex);
            cursor.close();
            return imageSize/ 1024; // returns size in bytes
        }
        return 0;
    }
    public String compressImage(String imageUri) {
        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
//      max Height and width values of the compressed image is taken as 816x612
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }



    private void compressImage(File actualImage) {
        try {
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "ReviewRating");
            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }
            if (success) {
                File compressedImage = new Compressor(this)
                        .setMaxWidth(1040)
                        .setMaxHeight(700)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setDestinationDirectoryPath(folder.getAbsolutePath())
                        .compressToFile(actualImage);
                /**
                 * Refersh Adapater
                 * */
//                lists.add(Uri.fromFile(compressedImage));
                imageAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {
        List<String> uriList;

        public ImageAdapter(Context context, List<String> uriList) {
            this.uriList = uriList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(AddNewProduct.this).inflate(R.layout.list_product_images_local, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {

                Uri uri=  Uri.fromFile(new File(uriList.get(i)));
                holder.ivimage.setImageURI(uri);

            holder.iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lists.remove(i);
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return uriList.size();
        }


        public long getItemId(int position) {
            return position;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivimage, iv_delete;

            public MyViewHolder(View v) {
                super(v);
                ivimage = v.findViewById(R.id.iv_product_images);
                iv_delete = v.findViewById(R.id.iv_delete);
            }
        }


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void UploadImage(HashMap<String, String> datapart) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        MultipartBody.Part[] fileToUpload10 = new MultipartBody.Part[lists.size()];
        for (int i = 0; i < lists.size(); i++) {
            file = new File(Objects.requireNonNull(getRealPathFromURI(lists.get(i))));
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            fileToUpload10[i] = MultipartBody.Part.createFormData("images[]", file.getName(), requestBody);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        }
        MultipartBody.Part uploadcsv;
        if (CSV_file != null && !CSV_file.isEmpty()) {
            filecsv = new File(Objects.requireNonNull(CSV_file));
            RequestBody requestBody_csv = RequestBody.create(MediaType.parse("*/*"), filecsv);
            uploadcsv = MultipartBody.Part.createFormData("csvfile", filecsv.getName(), requestBody_csv);
        } else {
            uploadcsv = MultipartBody.Part.createFormData("csvfile", "");
        }
        MultipartBody.Part fileToUpload1 = MultipartBody.Part.createFormData("userId", Objects.requireNonNull(datapart.get("userId")));
        MultipartBody.Part fileToUpload2 = MultipartBody.Part.createFormData("method", Objects.requireNonNull(datapart.get("method")));
        MultipartBody.Part fileToUpload3 = MultipartBody.Part.createFormData("MRP", Objects.requireNonNull(datapart.get("MRP")));
        MultipartBody.Part fileToUpload4 = MultipartBody.Part.createFormData("OfferPrize", Objects.requireNonNull(datapart.get("OfferPrize")));
        MultipartBody.Part fileToUpload11 = MultipartBody.Part.createFormData("quantity", Objects.requireNonNull(datapart.get("quantity")));
        MultipartBody.Part fileToUpload5 = MultipartBody.Part.createFormData("Producttitle", Objects.requireNonNull(datapart.get("Producttitle")));
        MultipartBody.Part fileToUpload6 = MultipartBody.Part.createFormData("Description", Objects.requireNonNull(datapart.get("Description")));
        MultipartBody.Part fileToUpload7 = MultipartBody.Part.createFormData("Category_id", Objects.requireNonNull(datapart.get("Category_id")));
        MultipartBody.Part fileToUpload8 = MultipartBody.Part.createFormData("Subcategory_id", Objects.requireNonNull(datapart.get("Subcategory_id")));
        MultipartBody.Part fileToUpload9 = MultipartBody.Part.createFormData("size_id", Objects.requireNonNull(datapart.get("size_id")));
        MultipartBody.Part fileToUpload12 = MultipartBody.Part.createFormData("type", Objects.requireNonNull(datapart.get("type")));
        MultipartBody.Part fileToUpload13 = MultipartBody.Part.createFormData("product_id", Objects.requireNonNull(datapart.get("product_id")));
        MultipartBody.Part fileToUpload15 = MultipartBody.Part.createFormData("newproductId", Objects.requireNonNull(datapart.get("newproductId")));

        MultipartBody.Part fileToUpload14 = MultipartBody.Part.createFormData("imageids", Objects.requireNonNull(datapart.get("imageids")));
        MultipartBody.Part fileToUpload16 = MultipartBody.Part.createFormData("size_value", Objects.requireNonNull(datapart.get("size_value")));
        MultipartBody.Part fileToUpload17 = MultipartBody.Part.createFormData("country", Objects.requireNonNull(datapart.get("country")));

        Call<Add_Product_Model> call = Apis.getAPIService().add_Product(fileToUpload1, fileToUpload2, fileToUpload3, fileToUpload11, fileToUpload4, fileToUpload5, fileToUpload6,
                fileToUpload7, fileToUpload8, fileToUpload9, fileToUpload12, fileToUpload15, fileToUpload14, fileToUpload16,fileToUpload17,fileToUpload13, uploadcsv, fileToUpload10
        );
        call.enqueue(new Callback<Add_Product_Model>() {
            @Override
            public void onResponse(Call<Add_Product_Model> call, Response<Add_Product_Model> response) {
                dialog.dismiss();
                Add_Product_Model user = response.body();
                if (user != null) {
                    //cleaning compress folder
                    FileUtils.cleanCompressFolder();
                    lists.clear();
                    if (getIntent().getStringExtra("tag").equals("edit_product")){
                        Intent intent = new Intent();
                        intent.putExtra("catid", getIntent().getStringExtra("subcat_id"));
                        setResult(RESULT_OK, intent);
                        finish();
                    }else {
                        startActivity(new Intent(AddNewProduct.this, Dashboard.class));
                    }
                    Log.d("response", "gh" + user.toString());
                    Custom_Toast_Activity.makeText(AddNewProduct.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    //   showmessage(user.getMessage() + "");
                } else {
                }
            }

            @Override
            public void onFailure(Call<Add_Product_Model> call, Throwable t) {
                dialog.dismiss();
                // Log.d("response", "gh" + t.getMessage());
                Snackbar.make(btn_signin, "CSV File Format Not Proper Please Download Sample CSV File then this file you can Upload",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
            }
        });
    }

    private void UploadImage1(HashMap<String, String> datapart) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        MultipartBody.Part[] fileToUpload10 = new MultipartBody.Part[lists.size()];
        for (int i = 0; i < lists.size(); i++) {
            file = new File(Objects.requireNonNull(getRealPathFromURI(lists.get(i))));
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            fileToUpload10[i] = MultipartBody.Part.createFormData("images[]", file.getName(), requestBody);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        }
        MultipartBody.Part uploadcsv;
        if (CSV_file != null && !CSV_file.isEmpty()) {
            filecsv = new File(Objects.requireNonNull(CSV_file));
            RequestBody requestBody_csv = RequestBody.create(MediaType.parse("*/*"), filecsv);
            uploadcsv = MultipartBody.Part.createFormData("csvfile", filecsv.getName(), requestBody_csv);
        } else {
            uploadcsv = MultipartBody.Part.createFormData("csvfile", "");
        }
        MultipartBody.Part fileToUpload1 = MultipartBody.Part.createFormData("userId", Objects.requireNonNull(datapart.get("userId")));
        MultipartBody.Part fileToUpload2 = MultipartBody.Part.createFormData("method", Objects.requireNonNull(datapart.get("method")));
        MultipartBody.Part fileToUpload3 = MultipartBody.Part.createFormData("MRP", Objects.requireNonNull(datapart.get("MRP")));
        MultipartBody.Part fileToUpload4 = MultipartBody.Part.createFormData("OfferPrize", Objects.requireNonNull(datapart.get("OfferPrize")));
        MultipartBody.Part fileToUpload11 = MultipartBody.Part.createFormData("quantity", Objects.requireNonNull(datapart.get("quantity")));
        MultipartBody.Part fileToUpload5 = MultipartBody.Part.createFormData("Producttitle", Objects.requireNonNull(datapart.get("Producttitle")));
        MultipartBody.Part fileToUpload6 = MultipartBody.Part.createFormData("Description", Objects.requireNonNull(datapart.get("Description")));
        MultipartBody.Part fileToUpload7 = MultipartBody.Part.createFormData("Category_id", Objects.requireNonNull(datapart.get("Category_id")));
        MultipartBody.Part fileToUpload8 = MultipartBody.Part.createFormData("Subcategory_id", Objects.requireNonNull(datapart.get("Subcategory_id")));
        MultipartBody.Part fileToUpload9 = MultipartBody.Part.createFormData("size_id", Objects.requireNonNull(datapart.get("size_id")));
        MultipartBody.Part fileToUpload12 = MultipartBody.Part.createFormData("type", Objects.requireNonNull(datapart.get("type")));
        MultipartBody.Part fileToUpload13 = MultipartBody.Part.createFormData("newproductId", Objects.requireNonNull(datapart.get("newproductId")));

        MultipartBody.Part fileToUpload14 = MultipartBody.Part.createFormData("imageids", Objects.requireNonNull(datapart.get("imageids")));
        MultipartBody.Part fileToUpload15 = MultipartBody.Part.createFormData("size_value", Objects.requireNonNull(datapart.get("size_value")));
        MultipartBody.Part fileToUpload16 = MultipartBody.Part.createFormData("country", datapart.get("country"));

        Call<Add_Product_Model> call = Apis.getAPIService().add_Product1(fileToUpload1, fileToUpload2, fileToUpload3, fileToUpload11, fileToUpload4, fileToUpload5, fileToUpload6,
                fileToUpload7, fileToUpload8, fileToUpload9, fileToUpload12, fileToUpload13, fileToUpload14,fileToUpload15,fileToUpload16, uploadcsv, fileToUpload10
        );
        call.enqueue(new Callback<Add_Product_Model>() {
            @Override
            public void onResponse(Call<Add_Product_Model> call, Response<Add_Product_Model> response) {
                dialog.dismiss();
                Add_Product_Model user = response.body();
                if (user != null) {
                    //cleaning compress folder
                    FileUtils.cleanCompressFolder();
                    lists.clear();
                    if (getIntent().getStringExtra("tag").equals("new_product")){
                        Intent intent = new Intent();
                        intent.putExtra("catid", getIntent().getStringExtra("subcat_id"));
                        setResult(RESULT_OK, intent);
                        finish();
                    }else {
                        startActivity(new Intent(AddNewProduct.this, Dashboard.class));
                    }

                    Log.d("response", "gh" + user.toString());
                    Custom_Toast_Activity.makeText(AddNewProduct.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    //   showmessage(user.getMessage() + "");
                } else {
                }
            }

            @Override
            public void onFailure(Call<Add_Product_Model> call, Throwable t) {
                dialog.dismiss();
                // Log.d("response", "gh" + t.getMessage());
                Snackbar.make(btn_signin, "CSV File Format Not Proper Please Download Sample CSV File then this file you can Upload",
                        Snackbar.LENGTH_SHORT)
                        .setActionTextColor(ContextCompat.getColor(
                                AddNewProduct.this, R.color.colorWhite))
                        .setBackgroundTint(ContextCompat.getColor(AddNewProduct.this, R.color.alertcolor))
                        .show();
            }
        });
    }

    private void getProductDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_Detail_Model> call = Apis.getAPIService().getProductDetails(map);
        call.enqueue(new Callback<Product_Detail_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_Detail_Model> call, @NonNull Response<Product_Detail_Model> response) {
                dialog.dismiss();
                Product_Detail_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        if (userdata.getResponse().get(0).getImages().size() != 0) {
                            final List<Product_Detail_Model.Images> array_image_list = new ArrayList<>();
                            array_image_list.addAll(userdata.getResponse().get(0).getImages());
                            productImagesAdapter = new ProductImagesAdapter(AddNewProduct.this, array_image_list);
                            recycler_product_images1.setAdapter(productImagesAdapter);
                        }
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_Detail_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {

        List<Product_Detail_Model.Images> imagelist;
        Context context;
        LayoutInflater inflater;

        public ProductImagesAdapter(Context context, List<Product_Detail_Model.Images> imagelist) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.imagelist = imagelist;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_product_images_local, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_product_images);
            holder.iv_product_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //image handling here
                    SweetAlertDialog pDialog = new SweetAlertDialog(AddNewProduct.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText("Do You Want to Delete this Product Image?");
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "deleteimage");
                            map.put("imageId", imagelist.get(position).getImage_Id());
                            Delete_ProductImage(map);
                        }
                    }).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return imagelist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView iv_product_images;

            public ViewHolder(View itemView) {

                super(itemView);
                this.iv_product_images = itemView.findViewById(R.id.iv_product_images);
            }
        }
    }

    private void Delete_ProductImage(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Delete_Image_Model> call = Apis.getAPIService().Delete_Image(map);
        call.enqueue(new Callback<Delete_Image_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Delete_Image_Model> call, @NonNull Response<Delete_Image_Model> response) {
                dialog.dismiss();
                Delete_Image_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "Product_Details");
                        map1.put("ProductId", getIntent().getStringExtra("product_id"));
                        //user.get(UserSessionManager.KEY_ID)
                        getProductDetail(map1);
                    } else {
                        Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Delete_Image_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class SearchImagesAdapter extends RecyclerView.Adapter<SearchImagesAdapter.ViewHolder> {

        List<Product_Search_Model.Images> imagelist;
        Context context;
        LayoutInflater inflater;

        public SearchImagesAdapter(Context context, List<Product_Search_Model.Images> imagelist) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.imagelist = imagelist;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_product_images_search, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_product_images);
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //image handling here
                    if (holder.checkbox.isChecked()) {
                        Log.d("idvikash1111", "gdfgdfgdfgdxg");
                        Size_id_Model model = new Size_id_Model();
                        model.setId(imagelist.get(position).getImage_Id());
                        //model.setName(shopcategorylist.get(position).getName());
                        array_count_id.add(model);
                        Log.d("idvikash", removeLastCharacter(id));
                    } else {
                        for (int i = 0; i < array_count_id.size(); i++) {
                            if (imagelist.get(position).getImage_Id().equals(array_count_id.get(i).getId())) {
                                Log.d("idvikash1111", "gdfgdfgdfgdxg");
                                array_count_id.remove(i);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return imagelist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView iv_product_images;
            CheckBox checkbox;

            public ViewHolder(View itemView) {

                super(itemView);
                this.iv_product_images = itemView.findViewById(R.id.iv_product_images);
                this.checkbox = itemView.findViewById(R.id.iv_delete);
            }
        }
    }


    //get All Categories
    private void GetCategories(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Vendor_Category_Model> call = Apis.getAPIService().getVendorCategory(map);
        call.enqueue(new Callback<Vendor_Category_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Vendor_Category_Model> call, @NonNull Response<Vendor_Category_Model> response) {
                dialog.dismiss();
                Vendor_Category_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_categories_list = new ArrayList<>();
                        Vendor_Category_Model.Response model = new Vendor_Category_Model.Response();
                        model.setCatId("0");
                        model.setCatName("Choose Category");
                        array_categories_list.add(0, model);
                        array_categories_list.addAll(userdata.getResponse());
                        // array_categories_list.add(0, model);
                        Category listAdapter = new Category(AddNewProduct.this, array_categories_list);
                        choose_category.setAdapter(listAdapter);
                        for (int i = 0; i < array_categories_list.size(); i++) {
                            if (array_categories_list.get(i).getCatId().equals(maincat_id)) {
                                choose_category.setSelection(i);
                            }
                        }
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "countrylist");
                        Get_CountryList(map1);

                    } else {
//                        array_categories_list.clear();
//                        array_categories_list = new ArrayList<>();
//                        Vendor_Category_Model.Response model = new Vendor_Category_Model.Response();
//                        model.setCatId("0");
//                        model.setCatName("Category Not Found");
//                        array_categories_list.add(0, model);
//                        Category listAdapter = new Category(AddNewProduct.this, array_categories_list);
//                        choose_category.setAdapter(listAdapter);
//                        listAdapter.notifyDataSetChanged();
                        Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "countrylist");
                        Get_CountryList(map1);
                    }
                } else {
                    Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Vendor_Category_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //get all sub categories
    private void Get_Sub_Categories(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Get_Sub_Category_Model> call = Apis.getAPIService().getSubcategory(map);
        call.enqueue(new Callback<Get_Sub_Category_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Get_Sub_Category_Model> call, @NonNull Response<Get_Sub_Category_Model> response) {
                dialog.dismiss();
                Get_Sub_Category_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array__sub_categories_list = new ArrayList<>();
                        array__sub_categories_list.clear();
                        Get_Sub_Category_Model.Response model = new Get_Sub_Category_Model.Response();
                        model.setCatId("0");
                        model.setCatName("Choose Sub Category");
                        array__sub_categories_list.add(0, model);
                        array__sub_categories_list.addAll(userdata.getResponse());
                        // array_categories_list.add(0, model);
                        listAdapter = new Sub_Category(AddNewProduct.this, array__sub_categories_list);
                        choose_sub_category.setAdapter(listAdapter);
                        for (int i = 0; i < array__sub_categories_list.size(); i++) {
                            if (array__sub_categories_list.get(i).getCatId().equals(subcate_id)) {
                                choose_sub_category.setSelection(i);
                            }
                        }
                    } else {
                        //.clear(); // clear items
                        array__sub_categories_list = new ArrayList<>();
                        array__sub_categories_list.clear();
                        Get_Sub_Category_Model.Response model = new Get_Sub_Category_Model.Response();
                        model.setCatId("0");
                        model.setCatName("Sub Category Not Found");
                        array__sub_categories_list.add(0, model);
                        listAdapter = new Sub_Category(AddNewProduct.this, array__sub_categories_list);
                        choose_sub_category.setAdapter(listAdapter);
                        listAdapter.notifyDataSetChanged();
                        // Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Get_Sub_Category_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }


    //getcountry list
    //get all sub categories
    private void Get_CountryList(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Country_List_Model> call = Apis.getAPIService().Country_List(map);
        call.enqueue(new Callback<Country_List_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Country_List_Model> call, @NonNull Response<Country_List_Model> response) {
                dialog.dismiss();
                Country_List_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_country_list = new ArrayList<>();
                        array_country_list.clear();
                        Country_List_Model.Response model = new Country_List_Model.Response();
                        model.setId("0");
                        model.setSize("Choose Country");
                        array_country_list.add(0, model);
                        array_country_list.addAll(userdata.getResponse());
                        // array_categories_list.add(0, model);
                        countryadapter = new Country(AddNewProduct.this, array_country_list);
                        choose_country.setAdapter(countryadapter);
                        for (int i = 0; i < array_country_list.size(); i++) {
                            if (array_country_list.get(i).getSize().equals(country)) {
                                choose_country.setSelection(i);
                            }
                        }
                    } else {
                        //.clear(); // clear items
                        array_categories_list = new ArrayList<>();
                        array_country_list.clear();
                        Country_List_Model.Response model = new Country_List_Model.Response();
                        model.setId("0");
                        model.setSize("Sub Category Not Found");
                        array_country_list.add(0, model);
                        countryadapter = new Country(AddNewProduct.this, array_country_list);
                        choose_country.setAdapter(countryadapter);
                        countryadapter.notifyDataSetChanged();
                        // Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Country_List_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //get all product size
    private void Product_Size(Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewProduct.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Size_Model> call = Apis.getAPIService().getSize(map);
        call.enqueue(new Callback<Size_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Size_Model> call, @NonNull Response<Size_Model> response) {
                dialog.dismiss();
                Size_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_size_list = new ArrayList<>();
                        array_size_list.clear();
                        // array__sub_categories_list.addAll(userdata.getResponse());
                        Size_Model.Response model = new Size_Model.Response();
                        model.setId("0");
                        model.setSize("Choose Product Size");
                        array_size_list.addAll(userdata.getResponse());
                        array_size_list.add(0, model);
                        Size listAdapter = new Size(AddNewProduct.this, array_size_list);
                        choose_size.setAdapter(listAdapter);
                        for (int i = 0; i < array_size_list.size(); i++) {
                            if (array_size_list.get(i).getId().equals(Product_Size_id)) {
                                choose_size.setSelection(i);
                            }
                        }
                    } else {
                        array_size_list = new ArrayList<>();
                        array_size_list.clear();
                        Size_Model.Response model = new Size_Model.Response();
                        model.setId("0");
                        model.setSize("Product Size Not Found");
                        array_size_list.add(0, model);
                        Size listAdapter = new Size(AddNewProduct.this, array_size_list);
                        choose_size.setAdapter(listAdapter);
                        //    Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Size_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class Category extends BaseAdapter {
        List<Vendor_Category_Model.Response> list;
        Context c;

        public Category(Context c, List<Vendor_Category_Model.Response> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getCatName());


            return row;
        }
    }

    public class Sub_Category extends BaseAdapter {
        List<Get_Sub_Category_Model.Response> list;
        Context c;

        public Sub_Category(Context c, List<Get_Sub_Category_Model.Response> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getCatName());


            return row;
        }
    }

    public class Country extends BaseAdapter {
        List<Country_List_Model.Response> list;
        Context c;

        public Country(Context c, List<Country_List_Model.Response> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getSize());


            return row;
        }
    }

    public class Size extends BaseAdapter {
        List<Size_Model.Response> list;
        Context c;

        public Size(Context c, List<Size_Model.Response> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getSize());


            return row;
        }
    }

    public class SearchProduct_adapter extends RecyclerView.Adapter<SearchProduct_adapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
        Context context;
        LayoutInflater inflater;

        SearchProduct_adapter(Context context) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.size_adapeter, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tvtitle.setText(array_search_list.get(position).getPtitle());


            holder.tvtitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // et_Actual_Price.setText(getIntent().getStringExtra("mrp"));
                    // et_Discounted_Price.setText(getIntent().getStringExtra("offerprize"));
                    search_list.setVisibility(View.GONE);
                    flag="0";
                    et_product_title.setText(array_search_list.get(position).getPtitle());
                    et_product_description.setText(array_search_list.get(position).getDiscription());
                    maincat_id = array_search_list.get(position).getCid();
                    subcate_id = array_search_list.get(position).getScid();
                    Product_Size_id = array_search_list.get(position).getSize_id();
                    for (int i = 0; i < array_categories_list.size(); i++) {
                        if (array_categories_list.get(i).getCatId().equals(maincat_id)) {
                            choose_category.setSelection(i);
                        }
                    }
                    for (int i = 0; i < array__sub_categories_list.size(); i++) {
                        if (array__sub_categories_list.get(i).getCatId().equals(subcate_id)) {
                            choose_sub_category.setSelection(i);
                        }
                    }
                    for (int i = 0; i < array_size_list.size(); i++) {
                        if (array_size_list.get(i).getId().equals(Product_Size_id)) {
                            choose_size.setSelection(i);
                        }
                    }
                    Product_Search_Model.Response ddd = array_search_list.get(position);
                    newproductId = array_search_list.get(position).getId();
                    array_search_image_list = ddd.getImages();
                    SearchImagesAdapter productImagesAdapter1 = new SearchImagesAdapter(AddNewProduct.this, array_search_image_list);
                    recycler_product_images2.setAdapter(productImagesAdapter1);
                    // et_Quantity.setText(getIntent().getStringExtra("quantity"));
                    // dialog.dismiss();
                }
            });


        }

        @Override
        public int getItemCount() {
            return array_search_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_cat_images;
            private TextView tvtitle;
            private CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);
                this.checkbox = itemView.findViewById(R.id.checkbox);


            }
        }
    }

    private void SearchProduct(Map<String, String> map) {
//        final Dialog dialog = new Dialog(AddNewProduct.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        Call<Product_Search_Model> call = Apis.getAPIService().Search_Product(map);
        call.enqueue(new Callback<Product_Search_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_Search_Model> call, @NonNull Response<Product_Search_Model> response) {
                //dialog.dismiss();
                Product_Search_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
//                        final Dialog dialog = new Dialog(AddNewProduct.this);
//                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.setContentView(R.layout.size_dialog);
//                        dialog.setCancelable(true);
//                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                        RecyclerView search_list = dialog.findViewById(R.id.search_list);
//                        TextView done = dialog.findViewById(R.id.done);
//
//                        done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                et_product_title.setText("");
//                                et_product_description.setText("");
//                                array_search_image_list.clear();
//                                dialog.dismiss();
//                            }
//                        });
                        search_list.setVisibility(View.VISIBLE);
                        search_not_found.setVisibility(View.GONE);
                        array_search_list = new ArrayList<>();
                        array_search_list.addAll(userdata.getResponse());
                        search_list.setAdapter(new SearchProduct_adapter(AddNewProduct.this));
                        // dialog.show();

                    } else {
                        search_not_found.setVisibility(View.VISIBLE);
                        search_list.setVisibility(View.GONE);
                        // Custom_Toast_Activity.makeText(AddNewProduct.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<Product_Search_Model> call, @NonNull Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
}
