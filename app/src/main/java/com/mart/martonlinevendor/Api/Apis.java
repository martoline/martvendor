package com.mart.martonlinevendor.Api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mart.martonlinevendor.Interfaces.Service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Apis {
    public static String BASE_URL = "http://martonline.co.in/";
//    public static String BASE_URL = "http://martoline.in/";
    public static String URL = "http://martoline.in/";
//    public static String IMAGE_PATH="http://martoline.in/admin/image/";
    public static String IMAGE_PATH="http://martonline.co.in/admin/image/";
//    public static String WHATSAPP_IMAGE_PATH="http://martoline.in/admin/orderimage/";
    public static String WHATSAPP_IMAGE_PATH="http://martonline.co.in/admin/orderimage/";
//    public static String USER_IMAGE_PATH="http://martoline.in/";
    public static String USER_IMAGE_PATH="http://martonline.co.in/";
//    public static String CSV_URL="http://martoline.in/admin/sample/products.csv";
    public static String CSV_URL="http://martonline.co.in/admin/sample/products.csv";
    public static String PAYMENT_URL = "http://martoline.in/marto/";
    public static String SHOP_IMAGE_PATH="http://martoline.in/";
  /*  public static String BASE_URL = " http://stageofproject.com/";
    public static String SHOP_IMAGE_PATH = "https://stageofproject.com/martov2/";
    public static String URL = "https://stageofproject.com/martov2/";
    public static String IMAGE_PATH="https://stageofproject.com/martov2/admin/image/";
    public static String WHATSAPP_IMAGE_PATH="http://stageofproject.com/martov2/admin/orderimage/";
    public static String USER_IMAGE_PATH="https://stageofproject.com/martov2/";
    public static String CSV_URL="http://stageofproject.com/martov2/admin/sample/products.csv";*/


    public static Service getAPIService() {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(interceptor).build();
            // Change base URL to your upload server URL.
            Service uploadService = new Retrofit.Builder()
                    .baseUrl(Apis.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    //.addConverterFactory(GsonConverterFactory.create())
                    .build()

                    .create(Service.class);
            return uploadService;
    }
}
