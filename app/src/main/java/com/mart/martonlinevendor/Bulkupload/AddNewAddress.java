package com.mart.martonlinevendor.Bulkupload;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.UpdateAddressModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewAddress extends AppCompatActivity implements View.OnClickListener /*GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener*/ {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private EditText et_username;
    private EditText et_mobile;
    private EditText et_Landmark;
    private EditText et_Pincode;
    private EditText et_Address;
    private ImageView colorheader;
    private Button btn_Save;
    private LinearLayout layout_bottom;
    UserSessionManager session;
    HashMap<String, String> user;
    GoogleApiClient mGoogleApiClient;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        session = new UserSessionManager(AddNewAddress.this);
        user = session.getUserDetails();
        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();*/
       /* if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            Toast.makeText(this, "Not Connected!", Toast.LENGTH_SHORT).show();
        }*/
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddNewAddress.this, OuterCart.class));
            }
        });
        header = findViewById(R.id.header);
        et_username = findViewById(R.id.et_username);
        et_mobile = findViewById(R.id.et_mobile);
        et_Landmark = findViewById(R.id.et_Landmark);
        et_Pincode = findViewById(R.id.et_Pincode);
        et_Address = findViewById(R.id.et_Address);
        colorheader = findViewById(R.id.colorheader);
        btn_Save = findViewById(R.id.btn_Save);
        layout_bottom = findViewById(R.id.layout_bottom);
        et_username.setText(user.get(UserSessionManager.KEY_FIRST_NAME));
        et_mobile.setText(user.get(UserSessionManager.KEY_PHONE));
        colorheader.setOnClickListener(this);
        btn_Save.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
    }
    int LAUNCH_SECOND_ACTIVITY = 10;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Save:
                submit();
                break;
                case R.id.iv_menu:
                finish();
                break;
                case R.id.colorheader:
                settingsrequest();

                break;
        }
    }

//    public void turnGPSOn()
//    {
//        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//        intent.putExtra("enabled", true);
//        this.sendBroadcast(intent);
//
//        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//        if(!provider.contains("gps")){ //if gps is disabled
//            final Intent poke = new Intent();
//            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
//            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//            poke.setData(Uri.parse("3"));
//            this.sendBroadcast(poke);
//
//
//        }
//    }

    public void settingsrequest()
    {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                      /*  Intent i = new Intent(AddNewAddress.this, MapsActivity.class);
                        startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);*/

                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(AddNewAddress.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Intent i = new Intent(this, MapsActivity.class);
                        startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;

        }
    }
*/
    String lat="",lng="", straddress;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if (requestCode==REQUEST_CHECK_SETTINGS){
           if(resultCode == Activity.RESULT_OK) {
               /*Intent i = new Intent(this, MapsActivity.class);
               startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);*/
           }
           if (requestCode==RESULT_CANCELED) {
                   settingsrequest();//keep asking if imp or do whatever
           }
        }
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                lat=data.getStringExtra("lat");
                lng=data.getStringExtra("lng");
                straddress=data.getStringExtra("address");
                et_Address.setText(straddress);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    private void submit() {
        // validate
        String username = et_username.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, "Full Name", Toast.LENGTH_SHORT).show();
            return;
        }

        String mobile = et_mobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            Toast.makeText(this, "Phone Number", Toast.LENGTH_SHORT).show();
            return;
        }

        String Landmark = et_Landmark.getText().toString().trim();
      /*  if (TextUtils.isEmpty(Landmark)) {
            Toast.makeText(this, "Landmark", Toast.LENGTH_SHORT).show();
            return;
        }
*/
        String Pincode = et_Pincode.getText().toString().trim();

        String Address = et_Address.getText().toString().trim();
        if (TextUtils.isEmpty(Address)) {
            Toast.makeText(this, "Address", Toast.LENGTH_SHORT).show();
            return;
        }
        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("method", "updateAddress");
        map.put("name", username);
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("phone", mobile);
        map.put("landmark", Landmark);
        map.put("pincode", Pincode);
        map.put("address", Address);
        map.put("latitude", lat);
        map.put("longitude", lng);
        map.put("addressId", "");
        UpdateAddressAPI(map);
    }

    public void UpdateAddressAPI(final Map<String, String> map) {
        final Dialog dialog = new Dialog(AddNewAddress.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UpdateAddressModel> call = Apis.getAPIService().UpdateAddress(map);

        call.enqueue(new Callback<UpdateAddressModel>() {
            @Override
            public void onResponse(Call<UpdateAddressModel> call, Response<UpdateAddressModel> response) {
                dialog.dismiss();
                UpdateAddressModel userdata = response.body();
                if (userdata.getStatus().equals("1")) {
                    Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    finish();
                }else {
                    Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(Call<UpdateAddressModel> call, Throwable t) {
                dialog.dismiss();
                t.getMessage();
                Log.d("Asdfasdf",t.getMessage()+"");
            }
        });
    }

}
