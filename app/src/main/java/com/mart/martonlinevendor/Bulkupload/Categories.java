package com.mart.martonlinevendor.Bulkupload;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Bulkupload.Extra.Utils_location;
import com.mart.martonlinevendor.Extra.Utility;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.Model.Home_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Categories extends Activity {
    private final String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private final int LOCATION_PERMISSIONS_REQUEST = 123;
    private final Boolean currentLocationPermissionDeniedWithDontAskMeAgain = false;
    UserSessionManager session;
    HashMap<String, String> user;
    AlertDialog.Builder builder;
    List<Home_Model.ShopByCategory> array_list_category = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    List<Categories_Model.Response> array_categories_list;
    private ImageView iv_menu;
    private ImageView iv_bellnotification;
    private TextView tv_currentlocation;
    private TextView tvaddress;
    private RelativeLayout header;
    private ImageView iv_send;
    private TextView et_search;
    private TextView tvcartcount;
    private ImageView iv_voice;
    private TextView Shop_category_title;
    private RecyclerView recycler_shopByCategory;
    private SliderView itemPicker;
    List<Home_Model.Banners> array_banner_list;
    ShopAdapter shopAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_categories);
        session = new UserSessionManager(this);
        user = session.getUserDetails();
        TextView et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Categories.this, SearchProduct.class));
            }
        });
        intit();
    }

    private void intit() {
        builder = new AlertDialog.Builder(this);
        recycler_shopByCategory = findViewById(R.id.recycler_shopByCategory);
        tvcartcount = findViewById(R.id.tvcartcount);
        itemPicker = findViewById(R.id.item_picker);
        tvaddress = findViewById(R.id.tvaddress);
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
//        recycler_shopByCategory.setAdapter(new CategoryAdapter(getActivity()));
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
             /*   DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.*/
//                navDrawer.openDrawer(GravityCompat.START);
            }
        });

        ImageView iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {
                    startActivity(new Intent(Categories.this, OuterCart.class));
                }
            }
        });
        if (Utility.getlng().equals("0.0")) {

        } else {
            getAddress();
        }


        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            Map<String, String> map = new HashMap<>();
            map.put("method", "homeData");
            map.put("userId", "0");

            Home_Data(map);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("method", "homeData");
            map.put("userId", user.get(UserSessionManager.KEY_ID));

            Home_Data(map);
        }

        checkPermissionForCurrentLocation();
    }

    private void checkPermissionForCurrentLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils_location.hasPermissions(this, LOCATION_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Utils_location.showDialogToUserPermissionDenied(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                } else if (currentLocationPermissionDeniedWithDontAskMeAgain) {
                    Utils_location.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                }
            } else {
                if (!(Utils_location.isGPSEnabled(this))) {
                    Utils_location.enableGPS(this);
                } else {
                    setUserCurrentLocation();
                }
            }
        } else {
            if (!(Utils_location.isGPSEnabled(this))) {
                Utils_location.enableGPS(this);
            } else {
                setUserCurrentLocation();
            }

        }
    }

    //current location
    @SuppressLint("SetTextI18n")
    private void setUserCurrentLocation() {
        Location location = Utils_location.getLocation(this);
        if (location != null) {
            Address address = Utils_location.getAddressUsingLatLang(this, location.getLatitude(), location.getLongitude());
            if (address != null) {
//                et_location.setText(address.getLocality()+","+address.getCountryName());

            }
        } else {
            fetchLocation();
        }
    }

    private void fetchLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    Address address = Utils_location.getAddressUsingLatLang(Categories.this, location.getLatitude(), location.getLongitude());
                    if (address != null) {
//                        et_location.setText(address.getAddressLine(0));


                    }
                }
            }
        });
    }

    public void getAddress() {
        Address locationAddress = getAddress(Double.parseDouble(Utility.getlat()), Double.parseDouble(Utility.getlng()));
        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

               /* if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;*/
                tv_currentlocation.setText(city);
                tvaddress.setText(currentLocation);
//                userlocation.setVisibility(View.VISIBLE);
            }
        }
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }


    private void Home_Data(Map<String, String> map) {
        final Dialog dialog = new Dialog(Categories.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Home_Model> call = Apis.getAPIService().getHomeData(map);
        call.enqueue(new Callback<Home_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Home_Model> call, @NonNull Response<Home_Model> response) {
                dialog.dismiss();
                Home_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        tvcartcount.setText(userdata.getCartCount());
                      /*  SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Image, userdata.getResponse().getBannerCategory().getImage());
                        editor.commit();*/
                      /*  if (Utility.getCartcount().equals("0")){

                        }else{
                            Utility.setCartCount(userdata.getCartCount());
                            ((Dashboard) getActivity()).setSetbadgeText(Utility.getCartcount());
                        }*/
                        if (userdata.getResponse().getBanners().size() != 0) {
                            itemPicker.setVisibility(View.VISIBLE);
                            array_banner_list = new ArrayList<>();
                            array_banner_list.addAll(userdata.getResponse().getBanners());
                            if (array_banner_list.size() > 0) {
                                shopAdapter = new ShopAdapter(Categories.this, array_banner_list);
                                itemPicker.setSliderAdapter(shopAdapter);
                                shopAdapter.notifyDataSetChanged();
                            }
                        }else {
                            itemPicker.setVisibility(View.GONE);
                        }
                        if (userdata.getResponse().getShopByCategory().size() > 0) {
                            array_list_category.clear();
                            array_list_category.addAll(userdata.getResponse().getShopByCategory());
                            categoriesAdapter = new CategoriesAdapter();
                            recycler_shopByCategory.setAdapter(categoriesAdapter);
                        } else {
                        }
                    } else {
                        Custom_Toast_Activity.makeText(Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Home_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public class ShopAdapter extends SliderViewAdapter<ShopAdapter.SliderAdapterVH> {
        List<Home_Model.Banners> bannersList;
        Context context;
        LayoutInflater inflater;

        ShopAdapter(Context context, List<Home_Model.Banners> bannersList) {
            this.context = context;
//            this.inflater = LayoutInflater.from(context);
            this.bannersList = bannersList;
        }

        @Override
        public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop_card, null);
            return new SliderAdapterVH(inflate);
        }

        @Override
        public void onBindViewHolder(SliderAdapterVH holder, int position) {
            Picasso.get().load(Apis.IMAGE_PATH + bannersList.get(position).getImage())
                    .priority(Picasso.Priority.HIGH)
                    .error(R.drawable.no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.image);
        }

        @Override
        public int getCount() {
            //slider view count could be dynamic size
            return bannersList.size();
        }

        class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

            View view;
            private ImageView image;

            public SliderAdapterVH(View view) {
                super(view);
                image = view.findViewById(R.id.image);
                this.view = view;
            }
        }
    }
    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(getActivity(), LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_category, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.tv_categoryName.setText(array_list_category.get(position).getCatName());
            holder.tv_categoryName.setSelected(true);
//            holder.tv_category_details.setText(array_categories_list.get(position).getOffertest());
            Glide.with(Categories.this)
                    .load(Apis.IMAGE_PATH + array_list_category.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_category);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startActivity(new Intent(Categories.this, ShopList.class).putExtra("cat_id", array_list_category.get(position).getCatId()));

                }
            });
        }

        @Override
        public int getItemCount() {
            return array_list_category.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final CardView cardMain;
            private final ImageView iv_item;
            private final RelativeLayout card_shop_category;
            private final ImageView iv_category;
            private final TextView tv_categoryName;

            public ViewHolder(View itemView) {
                super(itemView);
                this.cardMain = itemView.findViewById(R.id.cardMain);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.card_shop_category = itemView.findViewById(R.id.card_shop_category);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            }
        }
    }
}
