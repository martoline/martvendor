package com.mart.martonlinevendor.Bulkupload.Extra;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import static android.content.Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;

public class Utils_location {

    private static Dialog progressDialog;


    public static void warningSnackBar(Context context, View v, String msg) {
        Snackbar snackbar = Snackbar.make(v, msg, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static boolean isDeviceOnline(Context context) {
        boolean isDeviceOnLine = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            isDeviceOnLine = netInfo != null && netInfo.isConnected();
        }
        return isDeviceOnLine;

    }

    public static boolean isValidEmail(String email) {
        String emailExp = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,10}$";
        Pattern pattern = Pattern.compile(emailExp, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
/*
    public static Dialog showProDialog(Context context) {

        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = new Dialog(context);
            progressDialog.setContentView(R.layout.dialog_login);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCancelable(false);

//            TextView tvPercentage = progressDialog.findViewById(R.id.tvPercentage);
//            ProgressBar progress_bar = progressDialog.findViewById(R.id.progress_bar);
//            a = progress_bar.getProgress();
//            Handler handle = new Handler() {
//                @Override
//                public void handleMessage(Message msg) {
//                    super.handleMessage(msg);
//                    progressDialog.incrementProgressBy(1);
//                }
//            };
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                     handler = new Handler();
//                    while (a < 100) {
//
//                        a += 1;
//
//                        try {
//                            Thread.sleep(200);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                progress_bar.setProgress(a);
//                                tvPercentage.setText(String.valueOf(a));
//
//                                if (a == 100) {
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        });
//                    }
//                }
//            }).start();
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return progressDialog;
    }*/

  /*  public static void dismissProDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }*/

    public static Bitmap getCompressImage(Uri uri) {
        try {
            File f = new File(uri.getPath());
            if (!f.exists()) {
                return null;
            }
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 80;

            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveImage(Bitmap image, File file) {
        if (file == null) {
            Log.d("Save Image", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("Save Image", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Save Image", "Error accessing file: " + e.getMessage());
        }
    }

    public static Uri takePhotoFromCamera(Activity activity, int requestCode) {
        Uri photoURI = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.addFlags(FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utils_location.createImageFile(activity);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully Created by Raj on 02-June-19.
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(activity,
                        "com.twc.tiecoon.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(intent, requestCode);
            }
        }
        return photoURI;
    }

    public static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /*public static Uri getImageUri(Bitmap image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (image != null) {
            image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        }
        String path = MediaStore.Images.Media.insertImage(ApplicationClass.getCurrActivity().getContentResolver(), image, "Title", null);
        return Uri.parse(path);
    }*/


    public static Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

    public static String getUriRealPath(Context context, Uri uri) {
        String ret;
        if (isAboveKitKat()) {
            // Android OS above sdk version 19.
            ret = getUriRealPathAboveKitkat(context, uri);
        } else {
            // Android OS below sdk version 19
            ret = getImageRealPath(context, context.getContentResolver(), uri, null);
        }

        return ret;
    }

    public static String getUriRealPathAboveKitkat(Context context, Uri uri) {
        String ret = "";
        if (context != null && uri != null) {
            if (isContentUri(uri)) {
                if (isGooglePhotoDoc(uri.getAuthority())) {
                    ret = uri.getLastPathSegment();
                } else {
                    ret = getImageRealPath(context, context.getContentResolver(), uri, null);
                }
            } else if (isFileUri(uri)) {
                ret = uri.getPath();
            } else if (isDocumentUri(context, uri)) {

                // Get uri related document id.
                String documentId = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    documentId = DocumentsContract.getDocumentId(uri);
                }

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if (isMediaDoc(uriAuthority)) {
                    String idArr[] = new String[0];
                    if (documentId != null) {
                        idArr = documentId.split(":");
                    }
                    if (idArr.length == 2) {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if ("image" .equals(docType)) {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video" .equals(docType)) {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio" .equals(docType)) {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getImageRealPath(context, context.getContentResolver(), mediaContentUri, whereClause);
                    }

                } else if (isDownloadDoc(uriAuthority)) {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = null;
                    if (documentId != null) {
                        downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));
                    }

                    ret = getImageRealPath(context, context.getContentResolver(), downloadUriAppendId, null);

                } else if (isExternalStoreDoc(uriAuthority)) {
                    String idArr[] = new String[0];
                    if (documentId != null) {
                        idArr = documentId.split(":");
                    }
                    if (idArr.length == 2) {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if ("primary" .equalsIgnoreCase(type)) {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }
    public static boolean isValidMobile(String phone) {
        return  phone.matches("[1-9][0-9]{9}");
    }
    /* Check whether current android os version is bigger than kitkat or not. */
    public static boolean isAboveKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

    }

    /* Check whether this uri represent a document or not. */
    public static boolean isDocumentUri(Context ctx, Uri uri) {
        boolean ret = false;
        if (ctx != null && uri != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                ret = DocumentsContract.isDocumentUri(ctx, uri);
            }
        }
        return ret;
    }

    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    public static boolean isContentUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("content" .equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    public static boolean isFileUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("file" .equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }


    /* Check whether this document is provided by ExternalStorageProvider. */
    public static boolean isExternalStoreDoc(String uriAuthority) {
        boolean ret = false;
        if ("com.android.externalstorage.documents" .equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    public static boolean isDownloadDoc(String uriAuthority) {
        boolean ret = false;
        if ("com.android.providers.downloads.documents" .equals(uriAuthority)) {
            ret = true;
        }
        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    public static boolean isMediaDoc(String uriAuthority) {
        boolean ret = false;
        if ("com.android.providers.media.documents" .equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    public static boolean isGooglePhotoDoc(String uriAuthority) {
        boolean ret = false;
        if ("com.google.android.apps.photos.content" .equals(uriAuthority)) {
            ret = true;
        }
        return ret;
    }

    /* Return uri represented document file real local path.*/
    public static String getImageRealPath(Context context, ContentResolver contentResolver, Uri uri, String whereClause) {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if (cursor != null) {
            boolean moveToFirst = cursor.moveToFirst();
            if (moveToFirst) {

                // Get columns name by uri type.
                //   String columnName = MediaStore.Images.Media.DATA;

//                if (uri == MediaStore.Images.Media.EXTERNAL_CONTENT_URI) {
//                    columnName = MediaStore.Images.Media.DATA;
//                } else if (uri == MediaStore.Audio.Media.EXTERNAL_CONTENT_URI) {
//                    columnName = MediaStore.Audio.Media.DATA;
//                } else if (uri == MediaStore.Video.Media.EXTERNAL_CONTENT_URI) {
//                    columnName = MediaStore.Video.Media.DATA;
//                }
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor2 = null;
                try {
                    cursor2 = context.getContentResolver().query(uri, projection, null, null, null);
                    int column_index = 0;
                    if (cursor2 != null) {
                        column_index = cursor2.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    }
                    if (cursor2 != null && cursor2.moveToFirst()) {
                        return cursor2.getString(column_index);
                    }
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                } catch (Exception e) {
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    e.printStackTrace();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return ret;
    }


    public static Bitmap getScaledBitmap(Context context, Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, sizeOptions);
        int inSampleSize = calculateInSampleSize(sizeOptions);

        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;

        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, sizeOptions);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > 800 || width > 800) {
            // Calculate ratios of height and width to requested one
            final int heightRatio = Math.round((float) height / (float) 800);
            final int widthRatio = Math.round((float) width / (float) 800);

            // Choose the smallest ratio as inSampleSize value
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }


    public static String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static void choosePhotoFromGallery(Activity activity, int requestCode) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        activity.startActivityForResult(galleryIntent, requestCode);
    }


    public static void selectImageFromGallery(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        activity.startActivityForResult(intent, requestCode);
    }

   /* public static void
    cropImage(Activity activity, Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(1, 1)
                .setBorderCornerColor(activity.getResources().getColor(R.color.white))
                .setBorderLineThickness(1.0f)
                .setGuidelinesThickness(1.0f)
                .setInitialCropWindowPaddingRatio(0.0f)
                .setBorderLineColor(activity.getResources().getColor(R.color.half_white))
                .setActivityMenuIconColor(activity.getResources().getColor(R.color.white))
                .setGuidelines(CropImageView.Guidelines.ON)
                .setGuidelinesColor(activity.getResources().getColor(R.color.half_white))
                .setAutoZoomEnabled(true)
                .start(activity);
    }*/

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void showDialogToUserPermissionDenied(Activity context, String msg, String[] permissions, int requestCode) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setTitle("Permissions Denied")
                .setMessage(msg)
                .setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(context, permissions, requestCode);
                    }
                })
                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    public static void allowPermissionDeniedWithDontAskMeAgain(Activity activity, String message, int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.permissions_settings));
        builder.setCancelable(true);
        builder.setPositiveButton("GO TO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivityForResult(intent, requestCode);
        });
//        builder.setNegativeButton("I'M SURE", (dialog, which) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showDialogToUserPermissionDeniedWithoutNegative(Activity context, String msg, String[] permissions, int requestCode) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle("Permissions Denied")
                .setMessage(msg)
                .setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(context, permissions, requestCode);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    public static void enableGPS(Activity activity) {
        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1000);
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(activity)
                .checkLocationSettings(settingsBuilder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response =
                            task.getResult(ApiException.class);
                } catch (ApiException ex) {
                    switch (ex.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvableApiException =
                                        (ResolvableApiException) ex;
                                resolvableApiException
                                        .startResolutionForResult(activity,
                                                15);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            }
        });
    }


    public static Location setCurrentLocationUsingLocationManager(Context context) {
        Location currentLocation = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            // getting GPS status
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            // no network provider is enabled
            if (isNetworkEnabled) {//get the location by gps
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    currentLocation = location;
                }
            } else if (isGPSEnabled) { // get location from Network Provider
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    currentLocation = location;
                }
            }
        }
        return currentLocation;
    }

    public static Location getLocationUsingFused(Context context) {
        final Location[] currentLocation = new Location[1];
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    currentLocation[0] = location;
                }
            }
        });
        return currentLocation[0];
    }

    public static Location getLocation(Context context) {
        Location location = null;
        if (isDeviceOnline(context)) {
            Location currentLocation = setCurrentLocationUsingLocationManager(context);
            if (currentLocation != null) {
                location = currentLocation;
            }
        }
        return location;
    }

    public static boolean isGPSEnabled(Context context) {
        boolean GPSEnabled = false;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                GPSEnabled = true;
            }
        }
        return GPSEnabled;
    }

    public static Address getAddressUsingLatLang(Context context, double latitude, double longitude) {
        Address address = null;
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
            if (list != null && !(list.isEmpty())) {
                address = list.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return address;
    }

}
