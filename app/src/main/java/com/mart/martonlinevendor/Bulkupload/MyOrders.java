package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrders extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView_myOrders;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    MyOrdersAdapter myOrdersAdapter;

    List<My_Order_Model.Response> array_order_list;
//    List<My_Order_Model.Response> array_order_list;
    UserSessionManager session;
    HashMap<String, String> user;
    String producttitle;
    String image;
    String itmeprice;
    String orderStatus;

    private LinearLayout no_found;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private RelativeLayout header;
    private LinearLayout rl_header;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
//    List<My_Order_Model.ProductDetail> productlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    public void initView() {
        no_found = findViewById(R.id.no_data_found);
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        recyclerView_myOrders = findViewById(R.id.recyclerView_myOrders);

//        myOrdersAdapter = new MyOrdersAdapter(MyOrders.this);
//        recyclerView_myOrders.setAdapter(myOrdersAdapter);

        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        iv_send.setOnClickListener(this);
        et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyOrders.this, SearchProduct.class));
            }
        });
        iv_bellnotification.setOnClickListener(this);
        iv_voice = findViewById(R.id.iv_voice);
        iv_voice.setOnClickListener(this);
        header = findViewById(R.id.header);
        header.setOnClickListener(this);
        rl_header = findViewById(R.id.rl_header);
        rl_header.setOnClickListener(this);
        cart_image = findViewById(R.id.cart_image);
        cart_image.setOnClickListener(this);
        not_found_text = findViewById(R.id.not_found_text);
        not_found_text.setOnClickListener(this);
        dialogView = findViewById(R.id.dialogView);
        dialogView.setOnClickListener(this);
        no_data_found = findViewById(R.id.no_data_found);
        no_data_found.setOnClickListener(this);

        Map<String, String> map = new HashMap<>();
        map.put("method", "orderList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getOrderList(map);
    }

    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(MyOrders.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<My_Order_Model> call = Apis.getAPIService().getOrderList1(map);
        call.enqueue(new Callback<My_Order_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
                dialog.dismiss();
                My_Order_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_found.setVisibility(View.GONE);
                        recyclerView_myOrders.setVisibility(View.VISIBLE);
                        array_order_list = new ArrayList<>();
                        array_order_list.addAll(userdata.getResponse());
                        myOrdersAdapter = new MyOrdersAdapter(MyOrders.this, array_order_list);
                        recyclerView_myOrders.setAdapter(myOrdersAdapter);
                        myOrdersAdapter.notifyDataSetChanged();
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        no_found.setVisibility(View.VISIBLE);
                        recyclerView_myOrders.setVisibility(View.GONE);
                        // no_found.setText("My Order not Found");
                        // Custom_Toast_Activity.makeText(MyOrders.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tv_currentlocation:
                Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;
            case R.id.iv_bellnotification:
                startActivity(new Intent(this, OuterCart.class));
                break;
        }
    }

    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        List<My_Order_Model.Response> orderlist;
        Context context;
        LayoutInflater inflater;

        MyOrdersAdapter(Context context, List<My_Order_Model.Response> orderlist) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.orderlist = orderlist;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders1, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_panding.setVisibility(View.GONE);
            holder.tv_delivered.setVisibility(View.GONE);
            holder.tv_cancel.setVisibility(View.GONE);
            holder.tv_item_order_date.setText(orderlist.get(position).getDate());
            holder.tv_price.setText("Total: " + "Rs " + orderlist.get(position).getTotalAmount());
            holder.tv_name.setText("Order No: "+orderlist.get(position).getOrderId());
            orderStatus = orderlist.get(position).getOrderStatus();
            holder.tv_otp.setText("Pin:" + orderlist.get(position).getOtp());
            holder.order_rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
                            .putExtra("id", orderlist.get(position).getId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
                            .putExtra("id", orderlist.get(position).getId()));
                }
            });
//             <option value="1">Pending</option>
//          <option value="2">Accepted</option>
//          <option value="3" selected="">Under Process</option>
//          <option value="4">Delivered</option>
            Log.d("qwerty",orderStatus+"_____"+orderlist.get(position).getOrderId());
            switch (orderStatus) {
                case "New":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_cancel.setVisibility(View.GONE);
                    holder.tv_panding.setText("Pending");
                    break;
                case "Ready To Ship":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_cancel.setVisibility(View.GONE);
                    holder.tv_panding.setText("Ready To Ship");
                    break;
                case "On The Way":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_cancel.setVisibility(View.GONE);
                    holder.tv_panding.setText("On The Way");
                    break;
                case "Delivered":
                    holder.tv_delivered.setVisibility(View.VISIBLE);
                    holder.tv_panding.setVisibility(View.GONE);
                    holder.tv_cancel.setVisibility(View.GONE);
                    holder.tv_delivered.setText("Delivered");
                    break;
                case "Cancelled":
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setVisibility(View.GONE);
//                    btn_cancel.setVisibility(View.GONE);
                    holder.tv_cancel.setVisibility(View.VISIBLE);
                    holder.tv_cancel.setText("Cancelled");
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return orderlist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private CardView card_myOrders;
            private ImageView iv_item;
            private TextView tv_name, tv_otp;
            private TextView tv_item_order_date;
            private TextView tv_price;
            private TextView tv_panding;
            private TextView tv_delivered;
            private TextView tv_cancel;
            private RelativeLayout order_rel;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_cancel = itemView.findViewById(R.id.tv_cancel);
                this.tv_item_order_date = itemView.findViewById(R.id.tv_item_order_date);
                this.tv_price = itemView.findViewById(R.id.tv_price);
                this.tv_panding = itemView.findViewById(R.id.tv_panding);
                this.tv_delivered = itemView.findViewById(R.id.tv_delivered);
                this.order_rel = itemView.findViewById(R.id.order_rel);
                this.tv_otp = itemView.findViewById(R.id.tv_otp);
            }
        }
    }
/*
    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(MyOrders.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<My_Order_Model> call = Apis.getAPIService().getOrderList(map);
        call.enqueue(new Callback<My_Order_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
                dialog.dismiss();
                My_Order_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_found.setVisibility(View.GONE);
                        recyclerView_myOrders.setVisibility(View.VISIBLE);
                        array_order_list = new ArrayList<>();
                        array_order_list.addAll(userdata.getResponse());
                        myOrdersAdapter = new MyOrdersAdapter(MyOrders.this, array_order_list);
                        recyclerView_myOrders.setAdapter(myOrdersAdapter);
                        myOrdersAdapter.notifyDataSetChanged();
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        no_found.setVisibility(View.VISIBLE);
                        recyclerView_myOrders.setVisibility(View.GONE);
                        // no_found.setText("My Order not Found");
                         // Custom_Toast_Activity.makeText(MyOrders.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }*/


    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        map.put("method", "orderList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getOrderList(map);
    }
}
