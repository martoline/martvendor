package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.OrderDetailBulk_Model;
import com.mart.martonlinevendor.Model.OrderDetail_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Order_History_Details extends AppCompatActivity implements View.OnClickListener {
    HashMap<String, String> user;
    MyOrders_detailsAdapter myOrders_detailsAdapter;
    List<OrderDetailBulk_Model.Data> array_order_list;
    List<OrderDetailBulk_Model.Data> array_order_list1;
    UserSessionManager session;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    private RelativeLayout ll_header;
    private RecyclerView recyclerView_myOrders;
    private ImageView cart_image, iv_menu;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private RelativeLayout header;
    private LinearLayout rl_header;
    private LinearLayout llratiing;
    private LinearLayout llrating;
    private TextView tv_orderID;
    private TextView tv_vendor_name;
    private TextView tv_datetime;
    private TextView tv_itemtotal;
    private TextView tv_Delivery_Charge;
    private TextView tv_Discount_Applied;
    private TextView tv_total;
    private TextView tv_pin;
    private TextView tv_panding;
    private TextView tv_cancel;
    private TextView tv_delivered;
    private RatingBar rating;
    private EditText et_review;
    private Button btn_submit;
    private Button btn_repetorder;
    private Button btn_cancel;
    private TextView tv_timeslot;
    private TextView tv_address;
    private TextView tv_paymentType;
    private TextView tv_deliveryType;
    private TextView tv_payorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__history__details1);
        session = new UserSessionManager(Order_History_Details.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        btn_repetorder = findViewById(R.id.btn_repetorder);
        btn_cancel = findViewById(R.id.btn_cancel);
        llrating = findViewById(R.id.llrating);
        tv_pin = findViewById(R.id.tv_pin);
        llratiing = findViewById(R.id.llratiing);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Order_History_Details.this, OuterCart.class));
            }
        });
        iv_menu = findViewById(R.id.iv_menu);

        recyclerView_myOrders = findViewById(R.id.recyclerView_myOrders);
        cart_image = findViewById(R.id.cart_image);
        not_found_text = findViewById(R.id.not_found_text);
        dialogView = findViewById(R.id.dialogView);
        tv_payorder = findViewById(R.id.tv_payorder);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_paymentType = findViewById(R.id.tv_paymentType);
        no_data_found = findViewById(R.id.no_data_found);
        myOrders_detailsAdapter = new MyOrders_detailsAdapter();
//        recyclerView_myOrders.setAdapter(myOrders_detailsAdapter);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        iv_voice = findViewById(R.id.iv_voice);
        header = findViewById(R.id.header);
        rl_header = findViewById(R.id.rl_header);
        Map<String, String> map = new HashMap<>();
        map.put("method", "orderDetail");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("orderId", getIntent().getStringExtra("id"));
        // user.get(UserSessionManager.KEY_ID);
        getOrderList(map);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_orderID = findViewById(R.id.tv_orderID);
        tv_vendor_name = findViewById(R.id.tv_vendor_name);
        tv_datetime = findViewById(R.id.tv_datetime);
        tv_itemtotal = findViewById(R.id.tv_itemtotal);
        tv_Delivery_Charge = findViewById(R.id.tv_Delivery_Charge);
        tv_Discount_Applied = findViewById(R.id.tv_Discount_Applied);
        tv_total = findViewById(R.id.tv_total);
        tv_panding = findViewById(R.id.tv_panding);
        tv_panding.setOnClickListener(this);
        tv_delivered = findViewById(R.id.tv_delivered);
        tv_deliveryType = findViewById(R.id.tv_deliveryType);
        tv_delivered.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        rating = findViewById(R.id.rating);
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                if (rating < 1.0f)
                    ratingBar.setRating(1.0f);
            }
        });
        et_review = findViewById(R.id.et_review);
        et_review.setOnClickListener(this);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        tv_timeslot = findViewById(R.id.tv_timeslot);
        tv_timeslot.setOnClickListener(this);
        tv_address = findViewById(R.id.tv_address);
        tv_address.setOnClickListener(this);
        btn_repetorder.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(Order_History_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OrderDetailBulk_Model> call = Apis.getAPIService().getOrderDeatilsBulk(map);
        call.enqueue(new Callback<OrderDetailBulk_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<OrderDetailBulk_Model> call, @NonNull Response<OrderDetailBulk_Model> response) {
                dialog.dismiss();
                OrderDetailBulk_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_order_list = new ArrayList<>();
//                        array_order_list1 = new ArrayList<>();
//                        for (int i=0;)
                        int shipping = 0;
                        if (userdata.getResponse().getSummary().getReview().equals("0")) {
                            llrating.setVisibility(View.VISIBLE);
                        } else {
                            llrating.setVisibility(View.GONE);
                            btn_cancel.setVisibility(View.GONE);
                        }
                        tv_orderID.setText(userdata.getResponse().getSummary().getOrderid());
                        tv_vendor_name.setText(userdata.getResponse().getSummary().getShopname());
                        tv_pin.setText(userdata.getResponse().getSummary().getOtp());
                        tv_datetime.setText(userdata.getResponse().getSummary().getDate());
                        tv_deliveryType.setText(userdata.getResponse().getSummary().getDeliverytype());
                        tv_paymentType.setText(userdata.getResponse().getSummary().getPaymenttype());
                        tv_address.setText(userdata.getResponse().getSummary().getAddress());
                        tv_payorder.setText(userdata.getResponse().getSummary().getPayment_status());
                        tv_timeslot.setText(userdata.getResponse().getSummary().getDeliveryslotid());
                        int abc = Integer.parseInt(userdata.getResponse().getSummary().getTotalprice());
                        if (!userdata.getResponse().getSummary().getShippingcharge().equals("0")) {
                            shipping = Integer.parseInt(userdata.getResponse().getSummary().getShippingcharge());
                        } else {
                        }
                        int discount = 0;
                        if (!userdata.getResponse().getSummary().getDiscount().equals("0")) {
                            discount = Integer.parseInt(userdata.getResponse().getSummary().getDiscount());
                        } else {
                        }
                        Log.d("sdfa____sd", abc + "__" + discount + "__" + shipping);
                        int finaltotl = abc + discount - shipping;
                        tv_itemtotal.setText(abc + "");
                        tv_Delivery_Charge.setText(userdata.getResponse().getSummary().getShippingcharge());
                        tv_Discount_Applied.setText("-" + userdata.getResponse().getSummary().getDiscount());
                        int finaltotl1 = abc + shipping - discount;
                        tv_total.setText(finaltotl1 + "");
                        array_order_list = userdata.getResponse().getData();
                        if (array_order_list.size() > 0) {
                            myOrders_detailsAdapter = new MyOrders_detailsAdapter();
                            recyclerView_myOrders.setAdapter(myOrders_detailsAdapter);
                            myOrders_detailsAdapter.notifyDataSetChanged();
                        }
                        switch (userdata.getResponse().getSummary().getOrder_status()) {
                            case "Pending":
                                tv_panding.setVisibility(View.VISIBLE);
                                tv_delivered.setVisibility(View.GONE);
                                llratiing.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.GONE);
                                tv_panding.setText("Pending");
                                btn_cancel.setVisibility(View.VISIBLE);
                                break;
                            case "Ready To Ship":
                                llratiing.setVisibility(View.GONE);
                                tv_panding.setVisibility(View.VISIBLE);
                                tv_delivered.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.GONE);
                                tv_panding.setText("Ready To Ship");
                                btn_cancel.setVisibility(View.VISIBLE);
                                break;
                            case "On The Way":
                                llratiing.setVisibility(View.GONE);
                                tv_panding.setVisibility(View.VISIBLE);
                                tv_delivered.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.GONE);
                                tv_panding.setText("On The Way");
                                btn_cancel.setVisibility(View.VISIBLE);
                                break;
                            case "Delivered":
                                llratiing.setVisibility(View.VISIBLE);
                                btn_cancel.setVisibility(View.GONE);
                                tv_delivered.setVisibility(View.VISIBLE);
                                tv_panding.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.GONE);
                                tv_panding.setText("Delivered");
                                break;
                            case "Cancelled":
                                llratiing.setVisibility(View.GONE);
                                btn_cancel.setVisibility(View.GONE);
                                tv_delivered.setVisibility(View.GONE);
                                tv_panding.setVisibility(View.GONE);
                                btn_cancel.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.VISIBLE);
                                tv_panding.setText("Cancelled");
                                break;
                            case "Failed":
                                llratiing.setVisibility(View.GONE);
                                btn_cancel.setVisibility(View.GONE);
                                tv_delivered.setVisibility(View.GONE);
                                tv_panding.setVisibility(View.GONE);
                                btn_cancel.setVisibility(View.GONE);
                                tv_cancel.setVisibility(View.VISIBLE);
                                tv_panding.setText("Payment Faild");
                                break;
                        }
//                        Log.d("sfsdf", array_order_list1.size() + "");
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        //no_found.setVisibility(View.VISIBLE);
                        // no_found.setText("My Order not Found");
                        Custom_Toast_Activity.makeText(Order_History_Details.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetailBulk_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void addReview(Map<String, String> map) {
        final Dialog dialog = new Dialog(Order_History_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OrderDetail_Model> call = Apis.getAPIService().getOrderDeatils(map);
        call.enqueue(new Callback<OrderDetail_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<OrderDetail_Model> call, @NonNull Response<OrderDetail_Model> response) {
                dialog.dismiss();
                OrderDetail_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(Order_History_Details.this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("MartOline");
                        sweetAlertDialog.setContentText(userdata.getMsg());
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCancelable(false);

                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                finish();
                            }
                        });
                        if (sweetAlertDialog.isShowing()) {
                        } else {
                            sweetAlertDialog.show();
                        }
                    } else {
                        Custom_Toast_Activity.makeText(Order_History_Details.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetail_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void cancel_order(Map<String, String> map) {
        final Dialog dialog = new Dialog(Order_History_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OrderDetailBulk_Model> call = Apis.getAPIService().getOrderDeatilsBulk(map);
        call.enqueue(new Callback<OrderDetailBulk_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<OrderDetailBulk_Model> call, @NonNull Response<OrderDetailBulk_Model> response) {
                dialog.dismiss();
                OrderDetailBulk_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "orderDetail");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("orderId", getIntent().getStringExtra("id"));
                        // user.get(UserSessionManager.KEY_ID);
                        getOrderList(map);
                    } else {
                        Custom_Toast_Activity.makeText(Order_History_Details.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetailBulk_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void RepetOrder(Map<String, String> map) {
        final Dialog dialog = new Dialog(Order_History_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OrderDetail_Model> call = Apis.getAPIService().getOrderDeatilsB2B(map);
        call.enqueue(new Callback<OrderDetail_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<OrderDetail_Model> call, @NonNull Response<OrderDetail_Model> response) {
                dialog.dismiss();
                OrderDetail_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                       startActivity(new Intent(Order_History_Details.this, OuterCart.class));
                    } else {
                        Custom_Toast_Activity.makeText(Order_History_Details.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetail_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                submit();
                break;

                case R.id.btn_cancel:
                    final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(Order_History_Details.this, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("MartOline");
                    sweetAlertDialog.setContentText("Are you sure you want to cancel this order?");
                    sweetAlertDialog.setConfirmText("Yes");
                    sweetAlertDialog.setCancelText("No");
                    sweetAlertDialog.setCancelable(false);

                    sweetAlertDialog.showCancelButton(true);
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sweetAlertDialog.cancel();
//                            finish();
                        }
                    });
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sweetAlertDialog.cancel();
//                            finish();
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "cancelorder");
                            map.put("orderId", getIntent().getStringExtra("id"));
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            // user.get(UserSessionManager.KEY_ID);
                            cancel_order(map);
                        }
                    });
                    if (sweetAlertDialog.isShowing()) {
                    } else {
                        sweetAlertDialog.show();
                    }

                break;
                case R.id.btn_repetorder:
                    // TODO validate success, do something
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "repeatorder");
                    map.put("orderId", getIntent().getStringExtra("id"));
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    // user.get(UserSessionManager.KEY_ID);
                    RepetOrder(map);
                break;
        }
    }

    private void submit() {
        // validate
        String review = et_review.getText().toString().trim();
        if (TextUtils.isEmpty(review)) {
            Toast.makeText(this, "Add review", Toast.LENGTH_SHORT).show();
            return;
        }
        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("method", "rating");
        map.put("pid", getIntent().getStringExtra("id"));
        map.put("uid", user.get(UserSessionManager.KEY_ID));
        map.put("comment", review);
        map.put("rating", rating.getRating() + "");
        // user.get(UserSessionManager.KEY_ID);
        addReview(map);

    }

    public class MyOrders_detailsAdapter extends RecyclerView.Adapter<MyOrders_detailsAdapter.ViewHolder> {

        MyOrders_detailsAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.my_orders_details1, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tv_name.setText(array_order_list.get(position).getQty() + " X " + array_order_list.get(position).getTitle());
            holder.tv_name.setSelected(true);
            holder.tv_price.setText(" Rs " + array_order_list.get(position).getActualprice());
           /* if (array_order_list.get(position).getSize().isEmpty()){
                holder.tv_size.setVisibility(View.GONE);
            }else{
                holder.tv_size.setText("Size: "+array_order_list.get(position).getSize());
                holder.tv_size.setVisibility(View.VISIBLE);
            }*/

            Picasso.get().load(Apis.IMAGE_PATH + array_order_list.get(position).getImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);

          /*  switch (array_order_list1.get(position).getOrderStatus()) {
                case "1":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Pending");
                    break;
                case "2":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Accepted");

                    break;
                case "3":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Under Process");
                    break;
                case "4":
                    holder.tv_delivered.setVisibility(View.VISIBLE);
                    holder.tv_panding.setVisibility(View.GONE);
                    holder.tv_panding.setText("Delivered");
                    break;
            }*/
        }

        @Override
        public int getItemCount() {
            return array_order_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final ImageView iv_item;
            private final TextView tv_name;
            private final TextView tv_price;
            private final TextView tv_size;

            public ViewHolder(View itemView) {
                super(itemView);
//                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_size = itemView.findViewById(R.id.tv_size);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_price = itemView.findViewById(R.id.tv_price);

            }
        }
    }

}
