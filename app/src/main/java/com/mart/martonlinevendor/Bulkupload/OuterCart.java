package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Bulkupload.Payment.PaymentWebview;
import com.mart.martonlinevendor.Bulkupload.Payment.PaymenttSuccess;
import com.mart.martonlinevendor.Extra.Utility;
import com.mart.martonlinevendor.Model.ActiviteAddressModel;
import com.mart.martonlinevendor.Model.Add_Cart_Model;
import com.mart.martonlinevendor.Model.AddressListModel;
import com.mart.martonlinevendor.Model.Cart_List_Basket_Model;
import com.mart.martonlinevendor.Model.Coupon_Apply_Model;
import com.mart.martonlinevendor.Model.OrderConfirmModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OuterCart extends AppCompatActivity implements View.OnClickListener {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    List<Cart_List_Basket_Model.Deliveryslot> deliveryslotList;
    String android_id;
    CartAdapter cartAdapter;
    HashMap<String, String> user;
    String cart_list_json = "";
    String postal_code;
    Intent intent;
    List<Cart_List_Basket_Model.Cart> array_cart_list;
    String currentAdd;
    private ImageView iv_apply_coupon;
    AddressAdapter addressAdapter = new AddressAdapter();
    private ImageView iv_menu;
    private ImageView iv_bellnotification;
    private TextView tv_currentlocation;
    private TextView tvaddress;
    private RelativeLayout header;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private TextView tv_total_items;
    private LinearLayout item_total_in_bucket;
    private RecyclerView recyclerView_cart;
    private RecyclerView recyclerView_address;
    private TextView tv_add_address;
    private ImageView voucher;
    private TextView tvcoupontext;
    private TextView tv_subtotal;
    private TextView tv_tax;
    private TextView tv_shipping;
    private TextView tv_discount;
    private TextView tvslot;
    private TextView timeslot;
    private CardView card_amount_calculation;
    private TextView tv_price_checkout;
    private TextView tv_checkout;
    private RelativeLayout rl_bottomBar;
    private RelativeLayout cart_rel;
    private RelativeLayout rlcoupon;
    private ImageView cart_image;
    private TextView not_found_text;
    private TextView tv_yousaved;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
    private FusedLocationProviderClient fusedLocationClient;
    private Location currentLocation;
    private LocationCallback locationCallback;
    UserSessionManager session;
    List<AddressListModel.Response> addresslist = new ArrayList<>();
    private RadioButton cbselftpickup;
    private RelativeLayout rlselftpickup;
    private RadioButton cbdelivry;
    int deliverychrge = 0;
    int subtotal = 0;
    private RadioButton cbonline;
    private RadioButton cbcod;
    String paymentType = "online";
    String shipping = "0";
    String pickup = "delivery";
    String discount = "0";
    String coupon_id = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outer_cart);
        session = new UserSessionManager(OuterCart.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        android_id = Settings.Secure.getString(OuterCart.this.getContentResolver(),Settings.Secure.ANDROID_ID);
        iv_menu = findViewById(R.id.iv_menu);
        cbcod = findViewById(R.id.cbcod);
        cbonline = findViewById(R.id.cbonline);
        rlcoupon = findViewById(R.id.rlcoupon);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        tvaddress = findViewById(R.id.tvaddress);
        header = findViewById(R.id.header);
        cbdelivry = findViewById(R.id.cbdelivry);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        tv_total_items = findViewById(R.id.tv_total_items);
        item_total_in_bucket = findViewById(R.id.item_total_in_bucket);
        recyclerView_cart = findViewById(R.id.recyclerView_cart);
        recyclerView_address = findViewById(R.id.recyclerView_address);
        tv_add_address = findViewById(R.id.tv_add_address);
        voucher = findViewById(R.id.voucher);
        tvcoupontext = findViewById(R.id.tvcoupontext);
        tv_yousaved = findViewById(R.id.tv_yousaved);
        iv_apply_coupon = findViewById(R.id.iv_apply_coupon);
        tv_subtotal = findViewById(R.id.tv_subtotal);
        tv_tax = findViewById(R.id.tv_tax);
        tv_shipping = findViewById(R.id.tv_shipping);
        tv_discount = findViewById(R.id.tv_discount);
        tvslot = findViewById(R.id.tvslot);
        timeslot = findViewById(R.id.timeslot);
        card_amount_calculation = findViewById(R.id.card_amount_calculation);
        tv_price_checkout = findViewById(R.id.tv_price_checkout);
        tv_checkout = findViewById(R.id.tv_checkout);
        rl_bottomBar = findViewById(R.id.rl_bottomBar);
        cart_rel = findViewById(R.id.cart_rel);
        cart_image = findViewById(R.id.cart_image);
        not_found_text = findViewById(R.id.not_found_text);
        dialogView = findViewById(R.id.dialogView);
        no_data_found = findViewById(R.id.no_data_found);
        tv_checkout.setOnClickListener(this);
        tv_add_address.setOnClickListener(this);
        timeslot.setOnClickListener(this);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(OuterCart.this, Notification_Screen.class));
            }
        });
        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            Map<String, String> map = new HashMap<>();
            map.put("method", "CartList");
            map.put("deviceId", android_id);
//            map.put("userId", user.get(UserSessionManager.KEY_ID));
            GetCartList(map);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("method", "CartList");
            map.put("deviceId", android_id);
            map.put("userId", user.get(UserSessionManager.KEY_ID));
            GetCartList(map);
        }

        iv_apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aplycoupn();
            }
        });
        tvcoupontext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aplycoupn();
            }
        });
        rlcoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aplycoupn();
            }
        });
       /* if (Utility.getlng().equals("0.0")) {

        } else {
            getAddress();
        }*/
        cbselftpickup = findViewById(R.id.cbselftpickup);
        cbselftpickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbselftpickup.isChecked()){
//                    AddressId="0";
                    cbdelivry.setChecked(false);
                    tv_price_checkout.setText(""+ totalamount);
                }
            }
        });
        cbdelivry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbdelivry.isChecked()){
                    cbselftpickup.setChecked(false);
                    int a = Integer.parseInt(totalamount )+deliverychrge;
                    tv_price_checkout.setText(""+ a);
                }
            }
        });
        rlselftpickup = findViewById(R.id.rlselftpickup);
        rlselftpickup.setOnClickListener(this);
        cbonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbonline.isChecked()){
                    cbcod.setChecked(false);
                    paymentType = "online";
                }
            }
        });
        cbcod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbcod.isChecked()){
                    cbonline.setChecked(false);
                    paymentType = "cash";
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_checkout:
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {
                    if (cbselftpickup.isChecked()){
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "orderConfirm");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("totalAmount", totalamount);
                        map.put("billingAddressId", "0");
                        map.put("deliveryslotid", SlotId);
                        map.put("paymentType", paymentType);
                        map.put("obj", cart_list_json);
                        map.put("coupon_id", coupon_id);
                        map.put("pickup", "Self pickup");
                        map.put("discount", discount);
                        map.put("shipping", "0");
                        getOrderConfirm(map);
                    }else {
                        if (cbdelivry.isChecked()) {
                            if (AddressId.equals("0")) {
                                Custom_Toast_Activity.makeText(OuterCart.this, "Please select the Address", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                            } else {
                                Map<String, String> map = new HashMap<>();
                                map.put("method", "orderConfirm");
                                map.put("userId", user.get(UserSessionManager.KEY_ID));
                                map.put("totalAmount", totalamount);
                                map.put("billingAddressId", AddressId);
                                map.put("deliveryslotid", SlotId);
                                map.put("pickup", "delivery");
                                map.put("paymentType", paymentType);
                                map.put("obj", cart_list_json);
                                map.put("coupon_id", coupon_id);
                                map.put("shipping", shipping);
                                map.put("discount", discount);
                                getOrderConfirm(map);
                            }
                        }else {
                            Custom_Toast_Activity.makeText(OuterCart.this, "Please select shoping method", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                        }
                    }
                }
                break;
            case R.id.tv_add_address:
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {
                    startActivity(new Intent(OuterCart.this, AddNewAddress.class));
                }
                break;
            case R.id.timeslot:
                if (deliveryslotList.size() > 0) {
                    showdialog1();
                } else {
                    Custom_Toast_Activity.makeText(OuterCart.this, "No Time Slot Added", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
                break;
        }
    }

    public void showdialog1() {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_timeslot);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RecyclerView search_list = dialog.findViewById(R.id.places_recycler_view);
        MyOrdersAdapter myOrdersAdapter= new MyOrdersAdapter(OuterCart.this, dialog);
        search_list.setAdapter(myOrdersAdapter);
        myOrdersAdapter.notifyDataSetChanged();
        dialog.show();
    }

    public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
        public int counter;
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_cart, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_name.setText(array_cart_list.get(position).getTitle());
            holder.tv_price.setText(array_cart_list.get(position).getPrice());
            holder.tv_item_count.setText(array_cart_list.get(position).getQty());
            holder.tv_item_size.setText("Size: "+array_cart_list.get(position).getSize());
            holder.tv_item_details.setText(array_cart_list.get(position).getVendorname());
            if (array_cart_list.get(position).getPickup().isEmpty()&& !array_cart_list.get(position).getDelivery().equals("")){
                cbselftpickup.setVisibility(View.GONE);
                cbdelivry.setChecked(true);
                int a = Integer.parseInt(totalamount )+deliverychrge;
                tv_price_checkout.setText(""+ a);
                Log.d("asdf","3");

            }else if(array_cart_list.get(position).getDelivery().isEmpty()&& !array_cart_list.get(position).getPickup().equals("")){
                cbselftpickup.setChecked(true);
                cbdelivry.setVisibility(View.GONE);
                Log.d("asdf","2");
            }else if(array_cart_list.get(position).getDelivery().isEmpty()&& array_cart_list.get(position).getPickup().isEmpty()){
                cbselftpickup.setChecked(true);
                cbdelivry.setVisibility(View.GONE);
                tv_price_checkout.setText(""+ totalamount);
                Log.d("asdf","2");
            }else{
                Log.d("asdf","1");
                cbdelivry.setVisibility(View.VISIBLE);
                cbselftpickup.setVisibility(View.VISIBLE);
                cbdelivry.setChecked(true);
                int a = Integer.parseInt(totalamount )+deliverychrge;
                tv_price_checkout.setText(""+ a);
            }
            Picasso.get().load(Apis.IMAGE_PATH + array_cart_list.get(position).getImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);

            holder.tv_remove_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    startActivity(new Intent(Notification.this, OtherProfile.class));
                    // Toast.makeText(OuterCart.this, "You clicked on cart holder", Toast.LENGTH_LONG).show();
                    counter = Integer.parseInt(holder.tv_item_count.getText().toString());
                    if (counter > 0) {
                        counter--;
                        holder.tv_item_count.setText("" + counter);
                    } else {
                        Toast.makeText(OuterCart.this, "You haven't select any of the item", Toast.LENGTH_LONG).show();
                    }

                }
            });
            holder.iv_item_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    startActivity(new Intent(Notification.this, OtherProfile.class));
                    // Toast.makeText(OuterCart.this, "You clicked on cart holder", Toast.LENGTH_LONG).show();
                    if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
//                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", array_cart_list.get(position).getProductId());
                        map.put("Qty", 0 + "");
                        map.put("deviceId", android_id);
                        map.put("size", array_cart_list.get(position).getSizeId());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map);
                    } else {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", array_cart_list.get(position).getProductId());
                        map.put("Qty", 0 + "");
                        map.put("deviceId", android_id);
                        map.put("size", array_cart_list.get(position).getSizeId());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map);
                    }
                }
            });

            holder.tv_add_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    counter = Integer.parseInt(holder.tv_item_count.getText().toString());
                    counter++;
                    holder.tv_item_count.setText("" + counter);
                }
            });
            holder.tv_item_count.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
//                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", array_cart_list.get(position).getProductId());
                        map.put("Qty", counter + "");
                        map.put("deviceId", android_id);
                        map.put("size", array_cart_list.get(position).getSizeId());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map);
                    } else {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", array_cart_list.get(position).getProductId());
                        map.put("Qty", counter + "");
                        map.put("deviceId", android_id);
                        map.put("size", array_cart_list.get(position).getSizeId());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return array_cart_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private CardView card_cart;
            private ImageView iv_item;
            private TextView tv_item_size;
            private ImageView iv_item_remove;
            private TextView tv_name;
            private TextView tv_item_details;
            private TextView tv_price;
            private LinearLayout tv_remove_item;
            private TextView tv_item_count;
            private LinearLayout tv_add_item;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_cart = itemView.findViewById(R.id.card_cart);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.iv_item_remove = itemView.findViewById(R.id.iv_item_remove);
                this.tv_item_size = itemView.findViewById(R.id.tv_item_size);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_item_details = itemView.findViewById(R.id.tv_item_details);
                this.tv_price = itemView.findViewById(R.id.tv_price);
                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

    String AddressId = "0";

    public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

        public int counter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_address, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_name.setText(addresslist.get(position).getName());
            holder.tv_item_details.setText(addresslist.get(position).getAddress());
            if (addresslist.get(position).getActiveaddress().equals("1")) {
                holder.rlmain.setBackground(getResources().getDrawable(R.drawable.bg_check));
                holder.ivcheck.setVisibility(View.VISIBLE);
                AddressId = addresslist.get(position).getId();
            } else {
                holder.rlmain.setBackground(getResources().getDrawable(R.drawable.bg_uncheck));
                holder.ivcheck.setVisibility(View.GONE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "activeaddress");
                    map.put("id", addresslist.get(position).getId());
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    ActiviteAddress(map);
                }
            });
        }

        @Override
        public int getItemCount() {
            return addresslist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_item, ivcheck;
            TextView tv_item_details, tv_name;
            private RelativeLayout rlmain;

            public ViewHolder(View itemView) {
                super(itemView);
                this.rlmain = itemView.findViewById(R.id.rlmain);
                this.ivcheck = itemView.findViewById(R.id.ivcheck);
                this.tv_item_details = itemView.findViewById(R.id.tv_item_details);
                this.tv_name = itemView.findViewById(R.id.tv_name);

            }
        }
    }

    String totalamount = "0";

    private void GetCartList(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Cart_List_Basket_Model> call = Apis.getAPIService().getCartList(map);
        call.enqueue(new Callback<Cart_List_Basket_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Cart_List_Basket_Model> call, @NonNull Response<Cart_List_Basket_Model> response) {
                dialog.dismiss();
                Cart_List_Basket_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {

                        if (userdata.getResponse().getPaymentmethode().equals("1")){
                            cbcod.setVisibility(View.VISIBLE);
                            cbonline.setVisibility(View.GONE);
                            cbcod.setChecked(true);
                            paymentType = "cash";
                        }else if(userdata.getResponse().getPaymentmethode().equals("2")) {
                            cbcod.setVisibility(View.GONE);
                            cbonline.setVisibility(View.VISIBLE);
                            cbonline.setChecked(true);
                            paymentType = "online";
                        }
//                        Utility.setCartCount(userdata.getCartCount());
                        tv_discount.setText("Rs 00.00");
                        tv_yousaved.setText("Rs "+userdata.getResponse().getSaving() + "");
                        tv_shipping.setText(userdata.getResponse().getShipping() + "");
                        tv_tax.setText("Rs " + userdata.getResponse().getTax());

                        totalamount = userdata.getResponse().getTotalamount();
                        deliverychrge = Integer.parseInt(userdata.getResponse().getShipping());
                        int total = Integer.parseInt(totalamount)+deliverychrge;
                        tv_subtotal.setText("Rs " + totalamount);
                        shipping = deliverychrge+"";
                        tv_price_checkout.setText(total+ "");
                        array_cart_list = new ArrayList<>();
                        Gson gson = new Gson();
                        cart_list_json = gson.toJson(userdata.getResponse().getCart());
                        array_cart_list.addAll(userdata.getResponse().getCart());
                        cartAdapter = new CartAdapter();
                        recyclerView_cart.setAdapter(cartAdapter);
                        cartAdapter.notifyDataSetChanged();
                        //                        card_amount_calculation.postDelayed(new Runnable() {
                        //                            @Override
                        //                            public void run() {
                        //                                // make checks to see if activity is still available
                        //                                card_amount_calculation.setVisibility(View.VISIBLE);
                        //
                        //                            }
                        //                        }, 1000 * 1);    // 1 second
                        no_data_found.setVisibility(View.GONE);
                        cart_rel.setVisibility(View.VISIBLE);
                        item_total_in_bucket.setVisibility(View.VISIBLE);
                        tv_total_items.setText(array_cart_list.size() + "");
                        deliveryslotList = new ArrayList<>();
                        deliveryslotList = userdata.getResponse().getDeliveryslot();
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getAddressList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        getAddress(map);

                    } else {
                        //  tvid.setText(userdata.getMsg());
                        // tvid.setVisibility(View.VISIBLE);
                        //not_found_text.setText(userdata.getMsg());
//                        Utility.setCartCount(userdata.getCartCount());
                        no_data_found.setVisibility(View.VISIBLE);
                        cart_rel.setVisibility(View.GONE);
                        item_total_in_bucket.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Cart_List_Basket_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void GetCartList1(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Cart_List_Basket_Model> call = Apis.getAPIService().getCartList(map);
        call.enqueue(new Callback<Cart_List_Basket_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Cart_List_Basket_Model> call, @NonNull Response<Cart_List_Basket_Model> response) {
                dialog.dismiss();
                Cart_List_Basket_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
//                        Utility.setCartCount(userdata.getCartCount());
                        if (userdata.getResponse().getPaymentmethode().equals("1")){
                            cbcod.setVisibility(View.VISIBLE);
                            cbonline.setVisibility(View.GONE);
                            cbcod.setChecked(true);
                            paymentType = "cash";
                        }else if(userdata.getResponse().getPaymentmethode().equals("2")) {
                            cbcod.setVisibility(View.GONE);
                            cbonline.setVisibility(View.VISIBLE);
                            cbonline.setChecked(true);
                            paymentType = "online";
                        }
                        tv_discount.setText("Rs 00.00");
                        tv_shipping.setText(userdata.getResponse().getShipping() + "");
                        tv_yousaved.setText("Rs "+userdata.getResponse().getSaving() + "");
                        tv_tax.setText("Rs " + userdata.getResponse().getTax());
//                        Utility.setCartprice(userdata.getResponse().getTotalamount()+"");
                        totalamount = userdata.getResponse().getTotalamount();
                        deliverychrge = Integer.parseInt(userdata.getResponse().getShipping());
                        int total = Integer.parseInt(totalamount)+deliverychrge;
                        tv_price_checkout.setText("" + total);
                        shipping = deliverychrge+"";
                        tv_subtotal.setText("Rs " + userdata.getResponse().getTotalamount());
//                        tv_price_checkout.setText(userdata.getResponse().getTotalamount() + "");
                        array_cart_list = new ArrayList<>();
                        Gson gson = new Gson();
                        cart_list_json = gson.toJson(userdata.getResponse().getCart());
                        array_cart_list.addAll(userdata.getResponse().getCart());
                        cartAdapter = new CartAdapter();
                        recyclerView_cart.setAdapter(cartAdapter);
                        cartAdapter.notifyDataSetChanged();
                        //                        card_amount_calculation.postDelayed(new Runnable() {
                        //                            @Override
                        //                            public void run() {
                        //                                // make checks to see if activity is still available
                        //                                card_amount_calculation.setVisibility(View.VISIBLE);
                        //
                        //                            }
                        //                        }, 1000 * 1);    // 1 second
                        no_data_found.setVisibility(View.GONE);
                        cart_rel.setVisibility(View.VISIBLE);
                        item_total_in_bucket.setVisibility(View.VISIBLE);
                        tv_total_items.setText(array_cart_list.size() + "");
                        deliveryslotList = new ArrayList<>();
                        deliveryslotList = userdata.getResponse().getDeliveryslot();

                    } else {
//                        Utility.setCartCount(userdata.getCartCount());
                        //  tvid.setText(userdata.getMsg());
                        // tvid.setVisibility(View.VISIBLE);
                        //not_found_text.setText(userdata.getMsg());
                        no_data_found.setVisibility(View.VISIBLE);
                        cart_rel.setVisibility(View.GONE);
                        item_total_in_bucket.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Cart_List_Basket_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Add_Cart(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
        call.enqueue(new Callback<Add_Cart_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
                dialog.dismiss();
                Add_Cart_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        tvcoupontext.setText("Apply Coupon(Optional)");
                        tvcoupontext.setTextColor(getResources().getColor(R.color.black));
                        coupon_id = "";
                        discount ="";
                        tagshow = "0";
                        tv_discount.setText("0");
                        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "CartList");
                            map.put("deviceId", android_id);
//            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            GetCartList1(map);
                        } else {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "CartList");
                            map.put("deviceId", android_id);
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            GetCartList1(map);
                        }
                        if (userdata.getCartType().equals("1")) {
                            Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        } else {
                            Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                        }
                    } else {

                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    String tagshow = "0";
    String finaltoal = "";
    public void aplycoupn(){
        ViewGroup viewGroup = OuterCart.this.findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(OuterCart.this).inflate(R.layout.coupon_dialog, viewGroup, false);
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        Button buttonremove = dialogView.findViewById(R.id.buttonremove);
        LinearLayout llapycoupon = dialogView.findViewById(R.id.llapycoupon);
        LinearLayout llremovecoup = dialogView.findViewById(R.id.llremovecoup);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(OuterCart.this);
        if (tagshow.equals("1")){
            llremovecoup.setVisibility(View.VISIBLE);
            llapycoupon.setVisibility(View.GONE);
        }else {
            llremovecoup.setVisibility(View.GONE);
            llapycoupon.setVisibility(View.VISIBLE);
        }
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();

        final AppCompatEditText et_coupon = dialogView.findViewById(R.id.et_coupon);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finaltoal =totalamount;
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                Map<String, String> map = new HashMap<>();
                map.put("method", "apply_coupon");
                map.put("coupon_code", et_coupon.getText().toString());
                map.put("device_id", android_id);
                map.put("totalamount", totalamount+"");
                Apply_Coupon(map);
                alertDialog.dismiss();
                }else {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "apply_coupon");
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("coupon_code", et_coupon.getText().toString());
                    map.put("device_id", android_id);
                    map.put("totalamount", totalamount+"");
                    Apply_Coupon(map);
                    alertDialog.dismiss();
                }
            }
        });
        buttonremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "remove_coupon");
//                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("device_id", android_id);
                    remove_coupon(map);
                    alertDialog.dismiss();
                }else {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "remove_coupon");
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("device_id", android_id);
                    remove_coupon(map);
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }
    private void Apply_Coupon(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Coupon_Apply_Model> call = Apis.getAPIService().apply_coupon(map);
        call.enqueue(new Callback<Coupon_Apply_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Coupon_Apply_Model> call, @NonNull Response<Coupon_Apply_Model> response) {
                dialog.dismiss();
                Coupon_Apply_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        // tv_subtotal.setText(userdata.getResponse().getNetpay());
//                        coupon_id = userdata.getResponse().getCouponId();
                        //   tv_shipping.setText(userdata.getResponse().getNetpay());
                        tagshow = "1";
                        tv_discount.setText(userdata.getResponse().getDiscount());
//                        tv_price_checkout.setText(userdata.getResponse().getNetpay() + "");
                        totalamount = userdata.getResponse().getNetpay() + "";
//                        tv_discount.setText(userdata.getResponse().getDiscount());

                        if (cbdelivry.isChecked()){
                            int a = Integer.parseInt(totalamount )+deliverychrge;
                            tv_price_checkout.setText(""+ a);
                        }else{
                            tv_price_checkout.setText(userdata.getResponse().getNetpay() + "");
                        }
                        tvcoupontext.setText("Coupon Applied");
                        tvcoupontext.setTextColor(getResources().getColor(R.color.colorGreen));
                        coupon_id = userdata.getResponse().getCouponId();
                        discount =userdata.getResponse().getDiscount();
                       /* int total = Integer.parseInt(tv_total_amount.getText().toString()) - Integer.parseInt(tv_discount.getText().toString());
                        int total1 = total + Integer.parseInt(tv_shipping.getText().toString());
                        tv_subtotal.setText(String.valueOf(total1));*/
                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Coupon_Apply_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    private void remove_coupon(Map<String, String> map) {
            final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Coupon_Apply_Model> call = Apis.getAPIService().apply_coupon(map);
        call.enqueue(new Callback<Coupon_Apply_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Coupon_Apply_Model> call, @NonNull Response<Coupon_Apply_Model> response) {
                dialog.dismiss();
                Coupon_Apply_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        // tv_subtotal.setText(userdata.getResponse().getNetpay());
//                        coupon    _id = userdata.getResponse().getCouponId();
                        //   tv_shipping.setText(userdata.getResponse().getNetpay());
                        tagshow = "0";
                        totalamount = finaltoal + "";
                        tv_discount.setText("0");

                        if (cbdelivry.isChecked()){
                            int a = Integer.parseInt(totalamount )+deliverychrge;
                            tv_price_checkout.setText(""+ a);
                        }else{
                            tv_price_checkout.setText(totalamount+ "");
                        }
                        tvcoupontext.setText("Apply Coupon(Optional)");
                        tvcoupontext.setTextColor(getResources().getColor(R.color.black));
                        coupon_id = "";
                        discount ="";



                       /* int total = Integer.parseInt(tv_total_amount.getText().toString()) - Integer.parseInt(tv_discount.getText().toString());
                        int total1 = total + Integer.parseInt(tv_shipping.getText().toString());
                        tv_subtotal.setText(String.valueOf(total1));*/
                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Coupon_Apply_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void getAddress() {
        Address locationAddress = getAddress(Double.parseDouble(Utility.getlat()), Double.parseDouble(Utility.getlng()));
        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

               /* if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;*/
                tv_currentlocation.setText(city);
                tvaddress.setText(currentLocation);
//                userlocation.setVisibility(View.VISIBLE);
            }
        }
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(OuterCart.this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }


    private void getAddress(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<AddressListModel> call = Apis.getAPIService().getAddressList(map);
        call.enqueue(new Callback<AddressListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<AddressListModel> call, @NonNull Response<AddressListModel> response) {
                dialog.dismiss();
                AddressListModel userdata = response.body();
                if (userdata != null) {

                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {


                        addresslist.clear();

                        addresslist.addAll(userdata.getResponse());
                        if (addresslist.size() > 0) {
                            no_data_found.setVisibility(View.GONE);
                            recyclerView_address.setVisibility(View.VISIBLE);
                            AddressAdapter notificationAdapter = new AddressAdapter();
                            recyclerView_address.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else {
                            recyclerView_address.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);
                        }
                        /*array_notification_list = new ArrayList<>();
                        array_notification_list.addAll(userdata.getResponse());
                        notificationAdapter = new NotificationAdapter();
                        recyclerViewNotification.setAdapter(notificationAdapter);
                        notificationAdapter.notifyDataSetChanged();*/
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        //no_found.setText(userdata.getMsg());
                        //   Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddressListModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void getOrderConfirm(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OrderConfirmModel> call = Apis.getAPIService().getOrderConfirm(map);
        call.enqueue(new Callback<OrderConfirmModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<OrderConfirmModel> call, @NonNull Response<OrderConfirmModel> response) {
                dialog.dismiss();
                OrderConfirmModel userdata = response.body();
                if (userdata != null) {

                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {
                        if (userdata.getUrl().isEmpty()){
                            startActivity(new Intent(OuterCart.this, PaymenttSuccess.class).putExtra("tag", "1"));
                        }else {
                            startActivity(new Intent(OuterCart.this, PaymentWebview.class).putExtra("url", "http://martoline.in/" + userdata.getUrl()));
                        }
//                        startActivity(new Intent(OuterCart.this, PaymentWebview.class).putExtra("url", "http://martoline.in/marto/" + userdata.getUrl()));
                    } else {
//                        no_data_found.setVisibility(View.VISIBLE);
                        //no_found.setText(userdata.getMsg());
                           Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderConfirmModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }


    private void ActiviteAddress(Map<String, String> map) {
        final Dialog dialog = new Dialog(OuterCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ActiviteAddressModel> call = Apis.getAPIService().ActiviteAddress(map);
        call.enqueue(new Callback<ActiviteAddressModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<ActiviteAddressModel> call, @NonNull Response<ActiviteAddressModel> response) {
                dialog.dismiss();
                ActiviteAddressModel userdata = response.body();
                if (userdata != null) {

                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {
//                        Custom_Toast_Activity.makeText(SavedAddress.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getAddressList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));

                        getAddress(map);
                    } else {
                        Custom_Toast_Activity.makeText(OuterCart.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActiviteAddressModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class checklist {
        boolean check = false;

        public boolean isCheck() {
            return check;
        }

        public void setCheck(boolean check) {
            this.check = check;
        }
    }


    String SlotId = "0";

    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
        Context context;
        Dialog dialog;
        LayoutInflater inflater;

        MyOrdersAdapter(Context context, Dialog dialog) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.dialog = dialog;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_categoery_, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tvtitle.setText(deliveryslotList.get(position).getName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    timeslot.setText(deliveryslotList.get(position).getName());
                    SlotId = deliveryslotList.get(position).getId();
                    dialog.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return deliveryslotList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_cat_images;
            private TextView tvtitle;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);


            }
        }
    }

    public void showmessage(String message) {
        View parentLayout = OuterCart.this.findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(OuterCart.this, LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        map.put("method", "getAddressList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getAddress(map);
    }
}