package com.mart.martonlinevendor.Bulkupload.Payment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.mart.martonlinevendor.R;


public class PaymentWebview extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_webview);
        initView();
    }

    private void initView() {
//        String user_id = Utility.getLoginId(PaymentWebview.this);
        String packageid = getIntent().getStringExtra("packageId");
        String couponId = getIntent().getStringExtra("couponId");
        String netAmount = getIntent().getStringExtra("netAmount");
        String android_id = Settings.Secure.getString(PaymentWebview.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        webView = findViewById(R.id.webview);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(getIntent().getStringExtra("url"));
        Log.d("vishnal", getIntent().getStringExtra("url") + "___");
//      Log.d("vishnal", "http://app.lalsonline.com/WebserviceV1/placeOrder/" + user_id + "/" + android_id + "/" + packageid + "/" + couponId + "/" + netAmount);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView wView, String url) {
                Log.d("vishnal", url);
                if (url.equals("http://martoline.in/payumoney/backtoapp.php?orderstatu=1")) //check if that's a url you want to load internally
                {
                    startActivity(new Intent(PaymentWebview.this, PaymenttSuccess.class).putExtra("tag", "1"));
                } else if (url.equals("http://martoline.in/payumoney/backtoapp.php?orderstatu=0")) {
                    startActivity(new Intent(PaymentWebview.this, PaymenttSuccess.class).putExtra("tag", "2"));
                }
                return false;
            }
        });
    }


}
