package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Add_Cart_Model;
import com.mart.martonlinevendor.Model.Product_Detail_Model;
import com.mart.martonlinevendor.Model.Product_Detail_Model_bulk;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetails extends AppCompatActivity implements View.OnClickListener {
    public int counter;
    List<Drawable> list_image;
    String android_id;
    List<Product_Detail_Model.Size_chart> chart_list;
    UserSessionManager session;
    String product_id;
    ProductImagesAdapter productImagesAdapter;
    HashMap<String, String> user;
    //    List<Product_Detail_Model.Size_chart> chart_list;
    ArrayList<String> Quantity;
    String Product_id, quantity_count;
    String Size_id = "0";
    AlertDialog.Builder builder;
    ImageView ivfavorite;
    ImageView ivaddfavorite;
    private ImageView iv_product_full_image;
    private RecyclerView recycler_product_images;
    private TextView tv_price;
    private TextView tv_old_price;
    private TextView tv_off;
    private LinearLayout tv_remove_item;
    private TextView tv_item_count;
    private TextView tv_add;
    private LinearLayout tv_add_item;
    private TextView tv_product_description;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    private RelativeLayout rl_header;
    private TextView product_title;
    private TextView selling_product_prize;
    private Spinner quantity;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView ivout_of_stock;
    private RelativeLayout header;
    private LinearLayout sdf;
    private ImageView ivcart;
    private View ivview;
    private TextView tv_total_items;
    private TextView tvtoalprice;
    private TextView tvgotocart;
    private TextView tvdelivery;
    private TextView tvvendorname;
    private TextView tvcartcount;
    private CardView card_cart;
    private int minqount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_);
        session = new UserSessionManager(ProductDetails.this);
        user = session.getUserDetails();
        product_id = getIntent().getStringExtra("product_id");
        quantity_count = "0";
        builder = new AlertDialog.Builder(this);
        initView();
    }

    public void initView() {
        android_id = Settings.Secure.getString(ProductDetails.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        iv_product_full_image = findViewById(R.id.iv_product_full_image);
        ivaddfavorite = findViewById(R.id.ivaddfavorite);
        ivfavorite = findViewById(R.id.ivfavorite);
        tvdelivery = findViewById(R.id.tvdelivery);
        quantity = findViewById(R.id.quantity);
        quantity.setEnabled(false);
        recycler_product_images = findViewById(R.id.recycler_product_images);
        tv_price = findViewById(R.id.tv_price);
        tv_old_price = findViewById(R.id.tv_old_price);
        tvvendorname = findViewById(R.id.tvvendorname);
        tvcartcount = findViewById(R.id.tvcartcount);
        tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tv_off = findViewById(R.id.tv_off);
        tv_remove_item = findViewById(R.id.tv_remove_item);
        tv_item_count = findViewById(R.id.tv_item_count);
        tv_add = findViewById(R.id.tv_add);
        tv_add_item = findViewById(R.id.tv_add_item);
        tv_product_description = findViewById(R.id.tv_product_description);
        iv_menu = findViewById(R.id.iv_menu);
        ivout_of_stock = findViewById(R.id.ivout_of_stock);
        iv_menu.setOnClickListener(this);
        iv_send = findViewById(R.id.iv_send);
        iv_send.setOnClickListener(this);
        et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetails.this, SearchProduct.class));
            }
        });
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetails.this, OuterCart.class));
            }
        });
        iv_voice = findViewById(R.id.iv_voice);
        iv_voice.setOnClickListener(this);
        header = findViewById(R.id.header);
        header.setOnClickListener(this);
        sdf = findViewById(R.id.sdf);
        sdf.setOnClickListener(this);
        tv_add_item.setOnClickListener(this);
        tv_remove_item.setOnClickListener(this);
        ivfavorite.setOnClickListener(this);
        ivaddfavorite.setOnClickListener(this);

/*
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);

        tv_currentlocation.setOnClickListener(this);
        iv_bellnotification.setOnClickListener(this);*/

        iv_product_full_image.setOnClickListener(this);
        /*rl_header = (RelativeLayout) findViewById(R.id.rl_header);
        rl_header.setOnClickListener(this);*/
        product_title = findViewById(R.id.product_title);
        product_title.setOnClickListener(this);
        selling_product_prize = findViewById(R.id.selling_product_prize);
        selling_product_prize.setOnClickListener(this);

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_add.setVisibility(View.GONE);
                tv_remove_item.setVisibility(View.VISIBLE);
                tv_add_item.setVisibility(View.VISIBLE);
                tv_item_count.setVisibility(View.VISIBLE);
//                tv_item_count.setText(quantity_count + "");
                counter = Integer.parseInt(tv_item_count.getText().toString());
                counter++;
                Map<String, String> map = new HashMap<>();
                map.put("method", "AddToCart");
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                map.put("Pid", Product_id);
                map.put("deviceId", android_id);
                map.put("Qty", counter + "");
                map.put("size", Size_id);
                //user.get(UserSessionManager.KEY_ID)
                Add_Cart(map, counter, "1");
            }
        });


        quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              /*  selling_product_prize.setText("Rs " + chart_list.get(i).getSelling_price());
                tv_old_price.setText(chart_list.get(i).getActual_Price());
                Size_id = chart_list.get(i).getSize_id();
                Double Actual_Ammount = Double.parseDouble(chart_list.get(i).getActual_Price());
                Double Selling_Ammount = Double.parseDouble(chart_list.get(i).getSelling_price());
                Double remaining = Actual_Ammount - Selling_Ammount;
                Double totaldis = (remaining / Actual_Ammount) * 100;
                String value = new DecimalFormat("##").format(totaldis);
                tv_off.setText(value + "% off");
                tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

//        productImagesAdapter = new ProductImagesAdapter(ProductDetails.this);
//        recycler_product_images.setAdapter(productImagesAdapter);
        tv_item_count.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        tv_item_count.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });


        Map<String, String> map1 = new HashMap<>();
        map1.put("method", "Product_Details");
        map1.put("ProductId", product_id);
        map1.put("deviceId", android_id + "");
        map1.put("userId", user.get(UserSessionManager.KEY_ID));
        getProductDetail(map1);


        ivcart = findViewById(R.id.ivcart);
        ivview = findViewById(R.id.ivview);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvtoalprice = findViewById(R.id.tvtoalprice);
        tvgotocart = findViewById(R.id.tvgotocart);
        card_cart = findViewById(R.id.card_cart);
        tvgotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        card_cart = findViewById(R.id.card_cart);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_add_item:
                tv_add.setVisibility(View.GONE);
                tv_remove_item.setVisibility(View.VISIBLE);
                tv_item_count.setVisibility(View.VISIBLE);
                counter = Integer.parseInt(tv_item_count.getText().toString());
                counter++;
                Map<String, String> map = new HashMap<>();
                map.put("method", "AddToCart");
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                map.put("Pid", Product_id);
                map.put("deviceId", android_id);
                map.put("Qty", counter + "");
                map.put("size", Size_id);
                Add_Cart(map, counter, "1");
                break;
            case R.id.tv_remove_item:
                counter = Integer.parseInt(tv_item_count.getText().toString());
                Log.d("asdfasdf", counter + "_____" + minqount);
                if (counter > 0) {
                    if (minqount < counter) {
                        counter--;
                        Map<String, String> map4 = new HashMap<>();
                        map4.put("method", "AddToCart");
                        map4.put("userId", user.get(UserSessionManager.KEY_ID));
                        map4.put("Pid", Product_id);
                        map4.put("deviceId", android_id);
                        map4.put("Qty", counter + "");
                        map4.put("size", Size_id);
                        Add_Cart(map4, counter, "2");
                        tv_item_count.setText("" + counter);
                    }else if (minqount == counter) {
                        counter--;
                        Map<String, String> map4 = new HashMap<>();
                        map4.put("method", "AddToCart");
                        map4.put("userId", user.get(UserSessionManager.KEY_ID));
                        map4.put("Pid", Product_id);
                        map4.put("deviceId", android_id);
                        map4.put("Qty", 0 + "");
                        map4.put("size", Size_id);
                        Add_Cart(map4, counter, "2");
                        tv_add.setVisibility(View.VISIBLE);
                        tv_remove_item.setVisibility(View.GONE);
                        tv_item_count.setVisibility(View.GONE);
                        tv_add_item.setVisibility(View.GONE);
                    }  else {
                        tv_add.setVisibility(View.VISIBLE);
                        tv_remove_item.setVisibility(View.GONE);
                        tv_item_count.setVisibility(View.GONE);
                        tv_add_item.setVisibility(View.GONE);
                        Toast.makeText(ProductDetails.this, "You haven't select any of the item", Toast.LENGTH_LONG).show();
                    }
                } else {
                    tv_add.setVisibility(View.VISIBLE);
                    tv_remove_item.setVisibility(View.GONE);
                    tv_item_count.setVisibility(View.GONE);
                    tv_add_item.setVisibility(View.GONE);
                    Toast.makeText(ProductDetails.this, "You haven't select any of the item", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.iv_menu:
                finish();
                break;
            case R.id.ivfavorite:

                Map<String, String> map3 = new HashMap<>();
                map3.put("method", "RemoveWishList");
                map3.put("userId", user.get(UserSessionManager.KEY_ID));
                map3.put("ProductId", Product_id);
                map3.put("deviceId", android_id + "");
                //user.get(UserSessionManager.KEY_ID)
                remove_Wishlist(map3);

                break;
            case R.id.ivaddfavorite:
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    Map<String, String> map1 = new HashMap<>();
                    map1.put("method", "AddToWish");
                    map1.put("userId", user.get(UserSessionManager.KEY_ID));
                    map1.put("Pid", Product_id);
                    map1.put("deviceId", android_id + "");
                    //user.get(UserSessionManager.KEY_ID)
                    Add_Wishlist(map1);
                } else {
                    Map<String, String> map2 = new HashMap<>();
                    map2.put("method", "AddToWish");
                    map2.put("Pid", Product_id);
                    map2.put("deviceId", android_id + "");
                    //user.get(UserSessionManager.KEY_ID)
                    Add_Wishlist(map2);
                }
                break;

            case R.id.tv_currentlocation:
                Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;

            case R.id.iv_bellnotification:
                startActivity(new Intent(ProductDetails.this, Notification.class));
                break;

            case R.id.iv_product_full_image:
                startActivity(new Intent(ProductDetails.this, ProductImageView.class).putExtra("product_id", product_id));
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    private void getProductDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_Detail_Model_bulk> call = Apis.getAPIService().getProductDetails_bulk(map);
        call.enqueue(new Callback<Product_Detail_Model_bulk>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_Detail_Model_bulk> call, @NonNull Response<Product_Detail_Model_bulk> response) {
                dialog.dismiss();
                Product_Detail_Model_bulk userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        product_title.setText(userdata.getResponse().get(0).getProduct_Name());
                        Product_id = userdata.getResponse().get(0).getProduct_id();
                        tv_product_description.setText(userdata.getResponse().get(0).getDiscription());
                        tvvendorname.setText(userdata.getResponse().get(0).getShopname());
                        minqount = Integer.parseInt(userdata.getResponse().get(0).getMinqty());
                        //  Quantity=new ArrayList<>();
                        chart_list = new ArrayList<>();
                        Size_id = userdata.getResponse().get(0).getSize();
                        selling_product_prize.setText("Rs " + userdata.getResponse().get(0).getSale_price());
                        tv_old_price.setText("Rs " + userdata.getResponse().get(0).getMRP());
                        if (userdata.getResponse().get(0).getQty().equals("0")) {
                            ivout_of_stock.setVisibility(View.VISIBLE);
                        } else {
                            ivout_of_stock.setVisibility(View.GONE);
                        }
                        if (userdata.getResponse().get(0).getDelivery().isEmpty()) {
                            if (!userdata.getResponse().get(0).getPickup().isEmpty()) {
                                tvdelivery.setText("Delivery type: Pickup");
                            }
                        } else {
                            if (userdata.getResponse().get(0).getPickup().isEmpty()) {
                                tvdelivery.setText("Delivery type: Delivery");
                            } else {
                                tvdelivery.setText("Delivery type: Delivery/Pickup");
                            }
                        }
                        Double Actual_Ammount = Double.parseDouble(userdata.getResponse().get(0).getMRP());
                        Double Selling_Ammount = Double.parseDouble(userdata.getResponse().get(0).getSale_price());
                        Double remaining = Actual_Ammount - Selling_Ammount;
                        Double totaldis = (remaining / Actual_Ammount) * 100;
                        String value = new DecimalFormat("##").format(totaldis);
                        tv_off.setText(value + "% off");
                        tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        /*if (chart_list.size() > 0) {
                            chart_list.addAll(userdata.getResponse().get(0).getSize_chart());
                            Size_id = chart_list.get(0).getSize_id();

                        }*/
                        if (userdata.getResponse().get(0).getWish().equals("0")) {
                            ivaddfavorite.setVisibility(View.VISIBLE);
                            ivfavorite.setVisibility(View.GONE);
                        } else {
                            ivaddfavorite.setVisibility(View.GONE);
                            ivfavorite.setVisibility(View.VISIBLE);
                        }
                        Quntity_Adapter adapter = new Quntity_Adapter(ProductDetails.this,
                                R.layout.spinner_list_text, R.id.title, chart_list);
                        quantity.setAdapter(adapter);
                        if (userdata.getResponse().get(0).getImages().size() != 0) {
                            final List<Product_Detail_Model_bulk.Images> array_image_list = new ArrayList<>();
                            array_image_list.addAll(userdata.getResponse().get(0).getImages());
                            productImagesAdapter = new ProductImagesAdapter(ProductDetails.this, array_image_list);
                            recycler_product_images.setAdapter(productImagesAdapter);
                        }

                        if (userdata.getResponse().get(0).getQuantity_count().equals("0")) {
                            tv_add.setVisibility(View.VISIBLE);
                            tv_remove_item.setVisibility(View.GONE);
                            tv_add_item.setVisibility(View.GONE);
                            tv_item_count.setVisibility(View.GONE);
                            if (!userdata.getResponse().get(0).getMinqty().equals("0")) {
                                int i = Integer.parseInt(userdata.getResponse().get(0).getMinqty()) - 1;
                                tv_item_count.setText(i + "");
                            } else {
                                tv_item_count.setText("0");
                            }

                        } else {
                            tv_add.setVisibility(View.GONE);
                            tv_remove_item.setVisibility(View.VISIBLE);
                            tv_add_item.setVisibility(View.VISIBLE);
                            tv_item_count.setVisibility(View.VISIBLE);
                            tv_item_count.setText(userdata.getResponse().get(0).getQuantity_count() + "");
                        }
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        //  no_found.setVisibility(View.VISIBLE);
                        // no_found.setText(userdata.getMsg());
                        //   Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_Detail_Model_bulk> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("quantity_count", tv_item_count.getText().toString());
        intent.putExtra("product_id", getIntent().getStringExtra("product_id"));
        setResult(RESULT_OK, intent);
        finish();
    }

    private void Add_Wishlist(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
        call.enqueue(new Callback<Add_Cart_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
                dialog.dismiss();
                Add_Cart_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        ivaddfavorite.setVisibility(View.GONE);
                        ivfavorite.setVisibility(View.VISIBLE);

                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);

//                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);

                    } else {

                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void remove_Wishlist(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
        call.enqueue(new Callback<Add_Cart_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
                dialog.dismiss();
                Add_Cart_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        ivaddfavorite.setVisibility(View.VISIBLE);
                        ivfavorite.setVisibility(View.GONE);

                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);

//                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);

                    } else {

                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Add_Cart(final Map<String, String> map, final int counter, final String type) {
        final Dialog dialog = new Dialog(ProductDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
        call.enqueue(new Callback<Add_Cart_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
                dialog.dismiss();
                Add_Cart_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        tvcartcount.setText(userdata.getCartCount());
                       /* Utility.setCartCount(userdata.getCartCount());
                        Utility.setCartprice(userdata.getTotalcartamount());*/
                        tv_item_count.setText("" + counter);
                        if (tv_item_count.getText().toString().equals("0")) {
                            tv_add.setVisibility(View.VISIBLE);
                            tv_remove_item.setVisibility(View.GONE);
                            tv_item_count.setVisibility(View.GONE);
                        }
                        tvcartcount.setText(userdata.getCartCount());
                        /*if (Utility.getCartcount().equals("0")){
                            card_cart.setVisibility(View.GONE);
                        }else{
                            Utility.setCartCount(userdata.getCartCount());
                            Utility.setCartprice(userdata.getTotalcartamount());
                            card_cart.setVisibility(View.VISIBLE);
                            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
                            tv_total_items.setText(Utility.getCartcount()+" items");
                            Log.d("asdfdasedf",Utility.getCartcount()+" items");
                        }*/
                        if (userdata.getCartType().equals("1")) {
                            Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        } else {
                            Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                        }

//                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);

                    } else if (userdata.getStatus().equals("2")) {
                        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ProductDetails.this, SweetAlertDialog.WARNING_TYPE);
                        sweetAlertDialog.setTitleText("MartOline");
                        sweetAlertDialog.setContentText(userdata.getMsg());
                        sweetAlertDialog.setConfirmText("Yes");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCancelText("NO");

                        sweetAlertDialog.showCancelButton(true);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                map.put("forecedelete", "1");
                                Add_Cart(map, counter, type);
                            }
                        });
                        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        });


                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }
                    } else {

                        Custom_Toast_Activity.makeText(ProductDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }


//    private void getProductDetail(Map<String, String> map) {
//        final Dialog dialog = new Dialog(ProductDetails.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Product_Detail_Model> call = Apis.getAPIService().getProductDetails(map);
//        call.enqueue(new Callback<Product_Detail_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull Call<Product_Detail_Model> call, @NonNull Response<Product_Detail_Model> response) {
//                dialog.dismiss();
//                Product_Detail_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        product_title.setText(userdata.getResponse().get(0).getProduct_Name());
//                        Product_id = userdata.getResponse().get(0).getProduct_id();
//                        tv_product_description.setText(userdata.getResponse().get(0).getDiscription());
//                        //  Quantity=new ArrayList<>();
//                        chart_list = new ArrayList<>();
//                        chart_list.addAll(userdata.getResponse().get(0).getSize_chart());
////                        for (int i = 0; i < chart_list.size(); i++){
////                            Quantity.add(chart_list.get(i).getQuantity());
////                        }
//                        Quntity_Adapter adapter = new Quntity_Adapter(ProductDetails.this,
//                                R.layout.spinner_list_text, R.id.title, chart_list);
//                        quantity.setAdapter(adapter);
//                        if (userdata.getResponse().get(0).getImages().size() != 0) {
//                            final List<Product_Detail_Model.Images> array_image_list = new ArrayList<>();
//                            array_image_list.addAll(userdata.getResponse().get(0).getImages());
//                            productImagesAdapter = new ProductImagesAdapter(ProductDetails.this, array_image_list);
//                            recycler_product_images.setAdapter(productImagesAdapter);
//                        }
//                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
//                    } else {
//                        //  no_found.setVisibility(View.VISIBLE);
//                        // no_found.setText(userdata.getMsg());
//                        //   Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);
//
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<Product_Detail_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(ProductDetails.this, LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (Utility.getCartcount().equals("0")) {
            card_cart.setVisibility(View.GONE);
        } else {
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. " + Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount() + " items");
            Log.d("asdfdasedf", Utility.getCartcount() + " items");
        }*/

        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            Map<String, String> map1 = new HashMap<>();
            map1.put("method", "Product_Details");
            map1.put("ProductId", product_id);
            map1.put("deviceId", android_id + "");
//            map.put("userId", user.get(UserSessionManager.KEY_ID));
            getProductDetail(map1);
        } else {
            Map<String, String> map1 = new HashMap<>();
            map1.put("method", "Product_Details");
            map1.put("ProductId", product_id);
            map1.put("deviceId", android_id + "");
            map1.put("userId", user.get(UserSessionManager.KEY_ID));
            getProductDetail(map1);
        }
    }

    interface UpdateCallback {
        void update(String s);
    }

    public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {
        List<Product_Detail_Model_bulk.Images> imagelist;
        Context context;
        LayoutInflater inflater;

        public ProductImagesAdapter(Context context, List<Product_Detail_Model_bulk.Images> imagelist) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.imagelist = imagelist;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_product_images, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Log.d("sdsdsdsd", Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image());
            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_product_images);

            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(0).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_full_image);

            holder.iv_product_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //image handling here
                    Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                            .error(R.drawable.no_image)
                            .priority(Picasso.Priority.HIGH)
                            .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_full_image);
                }
            });
        }

        @Override
        public int getItemCount() {
            return imagelist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final ImageView iv_product_images;

            public ViewHolder(View itemView) {

                super(itemView);
                this.iv_product_images = itemView.findViewById(R.id.iv_product_images);
            }
        }
    }

    public class Quntity_Adapter extends ArrayAdapter<Product_Detail_Model.Size_chart> {

        LayoutInflater flater;

        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<Product_Detail_Model.Size_chart> list) {

            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return rowview(convertView, position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(convertView, position);
        }

        private View rowview(View convertView, int position) {

            final Product_Detail_Model.Size_chart rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
                holder.spinner_text = rowview.findViewById(R.id.spinner_text);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            holder.spinner_text.setText("Qty. " + rowItem.getSize());

            return rowview;
        }

        private class viewHolder {
            TextView spinner_text;
        }
    }
}
