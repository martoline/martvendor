package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Image_Zoom_Custom.ZoomImageView;
import com.mart.martonlinevendor.Model.Product_Detail_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductImageView extends AppCompatActivity implements View.OnClickListener {

    List<Drawable> list_image;
    ProductImagesAdapter productImagesAdapter;
    List<Product_Detail_Model.Images> array_image_list;
    String Product_id;
    HashMap<String, String> user;
    UserSessionManager session;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    private ImageView iv_menu;
    private ImageView iv_close;
    private ZoomImageView iv_product_image;
    private RecyclerView recycler_product_images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_image_view);
        session = new UserSessionManager(ProductImageView.this);
        user = session.getUserDetails();
        Product_id = getIntent().getStringExtra("product_id");
        initView();
    }

    private void initView() {

        list_image = new ArrayList<>();
        list_image.add(getResources().getDrawable(R.drawable.grapesmages));
        list_image.add(getResources().getDrawable(R.drawable.grapesmages));
        list_image.add(getResources().getDrawable(R.drawable.grapesmages));
        list_image.add(getResources().getDrawable(R.drawable.grapesmages));
        list_image.add(getResources().getDrawable(R.drawable.grapesmages));

        recycler_product_images = findViewById(R.id.recycler_product_images);
        iv_close = findViewById(R.id.iv_close);
        iv_product_image = findViewById(R.id.iv_product_image);
        recycler_product_images = findViewById(R.id.recycler_product_images);

//        iv_back = findViewById(R.id.iv_back);
//        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_menu = findViewById(R.id.iv_menu);

//        iv_back.setOnClickListener(this);
//        tv_currentlocation.setOnClickListener(this);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                }else {*/
                startActivity(new Intent(ProductImageView.this, OuterCart.class));
//                startActivity(new Intent(ProductImageView.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                /* }*/
            }
        });
        iv_close.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        productImagesAdapter = new ProductImagesAdapter();
//        recycler_product_images.setAdapter(productImagesAdapter);

        Map<String, String> map = new HashMap<>();
        map.put("method", "Product_Details");
        map.put("ProductId", Product_id);
        //user.get(UserSessionManager.KEY_ID)
        getProductDetail(map);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {


            case R.id.iv_menu:
                onBackPressed();
                break;

            case R.id.tv_currentlocation:
                Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;


            case R.id.iv_close:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void getProductDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductImageView.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_Detail_Model> call = Apis.getAPIService().getProductDetails(map);
        call.enqueue(new Callback<Product_Detail_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_Detail_Model> call, @NonNull Response<Product_Detail_Model> response) {
                dialog.dismiss();
                Product_Detail_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        if (userdata.getResponse().get(0).getImages().size() != 0) {
                            array_image_list = new ArrayList<>();
                            array_image_list.addAll(userdata.getResponse().get(0).getImages());
                            productImagesAdapter = new ProductImagesAdapter();
                            recycler_product_images.setAdapter(productImagesAdapter);
                        }
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                    } else {
                        //  no_found.setVisibility(View.VISIBLE);
                        // no_found.setText(userdata.getMsg());
                        Custom_Toast_Activity.makeText(ProductImageView.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_Detail_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(ProductImageView.this, LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {


        public ProductImagesAdapter() {

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_product_images, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + array_image_list.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_product_images);

            Picasso.get().load(Apis.IMAGE_PATH + array_image_list.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_image);

            holder.iv_product_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //image handling here
                    Picasso.get().load(Apis.IMAGE_PATH + array_image_list.get(position).getProduct_Image())
                            .error(R.drawable.no_image)
                            .priority(Picasso.Priority.HIGH)
                            .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_image);
                }
            });
        }


        @Override
        public int getItemCount() {
            return array_image_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final ImageView iv_product_images;

            public ViewHolder(View itemView) {

                super(itemView);
                this.iv_product_images = itemView.findViewById(R.id.iv_product_images);
            }
        }
    }

}
