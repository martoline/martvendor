package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Add_Cart_Model;
import com.mart.martonlinevendor.Model.WishListModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductList extends AppCompatActivity {

    UserSessionManager session;
    HashMap<String, String> user;
    String Sizeid = "0";
    LinearLayout carderror;
    String android_id;
    List<WishListModel.Response> productListAdapterList = new ArrayList<>();
    int currentpage = 1;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    ProductListAdapter productListAdapter;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recycler_productlist;
    private LinearLayout dialogView;
    private ImageView ivcart;
    private View ivview;
    private TextView tv_total_items;
    private TextView tvtoalprice;
    private TextView tvgotocart;
    private TextView tvcartcount;
    private CardView card_cart;
    private boolean loading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        session = new UserSessionManager(ProductList.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        android_id = Settings.Secure.getString(ProductList.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        iv_menu = findViewById(R.id.iv_menu);
        tvcartcount = findViewById(R.id.tvcartcount);
        carderror = findViewById(R.id.carderror);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        recycler_productlist = findViewById(R.id.recycler_productlist);
        productListAdapter = new ProductListAdapter();
        final LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        recycler_productlist.setLayoutManager(mLayoutManager);
//        LinearLayoutManager manager = new LinearLayoutManager(Notification_act.this, LinearLayoutManager.VERTICAL, false);
//        recyclerViewNotification.setLayoutManager(manager);
        recycler_productlist.setAdapter(productListAdapter);
        recycler_productlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        Log.d("Sdsdfa", currentpage + "+_____" + visibleItemCount + "______" + pastVisiblesItems + "____" + totalItemCount);
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {

                            loading = false;
                            currentpage = currentpage + 1;
                            if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                                Map<String, String> map = new HashMap<>();
                                map.put("method", "ProductList");
                                map.put("deviceId", android_id);
                                map.put("shopId", getIntent().getStringExtra("shopId"));
                                map.put("category_Id", getIntent().getStringExtra("cat_id"));
                                map.put("page", currentpage + "");
                                getShopDetail1(map);
                            } else {
                                Map<String, String> map = new HashMap<>();
                                map.put("method", "ProductList");
                                map.put("userId", user.get(UserSessionManager.KEY_ID));
                                map.put("shopId", getIntent().getStringExtra("shopId"));
                                map.put("category_Id", getIntent().getStringExtra("cat_id"));
                                map.put("page", currentpage + "");
                                getShopDetail1(map);
                            }
                            // Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(ProductList.this, SearchProduct_Shop.class).putExtra("vendor_id", getIntent().getStringExtra("shopId")));
//
//                startActivity(new Intent(ProductList.this, SearchProduct.class));
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {*/
                startActivity(new Intent(ProductList.this, OuterCart.class));
                /* }*/
            }
        });
        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            Map<String, String> map = new HashMap<>();
            map.put("method", "ProductList");
            map.put("deviceId", android_id);
            map.put("shopId", getIntent().getStringExtra("shopId"));
            map.put("category_Id", getIntent().getStringExtra("cat_id"));
            map.put("page", "1");
            getShopDetail(map);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("method", "ProductList");
            map.put("userId", user.get(UserSessionManager.KEY_ID));
            map.put("shopId", getIntent().getStringExtra("shopId"));
            map.put("category_Id", getIntent().getStringExtra("cat_id"));
            map.put("page", "1");
            getShopDetail(map);
        }
        ivcart = findViewById(R.id.ivcart);
        ivview = findViewById(R.id.ivview);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvtoalprice = findViewById(R.id.tvtoalprice);
        tvgotocart = findViewById(R.id.tvgotocart);
        card_cart = findViewById(R.id.card_cart);
        tvgotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(ProductList.this,OuterCart.class));
            }
        });
        card_cart = findViewById(R.id.card_cart);

        /*if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/
    }

    private void getShopDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<WishListModel> call = Apis.getAPIService().getWishList(map);
        call.enqueue(new Callback<WishListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<WishListModel> call, @NonNull Response<WishListModel> response) {
                dialog.dismiss();
                WishListModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        loading = true;
                        productListAdapterList.clear();
                        productListAdapterList = userdata.getResponse();
                        if (productListAdapterList.size() > 0) {
                            productListAdapter.notifyDataSetChanged();
                            carderror.setVisibility(View.GONE);
                            recycler_productlist.setVisibility(View.VISIBLE);
                        } else {
                            carderror.setVisibility(View.VISIBLE);
                            recycler_productlist.setVisibility(View.GONE);
                        }
                    } else {
                        Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                } else {
                    Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WishListModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void getShopDetail1(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<WishListModel> call = Apis.getAPIService().getWishList(map);
        call.enqueue(new Callback<WishListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<WishListModel> call, @NonNull Response<WishListModel> response) {
                dialog.dismiss();
                WishListModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        loading = true;
                        productListAdapterList.addAll(userdata.getResponse());
                        if (productListAdapterList.size() > 0) {
                            productListAdapter.notifyDataSetChanged();
                        } else {
                        }
                    } else {
//                        Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                } else {
//                    Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WishListModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Finish", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

      /*  if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/

        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            Map<String, String> map = new HashMap<>();
            map.put("method", "ProductList");
            map.put("deviceId", android_id);
            map.put("shopId", getIntent().getStringExtra("shopId"));
            map.put("category_Id", getIntent().getStringExtra("cat_id"));
            map.put("page", "1");
            getShopDetail(map);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("method", "ProductList");
            map.put("userId", user.get(UserSessionManager.KEY_ID));
            map.put("shopId", getIntent().getStringExtra("shopId"));
            map.put("category_Id", getIntent().getStringExtra("cat_id"));
            map.put("page", "1");
            getShopDetail(map);
        }
    }

    private void Add_Cart(final Map<String, String> map, final int positon, final TextView textView, final int type) {
        final Dialog dialog = new Dialog(ProductList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
        call.enqueue(new Callback<Add_Cart_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
                dialog.dismiss();
                Add_Cart_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        tvcartcount.setText(userdata.getCartCount());
//                        Utility.setCartCount(userdata.getCartCount());
//                        Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        if (userdata.getCartCount().equals("0")) {
                            card_cart.setVisibility(View.GONE);
                            int counter = Integer.parseInt(productListAdapterList.get(positon).getQuantity_count());
                            productListAdapterList.get(positon).setQuantity_count((counter - 1) + "");
                            textView.setText("" + productListAdapterList.get(positon).getQuantity_count());
                            Log.d("asdasd", productListAdapterList.get(positon).getQuantity_count() + "");
                        } else {
                            int counter = Integer.parseInt(productListAdapterList.get(positon).getQuantity_count());
                            if (type == 1) {
                                productListAdapterList.get(positon).setQuantity_count((counter + 1) + "");
                                textView.setText("" + productListAdapterList.get(positon).getQuantity_count());
                                Log.d("asdasd", productListAdapterList.get(positon).getQuantity_count() + "");
                            } else {
                                productListAdapterList.get(positon).setQuantity_count((counter - 1) + "");
                                textView.setText("" + productListAdapterList.get(positon).getQuantity_count());
                                Log.d("asdasd", productListAdapterList.get(positon).getQuantity_count() + "");
                            }
//                            textView.setText("" + productListAdapterList.get(positon).getQuantity_count());
//                            productListAdapterList.get(positon).setQuantity_count(counter);
                           /* Utility.setCartCount(userdata.getCartCount());
                            Utility.setCartprice(userdata.getTotalcartamount());*/
                           /* card_cart.setVisibility(View.VISIBLE);
                            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
                            tv_total_items.setText(Utility.getCartcount()+" items");
                            Log.d("asdfdasedf",Utility.getCartcount()+" items");*/
                        }
                        if (userdata.getCartType().equals("1")) {
                            Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        } else {
                            Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                        }
                    } else if (userdata.getStatus().equals("2")) {
                        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ProductList.this, SweetAlertDialog.WARNING_TYPE);
                        sweetAlertDialog.setTitleText("MartOline");
                        sweetAlertDialog.setContentText(userdata.getMsg());
                        sweetAlertDialog.setConfirmText("Yes");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCancelText("NO");

                        sweetAlertDialog.showCancelButton(true);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                map.put("forecedelete", "1");
                                Add_Cart(map, positon, textView, 1);
                            }
                        });
                        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        });


                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }
                    } else {
                        Custom_Toast_Activity.makeText(ProductList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
        public int counter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_products_bulk, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + productListAdapterList.get(position).getMainimage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            Log.d("imageurl", Apis.IMAGE_PATH + productListAdapterList.get(position).getMainimage());
            final List<WishListModel.Size_chart> sizelist = productListAdapterList.get(position).getSize_chart();
            Quntity_Adapter adapter = new Quntity_Adapter(ProductList.this,
                    R.layout.spinner_list_text, R.id.title, sizelist);
            holder.quatity_spinner.setAdapter(adapter);
            holder.tv_oldPrice.setText("Rs. " + productListAdapterList.get(position).getMrp());
            holder.tv_minmaxqty.setText("Quantity(Min/Max): " + productListAdapterList.get(position).getMinqty() + "/" + productListAdapterList.get(position).getMaxqty());

            holder.tv_new_price.setText(productListAdapterList.get(position).getOfferprice());
            Double Actual_Ammount = Double.parseDouble(productListAdapterList.get(position).getMrp());
            Double Selling_Ammount = Double.parseDouble(productListAdapterList.get(position).getOfferprice());
            Double remaining = Actual_Ammount - Selling_Ammount;
            Double totaldis = (remaining / Actual_Ammount) * 100;
            String value = new DecimalFormat("##").format(totaldis);
            holder.tv_off.setText(value + "%");
         /*   if (productListAdapterList.get(position).getSize_chart().size() > 0) {
//                Log.d("hjgjhgj", productListAdapterList.get(position).getSize_chart().get(0).getActual_Price() + "" + productListAdapterList.get(position).getSize_chart().get(0).getSelling_price());
                Sizeid = sizelist.get(0).getSize_id();
            }*/
            if (productListAdapterList.get(position).getQty().equals("0")) {
//                Log.d("hjgjhgj", productListAdapterList.get(position).getSize_chart().get(0).getActual_Price() + "" + productListAdapterList.get(position).getSize_chart().get(0).getSelling_price());
                holder.out_stock.setVisibility(View.VISIBLE);
            } else {
                holder.out_stock.setVisibility(View.GONE);
            }

            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (productListAdapterList.get(position).getCountry().isEmpty()) {
                holder.tv_date.setText("Country: Not available");
            } else {
                holder.tv_date.setText("Country: " + productListAdapterList.get(position).getCountry());
            }

            holder.tv_qty.setText("Size. " + productListAdapterList.get(position).getComplete_size());
            holder.tv_name.setText(productListAdapterList.get(position).getProduct_Name());
            holder.tv_name.setSelected(true);
            holder.tv_qty.setSelected(true);
//            holder.tv_off.setText(productListAdapterList.get(position).getOffer());
            holder.product_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ProductList.this, ProductDetails.class).putExtra("product_id", productListAdapterList.get(position).getProduct_id()));
                }
            });
            holder.iv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ProductList.this, ProductDetails.class).putExtra("product_id", productListAdapterList.get(position).getProduct_id()));
                }
            });
            holder.quatity_spinner.setEnabled(false);

            holder.quatity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    // DO Nothing here
                }
            });
            if (productListAdapterList.get(position).getQuantity_count().equals("0")) {
                holder.tv_add.setVisibility(View.VISIBLE);
                holder.tv_remove_item.setVisibility(View.GONE);
                holder.tv_add_item.setVisibility(View.GONE);
                holder.tv_item_count.setVisibility(View.GONE);
            } else {
                holder.tv_add.setVisibility(View.GONE);
                holder.tv_remove_item.setVisibility(View.VISIBLE);
                holder.tv_add_item.setVisibility(View.VISIBLE);
                holder.tv_item_count.setVisibility(View.VISIBLE);
                holder.tv_item_count.setText(productListAdapterList.get(position).getQuantity_count() + "");
            }

            holder.tv_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (productListAdapterList.get(position).getMinqty() == null
                            && productListAdapterList.get(position).getMaxqty() == null) {
                        productListAdapterList.get(position).setMinqty("0");
                        productListAdapterList.get(position).setMaxqty("0");
                        showmessage("this product is Currently not available");

                    } else {
                        if (!productListAdapterList.get(position).getQty().equals("0")) {
                            counter = 1;
                            productListAdapterList.get(position).setQuantity_count((Integer.parseInt(productListAdapterList.get(position).getMinqty()) - 1) + "");
                            holder.tv_add.setVisibility(View.GONE);
                            holder.tv_remove_item.setVisibility(View.VISIBLE);
                            holder.tv_add_item.setVisibility(View.VISIBLE);
                            holder.tv_item_count.setVisibility(View.VISIBLE);
//                        holder.tv_item_count.setText(0 + "");
                            int counter = Integer.parseInt(productListAdapterList.get(position).getMinqty() + 1);
                            if (productListAdapterList.get(position).getSize().isEmpty()) {
                                Map<String, String> map = new HashMap<>();
                                map.put("method", "AddToCart");
                                map.put("deviceId", android_id);
                                map.put("userId", user.get(UserSessionManager.KEY_ID));
                                map.put("Pid", productListAdapterList.get(position).getProduct_id());
                                map.put("Qty", counter + "");
                                map.put("size", productListAdapterList.get(position).getSize());
                                //user.get(UserSessionManager.KEY_ID)
                                Add_Cart(map, position, holder.tv_item_count, 1);
                            } else {
                                Map<String, String> map = new HashMap<>();
                                map.put("method", "AddToCart");
                                map.put("deviceId", android_id);
                                map.put("userId", user.get(UserSessionManager.KEY_ID));
                                map.put("Pid", productListAdapterList.get(position).getProduct_id());
                                map.put("Qty", counter + "");
                                map.put("size", productListAdapterList.get(position).getSize());
                                //user.get(UserSessionManager.KEY_ID)
                                Add_Cart(map, position, holder.tv_item_count, 1);
                            }
                        } else {
                            showmessage("this product is out of stock");
                        }
                    }
                }
            });
            holder.tv_remove_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    startActivity(new Intent(Notification.this, OtherProfile.class));
                    // Toast.makeText(getActivity(), "You clicked on cart holder", Toast.LENGTH_LONG).show();
                   /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                        showmessage("You need to Login to use this feature");
                    } else {*/
                    int counter = Integer.parseInt(productListAdapterList.get(position).getQuantity_count());
                    Log.d("asdasd", counter + "");
                    if (counter > Integer.parseInt(productListAdapterList.get(position).getMinqty())) {
                        counter--;
                        if (productListAdapterList.get(position).getSize().isEmpty()) {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "AddToCart");
                            map.put("deviceId", android_id);
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("Pid", productListAdapterList.get(position).getProduct_id());
                            map.put("Qty", counter + "");
                            map.put("size", productListAdapterList.get(position).getSize());
                            //user.get(UserSessionManager.KEY_ID)
                            Add_Cart(map, position, holder.tv_item_count, 2);
                        } else {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "AddToCart");
                            map.put("deviceId", android_id);
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("Pid", productListAdapterList.get(position).getProduct_id());
                            map.put("Qty", counter + "");
                            map.put("size", productListAdapterList.get(position).getSize());
                            //user.get(UserSessionManager.KEY_ID)
                            Add_Cart(map, position, holder.tv_item_count, 2);
                        }
                    } else if (counter == Integer.parseInt(productListAdapterList.get(position).getMinqty())) {
                        holder.tv_remove_item.setVisibility(View.GONE);
                        holder.tv_item_count.setVisibility(View.GONE);
                        holder.tv_add_item.setVisibility(View.GONE);
                        holder.tv_add.setVisibility(View.VISIBLE);
                        if (productListAdapterList.get(position).getSize().isEmpty()) {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "AddToCart");
                            map.put("deviceId", android_id);
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("Pid", productListAdapterList.get(position).getProduct_id());
                            map.put("Qty", "0");
                            map.put("size", productListAdapterList.get(position).getSize());
                            //user.get(UserSessionManager.KEY_ID)
                            Add_Cart(map, position, holder.tv_item_count, 2);
                        } else {
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "AddToCart");
                            map.put("deviceId", android_id);
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("Pid", productListAdapterList.get(position).getProduct_id());
                            map.put("Qty", "0");
                            map.put("size", productListAdapterList.get(position).getSize());
                            //user.get(UserSessionManager.KEY_ID)
                            Add_Cart(map, position, holder.tv_item_count, 2);
                        }
                    } else {
                        Toast.makeText(ProductList.this, "You haven't select any of the item", Toast.LENGTH_LONG).show();
                    }

                    /* }
                     */
                }
            });

            holder.tv_add_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int counter = Integer.parseInt(productListAdapterList.get(position).getQuantity_count());
                    counter++;
                    if (productListAdapterList.get(position).getSize().isEmpty()) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
                        map.put("deviceId", android_id);
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", productListAdapterList.get(position).getProduct_id());
                        map.put("Qty", counter + "");
                        map.put("size", productListAdapterList.get(position).getSize());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map, position, holder.tv_item_count, 1);
                    } else {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "AddToCart");
                        map.put("deviceId", android_id);
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("Pid", productListAdapterList.get(position).getProduct_id());
                        map.put("Qty", counter + "");
                        map.put("size", productListAdapterList.get(position).getSize());
                        //user.get(UserSessionManager.KEY_ID)
                        Add_Cart(map, position, holder.tv_item_count, 1);
                    }
                }
            });


            holder.tv_item_count.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (!holder.tv_add.getText().toString().equals("0")) {
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return productListAdapterList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final TextView tv_minmaxqty;
            private final LinearLayout product_details;
            private final ImageView iv_item;
            private final TextView tv_name;
            private final TextView tv_oldPrice;
            private final TextView tv_new_price;
            private final Spinner quatity_spinner;
            private final TextView tv_off;
            private final TextView tv_date;
            private final LinearLayout tv_remove_item;
            private final TextView tv_item_count;
            private final TextView tv_add;
            private final TextView out_stock;
            private final TextView tv_qty;
            private final LinearLayout tv_add_item;

            public ViewHolder(View itemView) {
                super(itemView);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.tv_qty = itemView.findViewById(R.id.tv_qty);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_oldPrice = itemView.findViewById(R.id.tv_oldPrice);
                this.tv_new_price = itemView.findViewById(R.id.tv_new_price);
                this.quatity_spinner = itemView.findViewById(R.id.quatity_spinner);
                this.tv_off = itemView.findViewById(R.id.tv_off);
                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
                this.tv_add = itemView.findViewById(R.id.tv_add);
                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
                this.out_stock = itemView.findViewById(R.id.out_stock);
                this.tv_minmaxqty = itemView.findViewById(R.id.tv_minmaxqty);
                this.tv_date = itemView.findViewById(R.id.tv_date);
            }
        }
    }

    public class Quntity_Adapter extends ArrayAdapter<WishListModel.Size_chart> {

        LayoutInflater flater;

        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<WishListModel.Size_chart> list) {

            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return rowview(convertView, position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(convertView, position);
        }

        private View rowview(View convertView, int position) {

            final WishListModel.Size_chart rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
                holder.spinner_text = rowview.findViewById(R.id.spinner_text);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            holder.spinner_text.setText("Qty." + rowItem.getSize());

            return rowview;
        }

        private class viewHolder {
            TextView spinner_text;
        }
    }

}
