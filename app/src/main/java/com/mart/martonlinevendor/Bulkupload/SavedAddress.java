package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.ActiviteAddressModel;
import com.mart.martonlinevendor.Model.AddressListModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedAddress extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recyclerView_address;
    private Button btn_submit;
    UserSessionManager session;
    HashMap<String, String> user;
    AddressAdapter addressAdapter = new AddressAdapter();
    private LinearLayout dialogView;
    private LinearLayout no_data_found;
    List<AddressListModel.Response> addresslist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_address);
        session = new UserSessionManager(SavedAddress.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SavedAddress.this, OuterCart.class));
//                startActivity(new Intent(SavedAddress.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        header = findViewById(R.id.header);
        recyclerView_address = findViewById(R.id.recyclerView_address);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
//        recyclerView_address.setAdapter(addressAdapter);
//        list_address = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("method", "getAddressList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getAddress(map);
        dialogView = findViewById(R.id.dialogView);
        dialogView.setOnClickListener(this);
        no_data_found = findViewById(R.id.no_data_found);
        no_data_found.setOnClickListener(this);
    }

    private void getAddress(Map<String, String> map) {
        final Dialog dialog = new Dialog(SavedAddress.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<AddressListModel> call = Apis.getAPIService().getAddressList(map);
        call.enqueue(new Callback<AddressListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<AddressListModel> call, @NonNull Response<AddressListModel> response) {
                dialog.dismiss();
                AddressListModel userdata = response.body();
                if (userdata != null) {

                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {
                        addresslist.clear();
                        addresslist.addAll(userdata.getResponse());
                        if (addresslist.size() > 0) {
                            no_data_found.setVisibility(View.GONE);
                            recyclerView_address.setVisibility(View.VISIBLE);
                            AddressAdapter notificationAdapter = new AddressAdapter();
                            recyclerView_address.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else {
                            recyclerView_address.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView_address.setVisibility(View.GONE);
                        no_data_found.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddressListModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void DeleteAddress(Map<String, String> map) {
        final Dialog dialog = new Dialog(SavedAddress.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<AddressListModel> call = Apis.getAPIService().deleteadddress(map);
        call.enqueue(new Callback<AddressListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<AddressListModel> call, @NonNull Response<AddressListModel> response) {
                dialog.dismiss();
                AddressListModel userdata = response.body();
                if (userdata != null) {
                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getAddressList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        getAddress(map);
                    } else {
                        Custom_Toast_Activity.makeText(SavedAddress.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddressListModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void ActiviteAddress(Map<String, String> map) {
        final Dialog dialog = new Dialog(SavedAddress.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ActiviteAddressModel> call = Apis.getAPIService().ActiviteAddress(map);
        call.enqueue(new Callback<ActiviteAddressModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<ActiviteAddressModel> call, @NonNull Response<ActiviteAddressModel> response) {
                dialog.dismiss();
                ActiviteAddressModel userdata = response.body();
                if (userdata != null) {
                    Log.d("hgf", "hghgjh");
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(SavedAddress.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getAddressList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        getAddress(map);
                     } else {
                      Custom_Toast_Activity.makeText(SavedAddress.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActiviteAddressModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                startActivity(new Intent(SavedAddress.this, AddNewAddress.class));
                break;
        }
    }

    public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

        public int counter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_address_profile, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_name.setText(addresslist.get(position).getName());
            holder.tv_item_details.setText(addresslist.get(position).getAddress());
            if (addresslist.get(position).getActiveaddress().equals("1")) {
                holder.rlmain.setBackground(getResources().getDrawable(R.drawable.bg_check));
                holder.ivcheck.setVisibility(View.VISIBLE);
            } else {
                holder.rlmain.setBackground(getResources().getDrawable(R.drawable.bg_uncheck));
                holder.ivcheck.setVisibility(View.GONE);
            }
            holder.rlmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "activeaddress");
                    map.put("id",addresslist.get(position).getId());
                    map.put("userId", user.get(UserSessionManager.KEY_ID));

                    ActiviteAddress(map);
                }
            });

            holder.delete_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        Map<String, String> map = new HashMap<>();
                        map.put("method", "deleteaddress");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("id",addresslist.get(position).getId());

                        DeleteAddress(map);
                }
            });

        }


        @Override
        public int getItemCount() {
            return addresslist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {


            private ImageView iv_item, ivcheck;
          TextView tv_item_details,tv_name;
            private RelativeLayout rlmain;
            private FrameLayout delete_layout;

            public ViewHolder(View itemView) {

                super(itemView);
                this.delete_layout = itemView.findViewById(R.id.delete_layout);
                this.rlmain = itemView.findViewById(R.id.rlmain);
                this.ivcheck = itemView.findViewById(R.id.ivcheck);
                this.tv_item_details = itemView.findViewById(R.id.tv_item_details);
                this.tv_name = itemView.findViewById(R.id.tv_name);

            }
        }
    }


    public class checklist {
        boolean check = false;

        public boolean isCheck() {
            return check;
        }

        public void setCheck(boolean check) {
            this.check = check;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        map.put("method", "getAddressList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getAddress(map);
    }
}
