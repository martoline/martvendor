package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.ShopDetailModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopDetails extends AppCompatActivity {
    String quantity_count = "0";
    String Sizeid = "0";
    List<ShopDetailModel.Products> productListAdapterList = new ArrayList<>();
    UserSessionManager session;
    HashMap<String, String> user;
    FloatingActionButton whatsapporder;
    String mobile = "";
    String lat, lng;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private TextView shoptime;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private ImageView iv_other_profile;
    private TextView tv_user_bio;
    private TextView tvuser_name;
    private TextView tv_respect;
    private TextView respect;
    private LinearLayout rl_respect;
    private TextView tv_follower, tvviewAll;
    private TextView follower;
    private LinearLayout rl_follower;
    private TextView tv_following;
    private TextView tvaddress;
    private TextView following;
    private LinearLayout rl_following;
    private RecyclerView recycler_productlist;
    private TextView tvcall;
    private TextView tvlocation;
    private ImageView ivcart;
    private View ivview;
    private TextView tv_total_items;
    private TextView tvtoalprice;
    private TextView tvdistence;
    private TextView tvgotocart;
    private TextView tvdelivery;
    private TextView tvcartcount;
    private CardView card_cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        session = new UserSessionManager(ShopDetails.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        shoptime = findViewById(R.id.shoptime);
        tvcartcount = findViewById(R.id.tvcartcount);
        whatsapporder = findViewById(R.id.whatsapporder);
        whatsapporder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {
//                    showdialogmapscreen();
                }
            }
        });
        shoptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {
//                    showdialogmapscreen();
                }
            }
        });
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        tvdelivery = findViewById(R.id.tvdelivery);
        tvviewAll = findViewById(R.id.tvviewAll);
        tvdistence = findViewById(R.id.tvdistence);
        tvdistence.setText("Distance: " + getIntent().getStringExtra("distence"));
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShopDetails.this, OuterCart.class));
//                startActivity(new Intent(ShopDetails.this, SearchProduct_Shop.class).putExtra("vendor_id", getIntent().getStringExtra("shopId")));
            }
        });
        tvviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(com.martoline.ShopList.ShopDetails.this, SubCatogries.class));
            }
        });
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {*/
//                startActivity(new Intent(com.martoline.ShopList.ShopDetails.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                /* }*/
            }
        });
        header = findViewById(R.id.header);
        iv_other_profile = findViewById(R.id.iv_other_profile);
        tv_user_bio = findViewById(R.id.tv_user_bio);
        tvuser_name = findViewById(R.id.tvuser_name);
        tv_respect = findViewById(R.id.tv_respect);
        tvaddress = findViewById(R.id.tvaddress);
        respect = findViewById(R.id.respect);
        rl_respect = findViewById(R.id.rl_respect);
        tv_follower = findViewById(R.id.tv_follower);
        follower = findViewById(R.id.follower);
        rl_follower = findViewById(R.id.rl_follower);
        tv_following = findViewById(R.id.tv_following);
        following = findViewById(R.id.following);
        rl_following = findViewById(R.id.rl_following);
        recycler_productlist = findViewById(R.id.recycler_productlist);


        tvcall = findViewById(R.id.tvcall);
        tvcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mobile.isEmpty()) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + mobile));
                    startActivity(callIntent);
                } else {
                    Custom_Toast_Activity.makeText(ShopDetails.this, "Sorry, Mobile number not provided", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                }
            }
        });
        tvlocation = findViewById(R.id.tvlocation);
        tvlocation.setSelected(true);  // Set focus to the textview
        tvlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lat.isEmpty()) {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=" + lat + "," + lng + "&mode=d"));
                    startActivity(intent);
                } else {
                    Custom_Toast_Activity.makeText(ShopDetails.this, "Sorry,Location not provided", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                }
            }
        });
        ivcart = findViewById(R.id.ivcart);
        ivview = findViewById(R.id.ivview);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvtoalprice = findViewById(R.id.tvtoalprice);
        tvgotocart = findViewById(R.id.tvgotocart);
        card_cart = findViewById(R.id.card_cart);
        tvgotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(com.martoline.ShopList.ShopDetails.this, OuterCart.class));
            }
        });
        card_cart = findViewById(R.id.card_cart);

//        if (Utility.getCartcount().equals("0")) {
//            card_cart.setVisibility(View.GONE);
//        } else {
//            card_cart.setVisibility(View.VISIBLE);
//            tvtoalprice.setText("Rs. " + Utility.gettotalPrice());
//            tv_total_items.setText(Utility.getCartcount() + " items");
//            Log.d("asdfdasedf", Utility.getCartcount() + " items");
//        }
        Map<String, String> map = new HashMap<>();
        map.put("method", "shopdetail");
        map.put("shopId", getIntent().getStringExtra("shopId"));
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getShopDetail(map);

    }

    private void getShopDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(ShopDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ShopDetailModel> call = Apis.getAPIService().getShopDetail1(map);
        call.enqueue(new Callback<ShopDetailModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<ShopDetailModel> call, @NonNull Response<ShopDetailModel> response) {
                dialog.dismiss();
                ShopDetailModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        Log.d("jfjhg", Apis.USER_IMAGE_PATH + userdata.getBannerimage() + "");
                        Picasso.get().load(Apis.USER_IMAGE_PATH + userdata.getBannerimage())
                                .error(R.drawable.no_image)
                                .priority(Picasso.Priority.HIGH)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_other_profile);
                        productListAdapterList.clear();
                        mobile = userdata.getData().getPhone();
                        tv_user_bio.setText(userdata.getData().getShopcategory());
                        tvuser_name.setText(userdata.getData().getShopname());
                        tv_respect.setText(userdata.getData().getRating());
//                        tvcartcount.setText(userdata.getData().get);
                        tvaddress.setText("Address: " + userdata.getData().getAddress());
                        respect.setText(userdata.getData().getTotalreview() + " Reviews");
                        tv_follower.setText(userdata.getData().getOpentime() + " to " + userdata.getData().getClosetime());
//                        tvlocation.setText("Direction("+userdata.getData().getKm()+")");
//                        tvdistence.setText("Distence: " + userdata.getData().getKm());
                       /* if (userdata.getData().getDelivery().isEmpty()) {
                            if (!userdata.getData().getPickup().isEmpty()) {
                                tvdelivery.setText("Delivery type: Pickup");
                            } else {
                                tvdelivery.setVisibility(View.GONE);
                            }
                        } else {
                            if (userdata.getData().getPickup().isEmpty()) {
                                tvdelivery.setText("Delivery type: Delivery");
                            } else {
                                tvdelivery.setText("Delivery type: Delivery/Pickup");
                            }
                        }*/

                        tv_following.setText("₹ " + userdata.getData().getMinorder());
                        productListAdapterList.clear();
                        productListAdapterList = userdata.getData().getProducts();
                        lat = userdata.getData().getLatitude();
                        lng = userdata.getData().getLongitude();
                        if (productListAdapterList.size() > 0) {
                            recycler_productlist.setAdapter(new ProductListAdapter());
                        }

                    } else {
                        Custom_Toast_Activity.makeText(ShopDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                } else {
                    Custom_Toast_Activity.makeText(ShopDetails.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShopDetailModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

   /* @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnnext:

                break;
        }
    }*/

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(ShopDetails.this, LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    public void showmessage1(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("close", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(ShopDetails.this, LoginOTP.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (Utility.getCartcount().equals("0")) {
            card_cart.setVisibility(View.GONE);
        } else {
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. " + Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount() + " items");
            Log.d("asdfdasedf", Utility.getCartcount() + " items");
        }*/
    }

//    public void showdialogmapscreen() {
//        final Dialog dialog = new Dialog(ShopDetails.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_privacy);
//        dialog.setCancelable(true);
//        Window window = dialog.getWindow();
//        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        final CheckBox cbone   = dialog.findViewById(R.id.cbone);
//        final CheckBox cbtwo  = dialog.findViewById(R.id.cbtwo);
//        Button btnnext  = dialog.findViewById(R.id.btnnext);
//        WebView view = dialog.findViewById(R.id.webView);
////        WebView view = (WebView) findViewById(R.id.webView);
//        view.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return false;
//            }
//        });
//        view.getSettings().setJavaScriptEnabled(true);
//        view.loadUrl("http://martoline.in/privacy.php");
//        btnnext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cbone.isChecked()) {
//                    if (cbtwo.isChecked()) {
//                        dialog.dismiss();
////                        startActivity(new Intent(ShopDetails.this,WhatsappOrder.class).putExtra("shopId",getIntent().getStringExtra("shopId")));
//                    } else {
//                        showmessage1("Please Accept Terms And Conditions");
//                    }
//                } else {
//                    showmessage1("Please Accept Terms And Conditions");
//                }
//            }
//        });
//
//        dialog.show();
//    }

    public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
        public int counter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_categoery_1, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + productListAdapterList.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_cat_images);
            holder.tvtitle.setText(productListAdapterList.get(position).getCatName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (productListAdapterList.get(position).getAdult().equals("1")) {
                        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ShopDetails.this, SweetAlertDialog.WARNING_TYPE);
                        sweetAlertDialog.setTitleText("MartOline");
                        sweetAlertDialog.setContentText("Are You 18+ ? please agree if you are.");
                        sweetAlertDialog.setConfirmText("Agree");
                        sweetAlertDialog.setCancelText("Cancel");
                        sweetAlertDialog.setCancelable(false);

                        sweetAlertDialog.showCancelButton(true);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                startActivity(new Intent(ShopDetails.this, SubCatList.class).putExtra("cid", productListAdapterList.get(position).getCatId())
                                        .putExtra("shopId", getIntent().getStringExtra("shopId")));
                            }
                        });
                        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
//                                sweetAlertDialog.dismiss();
                            }
                        });
                        if (sweetAlertDialog.isShowing()) {
                        } else {
                            sweetAlertDialog.show();
                        }
                    } else {
                        startActivity(new Intent(ShopDetails.this, SubCatList.class).putExtra("cid", productListAdapterList.get(position).getCatId())
                                .putExtra("shopId", getIntent().getStringExtra("shopId")));

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return productListAdapterList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final LinearLayout product_details;
            private final ImageView iv_cat_images;
            private final TextView tvtitle;

            public ViewHolder(View itemView) {
                super(itemView);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);
            }
        }
    }
}
