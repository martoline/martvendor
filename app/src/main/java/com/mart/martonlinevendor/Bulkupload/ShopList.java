package com.mart.martonlinevendor.Bulkupload;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Bulkupload.Extra.Utils_location;
import com.mart.martonlinevendor.Extra.Utility;
import com.mart.martonlinevendor.LoginRegister.SignIn;
import com.mart.martonlinevendor.Model.NearByShopModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopList extends AppCompatActivity {
    private final String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    List<NearByShopModel.Data> array_list_Bottom_offer = new ArrayList();
    List<NearByShopModel.Exclusive> exclusiveList = new ArrayList();
    UserSessionManager session;
    HashMap<String, String> user;
    LinearLayout no_data_found;
    int currentpage = 1;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    NestedScrollView nestedscrollview;
    ShopListAdapter shopListAdapter;
    private ImageView iv_menu;
    private ImageView iv_bellnotification;
    private TextView tv_currentlocation;
    private RelativeLayout header;
    private ImageView iv_send, offeeruppper;
    private TextView et_search;
    private ImageView iv_voice;
    private RecyclerView recycler_nerbyshop;
    private RecyclerView recycler_Exclusive_shop;
    private TextView Shop_category_title;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private ImageView ivcart;
    private View ivview;
    private TextView tv_total_items;
    private TextView tvtoalprice;
    private TextView tvcartcount;
    private TextView tvgotocart;
    private CardView card_cart;
    private boolean loading = true;
    private final int LOCATION_PERMISSIONS_REQUEST = 123;
    private Boolean currentLocationPermissionDeniedWithDontAskMeAgain=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        session = new UserSessionManager(ShopList.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        no_data_found = findViewById(R.id.no_data_found);
        tvcartcount = findViewById(R.id.tvcartcount);
        iv_menu = findViewById(R.id.iv_menu);
        nestedscrollview = findViewById(R.id.nestedscrollview);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShopList.this, OuterCart.class));
               /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {*/
//                startActivity(new Intent(com.martoline.ShopList.ShopList.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                /* }*/
            }
        });
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        recycler_Exclusive_shop = findViewById(R.id.recycler_Exclusive_shop);
        header = findViewById(R.id.header);
        iv_send = findViewById(R.id.iv_send);
        offeeruppper = findViewById(R.id.offeeruppper);
        et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(com.martoline.ShopList.ShopList.this, SearchProduct.class));
            }
        });
        iv_voice = findViewById(R.id.iv_voice);
        shopListAdapter = new ShopListAdapter();
        recycler_nerbyshop = findViewById(R.id.recycler_nerbyshop);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_nerbyshop.setLayoutManager(mLayoutManager);
        recycler_nerbyshop.setAdapter(shopListAdapter);
        nestedscrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        if (loading) {
                            Log.d("Sdsdfa", currentpage + "+_____" + visibleItemCount + "______" + pastVisiblesItems + "____" + totalItemCount);

                            loading = false;
                            currentpage = currentpage + 1;
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "nearbyshop");
                            map.put("categoryId", getIntent().getStringExtra("cat_id"));
                            map.put("latitude", Utility.getlat());
                            map.put("longitude", Utility.getlng());
                            map.put("page", currentpage + "");
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            GetNearBYShop2(map);
                            // Do pagination.. i.e. fetch new data

                        }
                    }
                }
            }
        });

       /* nestedscrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                Log.d("asdfasd","Sdfasd");
                if (dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        Log.d("Sdsdfa",currentpage+"+_____"+visibleItemCount +"______"+ pastVisiblesItems+"____"+totalItemCount);
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {

                            loading = false;
                            currentpage = currentpage + 1;
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "nearbyshop");
                            map.put("categoryId", getIntent().getStringExtra("cat_id"));
                            map.put("latitude", Utility.getlat());
                            map.put("longitude", Utility.getlng());
                            map.put("page", currentpage + "");
                            GetNearBYShop2(map);
                            // Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });*/

        Shop_category_title = findViewById(R.id.Shop_category_title);
        cart_image = findViewById(R.id.cart_image);
        not_found_text = findViewById(R.id.not_found_text);
        dialogView = findViewById(R.id.dialogView);
        ivcart = findViewById(R.id.ivcart);
        ivview = findViewById(R.id.ivview);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvtoalprice = findViewById(R.id.tvtoalprice);
        tvgotocart = findViewById(R.id.tvgotocart);
        tvgotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(ShopList.this,OuterCart.class));
            }
        });
        card_cart = findViewById(R.id.card_cart);
        checkPermissionForCurrentLocation();
       /* if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/


    }

    private void checkPermissionForCurrentLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils_location.hasPermissions(this, LOCATION_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Utils_location.showDialogToUserPermissionDenied(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                } else if (currentLocationPermissionDeniedWithDontAskMeAgain) {
                    Utils_location.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                }
            } else {
                if (!(Utils_location.isGPSEnabled(this))) {
                    Utils_location.enableGPS(this);
                } else {
                    setUserCurrentLocation();
                }
            }
        } else {
            if (!(Utils_location.isGPSEnabled(this))) {
                Utils_location.enableGPS(this);
            } else {
                setUserCurrentLocation();
            }

        }
    }

    //current location
    @SuppressLint("SetTextI18n")
    private void setUserCurrentLocation() {
        Location location = Utils_location.getLocation(this);
        if (location != null) {
            Address address = Utils_location.getAddressUsingLatLang(this, location.getLatitude(), location.getLongitude());
            if (address != null) {
//                et_location.setText(address.getLocality()+","+address.getCountryName());
                Log.d("adsfasdfasdf", location.getLatitude() + location.getLongitude() + "");
                Map<String, String> map = new HashMap<>();
                map.put("method", "nearbyshop");
                map.put("page", "1");
                map.put("categoryId", getIntent().getStringExtra("cat_id"));
                map.put("latitude", location.getLatitude() + "");
                map.put("longitude", location.getLongitude() + "");
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                GetNearBYShop(map);
            }
        } else {
            fetchLocation();
        }
    }

    private void fetchLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    Address address = Utils_location.getAddressUsingLatLang(ShopList.this, location.getLatitude(), location.getLongitude());
                    if (address != null) {
//                        et_location.setText(address.getAddressLine(0));

                        Map<String, String> map = new HashMap<>();
                        map.put("method", "nearbyshop");
                        map.put("page", "1");
                        map.put("categoryId", getIntent().getStringExtra("cat_id"));
                        map.put("latitude", location.getLatitude() + "");
                        map.put("longitude", location.getLongitude() + "");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        Log.d("adsfasdfasdf", location.getLatitude() + location.getLongitude() + "");
                        GetNearBYShop(map);
                    }
                }
            }
        });
    }

    private void GetNearBYShop(Map<String, String> map) {
        final Dialog dialog = new Dialog(ShopList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<NearByShopModel> call = Apis.getAPIService().getNearByShop(map);
        call.enqueue(new Callback<NearByShopModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<NearByShopModel> call, @NonNull Response<NearByShopModel> response) {
                dialog.dismiss();
                NearByShopModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {

                        loading = true;
                        array_list_Bottom_offer.clear();
                        exclusiveList.clear();
//                        tvcartcount.setText(userdata.getData().get);
                        array_list_Bottom_offer.addAll(userdata.getData());
                        exclusiveList.addAll(userdata.getExclusive());
                        if (array_list_Bottom_offer.size() > 0) {
                            no_data_found.setVisibility(View.GONE);
                            recycler_nerbyshop.setVisibility(View.VISIBLE);
                            shopListAdapter.notifyDataSetChanged();
                        } else {
//                            Custom_Toast_Activity.makeText(ShopList.this, "No Shop Around you", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.INFO);
                            no_data_found.setVisibility(View.VISIBLE);
                            recycler_nerbyshop.setVisibility(View.GONE);
                        }
                        if (exclusiveList.size() > 0) {
                            recycler_Exclusive_shop.setAdapter(new Exclusive_shopAdapter());
                            recycler_Exclusive_shop.setVisibility(View.VISIBLE);
                        } else {
                            recycler_Exclusive_shop.setVisibility(View.GONE);
                        }
                        Picasso.get().load(Apis.IMAGE_PATH + userdata.getBannerimage())
                                .priority(Picasso.Priority.HIGH)
                                .error(R.drawable.shopbanner)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(offeeruppper);
                    } else {
                        Picasso.get().load(Apis.IMAGE_PATH + userdata.getBannerimage())
                                .priority(Picasso.Priority.HIGH)
                                .error(R.drawable.shopbanner)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(offeeruppper);
                        no_data_found.setVisibility(View.VISIBLE);
                        recycler_nerbyshop.setVisibility(View.GONE);
                        Custom_Toast_Activity.makeText(ShopList.this, "No Shop Around you", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.INFO);
                    }
                } else {
                    Custom_Toast_Activity.makeText(ShopList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<NearByShopModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void GetNearBYShop2(Map<String, String> map) {
        final Dialog dialog = new Dialog(ShopList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<NearByShopModel> call = Apis.getAPIService().getNearByShop(map);
        call.enqueue(new Callback<NearByShopModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<NearByShopModel> call, @NonNull Response<NearByShopModel> response) {
                dialog.dismiss();
                NearByShopModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        loading = true;
                        exclusiveList.clear();
                        array_list_Bottom_offer.addAll(userdata.getData());
                        exclusiveList.addAll(userdata.getExclusive());
                        if (array_list_Bottom_offer.size() > 0) {
                            loading = true;
                            shopListAdapter.notifyDataSetChanged();
                            no_data_found.setVisibility(View.GONE);
                            recycler_nerbyshop.setVisibility(View.VISIBLE);
                        } else {
                        }
                        if (exclusiveList.size() > 0) {
                            recycler_Exclusive_shop.setAdapter(new Exclusive_shopAdapter());
                            recycler_Exclusive_shop.setVisibility(View.VISIBLE);
                        } else {
                            recycler_Exclusive_shop.setVisibility(View.GONE);
                        }
                        Picasso.get().load(Apis.IMAGE_PATH + userdata.getBannerimage())
                                .priority(Picasso.Priority.HIGH)
                                .error(R.drawable.shopbanner)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(offeeruppper);
                    } else {
                        Picasso.get().load(Apis.IMAGE_PATH + userdata.getBannerimage())
                                .priority(Picasso.Priority.HIGH)
                                .error(R.drawable.shopbanner)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(offeeruppper);
                        no_data_found.setVisibility(View.VISIBLE);
                        recycler_nerbyshop.setVisibility(View.GONE);
                        Custom_Toast_Activity.makeText(ShopList.this, "No Shop Around you", Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.INFO);
                    }
                } else {
                    Custom_Toast_Activity.makeText(ShopList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<NearByShopModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ShopList.this, SignIn.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/
    }



    public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_name.setText(array_list_Bottom_offer.get(position).getShopname());
            holder.tv_name.setSelected(true);
            holder.tv_address.setText(array_list_Bottom_offer.get(position).getShopname());
            holder.tv_offer.setText(array_list_Bottom_offer.get(position).getOffer());
            holder.tvaddress.setText(array_list_Bottom_offer.get(position).getAddress());
            if (array_list_Bottom_offer.get(position).getDelivery().isEmpty()) {
                if (!array_list_Bottom_offer.get(position).getPickup().isEmpty()) {
                    holder.tvdilivry.setText("Delivery type: Pickup");
                }
            } else {
                if (array_list_Bottom_offer.get(position).getPickup().isEmpty()) {
                    holder.tvdilivry.setText("Delivery type: Delivery");
                } else {
                    holder.tvdilivry.setText("Delivery type: Delivery/Pickup");
                }
            }

            holder.tvdistence.setText("Distance : " + array_list_Bottom_offer.get(position).getKm());
            holder.tvaddress.setSelected(true);
            holder.tv_rating.setText(array_list_Bottom_offer.get(position).getRating());
            Log.d("jfjhg", Apis.USER_IMAGE_PATH + array_list_Bottom_offer.get(position).getImage() + "");
            Picasso.get().load(Apis.USER_IMAGE_PATH + array_list_Bottom_offer.get(position).getImage())
                    .priority(Picasso.Priority.HIGH)
                    .error(R.drawable.no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
          /*  Glide.with(ShopList.this)
                    .load(Apis.SHOP_IMAGE_PATH + array_list_Bottom_offer.get(position).getImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_item);*/
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ShopList.this, ShopDetails.class)
                            .putExtra("shopId", array_list_Bottom_offer.get(position).getShopID())
                            .putExtra("distence", array_list_Bottom_offer.get(position).getKm()));
                }
            });
        }

        @Override
        public int getItemCount() {
            return array_list_Bottom_offer.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final ImageView iv_item;
            private final TextView tv_name;
            private final TextView tvdistence;
            private final TextView tv_offer;
            private final TextView tvaddress;
            private final TextView tvdilivry;
            private final TextView tv_rating;
            private final TextView tv_address;
            private CardView card_cart;
            private ImageView ivcheck;
            private LinearLayout tv_remove_item;
            private TextView tv_item_count;
            private LinearLayout tv_add_item;
            private RelativeLayout rlmain;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvdilivry = itemView.findViewById(R.id.tvdilivry);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tvaddress = itemView.findViewById(R.id.tvaddress);
                this.tv_address = itemView.findViewById(R.id.tv_address);
                this.tv_offer = itemView.findViewById(R.id.tv_offer);
                this.tvdistence = itemView.findViewById(R.id.tvdistence);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_rating = itemView.findViewById(R.id.tv_rating);
//                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

    public class Exclusive_shopAdapter extends RecyclerView.Adapter<Exclusive_shopAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_exclusive, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tv_name.setText(exclusiveList.get(position).getShopname());
            holder.tv_name.setSelected(true);
            holder.tv_address.setText(exclusiveList.get(position).getShopname());
            holder.tv_offer.setText(exclusiveList.get(position).getOffer());
            holder.tvaddress.setText(exclusiveList.get(position).getAddress());
            if (exclusiveList.get(position).getDelivery().isEmpty()) {
                if (!exclusiveList.get(position).getPickup().isEmpty()) {
                    holder.tvdilivry.setText("Delivery type: Pickup");
                }
            } else {
                if (exclusiveList.get(position).getPickup().isEmpty()) {
                    holder.tvdilivry.setText("Delivery type: Delivery");
                } else {
                    holder.tvdilivry.setText("Delivery type: Delivery/Pickup");
                }
            }

            holder.tvdistence.setText("Distance : " + exclusiveList.get(position).getKm());
            holder.tvaddress.setSelected(true);
            holder.tv_rating.setText(exclusiveList.get(position).getRating());
            Log.d("jfjhg", Apis.USER_IMAGE_PATH + exclusiveList.get(position).getImage() + "___");
           /* Glide.with(ShopList.this)
                    .load(Apis.SHOP_IMAGE_PATH + exclusiveList.get(position).getImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_item);*/
            Picasso.get().load(Apis.USER_IMAGE_PATH + exclusiveList.get(position).getImage())
                    .priority(Picasso.Priority.HIGH)
                    .error(R.drawable.no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ShopList.this, ShopDetails.class)
                            .putExtra("shopId", exclusiveList.get(position).getShopID())
                            .putExtra("distence", exclusiveList.get(position).getKm()));
                }
            });
        }

        @Override
        public int getItemCount() {
            return exclusiveList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final ImageView iv_item;
            private final TextView tv_name;
            private final TextView tvdistence;
            private final TextView tv_offer;
            private final TextView tvaddress;
            private final TextView tvdilivry;
            private final TextView tv_rating;
            private final TextView tv_address;
            private CardView card_cart;
            private ImageView ivcheck;
            private LinearLayout tv_remove_item;
            private TextView tv_item_count;
            private LinearLayout tv_add_item;
            private RelativeLayout rlmain;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvdilivry = itemView.findViewById(R.id.tvdilivry);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tvaddress = itemView.findViewById(R.id.tvaddress);
                this.tv_address = itemView.findViewById(R.id.tv_address);
                this.tv_offer = itemView.findViewById(R.id.tv_offer);
                this.tvdistence = itemView.findViewById(R.id.tvdistence);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_rating = itemView.findViewById(R.id.tv_rating);
//                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

}
