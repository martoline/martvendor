package com.mart.martonlinevendor.Bulkupload;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.SubcatModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCatList extends AppCompatActivity {

    UserSessionManager session;
    HashMap<String, String> user;
    List<SubcatModel.Response> productListAdapterList = new ArrayList<>();
    LinearLayout carderror;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recycler_productlist;
    private LinearLayout dialogView;
    private ImageView ivcart;
    private View ivview;
    private TextView tv_total_items;
    private TextView tvtoalprice;
    private TextView tvcartcount;
    private TextView tvgotocart;
    private CardView card_cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_cat_list);
        session = new UserSessionManager(SubCatList.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        tvcartcount = findViewById(R.id.tvcartcount);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        carderror = findViewById(R.id.carderror);
        et_search = findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(SubCatList.this, SearchProduct_Shop.class).putExtra("vendor_id", getIntent().getStringExtra("shopId")));
//                startActivity(new Intent(SubCatList.this, SearchProduct.class));
            }
        });
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        recycler_productlist = findViewById(R.id.recycler_productlist);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    showmessage("You need to Login to use this feature");
                } else {*/
                startActivity(new Intent(SubCatList.this, OuterCart.class));
                /*  }*/
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put("method", "getSubCategory");
        map.put("catId", getIntent().getStringExtra("cid"));
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("shopId", getIntent().getStringExtra("shopId"));
        getShopDetail(map);
        ivcart = findViewById(R.id.ivcart);
        ivview = findViewById(R.id.ivview);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvtoalprice = findViewById(R.id.tvtoalprice);
        tvgotocart = findViewById(R.id.tvgotocart);
        card_cart = findViewById(R.id.card_cart);
        tvgotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(SubCatList.this,OuterCart.class));
            }
        });
        card_cart = findViewById(R.id.card_cart);

       /* if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/
    }

    private void getShopDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(SubCatList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SubcatModel> call = Apis.getAPIService().getSubCat(map);
        call.enqueue(new Callback<SubcatModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<SubcatModel> call, @NonNull Response<SubcatModel> response) {
                dialog.dismiss();
                SubcatModel userdata = response.body();
                if (userdata != null) {
                    tvcartcount.setText(userdata.getCartCount());
                    if (userdata.getStatus().equals("1")) {
                        productListAdapterList.clear();
                        productListAdapterList = userdata.getResponse();
                        if (productListAdapterList.size() > 0) {
                            recycler_productlist.setAdapter(new ProductListAdapter());
                            carderror.setVisibility(View.GONE);
                            recycler_productlist.setVisibility(View.VISIBLE);
                        } else {
                            carderror.setVisibility(View.VISIBLE);
                            recycler_productlist.setVisibility(View.GONE);
                        }

                    } else {
                        Custom_Toast_Activity.makeText(SubCatList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);
                    }
                } else {
                    Custom_Toast_Activity.makeText(SubCatList.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_SHORT, Custom_Toast_Activity.ERROR);

                }
            }

            @Override
            public void onFailure(@NonNull Call<SubcatModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("finish", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* if (Utility.getCartcount().equals("0")){
            card_cart.setVisibility(View.GONE);
        }else{
            card_cart.setVisibility(View.VISIBLE);
            tvtoalprice.setText("Rs. "+Utility.gettotalPrice());
            tv_total_items.setText(Utility.getCartcount()+" items");
            Log.d("asdfdasedf",Utility.getCartcount()+" items");
        }*/
    }

    public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
        public int counter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_category, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + productListAdapterList.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_category);
            holder.tv_categoryName.setText(productListAdapterList.get(position).getCatName());
            holder.tv_categoryName.setSelected(true);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(SubCatList.this, ProductList.class)
                            .putExtra("shopId", getIntent().getStringExtra("shopId"))
                            .putExtra("cat_id", productListAdapterList.get(position).getCatId()));


                }
            });

        }


        @Override
        public int getItemCount() {
            return productListAdapterList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final LinearLayout product_details;
            private final ImageView iv_category;
            private final TextView tv_categoryName;


            public ViewHolder(View itemView) {
                super(itemView);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);

            }
        }
    }
}
