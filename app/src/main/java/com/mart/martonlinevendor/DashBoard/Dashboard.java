package com.mart.martonlinevendor.DashBoard;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.mart.martonlinevendor.AddNewProduct.AddNewProduct;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Bulkupload.Categories;
import com.mart.martonlinevendor.Bulkupload.MyOrders;
import com.mart.martonlinevendor.Bulkupload.SavedAddress;
import com.mart.martonlinevendor.Delivery_Time_Slot.Delivery_Time_Slot_Activity;
import com.mart.martonlinevendor.Fragment.Home;
import com.mart.martonlinevendor.Fragment.ShopDetails;
import com.mart.martonlinevendor.Fragment.UserProfile;
import com.mart.martonlinevendor.Model.UserProfileModel;
import com.mart.martonlinevendor.NetwrokCheck.NoInternetDialog;
import com.mart.martonlinevendor.OrderDetail.OrderByStatus;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.Shop_Information.Shop_Information_Activity;
import com.mart.martonlinevendor.SideScreen.AboutUs;
import com.mart.martonlinevendor.SideScreen.Support_Activity;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Dashboard extends AppCompatActivity implements View.OnClickListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String TAG = "kjnkjnk";
    private static final long delay = 2000L;
    private final Handler mExitHandler = new Handler();
    UserSessionManager session;
    HashMap<String, String> user;
    AlertDialog.Builder builder;
    BottomNavigationView bottom_navigation;
    String Status = "";
    NoInternetDialog noInternetDialog;
    //    @Override
//    protected void onStop() {
//        super.onStop();
//        if (user.get(UserSessionManager.KEY_ID).equals("0")){
//            session.logoutUser();
//        }
//    }
//    long back_pressed;
//
//    @Override
//    public void onBackPressed() {
////        if (user.get(UserSessionManager.KEY_ID).equals("0")) {
////            session.logoutUser();
////        } else{
////            bottom_navigation.setSelectedItemId(R.id.navigation_home);
////            addFragment(new Home());
////        }
////        if (mRecentlyBackPressed) {
////            mExitHandler.removeCallbacks(mExitRunnable);
////            mExitHandler = null;
////            bottom_navigation.setSelectedItemId(R.id.navigation_home);
////           addFragment(new Home());
////        }
////        else
////        {
////            mRecentlyBackPressed = true;
////            Toast.makeText(this, "press again to exit", Toast.LENGTH_SHORT).show();
////            mExitHandler.postDelayed(mExitRunnable, delay);
////            moveTaskToBack(true);
////        }
//    }
    boolean doubleBackToExitPressedOnce = false;
    private ImageView iv_menu;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    private DrawerLayout drawer;
    private NavigationView nav_view;
    private ImageView iv_close;
    private CircleImageView civProfileimage;
    private TextView tv_name;
    private TextView tv_email;
    private RelativeLayout nav_home;
    private RelativeLayout nav_myShop;
    private RelativeLayout nav_Availability;
    private RelativeLayout nav_Delivery_Time;
    private RelativeLayout nav_Account;
    private RelativeLayout nav_Orders;
    private RelativeLayout nav_categories;
    private RelativeLayout nav_B2B;
    private RelativeLayout nav_BulkOrders;
    private RelativeLayout nav_savedaddress;
    private RelativeLayout nav_terms_and_conditions;
    private RelativeLayout nav_logout;
    private RelativeLayout aboutus;
    private RelativeLayout rl_header1, rl_header;
    private boolean mRecentlyBackPressed = false;
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (Status.equals("2")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "Profile");
                        map.put("UserId", user.get(UserSessionManager.KEY_ID));
                        getProfile(map);
                        bottom_navigation.setSelectedItemId(R.id.navigation_account);
                        addFragment(new UserProfile());
                    } else {
                        addFragment(new Home());
                    }
                    return true;
                case R.id.navigation_categories:
                    if (Status.equals("2")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "Profile");
                        map.put("UserId", user.get(UserSessionManager.KEY_ID));
                        getProfile(map);
                        bottom_navigation.setSelectedItemId(R.id.navigation_account);
                        addFragment(new UserProfile());
                    } else {
                        addFragment(new ShopDetails());
                    }
                    return true;
                case R.id.navigation_cart:
//                    addFragment(new Basket());
                    if (Status.equals("2")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "Profile");
                        map.put("UserId", user.get(UserSessionManager.KEY_ID));
                        getProfile(map);
                        bottom_navigation.setSelectedItemId(R.id.navigation_account);
                        addFragment(new UserProfile());
                    } else {
                        startActivity(new Intent(Dashboard.this, AddNewProduct.class)
                                .putExtra("tag", "new_product"));

                    }
                    return true;
                case R.id.navigation_offers:
                    if (Status.equals("2")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "Profile");
                        map.put("UserId", user.get(UserSessionManager.KEY_ID));
                        getProfile(map);
                        bottom_navigation.setSelectedItemId(R.id.navigation_account);
                        addFragment(new UserProfile());
                    } else {
                        addFragment(new OrderByStatus());
                    }
                    return true;
                case R.id.navigation_account:
                    addFragment(new UserProfile());
                    return true;
            }

            return false;
        }
    };

    private final Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        builder = new AlertDialog.Builder(this);
        noInternetDialog = new NoInternetDialog.Builder(this).build();
        initView();
        addFragment(new Home());
       /* if (getIntent().getStringExtra("payment")==null){
            addFragment(new Home());
        }else {
            if (getIntent().getStringExtra("payment").equals("payment")) {
                addFragment(new ThankYou());
            }else {
                addFragment(new Home());
            }
        }*/

    }

    private void showResults(String currentAdd) {
        tv_currentlocation.setText(currentAdd);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);

        // Get the notifications MenuItem and
        // its LayerDrawable (layer-list)
        MenuItem item = menu.findItem(R.id.navigation_account);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
//        Utils.setBadgeCount(this, icon, 1);

        return true;
    }

    private void initView() {

        iv_menu = findViewById(R.id.iv_menu);
//        rl_header1 = findViewById(R.id.rl_header1);
        rl_header = findViewById(R.id.rl_header);
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        aboutus = findViewById(R.id.aboutus);
        drawer = findViewById(R.id.drawer);
        nav_view = findViewById(R.id.nav_view);

        iv_menu.setOnClickListener(this);
        tv_currentlocation.setOnClickListener(this);
        iv_bellnotification.setOnClickListener(this);
        aboutus.setOnClickListener(this);
        // View headerView = nav_view.getHeaderView(0);
        iv_close = findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottom_navigation.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(2);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(this)
                .inflate(R.layout.notification_badge, itemView, true);
//        itemView.addView(badge);
        civProfileimage = findViewById(R.id.civProfileimage);
        civProfileimage.setOnClickListener(this);
        tv_name = findViewById(R.id.tv_name);
        tv_email = findViewById(R.id.tv_email);
        nav_home = findViewById(R.id.nav_home);
        nav_home.setOnClickListener(this);
        nav_B2B = findViewById(R.id.nav_B2B);
        nav_BulkOrders = findViewById(R.id.nav_BulkOrders);
        nav_savedaddress = findViewById(R.id.nav_savedaddress);
        nav_B2B.setOnClickListener(this);
        nav_BulkOrders.setOnClickListener(this);
        nav_savedaddress.setOnClickListener(this);
        nav_myShop = findViewById(R.id.nav_myShop);
        nav_myShop.setOnClickListener(this);
        nav_Availability = findViewById(R.id.nav_Availability);
        nav_Availability.setOnClickListener(this);
        nav_Account = findViewById(R.id.nav_Account);
        nav_Account.setOnClickListener(this);
        nav_Delivery_Time = findViewById(R.id.nav_Delivery_Time);
        nav_Delivery_Time.setOnClickListener(this);
        nav_Orders = findViewById(R.id.nav_Orders);
        nav_Orders.setOnClickListener(this);
        nav_categories = findViewById(R.id.nav_categories);
        nav_categories.setOnClickListener(this);
        nav_terms_and_conditions = findViewById(R.id.nav_terms_and_conditions);
        nav_terms_and_conditions.setOnClickListener(this);
        nav_logout = findViewById(R.id.nav_logout);
        nav_logout.setOnClickListener(this);

       /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
            logout_text.setText("Login");
            civProfileimage.setVisibility(View.GONE);
            tv_name.setText("Guest User");
            tv_email.setVisibility(View.GONE);
        } else {
            ///profile API Call Funtion -------------------------------------------
            if (merlinsBeard.isConnected()) {
                Map<String, String> map = new HashMap<>();
                map.put("method", "Profile");
                map.put("UserId", user.get(UserSessionManager.KEY_ID));
                getProfile(map);
            } else {
                Custom_Toast_Activity.makeText(Dashboard.this, "Please Connect the internet", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);


            }
        }*/
        Map<String, String> map = new HashMap<>();
        map.put("method", "Profile");
        map.put("UserId", user.get(UserSessionManager.KEY_ID));
        getProfile(map);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open();
            }
        });

    }

    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void checkLocationPermission() {


        if (ContextCompat.checkSelfPermission(Dashboard.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(Dashboard.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(Dashboard.this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(Dashboard.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(Dashboard.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    public void addFragment(Fragment fragment) {
        FragmentManager fm1 = getSupportFragmentManager();
        fm1.popBackStack("Home", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fm1.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void open() {

        drawer.openDrawer(GravityCompat.START);
    }
/*
    private void getProfile(Map<String, String> map) {
        final Dialog dialog = new Dialog(Dashboard.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Get_User_Profile_Model> call = Apis.getAPIService().getProfile(map);
        call.enqueue(new Callback<Get_User_Profile_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Get_User_Profile_Model> call, @NonNull Response<Get_User_Profile_Model> response) {
                dialog.dismiss();
                Get_User_Profile_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        for (int i = 0; i < userdata.getResponse().size(); i++) {
                            tv_name.setText(userdata.getResponse().get(i).getName());
                            tv_email.setText(userdata.getResponse().get(i).getEmail());
                            Picasso.get().load(Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getImage())
                                    .error(R.drawable.no_image)
                                    .priority(Picasso.Priority.HIGH)
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(civProfileimage);
                        }

                    } else {

                        Custom_Toast_Activity.makeText(Dashboard.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Dashboard.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Get_User_Profile_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }*/

//    @Override
//    protected void onDestroy() {
//        if (user.get(UserSessionManager.KEY_ID).equals("0")){
//            session.logoutUser();
//        }
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onRestart() {
//        if (user.get(UserSessionManager.KEY_ID).equals("0")){
//            session.logoutUser();
//        }
//        super.onRestart();
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_currentlocation:

                //   Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;

            case R.id.iv_bellnotification:
               /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    builder.setMessage("You need to Register to use this feature?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(Dashboard.this, SignIn.class));
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle(R.string.app_name);
                    alert.show();
                } else {
                    startActivity(new Intent(this, Notification.class));
                }*/

                break;

            case R.id.iv_close:
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_home:
                if (Status.equals("2")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "Profile");
                    map.put("UserId", user.get(UserSessionManager.KEY_ID));
                    getProfile(map);
                    bottom_navigation.setSelectedItemId(R.id.navigation_account);
                    addFragment(new UserProfile());
                } else {
                    bottom_navigation.setSelectedItemId(R.id.navigation_home);
                    Home fragment = new Home();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack(null);
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }

                break;
            case R.id.nav_Orders:
                if (Status.equals("2")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "Profile");
                    map.put("UserId", user.get(UserSessionManager.KEY_ID));
                    getProfile(map);
                    bottom_navigation.setSelectedItemId(R.id.navigation_account);
                    addFragment(new UserProfile());
                } else {
                    bottom_navigation.setSelectedItemId(R.id.navigation_offers);
                    OrderByStatus fragment5 = new OrderByStatus();
                    FragmentManager fragmentManager5 = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction5 = fragmentManager5.beginTransaction().addToBackStack(null);
                    fragmentTransaction5.replace(R.id.frame, fragment5);
                    fragmentTransaction5.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }

                break;
            case R.id.nav_Delivery_Time:
                if (Status.equals("2")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "Profile");
                    map.put("UserId", user.get(UserSessionManager.KEY_ID));
                    getProfile(map);
                    bottom_navigation.setSelectedItemId(R.id.navigation_account);
                    addFragment(new UserProfile());
                } else {
                    startActivity(new Intent(Dashboard.this, Delivery_Time_Slot_Activity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }

                break;
            case R.id.nav_Availability:
                if (Status.equals("2")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "Profile");
                    map.put("UserId", user.get(UserSessionManager.KEY_ID));
                    getProfile(map);
                    bottom_navigation.setSelectedItemId(R.id.navigation_account);
                    addFragment(new UserProfile());
                } else {
                    startActivity(new Intent(Dashboard.this, Shop_Information_Activity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.nav_Account:
                Map<String, String> map = new HashMap<>();
                map.put("method", "Profile");
                map.put("UserId", user.get(UserSessionManager.KEY_ID));
                getProfile(map);
                bottom_navigation.setSelectedItemId(R.id.navigation_account);
                UserProfile fragment2 = new UserProfile();
                FragmentManager fragmentManager2 = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction().addToBackStack(null);
                fragmentTransaction2.replace(R.id.frame, fragment2);
                fragmentTransaction2.commit();
                drawer.closeDrawer(GravityCompat.START);
//                startActivity(new Intent(this, SavedAddress.class));
                break;
            case R.id.nav_myShop:
                if (Status.equals("2")) {
                    Map<String, String> map1 = new HashMap<>();
                    map1.put("method", "Profile");
                    map1.put("UserId", user.get(UserSessionManager.KEY_ID));
                    getProfile(map1);
                    bottom_navigation.setSelectedItemId(R.id.navigation_account);
                    addFragment(new UserProfile());
                } else {
                    bottom_navigation.setSelectedItemId(R.id.navigation_categories);
                    ShopDetails fragment1 = new ShopDetails();
                    FragmentManager fragmentManager1 = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction().addToBackStack(null);
                    fragmentTransaction1.replace(R.id.frame, fragment1);
                    fragmentTransaction1.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.nav_terms_and_conditions:
                startActivity(new Intent(Dashboard.this, Support_Activity.class)
                        .putExtra("Status", Status));
                break;
            case R.id.aboutus:
                startActivity(new Intent(Dashboard.this, AboutUs.class)
                        .putExtra("Status", Status));
                break;
            case R.id.nav_B2B:
                startActivity(new Intent(Dashboard.this, Categories.class));
                break;
            case R.id.nav_BulkOrders:
                startActivity(new Intent(Dashboard.this, MyOrders.class));
                break;
            case R.id.nav_savedaddress:
                startActivity(new Intent(Dashboard.this, SavedAddress.class));
                break;
            case R.id.nav_logout:
                builder.setMessage("Do you want to Logout this application ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                session.logoutUser();
                                drawer.closeDrawer(GravityCompat.START);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle(R.string.app_name);
                alert.show();
                break;


        }
        drawer.closeDrawer(GravityCompat.START);

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        /*Fragment fragmentInFrame = (Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.frame);
        if (fragmentInFrame instanceof Home ||
                fragmentInFrame instanceof Categories ||
                fragmentInFrame instanceof Search || fragmentInFrame
                instanceof Offers ||
                fragmentInFrame instanceof Basket) {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                session.logoutUser();
            } else {
                moveTaskToBack(true);
            }
        }
        else {
            super.onBackPressed();
        }*/
    }

    private void getProfile(Map<String, String> map) {
        final Dialog dialog = new Dialog(Dashboard.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UserProfileModel> call = Apis.getAPIService().getProfile(map);
        call.enqueue(new Callback<UserProfileModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<UserProfileModel> call, @NonNull Response<UserProfileModel> response) {
                dialog.dismiss();
                UserProfileModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        for (int i = 0; i < userdata.getResponse().size(); i++) {
                            tv_name.setText(userdata.getResponse().get(i).getFname());
                            tv_email.setText(userdata.getResponse().get(i).getEmail());
                            Log.d("gjh", Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getImage());
                            Picasso.get().load(Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getImage())
                                    .error(R.drawable.no_image)
                                    .priority(Picasso.Priority.HIGH)
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(civProfileimage);
                            Status = userdata.getResponse().get(0).getStatus();
                            if (userdata.getResponse().get(0).getStatus().equals("2")) {
                                bottom_navigation.setSelectedItemId(R.id.navigation_account);
                                addFragment(new UserProfile());
                            }
                            isStoragePermissionGranted1();
                        }
                    } else {
                        Custom_Toast_Activity.makeText(Dashboard.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Dashboard.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserProfileModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public boolean isStoragePermissionGranted1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");

            return true;
        }
    }
}
