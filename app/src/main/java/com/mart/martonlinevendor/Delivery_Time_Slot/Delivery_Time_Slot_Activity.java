package com.mart.martonlinevendor.Delivery_Time_Slot;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Add_Time_Slot_Model;
import com.mart.martonlinevendor.Model.Delete_Time_Slot_Model;
import com.mart.martonlinevendor.Model.Delivery_Time_Slot_Get_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.Swipe_Custom_Class.SwipeRevealLayout;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Delivery_Time_Slot_Activity extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView timeslot_list;
    List<Delivery_Time_Slot_Get_Model.Deliveryslot> array_time_slot_list;
    UserSessionManager session;
    HashMap<String, String> user;
    private LinearLayout no_data_found;
    private Button add_time_slot;
    private String[] AM_TIME = {"1AM", "2AM", "3AM", "4AM", "5AM","6AM","7AM","8AM","9AM","10AM","11AM","12AM","1PM", "2PM", "3PM", "4PM", "5PM","6PM","7PM","8PM","9PM","10PM","11PM","12PM"};
    private String[] PM_TIME = {"1PM", "2PM", "3PM", "4PM", "5PM","6PM","7PM","8PM","9PM","10PM","11PM","12PM","1AM", "2AM", "3AM", "4AM", "5AM","6AM","7AM","8AM","9AM","10AM","11AM","12AM"};
  String amtime,pmtime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery__time__slot_);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }
    private void getTimeSlot(Map<String, String> map) {
        final Dialog dialog = new Dialog(Delivery_Time_Slot_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Delivery_Time_Slot_Get_Model> call = Apis.getAPIService().getTimeSlot(map);
        call.enqueue(new Callback<Delivery_Time_Slot_Get_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull final Call<Delivery_Time_Slot_Get_Model> call, @NonNull Response<Delivery_Time_Slot_Get_Model> response) {
                dialog.dismiss();
                Delivery_Time_Slot_Get_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_time_slot_list = new ArrayList<>();
                        array_time_slot_list.addAll(userdata.getDeliveryslot());
                        if (array_time_slot_list.isEmpty()) {
                            timeslot_list.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);
                        } else {
                            timeslot_list.setVisibility(View.VISIBLE);
                            no_data_found.setVisibility(View.GONE);
                            Time_SlotAdapter time_slotAdapter = new Time_SlotAdapter();
                            timeslot_list.setAdapter(time_slotAdapter);
                        }

                    } else {
                        Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }


            @Override
            public void onFailure(@NonNull Call<Delivery_Time_Slot_Get_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Delete_TimeSlot(Map<String, String> map) {
        final Dialog dialog = new Dialog(Delivery_Time_Slot_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Delete_Time_Slot_Model> call = Apis.getAPIService().deleteTimeslot(map);
        call.enqueue(new Callback<Delete_Time_Slot_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Delete_Time_Slot_Model> call, @NonNull Response<Delete_Time_Slot_Model> response) {
                dialog.dismiss();
                Delete_Time_Slot_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "getdeliveryslot");
                        map1.put("userId",user.get(UserSessionManager.KEY_ID));
                        getTimeSlot(map1);
                    } else {
                        Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Delete_Time_Slot_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public class Time_SlotAdapter extends RecyclerView.Adapter<Time_SlotAdapter.ViewHolder> {


        public Time_SlotAdapter() {

        }

        @Override
        public Time_SlotAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.time_slot_adapter, parent, false);
            Time_SlotAdapter.ViewHolder viewHolder = new Time_SlotAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Time_SlotAdapter.ViewHolder holder, final int position) {
            holder.time.setText(array_time_slot_list.get(position).getName());
           holder.delivery_id.setText(array_time_slot_list.get(position).getId());

            holder.delete_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(Delivery_Time_Slot_Activity.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText("Do You Want to Delete this Delivery Time Slot?");
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "deletedeliverslot");
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("deliverslotId",holder.delivery_id.getText().toString());
                            Delete_TimeSlot(map);
                        }
                    }).show();

                }
            });
        }

        @Override
        public int getItemCount() {
            return array_time_slot_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView close;
            private AppCompatTextView time,delivery_id;
            private FrameLayout delete_layout;
            private SwipeRevealLayout swipe_layout;
            public ViewHolder(View itemView) {
                super(itemView);
                this.time = itemView.findViewById(R.id.time);
                this.delivery_id = itemView.findViewById(R.id.delivery_id);
                this.delete_layout = itemView.findViewById(R.id.delete_layout);
                this.swipe_layout = itemView.findViewById(R.id.swipe_layout);
            }
        }
    }
    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        add_time_slot = findViewById(R.id.add_time_slot);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        no_data_found = findViewById(R.id.no_data_found);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        timeslot_list = findViewById(R.id.timeslot_list);
        Map<String, String> map1 = new HashMap<>();
        map1.put("method", "getdeliveryslot");
        map1.put("userId",user.get(UserSessionManager.KEY_ID));
        getTimeSlot(map1);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Add_Time_Slot_dialog();
                startActivity(new Intent(Delivery_Time_Slot_Activity.this, Notification_Screen.class));

            }
        });
        add_time_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Add_Time_Slot_dialog();
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Delivery_Time_Slot_Activity.this, SearchProduct.class));

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void Add_Time_Slot_dialog() {
        final Dialog dialog = new Dialog(Delivery_Time_Slot_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_time_slot_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final EditText et_timeslot1=dialog.findViewById(R.id.et_timeslot1);
        Button btn_submit=dialog.findViewById(R.id.btn_submit);
        Spinner spinnertime_1=dialog.findViewById(R.id.spinnertime_1);
        Spinner spinnertime=dialog.findViewById(R.id.spinnertime);
        spinnertime.setAdapter(new AMTime(Delivery_Time_Slot_Activity.this, R.layout.spinner_list_text, AM_TIME));
        spinnertime_1.setAdapter(new PMTime(Delivery_Time_Slot_Activity.this, R.layout.spinner_list_text, PM_TIME));
        spinnertime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here
                 amtime = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        spinnertime_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here
                 pmtime = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Map<String, String> map1 = new HashMap<>();
                map1.put("method", "adddeliveryslot");
                map1.put("deliverslot",amtime +" To "+pmtime);
                map1.put("userId",user.get(UserSessionManager.KEY_ID));
                Add_Time_Slot(map1);
//                if(et_timeslot1.getText().toString().length()>0){
//                    dialog.dismiss();
//                    Map<String, String> map1 = new HashMap<>();
//                    map1.put("method", "adddeliveryslot");
//                    map1.put("deliverslot", amtime +" To "+pmtime);
//                    map1.put("userId",user.get(UserSessionManager.KEY_ID));
//                    Add_Time_Slot(map1);
//                }else {
//                    Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, "Enter Delivery Time Slot", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
//
//                }
            }
        });
        dialog.show();
    }


    private void Add_Time_Slot(Map<String, String> map) {
        final Dialog dialog = new Dialog(Delivery_Time_Slot_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Time_Slot_Model> call = Apis.getAPIService().addTimeSlot(map);
        call.enqueue(new Callback<Add_Time_Slot_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull final Call<Add_Time_Slot_Model> call, @NonNull Response<Add_Time_Slot_Model> response) {
                dialog.dismiss();
                Add_Time_Slot_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "getdeliveryslot");
                        map1.put("userId",user.get(UserSessionManager.KEY_ID));
                        getTimeSlot(map1);
                    } else {
                        Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Delivery_Time_Slot_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Time_Slot_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public class AMTime extends ArrayAdapter<String> {
        public AMTime(Context ctx, int txtViewResourceId, String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.spinner_list_text, parent, false);
            TextView spinner_text = mySpinner.findViewById(R.id.spinner_text);
            spinner_text.setText(AM_TIME[position]);
            return mySpinner;
        }
    }
    public class PMTime extends ArrayAdapter<String> {
        public PMTime(Context ctx, int txtViewResourceId, String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.spinner_list_text, parent, false);
            TextView spinner_text = mySpinner.findViewById(R.id.spinner_text);
            spinner_text.setText(PM_TIME[position]);
            return mySpinner;
        }
    }
}
