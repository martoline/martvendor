package com.mart.martonlinevendor.Download_CSV;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.R;


public class DownloadReceiver extends ResultReceiver {
    SeekBar progressBar;
    Context context;
    String url;

    public DownloadReceiver(Handler handler, SeekBar pbprogress, Context context,String url) {
        super(handler);
        this.progressBar = pbprogress;
        this.context = context;
        this.url = url;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        super.onReceiveResult(resultCode, resultData);

        if (resultCode == DownloadService.UPDATE_PROGRESS) {

            int progress = resultData.getInt("progress"); //get the progress
//            dialog.setProgress(progress);
            progressBar.setProgress(progress);
            Log.d("jhdgs", progress + "");

            if (progress == 100) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(progressBar, "CSV Sample File Download Successfully You can see this file in Your Internal Storage"+"Mart O Vendor Folder",
                        Snackbar.LENGTH_LONG)
                        .setActionTextColor(ContextCompat.getColor(
                                context, R.color.colorWhite))
                        .setAction("CLOSE", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        })
                        .setBackgroundTint(ContextCompat.getColor(context, R.color.colorAccent))
                        .show();
               // context.startActivity(new Intent(context, Dasboard_Screen.class));
//                int a = Integer.parseInt(Utility.getCounter(context));
//                if (Utility.getVideo(context, id).equals("0")) {
//                    int b = a + 1;
//                    Utility.setcounter(context, b);
//                    Log.d("jdkshf", b + "");
//                } else {
//
//                }
//                progressBar.setVisibility(View.GONE);
//                Utility.setVideo(context, id, "/data/data/com.lals.lalsacademy/files/" + id + ".mp4",video_name);
//                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_correct));

            }
        }
    }
}