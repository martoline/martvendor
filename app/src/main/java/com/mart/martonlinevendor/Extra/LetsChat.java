package com.mart.martonlinevendor.Extra;

import android.app.Application;
import android.content.Context;

import com.mart.martonlinevendor.awen.camera.CameraApplication;

/**
 * Created by HP on 3/21/2018.
 */

public class LetsChat extends Application {
    private static Context mContext;


    public static Context getAppContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CameraApplication.init(this,true);
        mContext = getApplicationContext();
    }
}
