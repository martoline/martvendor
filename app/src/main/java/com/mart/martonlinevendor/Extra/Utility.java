package com.mart.martonlinevendor.Extra;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.regex.Pattern;


public class Utility {
    public static SharedPreferences getPref() {
        return LetsChat.getAppContext().getSharedPreferences("LalsAcademy", Context.MODE_PRIVATE);
    }

    public static boolean isLogin() {
        return getPref().getBoolean("isalwaysLogin", false);
    }

    public static void saveNumber( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("number", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }

    public static void saveUserID( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("user_id", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }
    public static void saveNotification( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("Notification", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }

    public static void latlng( Double lat, Double lng) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("lat", String.valueOf(lat));
        editor.putString("lng", String.valueOf(lng));

        editor.apply();
    }

    public static String getlat() {
        return getPref().getString("lat", "0.0");
    }
    public static String getNotification() {
        return getPref().getString("Notification", "0");
    }

    public static String getlng() {
        return getPref().getString("lng", "0.0");
    }

    public static String getpic() {
        return getPref().getString("url", "");
    }


    public static void logout() {
        getPref().edit().clear().apply();
    }

    public static String getLoginNumber() {
        return getPref().getString("number", "xxxxxxxxxx");
    }

    public static String getLoginId() {
        return getPref().getString("user_id", "");
    }

    public static String getagent_id() {
        return getPref().getString("agent_id", "");
    }

    public static String getLoginFullName() {
        return getPref().getString("name", "Edso");
    }


    public static String getLoginEmail() {
        return getPref().getString("email", "");
    }


    public static String getdob() {
        return getPref().getString("dob", "");
    }


    public static String getLoginPhone() {
        return getPref().getString("number", "");
    }

    public static String getstate() {
        return getPref().getString("state", "");
    }

    public static String getGenter() {
        return getPref().getString("gender", "");
    }

    public static String getreligion() {
        return getPref().getString("religion", "");
    }

    public static String getEmail() {
        return getPref().getString("email", "");
    }

    public static String getPassword() {
        return getPref().getString("password", "");
    }

    public static void setdetail( String email, String password) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.apply();

    }

    public static String email() {
        return getPref().getString("email", "");
    }


  /*  public static void setDashBoardData( LoginModel.UserdataBean data) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("email", data.getEmail());
        editor.putString("fname", data.getFname());
        editor.putString("lname", data.getLname());
        editor.putString("gender", data.getGender());
        editor.putString("dob", data.getDob());
        editor.putString("phone", data.getPhone());
        editor.putString("state", data.getState());
        editor.putString("religion", data.getReligion());
        editor.putString("MotherTongue", data.getMotherTongue());
        editor.apply();

    }*/


    public static void setlogin() {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isalwaysLogin", true);
        editor.apply();

    }

    public static void setloginID( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("user_id", id);
        editor.apply();

    }
    public static void setFirbaseID( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("firbase_user_id", id);
        editor.apply();

    }
    public static void setAgentID( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("agent_id", id);
        editor.apply();

    }

    public static void setUserName( String fName, String Emnail) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("name", fName);
        editor.putString("Email", Emnail);
        editor.apply();

    }

    public static void setProfilePic( String image) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("profile_image", image);
        editor.apply();

    }


    public static String getUserName() {
        return getPref().getString("name", "");
    }
    public static String getFirbaseID() {
        return getPref().getString("firbase_user_id", "");
    }
    public static String getUserEmail() {
        return getPref().getString("Email", "");
    }

    public static String getProfilePic() {
        return getPref().getString("profile_image", "");
    }


    public static void setlogout() {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isalwaysLogin", false);
        editor.clear();
        editor.apply();

    }


    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    static public void setVibes( Object modal) {
        /**** storing object in preferences  ****/
        SharedPreferences.Editor editor = getPref().edit();
        Gson gson = new Gson();
        String jsonObject = gson.toJson(modal);
        editor.putString("vibes", jsonObject);
        editor.commit();

    }

    public static void setGender(String gender) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("gender", gender);
        editor.apply();
    }

    public static String getGender() {
        return getPref().getString("gender", "male");
    }

}
