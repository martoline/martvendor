package com.mart.martonlinevendor.Fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.Model.Vendor_Category_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.OrderDetail.Order_History_Details;
import com.mart.martonlinevendor.Product_Sub_Cat_Activity.Sub_Categories;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends Fragment implements View.OnClickListener {

    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;

    private TabLayout tablayout;
    private ViewPager viewPagerProducts;

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmenTitle = new ArrayList<>();
    String mainid;
    String CategoryId;
    TextView no_found;
    RecyclerView category_list;
    UserSessionManager session;
    HashMap<String, String> user;
    //  List<Categories_Model.Response> array_categories_list;
    List<Vendor_Category_Model.Response> array_categories_list;
    List<Categories_Model.SubCatData> array_sub_catgories_list;
    RecyclerView order_list;
    MyOrdersAdapter myOrdersAdapter;
    List<My_Order_Model.Response> array_order_list;
    private TextView title_order;
    Dialog dialog;
    Context context;
    LinearLayout no_data_found;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        dialog = new Dialog(context);
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        initView(view);
        return view;
    }

    //get data for viewpager tablayout
//    public void getData() {
//       /* Categories_Model vibesModel = (Categories_Model) Uitility_Activity.getCategories(getActivity());
//        categories_models = new ArrayList<>();
//        categories_models.clear();
//        categories_models = vibesModel.getResponse();
//        if (categories_models.size() > 0) {*/
//        fragmenTitle.clear();
//        fragmentList.clear();
//        for (int i = 0; i < 5; i++) {
//            for (int j = 0; j < 5; j++) {
//                for (int i1 = 0; i1 < 5; i1++) {
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("product_id", "123");
//                    bundle.putString("name", "Fruit");
//                    Fragment fragment = new ProductsViewPager();
//                    fragmenTitle.add("Grocery");
//                    fragment.setArguments(bundle);
//                    fragmentList.add(fragment);
//                }
//            }
//
//        }
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity(), getChildFragmentManager(), fragmentList, fragmenTitle);
//        viewPagerProducts.setAdapter(adapter);
//        tablayout.setupWithViewPager(viewPagerProducts);
//
//    }


    public void initView(View view) {
        ImageView iv_menu = view.findViewById(R.id.iv_menu);
        category_list = view.findViewById(R.id.category_list);
        order_list = view.findViewById(R.id.order_list);
        no_data_found = view.findViewById(R.id.no_data_found);
        title_order = view.findViewById(R.id.title_order);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.
                navDrawer.openDrawer(GravityCompat.START);
            }
        });
        iv_voice = view.findViewById(R.id.iv_voice);
        et_search = view.findViewById(R.id.et_search);

        no_found = view.findViewById(R.id.no_found);
        iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Notification_Screen.class));
            }
        });
      /*  array_sub_categories = new ArrayList<>();
        ssubCatDataList = new ArrayList<>();*/
        iv_voice.setOnClickListener(this);
        viewPagerProducts = view.findViewById(R.id.viewPagerProducts);
        tablayout = view.findViewById(R.id.tablayout);
        et_search.setOnClickListener(this);
        //  getData();
//        viewPagerProducts.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
//        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPagerProducts.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
        Map<String, String> map = new HashMap<>();
        map.put("method", "VendorCategory");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        GetCategories(map);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_voice:
//                Toast.makeText(Products.this, "You clicked on voice search", Toast.LENGTH_LONG).show();
                break;
            case R.id.et_search:
                startActivity(new Intent(getActivity(), SearchProduct.class));
                break;
        }
    }

//    //ViewPager Adapter ----------------------------
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//
//        private Context myContext;
//        private List<Fragment> fragmentList;
//        private List<String> titleList;
//
//        public ViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
//            super(fm);
//            myContext = context;
//            this.fragmentList = fragmentList;
//            this.titleList = titleList;
//        }
//
//        @Nullable
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return titleList.get(position);
//        }
//
//        // this is for fragment tabs
//        @Override
//        public Fragment getItem(int position) {
//            return fragmentList.get(position);
//        }
//
//        // this counts total number of tabs
//        @Override
//        public int getCount() {
//            return fragmentList.size();
//        }
//    }

    private void GetCategories(Map<String, String> map) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Vendor_Category_Model> call = Apis.getAPIService().getVendorCategory(map);
        call.enqueue(new Callback<Vendor_Category_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Vendor_Category_Model> call, @NonNull Response<Vendor_Category_Model> response) {
                // dialog.dismiss();
                Vendor_Category_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_data_found.setVisibility(View.GONE);
                        array_categories_list = new ArrayList<>();
                        array_categories_list.addAll(userdata.getResponse());
                        category_list.setAdapter(new CategoryAdapter());
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "orderList");
                        map1.put("userId", user.get(UserSessionManager.KEY_ID));
                        map1.put("type", "New");
                        getOrderList(map1);
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        Map<String, String> map1 = new HashMap<>();
                        map1.put("method", "orderList");
                        map1.put("userId", user.get(UserSessionManager.KEY_ID));
                        map1.put("type", "New");
                        getOrderList(map1);
                      //  Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Vendor_Category_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


        public CategoryAdapter() {

        }

        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_category, parent, false);
            CategoryAdapter.ViewHolder viewHolder = new CategoryAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
            holder.tv_categoryName.setText(array_categories_list.get(position).getCatName());
            holder.tv_categoryName.setSelected(true);
//            holder.tv_category_details.setText(array_categories_list.get(position).getOffertest());
            Glide.with(getActivity())
                    .load(Apis.IMAGE_PATH + array_categories_list.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_category);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), Sub_Categories.class)
                            .putExtra("cat_id", array_categories_list.get(position).getCatId())
                    );
                }
            });
        }


        @Override
        public int getItemCount() {
            return array_categories_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private RelativeLayout card_shop_category;
            private ImageView iv_category;
            private TextView tv_categoryName;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_shop_category = itemView.findViewById(R.id.card_shop_category);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            }
        }
    }

    private void getOrderList(Map<String, String> map) {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        Call<My_Order_Model> call = Apis.getAPIService().getOrderList(map);
        call.enqueue(new Callback<My_Order_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
                dialog.dismiss();
                My_Order_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getDisplaydialog().equals("1")) {
                        showdialog_appupdate(userdata.getVersion());
                    }
                    if (userdata.getStatus().equals("1")) {
                        // no_data_found.setVisibility(View.GONE);
                        // recyclerView_myOrders.setVisibility(View.VISIBLE);
                        title_order.setVisibility(View.VISIBLE);
                        no_data_found.setVisibility(View.GONE);
                        array_order_list = new ArrayList<>();
                        array_order_list.addAll(userdata.getResponse());
                        myOrdersAdapter = new MyOrdersAdapter();
                        order_list.setAdapter(myOrdersAdapter);
                        myOrdersAdapter.notifyDataSetChanged();
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        title_order.setVisibility(View.GONE);
                        // no_data_found.setVisibility(View.VISIBLE);
                        //recyclerView_myOrders.setVisibility(View.GONE);
                        // no_found.setText("My Order not Found");
                        // Custom_Toast_Activity.makeText(MyOrders.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public void showdialog_appupdate(String versionstring) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_update_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        String version = "";
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Button btnok = dialog.findViewById(R.id.btnok);
        Button btnno = dialog.findViewById(R.id.btnno);
        TextView tvcurrentversion = dialog.findViewById(R.id.tvcurrentversion);
        tvcurrentversion.setText("V." + version);
        TextView tvnewversion = dialog.findViewById(R.id.tvnewversion);
        tvnewversion.setText("V." + versionstring);
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.mart.martonlinevendor")));
            }
        });
        if (versionstring.equals(version)) {

        } else {
            dialog.show();
        }
    }
    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
//        Context context;
//        LayoutInflater inflater;
        MyOrdersAdapter() {
//            this.context = context;
//            this.inflater = LayoutInflater.from(context);
//            this.orderlist = orderlist;
        }

        @Override
        public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders_1, parent, false);
            MyOrdersAdapter.ViewHolder viewHolder = new MyOrdersAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrdersAdapter.ViewHolder holder, final int position) {
            holder.tv_item_order_date.setText(array_order_list.get(position).getDate());
            holder.tv_price.setText("Total: " + "Rs " + array_order_list.get(position).getTotalAmount());
            holder.tv_name.setText(array_order_list.get(position).getOrderId());
            holder.tvusername.setText("By : " + array_order_list.get(position).getUsername());
            holder.tv_panding.setVisibility(View.GONE);
            holder.tv_delivered.setVisibility(View.GONE);
            holder.tv_panding.setText(array_order_list.get(position).getOrderStatus());
            holder.order_rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), Order_History_Details.class)
                            .putExtra("id", array_order_list.get(position).getId())
                            .putExtra("username", array_order_list.get(position).getUsername())
                            .putExtra("date", array_order_list.get(position).getDate()));
                }
            });

//            switch (orderStatus) {
//                case "1":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Pending");
//                    break;
//                case "2":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Accepted");
//
//                    break;
//                case "3":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Under Process");
//                    break;
//                case "4":
//                    holder.tv_delivered.setVisibility(View.VISIBLE);
//                    holder.tv_panding.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Delivered");
//                    break;
//            }
        }

        @Override
        public int getItemCount() {
            return array_order_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private CardView card_myOrders;
            private ImageView iv_item;
            private TextView tv_name, tv_otp;
            private TextView tv_item_order_date;
            private TextView tv_price;
            private TextView tv_panding;
            private TextView tv_delivered;
            private TextView tvusername;
            private RelativeLayout order_rel;

            public ViewHolder(View itemView) {
                super(itemView);
//                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_item_order_date = itemView.findViewById(R.id.tv_item_order_date);
                this.tv_price = itemView.findViewById(R.id.tv_price);
                this.tv_panding = itemView.findViewById(R.id.tv_panding);
                this.tv_delivered = itemView.findViewById(R.id.tv_delivered);
                this.order_rel = itemView.findViewById(R.id.order_rel);
                this.tvusername = itemView.findViewById(R.id.tvusername);
            }
        }
    }
}
