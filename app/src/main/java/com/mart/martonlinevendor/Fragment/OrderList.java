package com.mart.martonlinevendor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.OrderDetail.OrderByStatus;
import com.mart.martonlinevendor.Order_Fragment.Delivered_Fragment;
import com.mart.martonlinevendor.Order_Fragment.On_the_Way_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Pending_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Ready_to_Ship_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Status_Change_Fragment;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;

import java.util.HashMap;
import java.util.List;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

public class OrderList extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView_myOrders;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
  //  MyOrdersAdapter myOrdersAdapter;
    private TabLayout tablayout;
    DasboardViewPagerAdapter dasboardViewPagerAdapter;
//    List<My_Order_Model.Response> array_order_list;

    HashMap<String, String> user;
    String producttitle;
    String image;
    String itmeprice;
    String orderStatus;
    List<My_Order_Model.Response> array_order_list;
    private LinearLayout no_found;
    UserSessionManager session;
    ViewPager viewPagerProducts;
    Pending_Fragment pending_fragment;
    Ready_to_Ship_Fragment ready_to_ship_fragment;
    On_the_Way_Fragment on_the_way_fragment;
    Delivered_Fragment delivered_fragment;
    Status_Change_Fragment status_change_fragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_order_list, container, false);
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();

        startActivity(new Intent(getActivity(), OrderByStatus.class));
        //initView(view);
        return view;
    }

    public void initView(View view) {
        ImageView iv_menu = view.findViewById(R.id.iv_menu);
        tablayout=view.findViewById(R.id.tablayout);
        viewPagerProducts=view.findViewById(R.id.viewPagerProducts);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.
                navDrawer.openDrawer(GravityCompat.START);
            }
        });
       ImageView iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Notification_Screen.class));
            }
        });
        no_found = view.findViewById(R.id.no_data_found);
        tv_currentlocation =  view.findViewById(R.id.tv_currentlocation);
        iv_bellnotification =  view.findViewById(R.id.iv_bellnotification);
        recyclerView_myOrders =  view.findViewById(R.id.recyclerView_myOrders);

        iv_bellnotification.setOnClickListener(this);
        tablayout.addTab(tablayout.newTab().setText("Pending"));
        tablayout.addTab(tablayout.newTab().setText("Ready To Ship"));
        tablayout.addTab(tablayout.newTab().setText("On The Way"));
        tablayout.addTab(tablayout.newTab().setText("Delivered"));
        tablayout.addTab(tablayout.newTab().setText("Status Change"));
        tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setupViewPager(viewPagerProducts);
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerProducts.setCurrentItem(tab.getPosition(), false);
                switch (tab.getPosition()) {
                    case 0:
                        Log.e("TAG", "TAB1");
//                        tablayout.setBackground(getDrawable(R.drawable.tab_background));
                        //   tablayout.setTabTextColors(ContextCompat.getColorStateList(Dasboard_Screen.this, R.color.tab_selected));
                        //tablayout.setTabTextColors(Color.parseColor("#DA251C"), Color.parseColor("#DA251C"));
                        break;
                    case 1:

                       // tablayout.setBackground(getDrawable(R.drawable.tab_background));
                        break;
                    case 2:
                      //  tablayout.setBackground(getDrawable(R.drawable.tab_background));
                        break;
                    case 3:
                     //   tablayout.setBackground(getDrawable(R.drawable.tab_background));
                        //  int tabIconColor3 = ContextCompat.getColor(Service_Details.this, R.color.tab_selected);
                        // tab.getIcon().setColorFilter(tabIconColor3, PorterDuff.Mode.SRC_IN);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
               // tablayout.setBackground(getDrawable(R.drawable.tab_background));
                //   tablayout.setTabTextColors(ContextCompat.getColorStateList(Dasboard_Screen.this, R.color.tab_unselected));


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPagerProducts.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tablayout.getTabAt(position).select();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_back:
//                onBackPressed();
                break;

            case R.id.tv_currentlocation:
//                Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;

            case R.id.iv_bellnotification:
//                startActivity(new Intent(this, Notification.class));
                break;
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        dasboardViewPagerAdapter = new DasboardViewPagerAdapter(getActivity().getSupportFragmentManager());
        pending_fragment = new Pending_Fragment();
        ready_to_ship_fragment = new Ready_to_Ship_Fragment();
        on_the_way_fragment = new On_the_Way_Fragment();
        delivered_fragment = new Delivered_Fragment();
        status_change_fragment =new Status_Change_Fragment();
        dasboardViewPagerAdapter.addFragment(pending_fragment, "HOME");
        dasboardViewPagerAdapter.addFragment(ready_to_ship_fragment, "INSURANCE");
        dasboardViewPagerAdapter.addFragment(on_the_way_fragment, "SERVICES");
        dasboardViewPagerAdapter.addFragment(delivered_fragment, "SUPPORT");
        dasboardViewPagerAdapter.addFragment(status_change_fragment, "SUPPORT");
        viewPager.setAdapter(dasboardViewPagerAdapter);
    }
////
//    private void getOrderList(Map<String, String> map) {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<My_Order_Model> call = Apis.getAPIService().getOrderList(map);
//        call.enqueue(new Callback<My_Order_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
//                dialog.dismiss();
//                My_Order_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        no_found.setVisibility(View.GONE);
//                        recyclerView_myOrders.setVisibility(View.VISIBLE);
//                        array_order_list = new ArrayList<>();
//                        array_order_list.addAll(userdata.getResponse());
//                        myOrdersAdapter = new MyOrdersAdapter(getActivity());
//                        recyclerView_myOrders.setAdapter(myOrdersAdapter);
//                        myOrdersAdapter.notifyDataSetChanged();
//                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//                    } else {
//                        no_found.setVisibility(View.VISIBLE);
//                        recyclerView_myOrders.setVisibility(View.GONE);
//                        // no_found.setText("My Order not Found");
//                        // Custom_Toast_Activity.makeText(MyOrders.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
//                    }
//                }
//            }
//            @Override
//            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }
//
//    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
//        //        List<My_Order_Model.Response> orderlist;
//        Context context;
//        LayoutInflater inflater;
//
//        MyOrdersAdapter(Context context) {
//            this.context = context;
//            this.inflater = LayoutInflater.from(context);
////            this.orderlist = orderlist;
//        }
//
//        @Override
//        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//            View listItem = layoutInflater.inflate(R.layout.list_my_orders, parent, false);
//            ViewHolder viewHolder = new ViewHolder(listItem);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(final MyOrdersAdapter.ViewHolder holder, final int position) {
//            holder.tv_item_order_date.setText(array_order_list.get(position).getDate());
//            holder.tv_price.setText("Total: " + "Rs " + array_order_list.get(position).getTotalAmount());
//            holder.tv_name.setText(array_order_list.get(position).getOrderId());
//            orderStatus = array_order_list.get(position).getOrderStatus();
//            holder.tvusername.setText( array_order_list.get(position).getUsername());
//           /* holder.order_rel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
//                            .putExtra("id", array_order_list.get(position).getId()));
//                }
//            });
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
//                            .putExtra("id", array_order_list.get(position).getId()));
//                }
//            });
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(getActivity(), OrderByStatus.class)
//                            .putExtra("id","11"));
//                }
//            });*/
////             <option value="1">Pending</option>
////          <option value="2">Accepted</option>
////          <option value="3" selected="">Under Process</option>
////          <option value="4">Delivered</option>
//
//            switch (orderStatus) {
//                case "1":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Pending");
//                    break;
//                case "2":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Accepted");
//
//                    break;
//                case "3":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Under Process");
//                    break;
//                case "4":
//                    holder.tv_delivered.setVisibility(View.VISIBLE);
//                    holder.tv_panding.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Delivered");
//                    break;
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return array_order_list.size();
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//
//            private CardView card_myOrders;
//            private ImageView iv_item;
//            private TextView tv_name,tv_otp;
//            private TextView tv_item_order_date;
//            private TextView tv_price;
//            private TextView tv_panding;
//            private TextView tv_delivered;
//            private TextView tvusername;
//            private RelativeLayout order_rel;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
//                this.iv_item = itemView.findViewById(R.id.iv_item);
//                this.tv_name = itemView.findViewById(R.id.tv_name);
//                this.tv_item_order_date = itemView.findViewById(R.id.tv_item_order_date);
//                this.tv_price = itemView.findViewById(R.id.tv_price);
//                this.tv_panding = itemView.findViewById(R.id.tv_panding);
//                this.tv_delivered = itemView.findViewById(R.id.tv_delivered);
//                this.order_rel = itemView.findViewById(R.id.order_rel);
//                this.tvusername = itemView.findViewById(R.id.tvusername);
//            }
//        }
//    }
}
