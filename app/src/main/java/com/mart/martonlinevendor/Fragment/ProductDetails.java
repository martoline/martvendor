package com.mart.martonlinevendor.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Product_Detail_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetails extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_product_full_image;
    private RecyclerView recycler_product_images;
    List<Drawable> list_image;
    private TextView tv_price;
    private TextView tv_old_price;
    private TextView tv_off;
    private LinearLayout tv_remove_item;
    private TextView tv_item_count;
    private TextView tv_add;
    private LinearLayout tv_add_item;
    private TextView tv_product_description;

    List<Product_Detail_Model.Size_chart> chart_list;
    private ImageView iv_back;
    private TextView tv_currentlocation;
    private ImageView iv_bellnotification;
    UserSessionManager session;
    public int counter;
    String product_id;
    private RelativeLayout rl_header;
    private TextView product_title;
    private TextView selling_product_prize;
    ProductImagesAdapter productImagesAdapter;

    HashMap<String, String> user;
    private Spinner quantity;
    //    List<Product_Detail_Model.Size_chart> chart_list;
    ArrayList<String> Quantity;
    String Product_id, quantity_count;
    String Size_id;
    AlertDialog.Builder builder;
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private RelativeLayout header;
    private LinearLayout sdf;
    private TextView size_quantity,vendor_shop,country;
   String Size_name;
   TextView outof_stock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        session = new UserSessionManager(ProductDetails.this);
        user = session.getUserDetails();
        product_id = getIntent().getStringExtra("product_id");
        quantity_count = "0";
        builder = new AlertDialog.Builder(this);

        initView();
    }

    public void initView() {
        size_quantity = findViewById(R.id.size_quantity);
        vendor_shop = findViewById(R.id.vendor_shop);
        outof_stock = findViewById(R.id.outof_stock);
        country = findViewById(R.id.country);
        iv_product_full_image = findViewById(R.id.iv_product_full_image);
        quantity = findViewById(R.id.quantity);
        recycler_product_images = findViewById(R.id.recycler_product_images);
        tv_price = findViewById(R.id.tv_price);
        tv_old_price = findViewById(R.id.tv_old_price);
        tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tv_off = findViewById(R.id.tv_off);
        tv_remove_item = findViewById(R.id.tv_remove_item);
        tv_item_count = findViewById(R.id.tv_item_count);
        tv_add = findViewById(R.id.tv_add);
        tv_add_item = findViewById(R.id.tv_add_item);
        tv_product_description = findViewById(R.id.tv_product_description);
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(this);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        iv_send.setOnClickListener(this);
        et_search = (TextView) findViewById(R.id.et_search);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetails.this, SearchProduct.class));
            }
        });
        iv_bellnotification =  findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    startActivity(new Intent(ProductDetails.this, Notification_Screen.class));
            }
        });
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_voice.setOnClickListener(this);
        header = (RelativeLayout) findViewById(R.id.header);
        header.setOnClickListener(this);
        sdf = (LinearLayout) findViewById(R.id.sdf);
        sdf.setOnClickListener(this);
        tv_add_item.setOnClickListener(this);
        tv_remove_item.setOnClickListener(this);

/*
        tv_currentlocation = findViewById(R.id.tv_currentlocation);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);

        tv_currentlocation.setOnClickListener(this);
        iv_bellnotification.setOnClickListener(this);*/

        iv_product_full_image.setOnClickListener(this);
        /*rl_header = (RelativeLayout) findViewById(R.id.rl_header);
        rl_header.setOnClickListener(this);*/
        product_title = (TextView) findViewById(R.id.product_title);
        product_title.setOnClickListener(this);
        selling_product_prize = (TextView) findViewById(R.id.selling_product_prize);
        selling_product_prize.setOnClickListener(this);

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    builder.setMessage("You need to Register to use this feature?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                               //     startActivity(new Intent(ProductDetails.this, Loginption.class));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle(R.string.app_name);
                    alert.show();
                } else {
                    tv_add.setVisibility(View.GONE);
                    tv_remove_item.setVisibility(View.VISIBLE);
                    tv_add_item.setVisibility(View.VISIBLE);
                    tv_item_count.setVisibility(View.VISIBLE);
                    tv_item_count.setText(quantity_count + "");
                }

            }
        });


        quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selling_product_prize.setText("Rs " + chart_list.get(i).getSelling_price());
                tv_old_price.setText(chart_list.get(i).getActual_Price());
                Size_id = chart_list.get(i).getSize_id();
                Size_name=chart_list.get(i).getSize();

                Double Actual_Ammount = Double.parseDouble(chart_list.get(i).getActual_Price());
                Double Selling_Ammount = Double.parseDouble(chart_list.get(i).getSelling_price());
                Double remaining = Actual_Ammount - Selling_Ammount;
                Double totaldis = (remaining / Actual_Ammount) * 100;
                String value = new DecimalFormat("##").format(totaldis);
                tv_off.setText(value + "% off");
                tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

//        productImagesAdapter = new ProductImagesAdapter(ProductDetails.this);
//        recycler_product_images.setAdapter(productImagesAdapter);
        tv_item_count.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (counter != 0) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "AddToCart");
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("Pid", Product_id);
                    map.put("Qty", counter + "");
                    map.put("size", Size_id);
                    //user.get(UserSessionManager.KEY_ID)
                    //Add_Cart(map);
                }
            }
        });

        tv_item_count.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (counter != 0) {
                    Map<String, String> map = new HashMap<>();
                    map.put("method", "AddToCart");
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("Pid", Product_id);
                    map.put("Qty", counter + "");
                    map.put("size", Size_id);
                    //user.get(UserSessionManager.KEY_ID)
                    //Add_Cart(map);
                }
            }
        });
        if (quantity_count.equals("0")) {
            tv_add.setVisibility(View.VISIBLE);
            tv_remove_item.setVisibility(View.GONE);
            tv_add_item.setVisibility(View.GONE);
            tv_item_count.setVisibility(View.GONE);
        } else {
            tv_add.setVisibility(View.GONE);
            tv_remove_item.setVisibility(View.VISIBLE);
            tv_item_count.setVisibility(View.VISIBLE);
            tv_item_count.setText(quantity_count);
        }


        Map<String, String> map1 = new HashMap<>();
        map1.put("method", "Product_Details");
        map1.put("ProductId", product_id);
        //user.get(UserSessionManager.KEY_ID)
        getProductDetail(map1);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_add_item:
               /* if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    builder.setMessage("You need to Register to use this feature?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(ProductDetails.this, SignIn.class));
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle(R.string.app_name);
                    alert.show();
                } else {*/
                // tv_add.setVisibility(View.GONE);
                tv_add.setVisibility(View.GONE);
                tv_remove_item.setVisibility(View.VISIBLE);
                tv_item_count.setVisibility(View.VISIBLE);
                counter = Integer.parseInt(tv_item_count.getText().toString());
                counter++;
                tv_item_count.setText("" + counter);


                break;
            case R.id.tv_remove_item:

                counter = Integer.parseInt(tv_item_count.getText().toString());
                if (counter > 0) {
                    counter--;
                    tv_item_count.setText("" + counter);
                } else {
                    tv_add.setVisibility(View.VISIBLE);
                    tv_remove_item.setVisibility(View.GONE);
                    tv_item_count.setVisibility(View.GONE);
                    tv_add_item.setVisibility(View.GONE);
                    Toast.makeText(ProductDetails.this, "You haven't select any of the item", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.iv_menu:
                finish();
                break;

            case R.id.tv_currentlocation:
                Toast.makeText(this, "You clicked on Current Location", Toast.LENGTH_LONG).show();
                break;

            case R.id.iv_bellnotification:
                startActivity(new Intent(ProductDetails.this, Notification.class));
                break;

            case R.id.iv_product_full_image:
                startActivity(new Intent(ProductDetails.this, ProductImageView.class).putExtra("product_id", product_id));
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    interface UpdateCallback {
        public void update(String s);
    }


    private void getProductDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(ProductDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_Detail_Model> call = Apis.getAPIService().getProductDetails(map);
        call.enqueue(new Callback<Product_Detail_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_Detail_Model> call, @NonNull Response<Product_Detail_Model> response) {
                dialog.dismiss();
                Product_Detail_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        product_title.setText(userdata.getResponse().get(0).getProduct_Name());
                        Product_id = userdata.getResponse().get(0).getProduct_id();
                        tv_product_description.setText(userdata.getResponse().get(0).getDiscription());
                        selling_product_prize.setText("Rs" + userdata.getResponse().get(0).getOfferprice());
                        tv_old_price.setText("Rs " + userdata.getResponse().get(0).getMrp());
                        vendor_shop.setText("Vendor : " + userdata.getResponse().get(0).getShopname());
                        country.setText(userdata.getResponse().get(0).getCountry());
                        Double Actual_Ammount = Double.parseDouble(userdata.getResponse().get(0).getMrp());
                        Double Selling_Ammount = Double.parseDouble(userdata.getResponse().get(0).getOfferprice());
                        Double remaining = Actual_Ammount - Selling_Ammount;
                        // Sizeid = sizelist.get(i).getSize_id();
                        Double totaldis = (remaining / Actual_Ammount) * 100;
                        String value = new DecimalFormat("##").format(totaldis);
                        if (value.equals("0")) {
                            tv_off.setVisibility(View.GONE);
                        } else {
                            tv_off.setVisibility(View.VISIBLE);
                        }
                        tv_off.setText(value + "% off");

                        tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        //  Quantity=new ArrayList<>();
                        chart_list = new ArrayList<>();
                        chart_list.addAll(userdata.getResponse().get(0).getSize_chart());
                        Quntity_Adapter adapter = new Quntity_Adapter(ProductDetails.this,
                                R.layout.spinner_list_text, R.id.title, chart_list);
                        quantity.setAdapter(adapter);
                         if (chart_list.isEmpty()){

                         }else {
                             size_quantity.setText(chart_list.get(0).getSize());
                         }
                         if (userdata.getResponse().get(0).getQty().equals("0")){
                             outof_stock.setText("Out of Stock");
                         }else {
                             outof_stock.setVisibility(View.GONE);
                         }
                        if (userdata.getResponse().get(0).getImages().size() != 0) {
                            final List<Product_Detail_Model.Images> array_image_list = new ArrayList<>();
                            array_image_list.addAll(userdata.getResponse().get(0).getImages());
                            productImagesAdapter = new ProductImagesAdapter(ProductDetails.this, array_image_list);
                            recycler_product_images.setAdapter(productImagesAdapter);
                        }
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_Detail_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("quantity_count", tv_item_count.getText().toString());
        intent.putExtra("product_id", getIntent().getStringExtra("product_id"));
        setResult(RESULT_OK, intent);
        finish();


    }

    public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {

        List<Product_Detail_Model.Images> imagelist;
        Context context;
        LayoutInflater inflater;

        public ProductImagesAdapter(Context context, List<Product_Detail_Model.Images> imagelist) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.imagelist = imagelist;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_product_images, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_product_images);

            Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(0).getProduct_Image())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_full_image);

            holder.iv_product_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //image handling here
                    Picasso.get().load(Apis.IMAGE_PATH + imagelist.get(position).getProduct_Image())
                            .error(R.drawable.no_image)
                            .priority(Picasso.Priority.HIGH)
                            .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_product_full_image);
                }
            });
        }

        @Override
        public int getItemCount() {
            return imagelist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView iv_product_images;

            public ViewHolder(View itemView) {

                super(itemView);
                this.iv_product_images = itemView.findViewById(R.id.iv_product_images);
            }
        }
    }

    public class Quntity_Adapter extends ArrayAdapter<Product_Detail_Model.Size_chart> {

        LayoutInflater flater;

        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<Product_Detail_Model.Size_chart> list) {

            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return rowview(convertView, position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(convertView, position);
        }

        private View rowview(View convertView, int position) {

            final Product_Detail_Model.Size_chart rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
                holder.spinner_text = (TextView) rowview.findViewById(R.id.spinner_text);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            holder.spinner_text.setText(rowItem.getSize());

            return rowview;
        }

        private class viewHolder {
            TextView spinner_text;
        }
    }






}
