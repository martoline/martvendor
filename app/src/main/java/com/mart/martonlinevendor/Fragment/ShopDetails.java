package com.mart.martonlinevendor.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.ShopDetailModel;
import com.mart.martonlinevendor.Product_Sub_Cat_Activity.Sub_Categories;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.Shop_Information.Shop_Information_Activity;
import com.mart.martonlinevendor.Vendor_Add_Category.Add_Category;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopDetails extends Fragment {
    String quantity_count = "0";
    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private ImageView iv_other_profile;
    private TextView tv_user_bio;
    private TextView tvuser_name;
    private TextView tv_respect;
    private TextView respect;
    private LinearLayout rl_respect;
    private TextView tv_follower,tvviewAll;
    private TextView follower;
    private TextView shoptime;
    private LinearLayout rl_follower;
    private TextView tv_following;
    private TextView following;
    private LinearLayout rl_following;
    private RecyclerView recycler_productlist;
    List<ShopDetailModel.Products> productListAdapterList =new ArrayList<>();
    UserSessionManager session;
    HashMap<String, String> user;
    String Sizeid="0";
    String tagg = "0";
    String adharnumberString = "";
    private TextView call;
    String Phone="";
    private TextView driection;
    String latitude,longtitude;
    Context context;
    RelativeLayout edit_rel;
    TextView error_mesg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_shop_details, container, false);
        context=getActivity();
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        initView(view);
        return view;
    }
    private void initView(View view) {
        iv_menu = view.findViewById(R.id.iv_menu);
        shoptime = view.findViewById(R.id.shoptime);
        call = view.findViewById(R.id.call);
        error_mesg = view.findViewById(R.id.error_mesg);
        edit_rel = view.findViewById(R.id.edit_rel);
        driection = view.findViewById(R.id.driection);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.
                navDrawer.openDrawer(GravityCompat.START);
            }
        });
        ImageView iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        edit_rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Shop_Information_Activity.class));
            }
        });
        iv_send = view.findViewById(R.id.iv_send);
        et_search = view.findViewById(R.id.et_search);
        tvviewAll = view.findViewById(R.id.tvviewAll);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(getActivity(), SearchProduct.class));
            }
        });
        tvviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             startActivity(new Intent(getActivity(), Add_Category.class));
            }
        });
        iv_voice = view.findViewById(R.id.iv_voice);
        iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        header = view.findViewById(R.id.header);
        iv_other_profile = view.findViewById(R.id.iv_other_profile);
        tv_user_bio = view.findViewById(R.id.tv_user_bio);
        tvuser_name = view.findViewById(R.id.tvuser_name);
        tv_respect = view.findViewById(R.id.tv_respect);
        respect = view.findViewById(R.id.respect);
        rl_respect = view.findViewById(R.id.rl_respect);
        tv_follower = view.findViewById(R.id.tv_follower);
        follower = view.findViewById(R.id.follower);
        rl_follower = view.findViewById(R.id.rl_follower);
        tv_following = view.findViewById(R.id.tv_following);
        following = view.findViewById(R.id.following);
        rl_following = view.findViewById(R.id.rl_following);
        recycler_productlist = view.findViewById(R.id.recycler_productlist);
//        recycler_productlist.setAdapter(new ProductListAdapter());
        Map<String, String> map = new HashMap<>();
        map.put("method", "shopdetail");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
       // map.put("userId", "2");
        getShopDetail(map);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent. setData(Uri.parse("tel:" + Phone));
                startActivity(callIntent);
            }
        });
        driection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longtitude + "&mode=d");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }
    private void getShopDetail(Map<String, String> map) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ShopDetailModel> call = Apis.getAPIService().getShopDetail(map);
        call.enqueue(new Callback<ShopDetailModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<ShopDetailModel> call, @NonNull Response<ShopDetailModel> response) {
                dialog.dismiss();
                ShopDetailModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        productListAdapterList.clear();
                        tv_user_bio.setText(userdata.getData().getShopcategory());
                        tvuser_name.setText(userdata.getData().getShopname());
                        tv_respect.setText(userdata.getData().getRating());
                        respect.setText(userdata.getData().getTotalreview()+" Reviews");
                        Phone=userdata.getData().getPhone();
                        latitude=userdata.getData().getLatitude();
                        longtitude=userdata.getData().getLongitude();
                        if (userdata.getErrormsg().equals("")){
                            error_mesg.setVisibility(View.GONE);
                        }else {
                            error_mesg.setText(userdata.getErrormsg());
                            error_mesg.setVisibility(View.VISIBLE);
                        }
                        if (userdata.getData().getDeliverytime()==null||userdata.getData().getDeliverytime().equals(""))
                        {
                            tv_follower.setText("N/A");
                        }else {
                            tv_follower.setText(userdata.getData().getDeliverytime());
                        }
                        if (userdata.getData().getMinorder()==null||userdata.getData().getMinorder().equals(""))
                        {
                            tv_following.setText("N/A");
                        }else {
                            tv_following.setText("Rs"+" "+userdata.getData().getMinorder());
                        }
                        if (userdata.getData().getOpentime()==null||userdata.getData().getClosetime()== null)
                        {
                            shoptime.setText("N/A");
                        }else {
                            shoptime.setText(userdata.getData().getOpentime() +" to " +userdata.getData().getClosetime());
                        }

                        Glide.with(context)
                                .load(Apis.URL + userdata.getData().getImage())
                                .error(R.drawable.no_image)
                                .into(iv_other_profile);
                        productListAdapterList.clear();
                        productListAdapterList = userdata.getData().getProducts();
                        if(productListAdapterList.size()>0){
                            recycler_productlist.setAdapter(new CategoryAdapter());
                        }
                    } else {
                        Custom_Toast_Activity.makeText(context, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(context, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShopDetailModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


        public CategoryAdapter() {

        }

        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_category, parent, false);
            CategoryAdapter.ViewHolder viewHolder = new CategoryAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
            holder.tv_categoryName.setText(productListAdapterList.get(position).getCatName());
            holder.tv_categoryName.setSelected(true);
//            holder.tv_category_details.setText(array_categories_list.get(position).getOffertest());
            Glide.with(getActivity())
                    .load(Apis.IMAGE_PATH + productListAdapterList.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_category);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), Sub_Categories.class)
                            .putExtra("cat_id",productListAdapterList.get(position).getCatId())
                    );
                }
            });
        }


        @Override
        public int getItemCount() {
            return productListAdapterList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private RelativeLayout card_shop_category;
            private ImageView iv_category;
            private TextView tv_categoryName;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_shop_category = itemView.findViewById(R.id.card_shop_category);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            }
        }
    }

//    public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
//        public int counter;
//        @Override
//        public ProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//            View listItem = layoutInflater.inflate(R.layout.list_products, parent, false);
//            ProductListAdapter.ViewHolder viewHolder = new ProductListAdapter.ViewHolder(listItem);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(final ProductListAdapter.ViewHolder holder, final int position) {
//            Picasso.get().load(Apis.IMAGE_PATH + productListAdapterList.get(position).getMainimage())
//                    .error(R.drawable.no_image)
//                    .priority(Picasso.Priority.HIGH)
//                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
//            Log.d("imageurl",Apis.IMAGE_PATH + productListAdapterList.get(position).getMainimage());
//            final List<ShopDetailModel.Size_chart> sizelist = productListAdapterList.get(position).getSize_chart();
//            Quntity_Adapter adapter = new Quntity_Adapter(getActivity(),
//                    R.layout.spinner_list_text, R.id.title, sizelist);
//            holder.quatity_spinner.setAdapter(adapter);
//            holder.tv_oldPrice.setText("Rs. "+productListAdapterList.get(position).getSize_chart().get(0).getActual_Price());
//            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.tv_date.setText("Valid to "+productListAdapterList.get(position).getValidto());
//            holder.tv_name.setText(productListAdapterList.get(position).getProduct_Name());
//            holder.tv_off.setText(productListAdapterList.get(position).getOffer());
//            holder.tv_new_price.setText(productListAdapterList.get(position).getSize_chart().get(0).getSelling_price());
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    startActivity(new Intent(getActivity(), ProductDetails.class).putExtra("product_id",productListAdapterList.get(position).getProduct_id()));
//                }
//            });
//
//            holder.quatity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
//                    holder.tv_new_price.setText(sizelist.get(i).getSelling_price());
//                    holder.tv_oldPrice.setText("Rs. "+sizelist.get(i).getActual_Price());
//                    Double Actual_Ammount = Double.parseDouble(sizelist.get(i).getActual_Price());
//                    Double Selling_Ammount = Double.parseDouble(sizelist.get(i).getSelling_price());
//                    Double remaining = Actual_Ammount - Selling_Ammount;
//                    Sizeid = sizelist.get(i).getSize_id();
//                    Double totaldis = (remaining / Actual_Ammount) * 100;
//                    String value = new DecimalFormat("##").format(totaldis);
//                    holder.tv_off.setText(value + "%");
//                    holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                    adapterView.setSelection(i);
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//                    // DO Nothing here
//                }
//            });
//
//            if (productListAdapterList.get(position).getQuantity_count()==0) {
//                holder.tv_add.setVisibility(View.VISIBLE);
//                holder.tv_remove_item.setVisibility(View.GONE);
//                holder.tv_add_item.setVisibility(View.GONE);
//                holder.tv_item_count.setVisibility(View.GONE);
//            } else {
//                holder.tv_add.setVisibility(View.GONE);
//                holder.tv_remove_item.setVisibility(View.VISIBLE);
//                holder.tv_add_item.setVisibility(View.VISIBLE);
//                holder.tv_item_count.setVisibility(View.VISIBLE);
//                holder.tv_item_count.setText(productListAdapterList.get(position).getQuantity_count() + "");
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return productListAdapterList.size();
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//            private LinearLayout product_details;
//            private ImageView iv_item;
//            private TextView tv_name;
//            private TextView tv_oldPrice;
//            private TextView tv_date;
//            private TextView tv_new_price;
//            private Spinner quatity_spinner;
//            private TextView tv_off;
//            private LinearLayout tv_remove_item;
//            private TextView tv_item_count;
//            private TextView tv_add;
//            private LinearLayout tv_add_item;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                this.product_details = itemView.findViewById(R.id.product_details);
//                this.iv_item = itemView.findViewById(R.id.iv_item);
//                this.tv_name = itemView.findViewById(R.id.tv_name);
//                this.tv_oldPrice = itemView.findViewById(R.id.tv_oldPrice);
//                this.tv_new_price = itemView.findViewById(R.id.tv_new_price);
//                this.quatity_spinner = itemView.findViewById(R.id.quatity_spinner);
//                this.tv_off = itemView.findViewById(R.id.tv_off);
//                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
//                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
//                this.tv_date = itemView.findViewById(R.id.tv_date);
//                this.tv_add = itemView.findViewById(R.id.tv_add);
//                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
//            }
//        }
//    }

//    public class Quntity_Adapter extends ArrayAdapter<ShopDetailModel.Size_chart> {
//
//        LayoutInflater flater;
//
//        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<ShopDetailModel.Size_chart> list) {
//            super(context, resouceId, textviewId, list);
//            flater = context.getLayoutInflater();
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            return rowview(convertView, position);
//        }
//
//        @Override
//        public View getDropDownView(int position, View convertView, ViewGroup parent) {
//            return rowview(convertView, position);
//        }
//
//        private View rowview(View convertView, int position) {
//            final ShopDetailModel.Size_chart rowItem = getItem(position);
//            Quntity_Adapter.viewHolder holder;
//            View rowview = convertView;
//            if (rowview == null) {
//
//                holder = new Quntity_Adapter.viewHolder();
//                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
//                holder.spinner_text = (TextView) rowview.findViewById(R.id.spinner_text);
//                rowview.setTag(holder);
//            } else {
//                holder = (Quntity_Adapter.viewHolder) rowview.getTag();
//            }
//            holder.spinner_text.setText(rowItem.getSize());
//            return rowview;
//        }
//
//        private class viewHolder {
//            TextView spinner_text;
//        }
//    }
    
}
