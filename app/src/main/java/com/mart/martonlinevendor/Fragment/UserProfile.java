package com.mart.martonlinevendor.Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.android.material.chip.Chip;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.Category_id_Model;
import com.mart.martonlinevendor.Model.Delete_Time_Slot_Model;
import com.mart.martonlinevendor.Model.Delivery_Time_Slot_Get_Model;
import com.mart.martonlinevendor.Model.ShopCategoeryModel;
import com.mart.martonlinevendor.Model.Update_Profile_Model;
import com.mart.martonlinevendor.Model.UserProfileModel;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.martonlinevendor.Api.Apis.IMAGE_PATH;

public class UserProfile extends Fragment implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private ImageView user_image;
    private EditText et_username;
    private LinearLayout layout_username;
    private EditText et_email;
    private LinearLayout layout_email;
    private EditText et_dob;
    private LinearLayout layout_dob;
    private EditText et_aniver;
    private LinearLayout layout_aniver;
    private EditText et_gender;
    private LinearLayout gender_text;
    private LinearLayout layout_gender;
    private EditText et_mobile;
    private LinearLayout layout_mobile;
    private Button change_pass_button;
    private LinearLayout layout_password;
    private Button btn_submit;
    private ImageView iv_menu;
    private RecyclerView recyclerView_address;
    //    AddressAdapter addressAdapter =new AddressAdapter();
    List<checklist> list_address;
    UserSessionManager session;
    HashMap<String, String> user;
    private CardView cardimage;
    private EditText et_timeslot;
    private LinearLayout layout_timeslot;
    private EditText et_times;
    private LinearLayout layout_time;
    private EditText et_password;
    private LinearLayout layout_password1;
    private EditText et_address;
    private LinearLayout ll_address;
    private EditText et_pincode;
    private LinearLayout ll_pincode;
    private TextView tvverified;
    private TextView tvnotvrified;
    String tagg = "0";
    String adharnumberString = "";
    private Calendar myCalendar;
    String shopname = "";
    private EditText et_shop_name;
    int hour, minute;
    String aTime, aTime1;
    private RecyclerView timeslot_list;
    private TextView close, open;
    Chip chip;
    List<Delivery_Time_Slot_Get_Model.Deliveryslot> array_time_slot_list;
    private TextView message,message1;
    RecyclerView search_list;
    String Status = "";
    List<ShopCategoeryModel.Category> shopcategorylist = new ArrayList<>();
    List<Category_id_Model> array_count_id;
    String id = "";
   Context context;
   Dialog dialog;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        context=getActivity();
         dialog = new Dialog(context);
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        intit(view);
        return view;
    }

    private void intit(View view) {
        array_count_id = new ArrayList<>();
        search_list = view.findViewById(R.id.search_list);
        TextView et_search = view.findViewById(R.id.et_search);
        btn_submit = view.findViewById(R.id.btn_submit);
        message = view.findViewById(R.id.message);
        message1 = view.findViewById(R.id.message1);
        timeslot_list = view.findViewById(R.id.timeslot_list);
        LinearLayout layout_password = view.findViewById(R.id.layout_password);
        Button change_pass_button = view.findViewById(R.id.change_pass_button);
        change_pass_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), Change_Password.class));
            }
        });
        layout_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), Change_Password.class));
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Status.equals("2")) {
                    Snackbar.make(btn_submit, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(getActivity(), R.color.alertcolor)).show();
                } else {
                    startActivity(new Intent(getActivity(), SearchProduct.class));
                }
            }
        });
        et_email = view.findViewById(R.id.et_email);
        tvverified = view.findViewById(R.id.tvverified);
        tvnotvrified = view.findViewById(R.id.tvnotvrified);
        et_shop_name = view.findViewById(R.id.et_shop_name);
        user_image = view.findViewById(R.id.user_image);
        et_username = view.findViewById(R.id.et_username);
        et_address = view.findViewById(R.id.et_address);
        et_dob = view.findViewById(R.id.et_dob);
        et_mobile = view.findViewById(R.id.et_mobile);
        et_pincode = view.findViewById(R.id.et_pincode);
        iv_menu = view.findViewById(R.id.iv_menu);
        et_password = view.findViewById(R.id.et_password);
        et_times = view.findViewById(R.id.et_times);
        et_timeslot = view.findViewById(R.id.et_timeslot);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.
                navDrawer.openDrawer(GravityCompat.START);
            }
        });
        ImageView iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Status.equals("2")) {
                    Snackbar.make(btn_submit, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(getActivity(), R.color.alertcolor)).show();
                } else {
                    startActivity(new Intent(getActivity(), Notification_Screen.class));
                }

            }
        });
        recyclerView_address = view.findViewById(R.id.recyclerView_address);
//        recyclerView_address.setAdapter(addressAdapter);
        list_address = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            checklist checklist = new checklist();
            checklist.setCheck(false);
            list_address.add(checklist);
        }
        Map<String, String> map = new HashMap<>();
        map.put("method", "Profile");
        map.put("UserId", user.get(UserSessionManager.KEY_ID));
        getProfile(map);
        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagg = "1";
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.mart.martonlinevendor")
                        .build();
                pickerDialog.show(getChildFragmentManager(), "picker");
            }
        });

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        et_times.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDialog();
            }
        });

        et_timeslot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Add_Time_Slot_dialog();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = "";
                for (int i = 0; i < array_count_id.size(); i++) {
                    id = id + array_count_id.get(i).getId() + "" + ",";
                }
                Log.d("seddddddd", removeLastCharacter(id) + "");
                Map<String, String> map = new HashMap<>();
                map.put("method", "UpdateProfile");
                map.put("UserId", user.get(UserSessionManager.KEY_ID));
                map.put("name", et_username.getText().toString());
                map.put("email", et_email.getText().toString());
                map.put("phone", et_mobile.getText().toString());
                map.put("dob", et_dob.getText().toString());
                map.put("category", removeLastCharacter(id));
                map.put("password", et_password.getText().toString());
                //  map.put("open", aTime);
                //  map.put("close", aTime1);
                //  map.put("address", et_address.getText().toString());
                // map.put("deliverytime", "10");
                // map.put("pincode", et_pincode.getText().toString());
                //  map.put("latitude", "10");
                //   map.put("longitude", "10");
                //  map.put("minorder", "10");
                //   map.put("shopname", et_shop_name.getText().toString());
                map.put("image", adharnumberString);
                Update_Profile(map);
            }
        });
    }

    public void TimeDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        int time1;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                updateTime(hour, minute);
                //time.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    public void TimeDialog1() {
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        int time1;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                updateTime1(hour, minute);
                //time.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        TimeDialog1();

    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime1(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;

        else
            minutes = String.valueOf(mins);
        // Append in a StringBuilder
        aTime1 = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        open.setText(aTime);
        close.setText(aTime1);
        et_times.setText(aTime + "-" + aTime1);

    }

    private void showDatePicker() {
        myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        et_dob.setText(sdf.format(myCalendar.getTime()));
    }

//    public void Add_Time_Slot_dialog() {
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.add_time_slot_dialog);
//        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        final EditText et_timeslot1 = dialog.findViewById(R.id.et_timeslot1);
//        Button btn_submit = dialog.findViewById(R.id.btn_submit);
//        btn_submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                Map<String, String> map1 = new HashMap<>();
//                map1.put("method", "adddeliveryslot");
//                map1.put("deliverslot", et_timeslot1.getText().toString());
//                map1.put("userId", user.get(UserSessionManager.KEY_ID));
//                Add_Time_Slot(map1);
//            }
//        });
//        dialog.show();
//    }

    public class Time_SlotAdapter extends RecyclerView.Adapter<Time_SlotAdapter.ViewHolder> {


        public Time_SlotAdapter() {

        }

        @Override
        public Time_SlotAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.time_slot_adapter, parent, false);
            Time_SlotAdapter.ViewHolder viewHolder = new Time_SlotAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Time_SlotAdapter.ViewHolder holder, final int position) {
            holder.time.setText(array_time_slot_list.get(position).getName());
//            holder.close.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Map<String, String> map = new HashMap<>();
//                    map.put("method", "deletedeliverslot");
//                    map.put("userId", user.get(UserSessionManager.KEY_ID));
//                    map.put("deliverslotId", array_time_slot_list.get(position).getId());
//                    Delete_TimeSlot(map);
//                }
//            });
        }


        @Override
        public int getItemCount() {
            return array_time_slot_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView close;
            private AppCompatTextView time;

            public ViewHolder(View itemView) {
                super(itemView);
//                this.close = itemView.findViewById(R.id.close);
                this.time = itemView.findViewById(R.id.time);
            }
        }
    }

//    private void Add_Time_Slot(Map<String, String> map) {
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Add_Time_Slot_Model> call = Apis.getAPIService().addTimeSlot(map);
//        call.enqueue(new Callback<Add_Time_Slot_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull final Call<Add_Time_Slot_Model> call, @NonNull Response<Add_Time_Slot_Model> response) {
//                dialog.dismiss();
//                Add_Time_Slot_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//                        Map<String, String> map1 = new HashMap<>();
//                        map1.put("method", "getdeliveryslot");
//                        map1.put("userId", user.get(UserSessionManager.KEY_ID));
//                        getTimeSlot(map1);
//                    } else {
//                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//                    }
//                } else {
//                    Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<Add_Time_Slot_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }

//    private void getTimeSlot(Map<String, String> map) {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Delivery_Time_Slot_Get_Model> call = Apis.getAPIService().getTimeSlot(map);
//        call.enqueue(new Callback<Delivery_Time_Slot_Get_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull final Call<Delivery_Time_Slot_Get_Model> call, @NonNull Response<Delivery_Time_Slot_Get_Model> response) {
//                dialog.dismiss();
//                Delivery_Time_Slot_Get_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        array_time_slot_list = new ArrayList<>();
//                        array_time_slot_list.addAll(userdata.getDeliveryslot());
//                        if (array_time_slot_list.isEmpty()) {
//                            timeslot_list.setVisibility(View.GONE);
//                        } else {
//                            timeslot_list.setVisibility(View.VISIBLE);
//                            Time_SlotAdapter time_slotAdapter = new Time_SlotAdapter();
//                            timeslot_list.setAdapter(time_slotAdapter);
//                        }
//
//                    } else {
//                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//                    }
//                } else {
//                    Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
//                }
//            }
//
//
//            @Override
//            public void onFailure(@NonNull Call<Delivery_Time_Slot_Get_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }

    private void Delete_TimeSlot(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Delete_Time_Slot_Model> call = Apis.getAPIService().deleteTimeslot(map);
        call.enqueue(new Callback<Delete_Time_Slot_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Delete_Time_Slot_Model> call, @NonNull Response<Delete_Time_Slot_Model> response) {
                dialog.dismiss();
                Delete_Time_Slot_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        // Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "Profile");
                        map.put("UserId", user.get(UserSessionManager.KEY_ID));
                        getProfile(map);
                    } else {
                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                } else {
                    Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Delete_Time_Slot_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void getProfile(Map<String, String> map) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UserProfileModel> call = Apis.getAPIService().getProfile(map);
        call.enqueue(new Callback<UserProfileModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<UserProfileModel> call, @NonNull Response<UserProfileModel> response) {
//                dialog.dismiss();
                UserProfileModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        for (int i = 0; i < userdata.getResponse().size(); i++) {
                            et_username.setText(userdata.getResponse().get(i).getFname());
                            et_email.setText(userdata.getResponse().get(i).getEmail());
                            et_mobile.setText(userdata.getResponse().get(i).getPhone());
                            et_dob.setText(userdata.getResponse().get(i).getDob());
                            //  et_timeslot.setText(userdata.getResponse().get(i).getDeliverytime());
                            et_password.setText(userdata.getResponse().get(i).getPassword());
                            shopname = userdata.getResponse().get(i).getShopname();
                            et_shop_name.setText(userdata.getResponse().get(i).getShopname());
                            Log.d("gjh", Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getImage());
                            Picasso.get().load(Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getImage())
                                    .error(R.drawable.no_image)
                                    .priority(Picasso.Priority.HIGH)
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(user_image);
                            Status = userdata.getResponse().get(0).getStatus();
                            if (userdata.getResponse().get(0).getStatus().equals("2")) {
                                tvnotvrified.setVisibility(View.VISIBLE);
                                tvverified.setVisibility(View.GONE);
                                message.setVisibility(View.VISIBLE);
                                message1.setVisibility(View.GONE);
                                message.setText("Thank You!! Your information is received, is under review and our executive shall get in touch with you soon to help you onboard.");
                            } else {
                                message1.setText("Congratulation!! Now you can start your shop online..");
                                tvnotvrified.setVisibility(View.GONE);
                                tvverified.setVisibility(View.VISIBLE);
                                message.setVisibility(View.GONE);
                                message1.setVisibility(View.VISIBLE);
                            }
                            et_address.setText(userdata.getResponse().get(i).getAddress());
                           // et_pincode.setText("302039");
                            et_dob.setText(userdata.getResponse().get(i).getDob());
                            et_times.setText(userdata.getResponse().get(i).getOpentime() + "-" + userdata.getResponse().get(i).getClosetime());
                            aTime = userdata.getResponse().get(i).getOpentime();
                            aTime1 = userdata.getResponse().get(i).getClosetime();
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "getshopcategory");
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            ShopCategoeryAPI(map);
                        }
                    } else {
                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserProfileModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void Update_Profile(final Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Update_Profile_Model> call = Apis.getAPIService().Update_Profile(map);
        call.enqueue(new Callback<Update_Profile_Model>() {
            @Override
            public void onResponse(Call<Update_Profile_Model> call, Response<Update_Profile_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Update_Profile_Model user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {

                        if (user.getAdminmsg().equals("0")){
                            Custom_Toast_Activity.makeText(getActivity(), user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                            startActivity(new Intent(getActivity(), Dashboard.class));
                        }else {
                            SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog.setTitleText("Mart'Oline");
                            pDialog.setContentText(user.getMsg());
                            pDialog.setConfirmText("Ok");
                            pDialog.setCancelable(false);
                            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivity(new Intent(getActivity(), Dashboard.class));
                                }
                            }).show();
                        }

                    } else {
                        dialog.dismiss();
                        Custom_Toast_Activity.makeText(getActivity(), user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                } else {
                    dialog.dismiss();
                    Custom_Toast_Activity.makeText(getActivity(), "Something wents Worng", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);


                }
            }

            @Override
            public void onFailure(Call<Update_Profile_Model> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    @Override
    public void onSingleImageSelected(Uri uri, String tag) {

        if (tagg.equals("1")) {
            // tv_passport_name.setText(uri.getPath());
            Log.d("image", uri.getPath());
            Bitmap thumbnail = (BitmapFactory.decodeFile(uri.getPath()));

            final InputStream imageStream;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                user_image.setImageBitmap(selectedImage);
                adharnumberString = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(getActivity()).load(imageUri).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {

    }

    public class checklist {
        boolean check = false;

        public boolean isCheck() {
            return check;
        }

        public void setCheck(boolean check) {
            this.check = check;
        }
    }

    public void ShopCategoeryAPI(final Map<String, String> map) {
        Call<ShopCategoeryModel> call = Apis.getAPIService().ShopCategoery(map);
        call.enqueue(new Callback<ShopCategoeryModel>() {
            @Override
            public void onResponse(Call<ShopCategoeryModel> call, Response<ShopCategoeryModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                ShopCategoeryModel user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        shopcategorylist.clear();
                        shopcategorylist = user.getCategory();
                        if (shopcategorylist.size() > 0) {
                            search_list.setAdapter(new MyOrdersAdapter());
                        }
                    } else {
                        dialog.dismiss();
                        Custom_Toast_Activity.makeText(getActivity(), user.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                } else {
                    dialog.dismiss();
                    Custom_Toast_Activity.makeText(getActivity(), "Something wents Worng", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(Call<ShopCategoeryModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
//        Context context;
//        Dialog dialog;
//        LayoutInflater inflater;

        MyOrdersAdapter() {
        }

        @Override
        public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_categoery, parent, false);
            MyOrdersAdapter.ViewHolder viewHolder = new MyOrdersAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrdersAdapter.ViewHolder holder, final int position) {
            try {
                Picasso.get()
                        .load(IMAGE_PATH + shopcategorylist.get(position).getImage())
                        .error(R.drawable.no_image)
                        .into(holder.iv_cat_images);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tvtitle.setText(shopcategorylist.get(position).getName());
            if (shopcategorylist.get(position).getSelected().equals("1")) {
                holder.checkbox.setChecked(true);
                Category_id_Model model = new Category_id_Model();
                model.setId(shopcategorylist.get(position).getId());
                model.setName(shopcategorylist.get(position).getName());
                array_count_id.add(model);
               // Log.d("idvikash", removeLastCharacter(id));
            } else {
                holder.checkbox.setChecked(false);
            }

//             holder.itemView.setOnClickListener(new View.OnClickListener() {
//                 @Override
//                 public void onClick(View view) {
//                     et_nationality.setText(shopcategorylist.get(position).getName());
//                     ShopcatogrieID = shopcategorylist.get(position).getId();
//                     dialog.dismiss();
//                 }
//             });
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("arrr", holder.checkbox.isChecked() + "");
                    if (holder.checkbox.isChecked()) {
                        Log.d("idvikash1111", "gdfgdfgdfgdxg");
                        Category_id_Model model = new Category_id_Model();
                        model.setId(shopcategorylist.get(position).getId());
                        model.setName(shopcategorylist.get(position).getName());
                        array_count_id.add(model);
                        Log.d("selectid", removeLastCharacter(id));
                    } else {
                        for (int i = 0; i < array_count_id.size(); i++) {
                            if (shopcategorylist.get(position).getId().equals(array_count_id.get(i).getId())) {
                                Log.d("idvikash1111", "gdfgdfgdfgdxg");
                                array_count_id.remove(i);
                            }
                        }
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return shopcategorylist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_cat_images;
            private TextView tvtitle;
            private CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);
                this.checkbox = itemView.findViewById(R.id.checkbox);


            }
        }
    }

    public static String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

}
