package com.mart.martonlinevendor.Interfaces;


import com.mart.martonlinevendor.Model.About_Us_Model;
import com.mart.martonlinevendor.Model.ActiviteAddressModel;
import com.mart.martonlinevendor.Model.Add_Cart_Model;
import com.mart.martonlinevendor.Model.Add_Category_Model;
import com.mart.martonlinevendor.Model.Add_Product_Model;
import com.mart.martonlinevendor.Model.Add_Sub_Categorey_List_Model;
import com.mart.martonlinevendor.Model.Add_Time_Slot_Model;
import com.mart.martonlinevendor.Model.AddressListModel;
import com.mart.martonlinevendor.Model.Cart_List_Basket_Model;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.Model.ChangePassModel;
import com.mart.martonlinevendor.Model.Country_List_Model;
import com.mart.martonlinevendor.Model.Coupon_Apply_Model;
import com.mart.martonlinevendor.Model.Delete_Image_Model;
import com.mart.martonlinevendor.Model.Delete_Product_Model;
import com.mart.martonlinevendor.Model.Delete_Time_Slot_Model;
import com.mart.martonlinevendor.Model.Delivery_Time_Slot_Get_Model;
import com.mart.martonlinevendor.Model.ForgetPassModel;
import com.mart.martonlinevendor.Model.Get_Sub_Category_Model;
import com.mart.martonlinevendor.Model.Get_Support_Subject_Model;
import com.mart.martonlinevendor.Model.Home_Model;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.Model.NearByShopModel;
import com.mart.martonlinevendor.Model.Notification_Model;
import com.mart.martonlinevendor.Model.OTPModel;
import com.mart.martonlinevendor.Model.OrderConfirmModel;
import com.mart.martonlinevendor.Model.OrderDetailBulk_Model;
import com.mart.martonlinevendor.Model.OrderDetail_Model;
import com.mart.martonlinevendor.Model.Out_Stock_Model;
import com.mart.martonlinevendor.Model.Product_Detail_Model;
import com.mart.martonlinevendor.Model.Product_Detail_Model_bulk;
import com.mart.martonlinevendor.Model.Product_Search_Model;
import com.mart.martonlinevendor.Model.Product_ViewPager_Model;
import com.mart.martonlinevendor.Model.RegisterModel;
import com.mart.martonlinevendor.Model.Resend_OTP_Model;
import com.mart.martonlinevendor.Model.SearchProduct_Model;
import com.mart.martonlinevendor.Model.Search_Model;
import com.mart.martonlinevendor.Model.ShopCategoeryModel;
import com.mart.martonlinevendor.Model.ShopDetailModel;
import com.mart.martonlinevendor.Model.Shop_Details_Update_Model;
import com.mart.martonlinevendor.Model.SignInModel;
import com.mart.martonlinevendor.Model.Size_Model;
import com.mart.martonlinevendor.Model.Sub_Cat_Deactive_Model;
import com.mart.martonlinevendor.Model.SubcatModel;
import com.mart.martonlinevendor.Model.Support_Submit_Model;
import com.mart.martonlinevendor.Model.UpdateAddressModel;
import com.mart.martonlinevendor.Model.Update_Profile_Model;
import com.mart.martonlinevendor.Model.Update_Status_Model;
import com.mart.martonlinevendor.Model.UserProfileModel;
import com.mart.martonlinevendor.Model.Vendor_Category_Model;
import com.mart.martonlinevendor.Model.WhatsApp_List_Model;
import com.mart.martonlinevendor.Model.WhatsappdetailModel;
import com.mart.martonlinevendor.Model.WishListModel;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Service {

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<ShopCategoeryModel> ShopCategoery(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<RegisterModel> RegisterAPI(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<OTPModel> OTPapi(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Resend_OTP_Model> Resendotp(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<ForgetPassModel> forgetPasswordApi(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<ChangePassModel> changePasswordApi(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<SignInModel> signInApi(

            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<UserProfileModel> getProfile(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<My_Order_Model> getOrderList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<OrderDetail_Model> getOrderDeatils(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<ShopDetailModel> getShopDetail(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Update_Profile_Model> Update_Profile(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<About_Us_Model> aboutus(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Get_Support_Subject_Model> getSupportSubject(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Support_Submit_Model> supportsubmit(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Categories_Model> getCategories(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Product_ViewPager_Model> getProductList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Product_Detail_Model> getProductDetails(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Delivery_Time_Slot_Get_Model> getTimeSlot(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Delete_Time_Slot_Model> deleteTimeslot(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Add_Time_Slot_Model> addTimeSlot(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Update_Status_Model> Submit_Status(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Shop_Details_Update_Model> Shop_Update(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Search_Model> getSearch(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Vendor_Category_Model> getVendorCategory(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Add_Category_Model> addCategory(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Get_Sub_Category_Model> getSubcategory(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Size_Model> getSize(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Notification_Model> getNotification(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Delete_Product_Model> Delete_Product(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Out_Stock_Model> Out_Stock(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Delete_Image_Model> Delete_Image(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Add_Sub_Categorey_List_Model> getSubList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Product_Search_Model> Search_Product(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Sub_Cat_Deactive_Model> Sub_Cat_Active(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<Country_List_Model> Country_List(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<WhatsApp_List_Model> Whats_Order_List(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorV3.php")
    Call<WhatsappdetailModel> getWhastappOrderdeatail(
            @FieldMap() Map<String, String> parem
    );

    @Multipart
    @POST("/apivendorV3.php")
    Call<Add_Product_Model> add_Product(@Part MultipartBody.Part user_id,
                                        @Part MultipartBody.Part method,
                                        @Part MultipartBody.Part MRP,
                                        @Part MultipartBody.Part OfferPrize,
                                        @Part MultipartBody.Part quantity,
                                        @Part MultipartBody.Part Producttitle,
                                        @Part MultipartBody.Part Description,
                                        @Part MultipartBody.Part Category_id,
                                        @Part MultipartBody.Part Subcategory_id,
                                        @Part MultipartBody.Part size_id,
                                        @Part MultipartBody.Part type,
                                        @Part MultipartBody.Part newproductId,
                                        @Part MultipartBody.Part imageids,
                                        @Part MultipartBody.Part size_value,
                                        @Part MultipartBody.Part country,
                                        @Part MultipartBody.Part product_id,
                                        @Part MultipartBody.Part csvfile,
                                        @Part MultipartBody.Part[] images);

    @Multipart
    @POST("/apivendorV3.php")
    Call<Add_Product_Model> add_Product1(@Part MultipartBody.Part user_id,
                                         @Part MultipartBody.Part method,
                                         @Part MultipartBody.Part MRP,
                                         @Part MultipartBody.Part OfferPrize,
                                         @Part MultipartBody.Part quantity,
                                         @Part MultipartBody.Part Producttitle,
                                         @Part MultipartBody.Part Description,
                                         @Part MultipartBody.Part Category_id,
                                         @Part MultipartBody.Part Subcategory_id,
                                         @Part MultipartBody.Part size_id,
                                         @Part MultipartBody.Part type,
                                         @Part MultipartBody.Part newproductId,
                                         @Part MultipartBody.Part imageids,
                                         @Part MultipartBody.Part size_value,
                                         @Part MultipartBody.Part country,
                                         @Part MultipartBody.Part csvfile,
                                         @Part MultipartBody.Part[] images);

    //new bulk shop
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<NearByShopModel> getNearByShop(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<SubcatModel> getSubCat(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<Add_Cart_Model> addCart(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<WishListModel> getWishList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<Home_Model> getHomeData(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<AddressListModel> getAddressList(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<ShopDetailModel> getShopDetail1(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<SearchProduct_Model> getSearch_product(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<Product_Detail_Model_bulk> getProductDetails_bulk(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<Cart_List_Basket_Model> getCartList(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<Coupon_Apply_Model> apply_coupon(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<OrderConfirmModel> getOrderConfirm(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<ActiviteAddressModel> ActiviteAddress(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<UpdateAddressModel> UpdateAddress(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<AddressListModel> deleteadddress(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<My_Order_Model> getOrderList1(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<OrderDetailBulk_Model> getOrderDeatilsBulk(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/apivendorecom.php")
    Call<OrderDetail_Model> getOrderDeatilsB2B(
            @FieldMap() Map<String, String> parem
    );

}

