package com.mart.martonlinevendor.LoginRegister;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.ChangePassModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_login;
    private EditText et_password;
    private EditText et_confirm_password;
    private Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initView();
    }

    private void initView() {
        et_password = findViewById(R.id.et_password);
        et_confirm_password = findViewById(R.id.et_confirmpassword);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
                case R.id.btn_submit:

                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            Snackbar.make(btn_submit, "Enter  Your Password",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            ChangePassword.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(ChangePassword.this,R.color.alertcolor))
                    .show();
          //  Custom_Toast_Activity.makeText(ChangePassword.this, "Enter  Your Password", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }

        String confirm_password = et_confirm_password.getText().toString().trim();
        if (TextUtils.isEmpty(confirm_password)) {
           // Custom_Toast_Activity.makeText(ChangePassword.this, "Enter  Your Confirm Password", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            Snackbar.make(btn_submit, "Enter  Your Confirm Password",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            ChangePassword.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(ChangePassword.this,R.color.alertcolor))
                    .show();
            return;
        }

        if (!password.equals(confirm_password)) {
            //Custom_Toast_Activity.makeText(ChangePassword.this, "Password does not match?", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            Snackbar.make(btn_submit, "Password does not match?",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            ChangePassword.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(ChangePassword.this,R.color.alertcolor))
                    .show();

            return;
        }

            Map<String, String> map;
            map = new HashMap<>();
            map.put("method", "changePassword");
            map.put("userId", getIntent().getStringExtra("user_id"));
            map.put("password", confirm_password);
            ChangePasswordApi(map);
    }


    public void ChangePasswordApi(final Map<String, String> map) {

        final Dialog dialog = new Dialog(ChangePassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ChangePassModel> call = Apis.getAPIService().changePasswordApi(map);

        call.enqueue(new Callback<ChangePassModel>() {
            @Override
            public void onResponse(Call<ChangePassModel> call, Response<ChangePassModel> response) {
                dialog.dismiss();
                ChangePassModel changePassModel = new ChangePassModel();
                changePassModel = response.body();

                if (changePassModel.getStatus().equals("1")) {
                    Custom_Toast_Activity.makeText(ChangePassword.this, changePassModel.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    startActivity(new Intent(ChangePassword.this, SignIn.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                } else {
                    Custom_Toast_Activity.makeText(ChangePassword.this, changePassModel.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }


            }

            @Override
            public void onFailure(Call< ChangePassModel > call, Throwable t) {
                dialog.dismiss();

            }
        });
    }

}
