package com.mart.martonlinevendor.LoginRegister;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.ForgetPassModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {

    private EditText et_mobile;
    private LinearLayout layout_mobile;
    private Button btn_sendotp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        initView();
    }

    private void initView() {

        et_mobile = findViewById(R.id.et_mobile);
        layout_mobile = findViewById(R.id.layout_mobile);
        btn_sendotp = findViewById(R.id.btn_sendotp);
        btn_sendotp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_sendotp:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String mobile = et_mobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            Snackbar.make(btn_sendotp, "Enter Mobile Number",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            ForgetPassword.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(ForgetPassword.this,R.color.alertcolor))
                    .show();
           // Custom_Toast_Activity.makeText(ForgetPassword.this, "Enter Mobile Number", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }
            Map<String, String> map;
            map = new HashMap<>();
            map.put("method", "forgotPassword");
            map.put("mobile", mobile);
            ForgetPasswordApi(map);
    }

    public void ForgetPasswordApi(final Map<String, String> map) {

        final Dialog dialog = new Dialog(ForgetPassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ForgetPassModel> call = Apis.getAPIService().forgetPasswordApi(map);
        call.enqueue(new Callback<ForgetPassModel>() {
            @Override
            public void onResponse(Call<ForgetPassModel> call, Response<ForgetPassModel> response) {
                ForgetPassModel forgetPassModel = new ForgetPassModel();
                forgetPassModel = response.body();
                if (forgetPassModel.getStatus() == 1) {
                    startActivity(new Intent(ForgetPassword.this, Forgot_Password_OTPVerification.class)
                            .putExtra("user_id", forgetPassModel.getData().getUserId())
                            .putExtra("mobile_number", et_mobile.getText().toString())
                            .putExtra("otp", forgetPassModel.getData().getOtp()+""));
                } else {
                    Custom_Toast_Activity.makeText(ForgetPassword.this, forgetPassModel.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ForgetPassModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}
