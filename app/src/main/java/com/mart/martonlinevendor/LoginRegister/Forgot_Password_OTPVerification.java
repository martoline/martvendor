package com.mart.martonlinevendor.LoginRegister;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.OTPModel;
import com.mart.martonlinevendor.Model.Resend_OTP_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.SmsBroadcastReceiver;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Forgot_Password_OTPVerification extends AppCompatActivity implements View.OnClickListener {

    String otptext = "";
    private TextView tv_checkOTP;

    private TextView tv_resendCode;
    private Button btn_verify;
    UserSessionManager session;
    private TextView tv_phone;
    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;
    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        session = new UserSessionManager(getApplicationContext());

        initView();
    }

    private void initView() {
        tv_checkOTP = findViewById(R.id.tv_checkOTP);

        tv_resendCode = findViewById(R.id.tv_resendCode);
        btn_verify = findViewById(R.id.btn_verify);
        tv_phone = findViewById(R.id.tv_phone);

        tv_resendCode.setOnClickListener(this);
        btn_verify.setOnClickListener(this);

        // Toast.makeText(this, getIntent().getStringExtra("otp"), Toast.LENGTH_SHORT).show();
        // otp_view.setText(getIntent().getStringExtra("otp"));
        tv_phone.setText(getIntent().getStringExtra("mobile_number"));
        et1 = findViewById(R.id.et1);
        et1.setOnClickListener(this);
        et2 = findViewById(R.id.et2);
        et2.setOnClickListener(this);
        et3 = findViewById(R.id.et3);
        et3.setOnClickListener(this);
        et4 = findViewById(R.id.et4);
        et4.setOnClickListener(this);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et2.requestFocus();
                }
                else if(s.length()==0)
                {
                    et1.clearFocus();
                }
            }
        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et3.requestFocus();
                }
                else if(s.length()==0)
                {
                    et1.requestFocus();
                }
            }
        });

        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et4.requestFocus();
                }
                else if(s.length()==0)
                {
                    et2.requestFocus();
                }
            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {

                }
                else if(s.length()==0)
                {
                    et3.requestFocus();
                }
            }
        });
        startSmsUserConsent();
       // Toast.makeText(Forgot_Password_OTPVerification.this,"Your OTP"+getIntent().getStringExtra("otp"),Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_resendCode:

                Map<String, String> map;
                map = new HashMap<>();
                map.put("method", "resendotp");
                map.put("userId", getIntent().getStringExtra("user_id"));
                ResendOTP(map);

                // Custom_Toast_Activity.makeText(getApplicationContext(), "Code has been sent to your registered mobile number", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
                break;

            case R.id.btn_verify:
                submit();
                break;
        }
    }

    public void submit() {

        String otp = et1.getText().toString()+et2.getText().toString()+et3.getText().toString()+et4.getText().toString();

        if (TextUtils.isEmpty(otp)) {
            Snackbar.make(btn_verify, "Enter Your OTP",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Forgot_Password_OTPVerification.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Forgot_Password_OTPVerification.this,R.color.alertcolor))
                    .show();
          //  Custom_Toast_Activity.makeText(getApplicationContext(), "Enter OTP for continue", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }
//        if (!otp.equals(getIntent().getStringExtra("otp"))) {
//            Custom_Toast_Activity.makeText(getApplicationContext(), "Wrong OTP", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
//            return;
//        }

        Map<String, String> map;
        map = new HashMap<>();
        map.put("method", "otpverify");
        map.put("otp", otp);
        map.put("userId", getIntent().getStringExtra("user_id"));
        VerifyOTPApi(map);


    }

    public void VerifyOTPApi(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Forgot_Password_OTPVerification.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OTPModel> call = Apis.getAPIService().OTPapi(map);

        call.enqueue(new Callback<OTPModel>() {
            @Override
            public void onResponse(Call<OTPModel> call, Response<OTPModel> response) {
                dialog.dismiss();
                OTPModel userdata = response.body();

                if (userdata != null) {
                    if (userdata.getStatus() == 1) {
                        startActivity(new Intent(Forgot_Password_OTPVerification.this, ChangePassword.class).putExtra("user_id", userdata.getUserData().getUserId()));
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }

            }

            @Override
            public void onFailure(Call<OTPModel> call, Throwable t) {
                dialog.dismiss();

            }
        });
    }

    public void ResendOTP(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Forgot_Password_OTPVerification.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Resend_OTP_Model> call = Apis.getAPIService().Resendotp(map);

        call.enqueue(new Callback<Resend_OTP_Model>() {
            @Override
            public void onResponse(Call<Resend_OTP_Model> call, Response<Resend_OTP_Model> response) {
                dialog.dismiss();
                Resend_OTP_Model userdata = response.body();

                if (userdata != null) {
                    if (userdata.getStatus()==1) {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);

                        //otp_view.setText(userdata.getData().getOtp() + "");
                     //   View parentLayout = findViewById(android.R.id.content);
//                        Snackbar.make(parentLayout, "your otp is "+userdata.getData().getOtp(), Snackbar.LENGTH_LONG)
//                                .setAction("CLOSE", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                    }
//                                })
//                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
//                                .show();
                    }
                } else {
                    Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(Call<Resend_OTP_Model> call, Throwable t) {
                dialog.dismiss();

            }
        });
    }
    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
               /* textViewMessage.setText(
                        String.format("%s - %s", getString(R.string.received_message), message));
*/
                getOtpFromMessage(message);
            }
        }
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            Log.d("sdfg",matcher.group(0)+"");
            int num = Integer.parseInt(matcher.group(0)+"");
            String number = String.valueOf(num);
            for(int i = 0; i < number.length(); i++) {
                int j = Character.digit(number.charAt(i), 10);
                System.out.println("digit: " + j);
                if (i == 0) {
                    Log.d("num1", j + "");
                    et1.setText(j+"");
                }
                if (i == 1) {
                    Log.d("num2", j + "");
                    et2.setText(j+"");
                }
                if (i == 2) {
                    Log.d("num3", j + "");
                    et3.setText(j+"");
                }
                if (i == 3) {
                    Log.d("num4", j + "");
                    et4.setText(j+"");
                }
            }
        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }
}
