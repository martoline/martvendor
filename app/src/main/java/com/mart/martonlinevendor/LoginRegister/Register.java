package com.mart.martonlinevendor.LoginRegister;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Extra.Utility;
import com.mart.martonlinevendor.Model.Category_id_Model;
import com.mart.martonlinevendor.Model.RegisterModel;
import com.mart.martonlinevendor.Model.ShopCategoeryModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Term_Condition.Term_Condition_Activity;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.martonlinevendor.Api.Apis.IMAGE_PATH;

public class Register extends AppCompatActivity implements View.OnClickListener, BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {

    private ImageView colorheader;
    private EditText et_username;
    private EditText et_email;
    private EditText et_mobile;
    private EditText et_nationality;
    private EditText et_password;
    private TextView tv_passport_name;
    private TextView tv_upload_photo;
    private RelativeLayout rl_upload_photo;
    private TextView tv_id_proof_name;
    private TextView tv_upload_id_proof;
    private RelativeLayout rl_upload_id_proof;
    private EditText et_gst;
    private Button btn_signUp;
    private LinearLayout layout_bottom;
    private TextView tv_signin;
    private EditText et_shop_name;
    List<ShopCategoeryModel.Category> shopcategorylist = new ArrayList<>();
    List<Category_id_Model> array_count_id;
    String id = "";
    RecyclerView category_list;
    String Token;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "Firbase_Token";
    private LinearLayout signin_link;
    private CheckBox cbaccept;
    private TextView tvterms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Token = getSharedPreferences("Firbase_Token", MODE_PRIVATE).getString("regId", "");

        initView();
    }

    private void initView() {
        array_count_id = new ArrayList<>();
        colorheader = findViewById(R.id.colorheader);
        et_username = findViewById(R.id.et_username);
        signin_link = findViewById(R.id.signin_link);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_shop_name = findViewById(R.id.et_shop_name);
        et_nationality = findViewById(R.id.et_nationality);
        et_password = findViewById(R.id.et_password);
        tv_passport_name = findViewById(R.id.tv_passport_name);
        tv_upload_photo = findViewById(R.id.tv_upload_photo);
        rl_upload_photo = findViewById(R.id.rl_upload_photo);
        tv_id_proof_name = findViewById(R.id.tv_id_proof_name);
        tv_upload_id_proof = findViewById(R.id.tv_upload_id_proof);
        rl_upload_id_proof = findViewById(R.id.rl_upload_id_proof);
        category_list = findViewById(R.id.category_list);
        et_gst = findViewById(R.id.et_gst);
        btn_signUp = findViewById(R.id.btn_signUp);
        layout_bottom = findViewById(R.id.layout_bottom);
        tv_signin = findViewById(R.id.tv_signin);
        btn_signUp.setOnClickListener(this);
        tv_upload_photo.setOnClickListener(this);
        tv_upload_id_proof.setOnClickListener(this);
        et_nationality.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        map.put("method", "getshopcategory");
        ShopCategoeryAPI(map);
        signin_link.setOnClickListener(this);
        cbaccept = (CheckBox) findViewById(R.id.cbaccept);
        cbaccept.setOnClickListener(this);
        tvterms = (TextView) findViewById(R.id.tvterms);
        tvterms.setOnClickListener(this);
    }

    String tagg = "0";

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signUp:
                if (cbaccept.isChecked()) {
                    submit();
                } else {
                    Snackbar.make(btn_signUp, "Please Accept Terms And Conditions",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    Register.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                            .show();
                }
                break;
            case R.id.signin_link:
                startActivity(new Intent(Register.this, SignIn.class));
                break;
            case R.id.et_nationality:
                array_count_id.clear();
                category_list.setVisibility(View.VISIBLE);
                showdialog();
                break;
            case R.id.tv_upload_photo:
                tagg = "1";
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.mart.martonlinevendor")
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
                break;
            case R.id.tv_upload_id_proof:
                tagg = "2";
                BSImagePicker pickerDialog1 = new BSImagePicker.Builder("com.mart.martonlinevendor")
                        .build();
                pickerDialog1.show(getSupportFragmentManager(), "picker");
                break;
            case R.id.tvterms:
                startActivity(new Intent(Register.this, Term_Condition_Activity.class));
                break;
        }
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(Register.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {

    }

    String adharnumberString = "";
    String pannumberString = "";

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
//        selectedImagePath = uri.getPath();
//        Bitmap bm = BitmapFactory.decodeFile(uri.getPath());
        if (tagg.equals("1")) {
            tv_passport_name.setText(uri.getPath());
            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                adharnumberString = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else if (tagg.equals("2")) {
            try {
                tv_id_proof_name.setText(uri.getPath());
                final InputStream imageStream = getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                pannumberString = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else {

        }


//        base64 = Base64.encodeToString(bOut.toByteArray(), Base64.DEFAULT);
    }

    private void submit() {
        // validate
        String username = et_username.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            // Toast.makeText(this, "Full Name", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Enter Your Full Name",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }
        String shopname = et_shop_name.getText().toString().trim();
        if (TextUtils.isEmpty(shopname)) {
            //Toast.makeText(this, "Shop Name", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Enter Your Shop Name",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }

        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            // Toast.makeText(this, "Email Address", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Enter Your Email Address",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }
        if (!Utility.isValidEmailId(email)) {
            //  Toast.makeText(this, "Please Enter a Vaild Email", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Please Enter a Valid Email",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
//            showmessage("Please Enter a Vaild Email");
            return;
        }


        String mobile = et_mobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            //   Toast.makeText(this, "Phone Number", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Enter Your Phone Number",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }
        if (TextUtils.isEmpty(mobile) || mobile.length() < 8) {
            // et_mobile.setError("You must have 8 characters in your Mobile");
            Snackbar.make(btn_signUp, "You must have 8 characters in your Mobile",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }

        String nationality = et_nationality.getText().toString().trim();
        if (array_count_id.isEmpty()) {
            //Toast.makeText(this, "Category", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Please Select Category",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }

        String password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            //Toast.makeText(this, "Password", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_signUp, "Enter Your Password",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }
        if (TextUtils.isEmpty(password) || password.length() < 8) {
            // et_password.setError("You must have 8 characters in your password");
            Snackbar.make(btn_signUp, "You must have 8 characters in your password",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Register.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Register.this, R.color.alertcolor))
                    .show();
            return;
        }


//        if (TextUtils.isEmpty(pannumberString)) {
//            Toast.makeText(this, "Please Select Pan card", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//
        String gst = et_gst.getText().toString().trim();
//        if (TextUtils.isEmpty(gst)) {
//            Toast.makeText(this, "GST number", Toast.LENGTH_SHORT).show();
//            return;
//        }

        Map<String, String> map = new HashMap<>();
        map.put("method", "registration");
        map.put("email", et_email.getText().toString());
        map.put("password", et_password.getText().toString());
        map.put("name", username);
        map.put("shopname", shopname);
        map.put("token", Token);
        map.put("phone", mobile);
        map.put("category", removeLastCharacter(id));
        map.put("aadhar", adharnumberString);
        map.put("pan", pannumberString);
        map.put("gst", gst);
        RegisterAPI(map);

    }

    public static boolean validateGST(CharSequence gst) {
        if (gst != null)
            return Pattern.matches("\\d{2}[a-zA-Z]{5}\\d{4}[a-zA-Z] {1}\\d{1}[zZ]{1}[\\da-zA-Z]{1}", gst);
        else

            return false;
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    public void ShopCategoeryAPI(final Map<String, String> map) {
        final Dialog dialog = new Dialog(Register.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ShopCategoeryModel> call = Apis.getAPIService().ShopCategoery(map);
        call.enqueue(new Callback<ShopCategoeryModel>() {
            @Override
            public void onResponse(Call<ShopCategoeryModel> call, Response<ShopCategoeryModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                ShopCategoeryModel user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        shopcategorylist.clear();
                        shopcategorylist = user.getCategory();
                    } else {
                        dialog.dismiss();
                        showmessage(user.getMessage());
                    }
                } else {
                    dialog.dismiss();
                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<ShopCategoeryModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void RegisterAPI(final Map<String, String> map) {
        final Dialog dialog = new Dialog(Register.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<RegisterModel> call = Apis.getAPIService().RegisterAPI(map);
        call.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                RegisterModel user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        startActivity(new Intent(Register.this, Verification.class)
                                .putExtra("otp", user.getData().getOtp())
                                .putExtra("user_id", user.getData().getUserId()));
                    } else {
                        dialog.dismiss();
                        showmessage(user.getMessage());
                    }
                } else {
                    dialog.dismiss();
                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public void showdialog() {
        final Dialog dialog = new Dialog(Register.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_shopcatogire);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RecyclerView search_list = dialog.findViewById(R.id.search_list);
        TextView done = dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = "";
                for (int i = 0; i < array_count_id.size(); i++) {
                    id = id + array_count_id.get(i).getId() + "" + ",";
                    CategoryList_adapter categoryList_adapter = new CategoryList_adapter(Register.this, dialog);
                    category_list.setAdapter(categoryList_adapter);
                    categoryList_adapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
                Log.d("ddddddd", removeLastCharacter(id) + "");
            }
        });
        if (shopcategorylist.size() > 0) {
            search_list.setAdapter(new MyOrdersAdapter(Register.this, dialog));
        }
        dialog.show();
    }


    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    String ShopcatogrieID = "0";

    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
        Context context;
        Dialog dialog;
        LayoutInflater inflater;

        MyOrdersAdapter(Context context, Dialog dialog) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.dialog = dialog;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_categoery, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                Picasso.get()
                        .load(IMAGE_PATH + shopcategorylist.get(position).getImage())
                        .error(R.drawable.no_image)
                        .into(holder.iv_cat_images);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tvtitle.setText(shopcategorylist.get(position).getName());

//             holder.itemView.setOnClickListener(new View.OnClickListener() {
//                 @Override
//                 public void onClick(View view) {
//                     et_nationality.setText(shopcategorylist.get(position).getName());
//                     ShopcatogrieID = shopcategorylist.get(position).getId();
//                     dialog.dismiss();
//                 }
//             });
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("arrr", holder.checkbox.isChecked() + "");
                    if (holder.checkbox.isChecked()) {
                        Log.d("idvikash1111", "gdfgdfgdfgdxg");
                        Category_id_Model model = new Category_id_Model();
                        model.setId(shopcategorylist.get(position).getId());
                        model.setName(shopcategorylist.get(position).getName());
                        array_count_id.add(model);
                        Log.d("idvikash", removeLastCharacter(id));
                    } else {
                        for (int i = 0; i < array_count_id.size(); i++) {
                            if (shopcategorylist.get(position).getId().equals(array_count_id.get(i).getId())) {
                                Log.d("idvikash1111", "gdfgdfgdfgdxg");
                                array_count_id.remove(i);
                            }
                        }
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return shopcategorylist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_cat_images;
            private TextView tvtitle;
            private CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);
                this.checkbox = itemView.findViewById(R.id.checkbox);


            }
        }
    }

    public class CategoryList_adapter extends RecyclerView.Adapter<CategoryList_adapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
        Context context;
        Dialog dialog;
        LayoutInflater inflater;

        CategoryList_adapter(Context context, Dialog dialog) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.dialog = dialog;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_categoery_1, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                Picasso.get()
                        .load(IMAGE_PATH + shopcategorylist.get(position).getImage())
                        .error(R.drawable.no_image)
                        .into(holder.iv_cat_images);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tvtitle.setText(array_count_id.get(position).getName());

//             holder.itemView.setOnClickListener(new View.OnClickListener() {
//                 @Override
//                 public void onClick(View view) {
//                     et_nationality.setText(shopcategorylist.get(position).getName());
//                     ShopcatogrieID = shopcategorylist.get(position).getId();
//                     dialog.dismiss();
//                 }
//             });


        }

        @Override
        public int getItemCount() {
            return array_count_id.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv_cat_images;
            private TextView tvtitle;
            private CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cat_images = itemView.findViewById(R.id.iv_cat_images);
                this.tvtitle = itemView.findViewById(R.id.tvtitle);
                this.checkbox = itemView.findViewById(R.id.checkbox);


            }
        }
    }

    public static String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
