package com.mart.martonlinevendor.LoginRegister;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.SignInModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity implements View.OnClickListener {
    private EditText et_email;
    private LinearLayout ll_email;
    private EditText et_password;
    private LinearLayout ll_password;
    private TextView tv_forgetPassword;
    private Button btn_signin;
    private ImageView iv_facebook;
    private ImageView iv_google;
    private TextView tv_signup;
    private TextView tv_guest_login;

    UserSessionManager session;
    private ImageView imageView;
    private FrameLayout FrameLayout1;
    private FrameLayout FrameLayout2;
    private LinearLayout ll_bottom;
    String Token;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "Firbase_Token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        session = new UserSessionManager(getApplicationContext());
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Token = getSharedPreferences("Firbase_Token", MODE_PRIVATE).getString("regId", "");

        initView();
    }


    private void initView() {
        et_email = findViewById(R.id.et_email);

        et_password = findViewById(R.id.et_password);

        tv_forgetPassword = findViewById(R.id.tv_forgetPassword);
        btn_signin = findViewById(R.id.btn_signin);

        tv_signup = findViewById(R.id.tv_signup);
        tv_guest_login = findViewById(R.id.tv_guest_login);

        tv_forgetPassword.setOnClickListener(this);
        btn_signin.setOnClickListener(this);

        tv_signup.setOnClickListener(this);
        tv_guest_login.setOnClickListener(this);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setOnClickListener(this);
        FrameLayout1 = (FrameLayout) findViewById(R.id.FrameLayout1);
        FrameLayout1.setOnClickListener(this);
        FrameLayout2 = (FrameLayout) findViewById(R.id.FrameLayout2);
        FrameLayout2.setOnClickListener(this);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);
        ll_bottom.setOnClickListener(this);
    }

    private void submit() {
//        // validate
        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Snackbar.make(btn_signin, "Enter Your Mobile Number",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(SignIn.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(SignIn.this,R.color.alertcolor))
                    .show();
          //  Custom_Toast_Activity.makeText(getApplicationContext(), "Enter Your Mobile Number", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }
        if (email.length() != 10) {

           // et_email.setError("Phone Number must be of 10 digit");
            Snackbar.make(btn_signin, "Phone Number must be of 10 digit",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            SignIn.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(SignIn.this,R.color.alertcolor))
                    .show();
            et_email.requestFocus();
            return;
        }


        String password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            Snackbar.make(btn_signin, "Enter Your Password",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            SignIn.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(SignIn.this,R.color.alertcolor))
                    .show();
            //Custom_Toast_Activity.makeText(getApplicationContext(), "Enter Your Password", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("phone", email);
        map.put("password", password);
        map.put("method", "login");
        map.put("token", Token);
        map.put("type", "password");
        User_SignIn(map);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forgetPassword:
                startActivity(new Intent(this, ForgetPassword.class));
                break;
            case R.id.btn_signin:
                submit();
                break;
            case R.id.tv_signup:
                startActivity(new Intent(this, Register.class));
                break;
            case R.id.tv_guest_login:
                break;
        }
    }

    private void User_SignIn(Map<String, String> map) {
        final Dialog dialog = new Dialog(SignIn.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SignInModel> call = Apis.getAPIService().signInApi(map);
        call.enqueue(new Callback<SignInModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                dialog.dismiss();
                SignInModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        session.iseditor();
                        session.createUserLoginSession(String.valueOf(userdata.getUserData().getUserId()),
                                userdata.getUserData().getUsername(),
                                userdata.getUserData().getName(), userdata.getUserData().getEmail(),
                                "", userdata.getUserData().getPhone(),
                                "", "",
                                userdata.getUserData().getToken());
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        startActivity(new Intent(SignIn.this, Dashboard.class));
                    } else if (userdata.getStatus().equals("2")) {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                        startActivity(new Intent(SignIn.this, Verification.class)
                                .putExtra("otp", userdata.getUserData().getOtp() + "")
                                .putExtra("user_id", userdata.getUserData().getUserId()));
                    } else {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
