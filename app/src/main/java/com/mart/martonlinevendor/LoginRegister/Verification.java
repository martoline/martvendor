package com.mart.martonlinevendor.LoginRegister;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Model.OTPModel;
import com.mart.martonlinevendor.Model.Resend_OTP_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.SmsBroadcastReceiver;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Verification extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_header;
    private RelativeLayout rl_header;

    private Button bt_verify;
    private TextView tv_resendOTP;
    EditText et1,et2,et3,et4;
    UserSessionManager session;
    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        session = new UserSessionManager(getApplicationContext());
        initView();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_header = findViewById(R.id.tv_header);
        rl_header = findViewById(R.id.rl_header);

        bt_verify = findViewById(R.id.bt_verify);
        tv_resendOTP = findViewById(R.id.tv_resendOTP);

        iv_back.setOnClickListener(this);
        bt_verify.setOnClickListener(this);
        tv_resendOTP.setOnClickListener(this);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et2.requestFocus();
                }
                else if(s.length()==0)
                {
                    et1.clearFocus();
                }
            }
        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et3.requestFocus();
                }
                else if(s.length()==0)
                {
                    et1.requestFocus();
                }
            }
        });

        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                    et4.requestFocus();
                }
                else if(s.length()==0)
                {
                    et2.requestFocus();
                }
            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1)
                {
                }
                else if(s.length()==0)
                {
                    et3.requestFocus();
                }
            }
        });

    //    Custom_Toast_Activity.makeText(getApplicationContext(),"your otp is "+getIntent().getStringExtra("otp"), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.DEFAULT);
         //  showmessage();
        startSmsUserConsent();
    }
    public void showmessage() {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, "your otp is "+getIntent().getStringExtra("otp"), Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }
    public void submit() {

        String otp = et1.getText().toString()+et2.getText().toString()+et3.getText().toString()+et4.getText().toString();
        if (TextUtils.isEmpty(otp)) {
          //  Custom_Toast_Activity.makeText(getApplicationContext(), "Enter OTP for continue", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            Snackbar.make(bt_verify, "Enter Your OTP",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Verification.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Verification.this,R.color.alertcolor))
                    .show();

            return;
        }
        Map<String, String> map;
        map = new HashMap<>();
        map.put("method", "otpverify");
        map.put("otp", otp);
        map.put("userId", getIntent().getStringExtra("user_id"));
        VerifyOTPApi(map);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.bt_verify:
                submit();

                break;

            case R.id.tv_resendOTP:
                Map<String, String> map;
                map = new HashMap<>();
                map.put("method", "resendotp");
                map.put("userId", getIntent().getStringExtra("user_id"));
                ResendOTP(map);
                break;
        }
    }


    public void VerifyOTPApi(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Verification.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OTPModel> call = Apis.getAPIService().OTPapi(map);
        call.enqueue(new Callback<OTPModel>() {
            @Override
            public void onResponse(Call<OTPModel> call, Response<OTPModel> response) {
                OTPModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus() == 1) {
                            session.iseditor();
                            session.createUserLoginSession(String.valueOf(userdata.getUserData().getUserId()),
                                    userdata.getUserData().getUsername(),
                                    userdata.getUserData().getName(), userdata.getUserData().getEmail(),
                                    "", "",
                                    "", "",
                                    "");
//                            Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                            startActivity(new Intent(Verification.this, Dashboard.class));
                        }
                    else {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<OTPModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void ResendOTP(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Verification.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Resend_OTP_Model> call = Apis.getAPIService().Resendotp(map);
        call.enqueue(new Callback<Resend_OTP_Model>() {
            @Override
            public void onResponse(Call<Resend_OTP_Model> call, Response<Resend_OTP_Model> response) {
                dialog.dismiss();
                Resend_OTP_Model userdata = response.body();

                if (userdata != null) {
                    if (userdata.getStatus()==1) {
                        Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                        //  otp_view.setText(userdata.getData().getOtp()+"");
//                        View parentLayout = findViewById(android.R.id.content);
//                        Snackbar.make(parentLayout, "your otp is "+userdata.getData().getOtp(), Snackbar.LENGTH_LONG)
//                                .setAction("CLOSE", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                    }
//                                })
//                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
//                                .show();
                    }
                } else {
                    Custom_Toast_Activity.makeText(getApplicationContext(), userdata.getMessage(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(Call<Resend_OTP_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
               /* textViewMessage.setText(
                        String.format("%s - %s", getString(R.string.received_message), message));
*/
                getOtpFromMessage(message);
            }
        }
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            Log.d("sdfg",matcher.group(0)+"");
            int num = Integer.parseInt(matcher.group(0)+"");
            String number = String.valueOf(num);
            for(int i = 0; i < number.length(); i++) {
                int j = Character.digit(number.charAt(i), 10);
                System.out.println("digit: " + j);
                if (i == 0) {
                    Log.d("num1", j + "");
                    et1.setText(j+"");
                }
                if (i == 1) {
                    Log.d("num2", j + "");
                    et2.setText(j+"");
                }
                if (i == 2) {
                    Log.d("num3", j + "");
                    et3.setText(j+"");
                }
                if (i == 3) {
                    Log.d("num4", j + "");
                    et4.setText(j+"");
                }
            }
        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }
}
