package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Add_Cart_Model {


    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @Expose
    @SerializedName("CartCount")
    private String CartCount;
    @Expose
    @SerializedName("totalcartamount")
    private String totalcartamount;
    @Expose
    @SerializedName("cartType")
    private String cartType;

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public String getCartCount() {
        return CartCount;
    }

    public String getTotalcartamount() {
        return totalcartamount;
    }

    public void setTotalcartamount(String totalcartamount) {
        this.totalcartamount = totalcartamount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }
}
