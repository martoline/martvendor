package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cart_List_Basket_Model {
    @Expose
    @SerializedName("response")
    private Response response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("CartCount")
    private String CartCount;

    public String getCartCount() {
        return CartCount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("paymentmethode")
        private String paymentmethode;

        public String getPaymentmethode() {
            return paymentmethode;
        }

        public void setPaymentmethode(String paymentmethode) {
            this.paymentmethode = paymentmethode;
        }
        @Expose
        @SerializedName("deliveryslot")
        private List<Deliveryslot> deliveryslot;
        @Expose
        @SerializedName("address")
        private List<Address> address;
        @Expose
        @SerializedName("discount")
        private String discount;
        @Expose
        @SerializedName("saving")
        private String saving;
        @Expose
        @SerializedName("tax")
        private String tax;
        @Expose
        @SerializedName("shipping")
        private String shipping;
        @Expose
        @SerializedName("totalamount")
        private String totalamount;
        @Expose
        @SerializedName("cart")
        private List<Cart> cart;

        public List<Deliveryslot> getDeliveryslot() {
            return deliveryslot;
        }

        public String getSaving() {
            return saving;
        }

        public void setSaving(String saving) {
            this.saving = saving;
        }

        public void setDeliveryslot(List<Deliveryslot> deliveryslot) {
            this.deliveryslot = deliveryslot;
        }

        public List<Address> getAddress() {
            return address;
        }

        public void setAddress(List<Address> address) {
            this.address = address;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getShipping() {
            return shipping;
        }

        public void setShipping(String shipping) {
            this.shipping = shipping;
        }

        public String getTotalamount() {
            return totalamount;
        }

        public void setTotalamount(String totalamount) {
            this.totalamount = totalamount;
        }

        public List<Cart> getCart() {
            return cart;
        }

        public void setCart(List<Cart> cart) {
            this.cart = cart;
        }
    }

    public static class Deliveryslot {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("uid")
        private String uid;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Address {
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("activeaddress")
        private String activeaddress;
        @Expose
        @SerializedName("uid")
        private String uid;
        @Expose
        @SerializedName("pin")
        private String pin;
        @Expose
        @SerializedName("landmark")
        private String landmark;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getActiveaddress() {
            return activeaddress;
        }

        public void setActiveaddress(String activeaddress) {
            this.activeaddress = activeaddress;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Cart {
        @Expose
        @SerializedName("sizeId")
        private String sizeId;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("discription")
        private String discription;
        @Expose
        @SerializedName("small_des")
        private String small_des;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("oldprice")
        private String oldprice;
        @Expose
        @SerializedName("Title")
        private String Title;
        @Expose
        @SerializedName("qty")
        private String qty;
        @Expose
        @SerializedName("vendorname")
        private String vendorname;
        @Expose
        @SerializedName("ProductId")
        private String ProductId;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("Pickup")
        private String Pickup;
        @Expose
        @SerializedName("Delivery")
        private String Delivery;

        public String getPickup() {
            return Pickup;
        }

        public void setPickup(String pickup) {
            Pickup = pickup;
        }

        public String getDelivery() {
            return Delivery;
        }

        public void setDelivery(String delivery) {
            Delivery = delivery;
        }

        public String getSizeId() {
            return sizeId;
        }

        public void setSizeId(String sizeId) {
            this.sizeId = sizeId;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDiscription() {
            return discription;
        }

        public void setDiscription(String discription) {
            this.discription = discription;
        }

        public String getSmall_des() {
            return small_des;
        }

        public void setSmall_des(String small_des) {
            this.small_des = small_des;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getOldprice() {
            return oldprice;
        }

        public void setOldprice(String oldprice) {
            this.oldprice = oldprice;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getVendorname() {
            return vendorname;
        }

        public void setVendorname(String vendorname) {
            this.vendorname = vendorname;
        }

        public String getProductId() {
            return ProductId;
        }

        public void setProductId(String ProductId) {
            this.ProductId = ProductId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
