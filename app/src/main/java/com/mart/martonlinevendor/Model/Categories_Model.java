package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class Categories_Model implements Serializable {


    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response  implements Serializable{
        @Expose
        @SerializedName("subCatData")
        private List<SubCatData> subCatData;
        @Expose
        @SerializedName("offertest")
        private String offertest;
        @Expose
        @SerializedName("catImage")
        private String catImage;
        @Expose
        @SerializedName("catName")
        private String catName;
        @Expose
        @SerializedName("catId")
        private String catId;

        public List<SubCatData> getSubCatData() {
            return subCatData;
        }

        public void setSubCatData(List<SubCatData> subCatData) {
            this.subCatData = subCatData;
        }

        public String getOffertest() {
            return offertest;
        }

        public void setOffertest(String offertest) {
            this.offertest = offertest;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }
    }

    public static class SubCatData  implements Serializable{
        @Expose
        @SerializedName("ssubCatData")
        private List<SsubCatData> ssubCatData;
        @Expose
        @SerializedName("offertest")
        private String offertest;
        @Expose
        @SerializedName("catImage")
        private String catImage;
        @Expose
        @SerializedName("catName")
        private String catName;
        @Expose
        @SerializedName("catId")
        private String catId;

        public List<SsubCatData> getSsubCatData() {
            return ssubCatData;
        }

        public void setSsubCatData(List<SsubCatData> ssubCatData) {
            this.ssubCatData = ssubCatData;
        }

        public String getOffertest() {
            return offertest;
        }

        public void setOffertest(String offertest) {
            this.offertest = offertest;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }
    }

    public static class SsubCatData  implements Serializable{
        @Expose
        @SerializedName("offertest")
        private String offertest;
        @Expose
        @SerializedName("catImage")
        private String catImage;
        @Expose
        @SerializedName("catName")
        private String catName;
        @Expose
        @SerializedName("catId")
        private String catId;

        public String getOffertest() {
            return offertest;
        }

        public void setOffertest(String offertest) {
            this.offertest = offertest;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }
    }
}
