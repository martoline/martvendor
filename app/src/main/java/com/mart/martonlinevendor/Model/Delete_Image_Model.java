package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Delete_Image_Model {

    @Expose
    @SerializedName("response")
    private String response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
