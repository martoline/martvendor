package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Delivery_Time_Slot_Get_Model {


    @Expose
    @SerializedName("deliveryslot")
    private List<Deliveryslot> deliveryslot;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public List<Deliveryslot> getDeliveryslot() {
        return deliveryslot;
    }

    public void setDeliveryslot(List<Deliveryslot> deliveryslot) {
        this.deliveryslot = deliveryslot;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Deliveryslot {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("uid")
        private String uid;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
