package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Home_Model {
    @Expose
    @SerializedName("response")
    private Response response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("CartCount")
    private String CartCount;
    @Expose
    @SerializedName("version")
    private String version;
    @Expose
    @SerializedName("displaydialog")
    private String displaydialog;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDisplaydialog() {
        return displaydialog;
    }

    public void setDisplaydialog(String displaydialog) {
        this.displaydialog = displaydialog;
    }

    public String getCartCount() {
        return CartCount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("shopByCategory")
        private List<ShopByCategory> shopByCategory;
        @Expose
        @SerializedName("HomeBottom")
        private List<HomeBottom> HomeBottom;
        @Expose
        @SerializedName("HomeTop")
        private HomeTop HomeTop;
        @Expose
        @SerializedName("Banners")
        private List<Banners> Banners;

        public List<ShopByCategory> getShopByCategory() {
            return shopByCategory;
        }

        public void setShopByCategory(List<ShopByCategory> shopByCategory) {
            this.shopByCategory = shopByCategory;
        }

        public List<HomeBottom> getHomeBottom() {
            return HomeBottom;
        }

        public void setHomeBottom(List<HomeBottom> HomeBottom) {
            this.HomeBottom = HomeBottom;
        }

        public HomeTop getHomeTop() {
            return HomeTop;
        }

        public void setHomeTop(HomeTop HomeTop) {
            this.HomeTop = HomeTop;
        }

        public List<Banners> getBanners() {
            return Banners;
        }

        public void setBanners(List<Banners> Banners) {
            this.Banners = Banners;
        }
    }

    public static class ShopByCategory {
        @Expose
        @SerializedName("catImage")
        private String catImage;
        @Expose
        @SerializedName("catName")
        private String catName;
        @Expose
        @SerializedName("catId")
        private String catId;

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }
    }

    public static class HomeBottom {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("id")
        private String id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class HomeTop {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("id")
        private String id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Banners {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("link")
        private String link;
        @Expose
        @SerializedName("id")
        private String id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
