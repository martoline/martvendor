package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class My_Order_Model {


    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("version")
    private String version;
    @Expose
    @SerializedName("displaydialog")
    private String displaydialog;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDisplaydialog() {
        return displaydialog;
    }
    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("productDetail")
        private List<ProductDetail> productDetail;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("paymenttype")
        private String paymenttype;
        @Expose
        @SerializedName("TotalAmount")
        private String TotalAmount;
        @Expose
        @SerializedName("totalqty")
        private String totalqty;
        @Expose
        @SerializedName("orderStatus")
        private String orderStatus;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("orderId")
        private String orderId;
        @Expose
        @SerializedName("id")
        private String id;

        public List<ProductDetail> getProductDetail() {
            return productDetail;
        }

        public void setProductDetail(List<ProductDetail> productDetail) {
            this.productDetail = productDetail;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getPaymenttype() {
            return paymenttype;
        }

        public void setPaymenttype(String paymenttype) {
            this.paymenttype = paymenttype;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getTotalqty() {
            return totalqty;
        }

        public void setTotalqty(String totalqty) {
            this.totalqty = totalqty;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class ProductDetail {
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("itmeprice")
        private String itmeprice;
        @Expose
        @SerializedName("productId")
        private String productId;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("title")
        private String title;

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getItmeprice() {
            return itmeprice;
        }

        public void setItmeprice(String itmeprice) {
            this.itmeprice = itmeprice;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
