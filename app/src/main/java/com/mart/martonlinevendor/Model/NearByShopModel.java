package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearByShopModel {

    @Expose
    @SerializedName("bannerimage")
    private String bannerimage;
    @Expose
    @SerializedName("exclusive")
    private List<Exclusive> exclusive;
    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("CartCount")
    private String CartCount;

    public String getCartCount() {
        return CartCount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }

    public String getBannerimage() {
        return bannerimage;
    }

    public void setBannerimage(String bannerimage) {
        this.bannerimage = bannerimage;
    }

    public List<Exclusive> getExclusive() {
        return exclusive;
    }

    public void setExclusive(List<Exclusive> exclusive) {
        this.exclusive = exclusive;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Exclusive {
        @Expose
        @SerializedName("km")
        private String km;
        @Expose
        @SerializedName("rating")
        private String rating;
        @Expose
        @SerializedName("Delivery")
        private String Delivery;
        @Expose
        @SerializedName("Pickup")
        private String Pickup;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("offer")
        private String offer;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("shopname")
        private String shopname;
        @Expose
        @SerializedName("shopID")
        private String shopID;

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getDelivery() {
            return Delivery;
        }

        public void setDelivery(String Delivery) {
            this.Delivery = Delivery;
        }

        public String getPickup() {
            return Pickup;
        }

        public void setPickup(String Pickup) {
            this.Pickup = Pickup;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getShopID() {
            return shopID;
        }

        public void setShopID(String shopID) {
            this.shopID = shopID;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("km")
        private String km;
        @Expose
        @SerializedName("rating")
        private String rating;
        @Expose
        @SerializedName("Delivery")
        private String Delivery;
        @Expose
        @SerializedName("Pickup")
        private String Pickup;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("offer")
        private String offer;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("shopname")
        private String shopname;
        @Expose
        @SerializedName("shopID")
        private String shopID;

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getDelivery() {
            return Delivery;
        }

        public void setDelivery(String Delivery) {
            this.Delivery = Delivery;
        }

        public String getPickup() {
            return Pickup;
        }

        public void setPickup(String Pickup) {
            this.Pickup = Pickup;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getShopID() {
            return shopID;
        }

        public void setShopID(String shopID) {
            this.shopID = shopID;
        }
    }
}
