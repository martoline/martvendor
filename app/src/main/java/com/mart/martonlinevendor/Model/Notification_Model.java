package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notification_Model {
    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("notificationlist")
        private List<Notificationlist> notificationlist;
        @Expose
        @SerializedName("date")
        private String date;

        public List<Notificationlist> getNotificationlist() {
            return notificationlist;
        }

        public void setNotificationlist(List<Notificationlist> notificationlist) {
            this.notificationlist = notificationlist;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    public static class Notificationlist {
        @Expose
        @SerializedName("readstatus")
        private String readstatus;
        @Expose
        @SerializedName("created_date")
        private String created_date;
        @Expose
        @SerializedName("notification")
        private String notification;

        public String getReadstatus() {
            return readstatus;
        }

        public void setReadstatus(String readstatus) {
            this.readstatus = readstatus;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }
    }
}
