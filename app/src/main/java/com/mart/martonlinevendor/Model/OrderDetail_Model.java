package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class OrderDetail_Model {


    @Expose
    @SerializedName("response")
    private Response response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("summary")
        private Summary summary;
        @Expose
        @SerializedName("data")
        private List<Data> data;

        public Summary getSummary() {
            return summary;
        }

        public void setSummary(Summary summary) {
            this.summary = summary;
        }

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }
    }

    public static class Summary {
        @Expose
        @SerializedName("discount")
        private String discount;
        @Expose
        @SerializedName("shippingcharge")
        private String shippingcharge;
        @Expose
        @SerializedName("deliverytype")
        private String deliverytype;
        @Expose
        @SerializedName("deliveryslotid")
        private String deliveryslotid;
        @Expose
        @SerializedName("payment_status")
        private String payment_status;
        @Expose
        @SerializedName("txn_id")
        private String txn_id;
        @Expose
        @SerializedName("shopname")
        private String shopname;
        @Expose
        @SerializedName("order_status_date")
        private String order_status_date;
        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("order_status")
        private String order_status;
        @Expose
        @SerializedName("totalqty")
        private String totalqty;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("paymenttype")
        private String paymenttype;
        @Expose
        @SerializedName("orderid")
        private String orderid;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("totalprice")
        private String totalprice;

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getShippingcharge() {
            return shippingcharge;
        }

        public void setShippingcharge(String shippingcharge) {
            this.shippingcharge = shippingcharge;
        }

        public String getDeliverytype() {
            return deliverytype;
        }

        public void setDeliverytype(String deliverytype) {
            this.deliverytype = deliverytype;
        }

        public String getDeliveryslotid() {
            return deliveryslotid;
        }

        public void setDeliveryslotid(String deliveryslotid) {
            this.deliveryslotid = deliveryslotid;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getTxn_id() {
            return txn_id;
        }

        public void setTxn_id(String txn_id) {
            this.txn_id = txn_id;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getOrder_status_date() {
            return order_status_date;
        }

        public void setOrder_status_date(String order_status_date) {
            this.order_status_date = order_status_date;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getTotalqty() {
            return totalqty;
        }

        public void setTotalqty(String totalqty) {
            this.totalqty = totalqty;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getPaymenttype() {
            return paymenttype;
        }

        public void setPaymenttype(String paymenttype) {
            this.paymenttype = paymenttype;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("imagess")
        private List<Imagess> imagess;
        @Expose
        @SerializedName("returnstatus")
        private String returnstatus;
        @Expose
        @SerializedName("vid")
        private String vid;
        @Expose
        @SerializedName("offer")
        private String offer;
        @Expose
        @SerializedName("vendorname")
        private String vendorname;
        @Expose
        @SerializedName("savedamount")
        private String savedamount;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("feature")
        private String feature;
        @Expose
        @SerializedName("qty")
        private String qty;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("discription")
        private String discription;
        @Expose
        @SerializedName("small_des")
        private String small_des;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("actualprice")
        private String actualprice;
        @Expose
        @SerializedName("Title")
        private String Title;
        @Expose
        @SerializedName("Productid")
        private String Productid;
        @Expose
        @SerializedName("itemId")
        private String itemId;

        public List<Imagess> getImagess() {
            return imagess;
        }

        public void setImagess(List<Imagess> imagess) {
            this.imagess = imagess;
        }

        public String getReturnstatus() {
            return returnstatus;
        }

        public void setReturnstatus(String returnstatus) {
            this.returnstatus = returnstatus;
        }

        public String getVid() {
            return vid;
        }

        public void setVid(String vid) {
            this.vid = vid;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getVendorname() {
            return vendorname;
        }

        public void setVendorname(String vendorname) {
            this.vendorname = vendorname;
        }

        public String getSavedamount() {
            return savedamount;
        }

        public void setSavedamount(String savedamount) {
            this.savedamount = savedamount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDiscription() {
            return discription;
        }

        public void setDiscription(String discription) {
            this.discription = discription;
        }

        public String getSmall_des() {
            return small_des;
        }

        public void setSmall_des(String small_des) {
            this.small_des = small_des;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getActualprice() {
            return actualprice;
        }

        public void setActualprice(String actualprice) {
            this.actualprice = actualprice;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getProductid() {
            return Productid;
        }

        public void setProductid(String Productid) {
            this.Productid = Productid;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }
    }

    public static class Imagess {
        @Expose
        @SerializedName("product_image")
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
