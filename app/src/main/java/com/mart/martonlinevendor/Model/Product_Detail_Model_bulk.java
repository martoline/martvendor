package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Product_Detail_Model_bulk {

    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("CartCount")
    private String CartCount;

    public String getCartCount() {
        return CartCount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("review")
        private List<String> review;
        @Expose
        @SerializedName("Images")
        private List<Images> Images;
        @Expose
        @SerializedName("Size_chart")
        private List<Size_chart> Size_chart;
        @Expose
        @SerializedName("feature")
        private String feature;

        @Expose
        @SerializedName("shopname")
        private String shopname;
        @Expose
        @SerializedName("wish")
        private String wish;
        @Expose
        @SerializedName("discription")
        private String discription;
        @Expose
        @SerializedName("small_des")
        private String small_des;

        @Expose
        @SerializedName("MRP")
        private String MRP;
        @Expose
        @SerializedName("sale_price")
        private String sale_price;

        @Expose
        @SerializedName("Product_Name")
        private String Product_Name;
        @Expose
        @SerializedName("Product_id")
        private String Product_id;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("quantity_count")
        private String quantity_count;
        @Expose
        @SerializedName("qty")
        private String qty;
        @Expose
        @SerializedName("minqty")
        private String minqty;
        @Expose
        @SerializedName("maxqty")
        private String maxqty;
        @Expose
        @SerializedName("Pickup")
        private String pickup;
        @Expose
        @SerializedName("Delivery")
        private String Delivery;

        public String getMinqty() {
            return minqty;
        }

        public void setMinqty(String minqty) {
            this.minqty = minqty;
        }

        public String getMaxqty() {
            return maxqty;
        }

        public void setMaxqty(String maxqty) {
            this.maxqty = maxqty;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getPickup() {
            return pickup;
        }

        public void setPickup(String pickup) {
            this.pickup = pickup;
        }

        public String getDelivery() {
            return Delivery;
        }

        public void setDelivery(String delivery) {
            Delivery = delivery;
        }
        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getShopname() {
            return shopname;
        }

        public String getMRP() {
            return MRP;
        }

        public void setMRP(String MRP) {
            this.MRP = MRP;
        }

        public String getSale_price() {
            return sale_price;
        }

        public void setSale_price(String sale_price) {
            this.sale_price = sale_price;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getQuantity_count() {
            return quantity_count;
        }

        public void setQuantity_count(String quantity_count) {
            this.quantity_count = quantity_count;
        }

        public String getWish() {
            return wish;
        }

        public void setWish(String wish) {
            this.wish = wish;
        }

        public List<String> getReview() {
            return review;
        }

        public void setReview(List<String> review) {
            this.review = review;
        }

        public List<Images> getImages() {
            return Images;
        }

        public void setImages(List<Images> Images) {
            this.Images = Images;
        }

        public List<Size_chart> getSize_chart() {
            return Size_chart;
        }

        public void setSize_chart(List<Size_chart> Size_chart) {
            this.Size_chart = Size_chart;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public String getDiscription() {
            return discription;
        }

        public void setDiscription(String discription) {
            this.discription = discription;
        }

        public String getSmall_des() {
            return small_des;
        }

        public void setSmall_des(String small_des) {
            this.small_des = small_des;
        }

        public String getProduct_Name() {
            return Product_Name;
        }

        public void setProduct_Name(String Product_Name) {
            this.Product_Name = Product_Name;
        }

        public String getProduct_id() {
            return Product_id;
        }

        public void setProduct_id(String Product_id) {
            this.Product_id = Product_id;
        }
    }

    public static class Images {
        @Expose
        @SerializedName("Product_Image")
        private String Product_Image;
        @Expose
        @SerializedName("Image_Id")
        private String Image_Id;

        public String getProduct_Image() {
            return Product_Image;
        }

        public void setProduct_Image(String Product_Image) {
            this.Product_Image = Product_Image;
        }

        public String getImage_Id() {
            return Image_Id;
        }

        public void setImage_Id(String Image_Id) {
            this.Image_Id = Image_Id;
        }
    }

    public static class Size_chart {
        @Expose
        @SerializedName("Quantity")
        private String Quantity;
        @Expose
        @SerializedName("selling_price")
        private String selling_price;
        @Expose
        @SerializedName("Actual_Price")
        private String Actual_Price;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("Size_id")
        private String Size_id;

        public String getQuantity() {
            return Quantity;
        }

        public void setQuantity(String Quantity) {
            this.Quantity = Quantity;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getActual_Price() {
            return Actual_Price;
        }

        public void setActual_Price(String Actual_Price) {
            this.Actual_Price = Actual_Price;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSize_id() {
            return Size_id;
        }

        public void setSize_id(String Size_id) {
            this.Size_id = Size_id;
        }
    }
}
