package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Product_Search_Model {


    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("Images")
        private List<Images> Images;
        @Expose
        @SerializedName("Quantity")
        private String Quantity;
        @Expose
        @SerializedName("selling_price")
        private String selling_price;
        @Expose
        @SerializedName("Actual_Price")
        private String Actual_Price;
        @Expose
        @SerializedName("size_id")
        private String size_id;
        @Expose
        @SerializedName("stock")
        private String stock;
        @Expose
        @SerializedName("view")
        private String view;
        @Expose
        @SerializedName("sscid")
        private String sscid;
        @Expose
        @SerializedName("today")
        private String today;
        @Expose
        @SerializedName("latest")
        private String latest;
        @Expose
        @SerializedName("featurep")
        private String featurep;
        @Expose
        @SerializedName("bestseller")
        private String bestseller;
        @Expose
        @SerializedName("discount")
        private String discount;
        @Expose
        @SerializedName("pincode")
        private String pincode;
        @Expose
        @SerializedName("color")
        private String color;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("ssid")
        private String ssid;
        @Expose
        @SerializedName("tags")
        private String tags;
        @Expose
        @SerializedName("vstatus")
        private String vstatus;
        @Expose
        @SerializedName("uid")
        private String uid;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("create_date")
        private String create_date;
        @Expose
        @SerializedName("feature")
        private String feature;
        @Expose
        @SerializedName("shipping")
        private String shipping;
        @Expose
        @SerializedName("freight")
        private String freight;
        @Expose
        @SerializedName("weight")
        private String weight;
        @Expose
        @SerializedName("product_status")
        private String product_status;
        @Expose
        @SerializedName("scid")
        private String scid;
        @Expose
        @SerializedName("cid")
        private String cid;
        @Expose
        @SerializedName("qty")
        private String qty;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("discription")
        private String discription;
        @Expose
        @SerializedName("small_des")
        private String small_des;
        @Expose
        @SerializedName("dummy")
        private String dummy;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("oldprice")
        private String oldprice;
        @Expose
        @SerializedName("ptitle")
        private String ptitle;
        @Expose
        @SerializedName("id")
        private String id;

        public List<Images> getImages() {
            return Images;
        }

        public void setImages(List<Images> Images) {
            this.Images = Images;
        }

        public String getQuantity() {
            return Quantity;
        }

        public void setQuantity(String Quantity) {
            this.Quantity = Quantity;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getActual_Price() {
            return Actual_Price;
        }

        public void setActual_Price(String Actual_Price) {
            this.Actual_Price = Actual_Price;
        }

        public String getSize_id() {
            return size_id;
        }

        public void setSize_id(String size_id) {
            this.size_id = size_id;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getView() {
            return view;
        }

        public void setView(String view) {
            this.view = view;
        }

        public String getSscid() {
            return sscid;
        }

        public void setSscid(String sscid) {
            this.sscid = sscid;
        }

        public String getToday() {
            return today;
        }

        public void setToday(String today) {
            this.today = today;
        }

        public String getLatest() {
            return latest;
        }

        public void setLatest(String latest) {
            this.latest = latest;
        }

        public String getFeaturep() {
            return featurep;
        }

        public void setFeaturep(String featurep) {
            this.featurep = featurep;
        }

        public String getBestseller() {
            return bestseller;
        }

        public void setBestseller(String bestseller) {
            this.bestseller = bestseller;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getVstatus() {
            return vstatus;
        }

        public void setVstatus(String vstatus) {
            this.vstatus = vstatus;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreate_date() {
            return create_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public String getShipping() {
            return shipping;
        }

        public void setShipping(String shipping) {
            this.shipping = shipping;
        }

        public String getFreight() {
            return freight;
        }

        public void setFreight(String freight) {
            this.freight = freight;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getProduct_status() {
            return product_status;
        }

        public void setProduct_status(String product_status) {
            this.product_status = product_status;
        }

        public String getScid() {
            return scid;
        }

        public void setScid(String scid) {
            this.scid = scid;
        }

        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDiscription() {
            return discription;
        }

        public void setDiscription(String discription) {
            this.discription = discription;
        }

        public String getSmall_des() {
            return small_des;
        }

        public void setSmall_des(String small_des) {
            this.small_des = small_des;
        }

        public String getDummy() {
            return dummy;
        }

        public void setDummy(String dummy) {
            this.dummy = dummy;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getOldprice() {
            return oldprice;
        }

        public void setOldprice(String oldprice) {
            this.oldprice = oldprice;
        }

        public String getPtitle() {
            return ptitle;
        }

        public void setPtitle(String ptitle) {
            this.ptitle = ptitle;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Images {
        @Expose
        @SerializedName("Product_Image")
        private String Product_Image;
        @Expose
        @SerializedName("Image_Id")
        private String Image_Id;

        public String getProduct_Image() {
            return Product_Image;
        }

        public void setProduct_Image(String Product_Image) {
            this.Product_Image = Product_Image;
        }

        public String getImage_Id() {
            return Image_Id;
        }

        public void setImage_Id(String Image_Id) {
            this.Image_Id = Image_Id;
        }
    }
}
