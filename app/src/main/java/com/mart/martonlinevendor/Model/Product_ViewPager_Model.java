package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Product_ViewPager_Model {


    @Expose
    @SerializedName("response")
    private List<Response> response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("Images")
        private List<Images> Images;
        @Expose
        @SerializedName("Size_chart")
        private List<Size_chart> Size_chart;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("qty")
        private String qty;
        @Expose
        @SerializedName("offerprice")
        private String offerprice;
        @Expose
        @SerializedName("mrp")
        private String mrp;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("mainimage")
        private String mainimage;
        @Expose
        @SerializedName("create_date")
        private String create_date;
        @Expose
        @SerializedName("feature")
        private String feature;
        @Expose
        @SerializedName("discription")
        private String discription;
        @Expose
        @SerializedName("small_des")
        private String small_des;
        @Expose
        @SerializedName("Product_Name")
        private String Product_Name;
        @Expose
        @SerializedName("Product_id")
        private String Product_id;

        public String getSizeid() {
            return Sizeid;
        }

        public void setSizeid(String sizeid) {
            Sizeid = sizeid;
        }

        private String Sizeid;


        public String getSizevalue() {
            return Sizevalue;
        }

        public void setSizevalue(String sizevalue) {
            Sizevalue = sizevalue;
        }

        private String Sizevalue;


        public List<Images> getImages() {
            return Images;
        }

        public void setImages(List<Images> Images) {
            this.Images = Images;
        }

        public List<Size_chart> getSize_chart() {
            return Size_chart;
        }

        public void setSize_chart(List<Size_chart> Size_chart) {
            this.Size_chart = Size_chart;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getOfferprice() {
            return offerprice;
        }

        public void setOfferprice(String offerprice) {
            this.offerprice = offerprice;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getMainimage() {
            return mainimage;
        }

        public void setMainimage(String mainimage) {
            this.mainimage = mainimage;
        }

        public String getCreate_date() {
            return create_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public String getDiscription() {
            return discription;
        }

        public void setDiscription(String discription) {
            this.discription = discription;
        }

        public String getSmall_des() {
            return small_des;
        }

        public void setSmall_des(String small_des) {
            this.small_des = small_des;
        }

        public String getProduct_Name() {
            return Product_Name;
        }

        public void setProduct_Name(String Product_Name) {
            this.Product_Name = Product_Name;
        }

        public String getProduct_id() {
            return Product_id;
        }

        public void setProduct_id(String Product_id) {
            this.Product_id = Product_id;
        }
    }

    public static class Images {
        @Expose
        @SerializedName("Product_Image")
        private String Product_Image;
        @Expose
        @SerializedName("Image_Id")
        private String Image_Id;

        public String getProduct_Image() {
            return Product_Image;
        }

        public void setProduct_Image(String Product_Image) {
            this.Product_Image = Product_Image;
        }

        public String getImage_Id() {
            return Image_Id;
        }

        public void setImage_Id(String Image_Id) {
            this.Image_Id = Image_Id;
        }
    }

    public static class Size_chart {
        @Expose
        @SerializedName("size_value")
        private String size_value;
        @Expose
        @SerializedName("Quantity")
        private String Quantity;
        @Expose
        @SerializedName("selling_price")
        private String selling_price;
        @Expose
        @SerializedName("Actual_Price")
        private String Actual_Price;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("Size_id")
        private String Size_id;

        public String getSize_value() {
            return size_value;
        }

        public void setSize_value(String size_value) {
            this.size_value = size_value;
        }

        public String getQuantity() {
            return Quantity;
        }

        public void setQuantity(String Quantity) {
            this.Quantity = Quantity;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getActual_Price() {
            return Actual_Price;
        }

        public void setActual_Price(String Actual_Price) {
            this.Actual_Price = Actual_Price;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSize_id() {
            return Size_id;
        }

        public void setSize_id(String Size_id) {
            this.Size_id = Size_id;
        }
    }
}
