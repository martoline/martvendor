package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ShopDetailModel {


    @Expose
    @SerializedName("errormsg")
    private String errormsg;
    @Expose
    @SerializedName("bannerimage")
    private String bannerimage;
    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("CartCount")
    private String CartCount;

    public String getCartCount() {
        return CartCount;
    }

    public void setCartCount(String cartCount) {
        CartCount = cartCount;
    }
    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public String getBannerimage() {
        return bannerimage;
    }

    public void setBannerimage(String bannerimage) {
        this.bannerimage = bannerimage;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("products")
        private List<Products> products;
        @Expose
        @SerializedName("offer")
        private String offer;
        @Expose
        @SerializedName("totalreview")
        private String totalreview;
        @Expose
        @SerializedName("rating")
        private String rating;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("closetime")
        private String closetime;
        @Expose
        @SerializedName("opentime")
        private String opentime;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("minorder")
        private String minorder;
        @Expose
        @SerializedName("deliverytime")
        private String deliverytime;
        @Expose
        @SerializedName("shopcategory")
        private String shopcategory;
        @Expose
        @SerializedName("shopname")
        private String shopname;

        public List<Products> getProducts() {
            return products;
        }

        public void setProducts(List<Products> products) {
            this.products = products;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getTotalreview() {
            return totalreview;
        }

        public void setTotalreview(String totalreview) {
            this.totalreview = totalreview;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getClosetime() {
            return closetime;
        }

        public void setClosetime(String closetime) {
            this.closetime = closetime;
        }

        public String getOpentime() {
            return opentime;
        }

        public void setOpentime(String opentime) {
            this.opentime = opentime;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMinorder() {
            return minorder;
        }

        public void setMinorder(String minorder) {
            this.minorder = minorder;
        }

        public String getDeliverytime() {
            return deliverytime;
        }

        public void setDeliverytime(String deliverytime) {
            this.deliverytime = deliverytime;
        }

        public String getShopcategory() {
            return shopcategory;
        }

        public void setShopcategory(String shopcategory) {
            this.shopcategory = shopcategory;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }
    }

    public static class Products {
        @Expose
        @SerializedName("catImage")
        private String catImage;
        @Expose
        @SerializedName("catName")
        private String catName;
        @Expose
        @SerializedName("catId")
        private String catId;
        @Expose
        @SerializedName("adult")
        private String adult;

        public String getAdult() {
            return adult;
        }

        public void setAdult(String adult) {
            this.adult = adult;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }
    }
}
