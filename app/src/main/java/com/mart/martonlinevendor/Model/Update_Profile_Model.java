package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Update_Profile_Model {


    @Expose
    @SerializedName("userData")
    private UserData userData;
    @Expose
    @SerializedName("adminmsg")
    private String adminmsg;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getAdminmsg() {
        return adminmsg;
    }

    public void setAdminmsg(String adminmsg) {
        this.adminmsg = adminmsg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class UserData {
        @Expose
        @SerializedName("Delivery")
        private String Delivery;
        @Expose
        @SerializedName("Pickup")
        private String Pickup;
        @Expose
        @SerializedName("category")
        private String category;
        @Expose
        @SerializedName("pincode")
        private String pincode;
        @Expose
        @SerializedName("shop_image")
        private String shop_image;
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("token")
        private String token;
        @Expose
        @SerializedName("pan")
        private String pan;
        @Expose
        @SerializedName("aadhar")
        private String aadhar;
        @Expose
        @SerializedName("gst")
        private String gst;
        @Expose
        @SerializedName("minorder")
        private String minorder;
        @Expose
        @SerializedName("deliverytime")
        private String deliverytime;
        @Expose
        @SerializedName("closetime")
        private String closetime;
        @Expose
        @SerializedName("opentime")
        private String opentime;
        @Expose
        @SerializedName("shopname")
        private String shopname;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("password")
        private String password;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("Username")
        private String Username;
        @Expose
        @SerializedName("lname")
        private String lname;
        @Expose
        @SerializedName("fname")
        private String fname;
        @Expose
        @SerializedName("id")
        private String id;

        public String getDelivery() {
            return Delivery;
        }

        public void setDelivery(String Delivery) {
            this.Delivery = Delivery;
        }

        public String getPickup() {
            return Pickup;
        }

        public void setPickup(String Pickup) {
            this.Pickup = Pickup;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getShop_image() {
            return shop_image;
        }

        public void setShop_image(String shop_image) {
            this.shop_image = shop_image;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPan() {
            return pan;
        }

        public void setPan(String pan) {
            this.pan = pan;
        }

        public String getAadhar() {
            return aadhar;
        }

        public void setAadhar(String aadhar) {
            this.aadhar = aadhar;
        }

        public String getGst() {
            return gst;
        }

        public void setGst(String gst) {
            this.gst = gst;
        }

        public String getMinorder() {
            return minorder;
        }

        public void setMinorder(String minorder) {
            this.minorder = minorder;
        }

        public String getDeliverytime() {
            return deliverytime;
        }

        public void setDeliverytime(String deliverytime) {
            this.deliverytime = deliverytime;
        }

        public String getClosetime() {
            return closetime;
        }

        public void setClosetime(String closetime) {
            this.closetime = closetime;
        }

        public String getOpentime() {
            return opentime;
        }

        public void setOpentime(String opentime) {
            this.opentime = opentime;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getUsername() {
            return Username;
        }

        public void setUsername(String Username) {
            this.Username = Username;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
