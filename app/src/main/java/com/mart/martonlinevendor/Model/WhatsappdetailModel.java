package com.mart.martonlinevendor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class WhatsappdetailModel {


    @Expose
    @SerializedName("response")
    private Response response;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Response {
        @Expose
        @SerializedName("orderId")
        private String orderId;
        @Expose
        @SerializedName("allimages")
        private List<Allimages> allimages;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("pickup")
        private String pickup;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("shop_id")
        private String shop_id;
        @Expose
        @SerializedName("images")
        private String images;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("payment_status")
        private String payment_status;
        @Expose
        @SerializedName("order_status")
        private String order_status;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("uid")
        private String uid;
        @Expose
        @SerializedName("id")
        private String id;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public List<Allimages> getAllimages() {
            return allimages;
        }

        public void setAllimages(List<Allimages> allimages) {
            this.allimages = allimages;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPickup() {
            return pickup;
        }

        public void setPickup(String pickup) {
            this.pickup = pickup;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Allimages {
        @Expose
        @SerializedName("image")
        private String image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
