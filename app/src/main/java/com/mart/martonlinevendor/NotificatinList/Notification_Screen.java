package com.mart.martonlinevendor.NotificatinList;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Notification_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notification_Screen extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recyclerViewNotification;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout dialogView;
    UserSessionManager session;
    HashMap<String, String> user;
    private LinearLayout no_data_found;
    private List<Notification_Model.Response> array_notification_list;
    private List<Notification_Model.Notificationlist> array_notification_list_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification__screen);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  startActivity(new Intent(Notification_Screen.this, OuterCart.class));
            }
        });
        header = findViewById(R.id.header);
        recyclerViewNotification = findViewById(R.id.recyclerViewNotification);
        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
        dialogView = findViewById(R.id.dialogView);
        no_data_found = findViewById(R.id.no_data_found);
        recyclerViewNotification.setVisibility(View.VISIBLE);
        no_data_found.setVisibility(View.GONE);

//        recyclerViewNotification.setAdapter(notificationAdapter);
        Map<String, String> map = new HashMap<>();
        map.put("method", "notification");
        map.put("userId", user.get(UserSessionManager.KEY_ID));

        getNotification(map);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Notification_Screen.this, SearchProduct.class));
            }
        });
    }

    private void getNotification(Map<String, String> map) {
        final Dialog dialog = new Dialog(Notification_Screen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Notification_Model> call = Apis.getAPIService().getNotification(map);
        call.enqueue(new Callback<Notification_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Notification_Model> call, @NonNull Response<Notification_Model> response) {
                dialog.dismiss();
                Notification_Model userdata = response.body();
                if (userdata != null) {

                    Log.d("hgf","hghgjh");
                    if (userdata.getStatus().equals("1")) {
                        no_data_found.setVisibility(View.GONE);
                        recyclerViewNotification.setVisibility(View.VISIBLE);
                        recyclerViewNotification.setVisibility(View.VISIBLE);
                        array_notification_list = new ArrayList<>();
                        array_notification_list_notification = new ArrayList<>();
                        array_notification_list.clear();
                        array_notification_list_notification.clear();
                        array_notification_list.addAll(userdata.getResponse());
                        if (array_notification_list.size() > 0) {
                            recyclerViewNotification.setVisibility(View.VISIBLE);
                            no_data_found.setVisibility(View.GONE);
                            NotificationAdaptermain notificationAdapter = new NotificationAdaptermain();
                            recyclerViewNotification.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else {
                            recyclerViewNotification.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);
                        }


                        /*array_notification_list = new ArrayList<>();
                        array_notification_list.addAll(userdata.getResponse());
                        notificationAdapter = new NotificationAdapter();
                        recyclerViewNotification.setAdapter(notificationAdapter);
                        notificationAdapter.notifyDataSetChanged();*/
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                    else {
                        no_data_found.setVisibility(View.VISIBLE);
                        recyclerViewNotification.setVisibility(View.GONE);
                        //no_found.setText(userdata.getMsg());
                        //   Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Notification_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }



    public class NotificationAdaptermain extends RecyclerView.Adapter<NotificationAdaptermain.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_notifiaction_upper, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
            String inputDateStr=array_notification_list.get(position).getDate();
            Date date = null;
            try {
                date = inputFormat.parse(inputDateStr);
                String outputDateStr = outputFormat.format(date);
                holder.tv_name.setText(outputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            array_notification_list_notification = array_notification_list.get(position).getNotificationlist();
            NotificationAdapter notificationAdapter = new NotificationAdapter(array_notification_list_notification);
            holder.recyclerViewNotification.setAdapter(notificationAdapter);
        }

        @Override
        public int getItemCount() {
            return array_notification_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_name;
            private RecyclerView recyclerViewNotification;
            public ViewHolder(View itemView) {
                super(itemView);
                this.tv_name = itemView.findViewById(R.id.tv_username);
                this.recyclerViewNotification = itemView.findViewById(R.id.recyclerViewNotification);
            }
        }
    }

    public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
        List<Notification_Model.Notificationlist> array_notification_list_notification;

        public NotificationAdapter(List<Notification_Model.Notificationlist> array_notification_list_notification) {
            this.array_notification_list_notification = array_notification_list_notification;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.notification_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            /*try {
                Picasso.get()
                        .load(array_notification_list_notification.get(position).getProfile())
                        .error(R.drawable.dummy_user)
                        .into(holder.iv_pic);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            DateFormat outputFormat = new SimpleDateFormat("hh:mm");
            String inputDateStr=array_notification_list_notification.get(position).getCreated_date();
            Date date = null;
            try {
                date = inputFormat.parse(inputDateStr);
                String outputDateStr = outputFormat.format(date);
                holder.time_notifiation.setText(outputDateStr);
//                holder.tv_name.setText(outputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            holder.tv_name.setText(array_notification_list_notification.get(position).getNotification());
//            holder.message.setText(array_notification_list_notification.get(position).getMessage());

            if (array_notification_list_notification.get(position).getReadstatus().equals("1")) {
                holder.btn_new.setVisibility(View.VISIBLE);
            } else {
                holder.btn_new.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return array_notification_list_notification.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private CircleImageView iv_pic;
            private LinearLayout ll_cardNotification;
            private TextView tv_name;
            private TextView time_notifiation, message;
            private TextView btn_new;
            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_pic = itemView.findViewById(R.id.iv_pic);
                this.ll_cardNotification = itemView.findViewById(R.id.ll_cardNotification);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.time_notifiation = itemView.findViewById(R.id.time_notifiation);
                this.message = itemView.findViewById(R.id.message);
                this.btn_new = itemView.findViewById(R.id.btn_new);
            }
        }
    }
}
