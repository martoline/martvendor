package com.mart.martonlinevendor.OrderDetail;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mart.martonlinevendor.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ChnageOrderByStatus extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private LinearLayout rl_header;
    private EditText et_Category;
    private RecyclerView recyclerView_myOrders;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chnage_order_by_status);
        initView();
    }

    private void initView() {
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (TextView) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        rl_header = (LinearLayout) findViewById(R.id.rl_header);
        et_Category = (EditText) findViewById(R.id.et_Category);
        recyclerView_myOrders = (RecyclerView) findViewById(R.id.recyclerView_myOrders);
        recyclerView_myOrders.setAdapter(new MyOrdersAdapter(ChnageOrderByStatus.this));
        cart_image = (ImageView) findViewById(R.id.cart_image);
        not_found_text = (TextView) findViewById(R.id.not_found_text);
        dialogView = (RelativeLayout) findViewById(R.id.dialogView);
        no_data_found = (LinearLayout) findViewById(R.id.no_data_found);
    }

    private void submit() {
        // validate
        String Category = et_Category.getText().toString().trim();
        if (TextUtils.isEmpty(Category)) {
            Toast.makeText(this, "Change order status", Toast.LENGTH_SHORT).show();
            return;
        }
        // TODO validate success, do something
    }


    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
        Context context;
        LayoutInflater inflater;

        MyOrdersAdapter(Context context) {
            this.context = context;
            this.inflater = LayoutInflater.from(context);
//            this.orderlist = orderlist;
        }

        @Override
        public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders_change_status, parent, false);
            MyOrdersAdapter.ViewHolder viewHolder = new MyOrdersAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrdersAdapter.ViewHolder holder, final int position) {
        }

        @Override
        public int getItemCount() {
            return 3;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private CardView card_myOrders;
            private ImageView iv_item;
            private TextView tv_name,tv_otp;
            private TextView tv_item_order_date;
            private TextView tv_price;
            private TextView tv_panding;
            private TextView tv_delivered;
            private RelativeLayout order_rel;
            public ViewHolder(View itemView) {
                super(itemView);

            }
        }
    }
}
