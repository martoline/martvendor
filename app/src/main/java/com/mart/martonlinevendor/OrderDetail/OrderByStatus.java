package com.mart.martonlinevendor.OrderDetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.Order_Fragment.Delivered_Fragment;
import com.mart.martonlinevendor.Order_Fragment.New_Order_Fragment;
import com.mart.martonlinevendor.Order_Fragment.On_the_Way_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Pending_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Ready_to_Ship_Fragment;
import com.mart.martonlinevendor.Order_Fragment.Whats_App_Order;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class OrderByStatus extends Fragment {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private LinearLayout ll_item_name_details;
    private CardView cardMain;
    private TabLayout tablayout;
    private ViewPager viewPagerProducts;
    private TextView no_found;

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmenTitle=new ArrayList<>();
    String mainid;
    String CategoryId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.activity_order_by_status, container, false);
        initView(view);
        return view;
    }
    private void initView(View view) {
        iv_menu = view.findViewById(R.id.iv_menu);
        iv_send = view.findViewById(R.id.iv_send);
        et_search = (TextView) view.findViewById(R.id.et_search);
        iv_voice = (ImageView)view. findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) view.findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) view.findViewById(R.id.header);
        ll_item_name_details = (LinearLayout) view.findViewById(R.id.ll_item_name_details);
        cardMain = (CardView) view.findViewById(R.id.cardMain);
        tablayout = (TabLayout)view. findViewById(R.id.tablayout);
        viewPagerProducts = (ViewPager)view. findViewById(R.id.viewPagerProducts);
        no_found = (TextView)view. findViewById(R.id.no_found);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = ((Dashboard) getActivity()).findViewById(R.id.drawer);
                // If navigation drawer is not open yet, open it else close it.
                navDrawer.openDrawer(GravityCompat.START);
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SearchProduct.class));
            }
        });
        ImageView iv_bellnotification = view.findViewById(R.id.iv_bellnotification);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Notification_Screen.class));
            }
        });
        getData();
        viewPagerProducts.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerProducts.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    //get data for viewpager tablayout
    public void getData() {
       /* Categories_Model vibesModel = (Categories_Model) Uitility_Activity.getCategories(getActivity());
        categories_models = new ArrayList<>();
        categories_models.clear();
        categories_models = vibesModel.getResponse();
        if (categories_models.size() > 0) {*/

       List<String> catlist = new ArrayList<>();

        catlist.clear();
        catlist.add("New");
        catlist.add("Order Via Chat");
        catlist.add("Pending");
        catlist.add("Ready To Ship");
        catlist.add("On The Way");
        catlist.add("Delivered");

        fragmentList.clear();
     /*   for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {*/
        fragmentList.add(new New_Order_Fragment());
        fragmentList.add(new Whats_App_Order());
        fragmentList.add(new Pending_Fragment());
        fragmentList.add(new Ready_to_Ship_Fragment());
        fragmentList.add(new On_the_Way_Fragment());
        fragmentList.add(new Delivered_Fragment());



            /*}

        }*/
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity(), getChildFragmentManager(), fragmentList, catlist);
        viewPagerProducts.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPagerProducts);

    }


    //ViewPager Adapter ----------------------------
    class ViewPagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public ViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
