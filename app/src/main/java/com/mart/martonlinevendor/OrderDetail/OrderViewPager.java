package com.mart.martonlinevendor.OrderDetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mart.martonlinevendor.R;

import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class OrderViewPager extends Fragment {
    private RecyclerView recycler_products;
    String id, name;
    //    List<Product_ViewPager_Model.Response> array_product_list;
    ProductsAdapter productsAdapter;
    //    private MerlinsBeard merlinsBeard;
//    UserSessionManager session;
    HashMap<String, String> user;
    String Size_id, size, Actual_Price, selling_price, Quantity;
    //    private Categories_Model.SsubCatData ssubCatDataList;
    LinearLayout no_data_found;
    AlertDialog.Builder builder;
    int QUANTITY_COUNT = 18946;
    String quantity_count="0",product_id;
    Button btn_addnewProduct;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_view_pager, container, false);
//        name = getArguments().getString("name");
//        Log.d("id1=", "kjgkljg" + ssubCatDataList.getCatId());
        recycler_products = view.findViewById(R.id.recycler_products);
        no_data_found = view.findViewById(R.id.no_data_found);
        productsAdapter = new ProductsAdapter();
        recycler_products.setAdapter(productsAdapter);
        productsAdapter.notifyDataSetChanged();

        return view;
    }

    public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

        public int counter;
        public String Sizeid;

        @Override
        public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders, parent, false);
            ProductsAdapter.ViewHolder viewHolder = new ProductsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ProductsAdapter.ViewHolder holder, final int position) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(),ChnageOrderByStatus.class));
                }
            });
        }


        @Override
        public int getItemCount() {
            return 5;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout product_details;
            private ImageView iv_item;
            private TextView tv_name;
            private TextView tv_oldPrice;
            private TextView tv_new_price;
            private Spinner quatity_spinner;
            private TextView tv_off;

            private LinearLayout tv_remove_item;
            private TextView tv_item_count;
            private TextView tv_add;
            private LinearLayout tv_add_item;


            public ViewHolder(View itemView) {

                super(itemView);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_oldPrice = itemView.findViewById(R.id.tv_oldPrice);
                this.tv_new_price = itemView.findViewById(R.id.tv_new_price);
                this.quatity_spinner = itemView.findViewById(R.id.quatity_spinner);
                this.tv_off = itemView.findViewById(R.id.tv_off);
                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
                this.tv_add = itemView.findViewById(R.id.tv_add);
                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

}
