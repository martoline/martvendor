package com.mart.martonlinevendor.OrderDetail;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.WhatsappdetailModel;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WhatsappOrderHistoryDetail extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private LinearLayout rl_header;
    private TextView tvorderId;
    private TextView tv_vendor_name;
    private TextView tv_vendor_otp;
    private TextView tv_datetime;
    private TextView tvdiscription;
    private RecyclerView recyclerView_address;
    private TextView tv_panding;
    private TextView tv_delivered;
    private LinearLayout llrating;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
    HashMap<String, String> user;
    UserSessionManager session;
    List<WhatsappdetailModel.Allimages> allimages;
    private TextView call,address;
    String Phone="";
    TextView mobile;
    TextView delivery_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whatsapp_order_history_detail);
        session = new UserSessionManager(WhatsappOrderHistoryDetail.this);
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (TextView) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        rl_header = (LinearLayout) findViewById(R.id.rl_header);
        tvorderId = (TextView) findViewById(R.id.tvorderId);
        tv_vendor_name = (TextView) findViewById(R.id.tv_vendor_name);
        tv_vendor_otp = (TextView) findViewById(R.id.tv_vendor_otp);
        tv_datetime = (TextView) findViewById(R.id.tv_datetime);
        tvdiscription = (TextView) findViewById(R.id.tvdiscription);
        recyclerView_address = (RecyclerView) findViewById(R.id.recyclerView_address);
        tv_panding = (TextView) findViewById(R.id.tv_panding);
        delivery_type = (TextView) findViewById(R.id.delivery_type);
        tv_delivered = (TextView) findViewById(R.id.tv_delivered);
        llrating = (LinearLayout) findViewById(R.id.llrating);
        cart_image = (ImageView) findViewById(R.id.cart_image);
        not_found_text = (TextView) findViewById(R.id.not_found_text);
        address = (TextView) findViewById(R.id.address);
        dialogView = (RelativeLayout) findViewById(R.id.dialogView);
        call = findViewById(R.id.call);
        mobile = findViewById(R.id.mobile);
        no_data_found = (LinearLayout) findViewById(R.id.no_data_found); Map<String, String> map = new HashMap<>();
        map.put("method", "orderDetailWhatsapp");
        map.put("orderId", getIntent().getStringExtra("id"));
        // user.get(UserSessionManager.KEY_ID);
        getOrderList(map);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WhatsappOrderHistoryDetail.this, Notification_Screen.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WhatsappOrderHistoryDetail.this, SearchProduct.class));
            }
        });
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent. setData(Uri.parse("tel:" + Phone));
                startActivity(callIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(WhatsappOrderHistoryDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<WhatsappdetailModel> call = Apis.getAPIService().getWhastappOrderdeatail(map);
        call.enqueue(new Callback<WhatsappdetailModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<WhatsappdetailModel> call, @NonNull Response<WhatsappdetailModel> response) {
                dialog.dismiss();
                WhatsappdetailModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        /*if(userdata.getResponse().get().getReview().equals("0")){
                            llrating.setVisibility(View.VISIBLE);
                        }else{
                            llrating.setVisibility(View.GONE);
                        }*/
                        tvorderId.setText("Order ID: "+userdata.getResponse().getOrderId());
                        tv_vendor_name.setText("By : "+userdata.getResponse().getName());
                        tvdiscription.setText(userdata.getResponse().getDescription());
                        tv_datetime.setText("Date/Time: "+userdata.getResponse().getDate());
                        address.setText("Address: "+userdata.getResponse().getAddress());
                        Phone=userdata.getResponse().getMobile();
                        mobile.setText("Mobile No. : "+userdata.getResponse().getMobile());
                        delivery_type.setText("Delivery Type : "+userdata.getResponse().getPickup());
                        if(userdata.getResponse().getOrder_status().equals("Delivered")){
                           tv_panding.setBackground(getResources().getDrawable(R.drawable.background14));
                            tv_panding.setText(userdata.getResponse().getOrder_status());
                        }else {
                            tv_panding.setText(userdata.getResponse().getOrder_status());
                        }
                        allimages = userdata.getResponse().getAllimages();
                        if (allimages.size() > 0) {
                            MyOrders_detailsAdapter    myOrders_detailsAdapter = new MyOrders_detailsAdapter();
                            recyclerView_address.setAdapter(myOrders_detailsAdapter);
                            myOrders_detailsAdapter.notifyDataSetChanged();
                        }

//                        Log.d("sfsdf", array_order_list1.size() + "");
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        //no_found.setVisibility(View.VISIBLE);
                        // no_found.setText("My Order not Found");
                        Custom_Toast_Activity.makeText(WhatsappOrderHistoryDetail.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<WhatsappdetailModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public class MyOrders_detailsAdapter extends RecyclerView.Adapter<MyOrders_detailsAdapter.ViewHolder> {

        MyOrders_detailsAdapter() {
        }

        @Override
        public MyOrders_detailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.my_whatsapporders_details, parent, false);
            MyOrders_detailsAdapter.ViewHolder viewHolder = new MyOrders_detailsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrders_detailsAdapter.ViewHolder holder, final int position) {

//            holder.tv_name.setText(array_order_list.get(position).getQty() + " X " + array_order_list.get(position).getTitle());
            Picasso.get().load(Apis.WHATSAPP_IMAGE_PATH + allimages.get(position).getImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            Log.d("sdfasdf",Apis.WHATSAPP_IMAGE_PATH + allimages.get(position).getImage());
          /*  switch (array_order_list1.get(position).getOrderStatus()) {
                case "1":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Pending");
                    break;
                case "2":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Accepted");

                    break;
                case "3":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText("Under Process");
                    break;
                case "4":
                    holder.tv_delivered.setVisibility(View.VISIBLE);
                    holder.tv_panding.setVisibility(View.GONE);
                    holder.tv_panding.setText("Delivered");
                    break;
            }*/
        }

        @Override
        public int getItemCount() {
            return allimages.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView iv_item;
            private TextView tv_name;

            public ViewHolder(View itemView) {
                super(itemView);
//                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);

            }
        }
    }
}