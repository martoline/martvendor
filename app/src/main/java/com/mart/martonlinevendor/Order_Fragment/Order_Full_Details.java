package com.mart.martonlinevendor.Order_Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Order_Full_Details extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private EditText et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView recyclerView_myOrders;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
    UserSessionManager session;
    HashMap<String, String> user;
    MyOrders_detailsAdapter myOrders_detailsAdapter;
    List<My_Order_Model.Response> array_order_list;
    List<My_Order_Model.ProductDetail> array_order_list1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__full__details);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (EditText) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        recyclerView_myOrders = (RecyclerView) findViewById(R.id.recyclerView_myOrders);
        cart_image = (ImageView) findViewById(R.id.cart_image);
        not_found_text = (TextView) findViewById(R.id.not_found_text);
        dialogView = (RelativeLayout) findViewById(R.id.dialogView);
        no_data_found = (LinearLayout) findViewById(R.id.no_data_found);

        Map<String, String> map = new HashMap<>();
        map.put("method", "orderList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        // user.get(UserSessionManager.KEY_ID);
        getOrderList(map);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Order_Full_Details.this, SearchProduct.class));
            }
        });
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Order_Full_Details.this, Notification_Screen.class));

            }
        });
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(Order_Full_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<My_Order_Model> call = Apis.getAPIService().getOrderList(map);
        call.enqueue(new Callback<My_Order_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
                dialog.dismiss();
                My_Order_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_order_list = new ArrayList<>();
                        array_order_list1 = new ArrayList<>();
//                        for (int i=0;)
                        array_order_list.addAll(userdata.getResponse());
                        for (int i = 0; i < array_order_list.size(); i++) {
                            if (array_order_list.get(i).getId().equals(getIntent().getStringExtra("id"))) {
                                array_order_list1.addAll(userdata.getResponse().get(i).getProductDetail());
                            }
                            myOrders_detailsAdapter = new MyOrders_detailsAdapter();
                            recyclerView_myOrders.setAdapter(myOrders_detailsAdapter);
                            myOrders_detailsAdapter.notifyDataSetChanged();
                        }

                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        //no_found.setVisibility(View.VISIBLE);
                        // no_found.setText("My Order not Found");
                        Custom_Toast_Activity.makeText(Order_Full_Details.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class MyOrders_detailsAdapter extends RecyclerView.Adapter<MyOrders_detailsAdapter.ViewHolder> {


        MyOrders_detailsAdapter() {

        }

        @Override
        public MyOrders_detailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders, parent, false);
            MyOrders_detailsAdapter.ViewHolder viewHolder = new MyOrders_detailsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrders_detailsAdapter.ViewHolder holder, final int position) {
            // My_Order_Model.Response ddd = orderlist.get(position);
            // list to carry cuisineList
//            for (int i = 0; i < productlist.size(); i++) {
//                producttitle = productlist.get(i).getTitle();
//                image = productlist.get(i).getImage();
//                itmeprice = productlist.get(i).getItmeprice();
//                orderStatus = productlist.get(i).getOrderStatus();
//
//            }
            holder.tv_name.setText(array_order_list1.get(position).getTitle());
            holder.tv_price.setText("Rs " + array_order_list1.get(position).getItmeprice());
            holder.tvusername.setText("By : "+getIntent().getStringExtra("username"));
            holder.tv_item_order_date.setText(getIntent().getStringExtra("date"));
            Picasso.get().load(Apis.IMAGE_PATH + array_order_list1.get(position).getImage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
//            holder.tv_otp.setVisibility(View.GONE);
//             <option value="1">Pending</option>
//          <option value="2">Accepted</option>
//          <option value="3" selected="">Under Process</option>
//          <option value="4">Delivered</option>
            holder.tv_panding.setVisibility(View.GONE);
            holder.tv_delivered.setVisibility(View.GONE);
//            switch (array_order_list1.get(position).getOrderStatus()) {
//                case "1":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Pending");
//                    break;
//                case "2":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Accepted");
//
//                    break;
//                case "3":
//                    holder.tv_panding.setVisibility(View.VISIBLE);
//                    holder.tv_delivered.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Under Process");
//                    break;
//                case "4":
//                    holder.tv_delivered.setVisibility(View.VISIBLE);
//                    holder.tv_panding.setVisibility(View.GONE);
//                    holder.tv_panding.setText("Delivered");
//                    break;
//            }
        }

        @Override
        public int getItemCount() {
            return array_order_list1.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private CardView card_myOrders;
            private ImageView iv_item;
            private TextView tv_name, tv_otp;
            private TextView tv_item_order_date;
            private TextView tv_price;
            private TextView tv_panding;
            private TextView tv_delivered,tvusername;
            private RelativeLayout order_rel;


            public ViewHolder(View itemView) {

                super(itemView);
                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_item_order_date = itemView.findViewById(R.id.tv_item_order_date);
                this.tv_price = itemView.findViewById(R.id.tv_price);
                this.tv_panding = itemView.findViewById(R.id.tv_panding);
                this.tv_delivered = itemView.findViewById(R.id.tv_delivered);
                this.order_rel = itemView.findViewById(R.id.order_rel);
                this.tvusername = itemView.findViewById(R.id.tvusername);
               // this.tv_otp = itemView.findViewById(R.id.tv_otp);
            }
        }
    }

}
