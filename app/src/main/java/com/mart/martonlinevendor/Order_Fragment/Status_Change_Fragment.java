package com.mart.martonlinevendor.Order_Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.My_Order_Model;
import com.mart.martonlinevendor.Model.Status_id_Model;
import com.mart.martonlinevendor.Model.Update_Status_Model;
import com.mart.martonlinevendor.OrderDetail.Order_History_Details;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Status_Change_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Status_Change_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private RecyclerView recyclerView_myOrders;
    private ImageView cart_image;
    private TextView not_found_text;
    private RelativeLayout dialogView;
    private LinearLayout no_data_found;
    // TODO: Rename parameter arguments, choose names that match
    MyOrdersAdapter myOrdersAdapter;
    List<My_Order_Model.Response> array_order_list;
    private LinearLayout no_found;
    String orderStatus;
    private String[] StatusList = {"Change Order Status", "Pending", "Ready To Ship", "On The Way", "Delivered"};
    private Spinner choose_status;
    String statustext;
    List<Status_id_Model> array_count_id;
    private Button btn_addnewProduct;
    String id = "";
    UserSessionManager session;
    HashMap<String, String> user;
    private Calendar myCalendar;
    private TextView filter_text;
    private LinearLayout filter;
    String date="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_status__change_, container, false);
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        initView(view);
        return view;
    }

    public void initView(View view) {
        filter_text = view.findViewById(R.id.filter_text);
        filter = view.findViewById(R.id.filter);
        array_count_id = new ArrayList<>();
        ImageView iv_menu = view.findViewById(R.id.iv_menu);
        no_data_found = view.findViewById(R.id.no_data_found);
        btn_addnewProduct = view.findViewById(R.id.btn_addnewProduct);
        choose_status = view.findViewById(R.id.choose_status);
        recyclerView_myOrders = view.findViewById(R.id.recyclerView_myOrders);
        Map<String, String> map = new HashMap<>();
        map.put("method", "orderList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        getOrderList(map);
        choose_status.setAdapter(new Status_List(getActivity(), R.layout.spinner_list_text, StatusList));
        choose_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here
                try {
                    statustext = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();
                }catch (Exception e){
                    e.getStackTrace();
                }

//                if (statustext.equals("Change Order Status")) {
//
//                } else {
////                    Map<String, String> map = new HashMap<>();
////                    map.put("method", "orderList");
////                    map.put("userId", "2");
////                    map.put("type", statustext);
////                    getOrderList(map);
//                }

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        btn_addnewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = "";
                for (int i = 0; i < array_count_id.size(); i++) {
                    id = id + array_count_id.get(i).getId() + "" + ",";
                }
                Log.d("ddddddd", removeLastCharacter(id) + "");
                if (id.equals("")) {
                    Custom_Toast_Activity.makeText(getActivity(), "Please Select Order to Change the Status", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                } else {
                    Map<String, String> map = new HashMap<>();
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("productId", removeLastCharacter(id));
                    map.put("method", "updateStatus");
                    map.put("type", statustext);
                    Submit_Status(map);
                }
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> map = new HashMap<>();
                map.put("method", "orderList");
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                getOrderList(map);
                filter_text.setText("Filter");
                showDatePicker();
            }
        });
    }
    private void showDatePicker() {
        myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        date=sdf.format(myCalendar.getTime());
        filter_text.setText(sdf.format(myCalendar.getTime()));
        Map<String, String> map = new HashMap<>();
        map.put("method", "orderList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("date", date);
        getOrderList(map);
    }
    public static String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    private void getOrderList(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<My_Order_Model> call = Apis.getAPIService().getOrderList(map);
        call.enqueue(new Callback<My_Order_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<My_Order_Model> call, @NonNull Response<My_Order_Model> response) {
                dialog.dismiss();
                My_Order_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_data_found.setVisibility(View.GONE);
                        recyclerView_myOrders.setVisibility(View.VISIBLE);
                        btn_addnewProduct.setVisibility(View.VISIBLE);
                        array_order_list = new ArrayList<>();
                        array_order_list.addAll(userdata.getResponse());
                        myOrdersAdapter = new MyOrdersAdapter();
                        recyclerView_myOrders.setAdapter(myOrdersAdapter);
                        myOrdersAdapter.notifyDataSetChanged();
                        //  Custom_Toast_Activity.makeText(Notification.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        recyclerView_myOrders.setVisibility(View.GONE);
                        btn_addnewProduct.setVisibility(View.GONE);
                        // no_found.setText("My Order not Found");
                        // Custom_Toast_Activity.makeText(MyOrders.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<My_Order_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    private void Submit_Status(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Update_Status_Model> call = Apis.getAPIService().Submit_Status(map);
        call.enqueue(new Callback<Update_Status_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Update_Status_Model> call, @NonNull Response<Update_Status_Model> response) {
                dialog.dismiss();
                Update_Status_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "orderList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        getOrderList(map);
                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    } else {
                        // no_found.setText("My Order not Found");
                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Update_Status_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
        //        List<My_Order_Model.Response> orderlist;
//        Context context;
//        LayoutInflater inflater;

        MyOrdersAdapter() {
//            this.context = context;
//            this.inflater = LayoutInflater.from(context);
//            this.orderlist = orderlist;
        }

        @Override
        public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_my_orders_change_status, parent, false);
            MyOrdersAdapter.ViewHolder viewHolder = new MyOrdersAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyOrdersAdapter.ViewHolder holder, final int position) {
            holder.tv_item_order_date.setText(array_order_list.get(position).getDate());
            holder.tv_price.setText("Total: " + "Rs " + array_order_list.get(position).getTotalAmount());
            holder.tv_name.setText(array_order_list.get(position).getOrderId());
            orderStatus = array_order_list.get(position).getOrderStatus();
            holder.tvusername.setText("By : "+array_order_list.get(position).getUsername());
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("arrr", holder.checkbox.isChecked() + "");
                    if (holder.checkbox.isChecked()) {
                        Log.d("idvikash1111", "gdfgdfgdfgdxg");
                        Status_id_Model model = new Status_id_Model();
                        model.setId(array_order_list.get(position).getId());
                        array_count_id.add(model);
                        Log.d("idvikash", removeLastCharacter(id));
                    } else {
                        for (int i = 0; i < array_count_id.size(); i++) {
                            if (array_order_list.get(position).getId().equals(array_count_id.get(i).getId())) {
                                Log.d("idvikash1111", "gdfgdfgdfgdxg");
                                array_count_id.remove(i);
                            }
                        }
                    }
                }
            });
            holder.order_rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), Order_History_Details.class)
                            .putExtra("id", array_order_list.get(position).getId())
                            .putExtra("username", array_order_list.get(position).getUsername())
                            .putExtra("date", array_order_list.get(position).getDate()));
                }
            });
           /* holder.order_rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
                            .putExtra("id", array_order_list.get(position).getId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MyOrders.this, Order_History_Details.class)
                            .putExtra("id", array_order_list.get(position).getId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), OrderByStatus.class)
                            .putExtra("id","11"));
                }
            });*/
//             <option value="1">Pending</option>
//          <option value="2">Accepted</option>
//          <option value="3" selected="">Under Process</option>
//          <option value="4">Delivered</option>
            switch (orderStatus) {
                case "Pending":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText(orderStatus);
                    break;
                case "Ready To Ship":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText(orderStatus);

                    break;
                case "On The Way":
                    holder.tv_panding.setVisibility(View.VISIBLE);
                    holder.tv_delivered.setVisibility(View.GONE);
                    holder.tv_panding.setText(orderStatus);
                    break;
                case "Delivered":
                    holder.tv_delivered.setVisibility(View.VISIBLE);
                    holder.tv_panding.setVisibility(View.GONE);
                    holder.tv_panding.setText(orderStatus);
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return array_order_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private CardView card_myOrders;
            private ImageView iv_item;
            private TextView tv_name, tv_otp;
            private TextView tv_item_order_date;
            private TextView tv_price;
            private TextView tv_panding;
            private TextView tv_delivered;
            private TextView tvusername;
            private RelativeLayout order_rel;
            private CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_myOrders = itemView.findViewById(R.id.card_myOrders);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.tv_item_order_date = itemView.findViewById(R.id.tv_item_order_date);
                this.tv_price = itemView.findViewById(R.id.tv_price);
                this.tv_panding = itemView.findViewById(R.id.tv_panding);
                this.tv_delivered = itemView.findViewById(R.id.tv_delivered);
                this.order_rel = itemView.findViewById(R.id.order_rel);
                this.tvusername = itemView.findViewById(R.id.tvusername);
                this.checkbox = itemView.findViewById(R.id.checkbox);
            }
        }
    }

    public class Status_List extends ArrayAdapter<String> {
        public Status_List(Context ctx, int txtViewResourceId, String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.spinner_list_text, parent, false);
            TextView spinner_text = mySpinner.findViewById(R.id.spinner_text);
            spinner_text.setText(StatusList[position]);
            return mySpinner;
        }
    }
}
