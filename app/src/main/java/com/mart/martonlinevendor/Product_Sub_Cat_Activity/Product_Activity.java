package com.mart.martonlinevendor.Product_Sub_Cat_Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Product_Activity extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private LinearLayout ll_item_name_details;
    private CardView cardMain;
    private TabLayout tablayout;
    private ViewPager viewPagerProducts;
    private TextView no_found;
    private List<Categories_Model.SsubCatData> ssubCatDataList;
    List<Categories_Model.SubCatData> array_sub_categories;
    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmenTitle = new ArrayList<>();
    String mainid;
    String CategoryId;
    UserSessionManager session;
    HashMap<String, String> user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_);
        session = new UserSessionManager(Product_Activity.this);
        user = session.getUserDetails();
        CategoryId = getIntent().getStringExtra("catid");
        mainid = getIntent().getStringExtra("mainid");
        initView();
    }
    private void initView() {
        array_sub_categories = new ArrayList<>();
        ssubCatDataList = new ArrayList<>();
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (TextView) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        ll_item_name_details = (LinearLayout) findViewById(R.id.ll_item_name_details);
        cardMain = (CardView) findViewById(R.id.cardMain);
        tablayout = (TabLayout) findViewById(R.id.tablayout);
        viewPagerProducts = (ViewPager) findViewById(R.id.viewPagerProducts);
        no_found = (TextView) findViewById(R.id.no_found);
        Map<String, String> map = new HashMap<>();
        map.put("method", "shopByCategory");
        GetCategories(map);

        viewPagerProducts.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerProducts.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    private void GetCategories(Map<String, String> map) {
        final Dialog dialog = new Dialog(Product_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Categories_Model> call = Apis.getAPIService().getCategories(map);
        call.enqueue(new Callback<Categories_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Categories_Model> call, @NonNull Response<Categories_Model> response) {
                dialog.dismiss();
                Categories_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        if (userdata.getResponse().size() > 0) {
                            for (int i = 0; i < userdata.getResponse().size(); i++) {
                                if (mainid.equals(userdata.getResponse().get(i).getCatId())) {
                                    array_sub_categories.clear();
                                    array_sub_categories.addAll(userdata.getResponse().get(i).getSubCatData());
                                    for (int j = 0; j < array_sub_categories.size(); j++) {
                                        if (CategoryId.equals(array_sub_categories.get(j).getCatId())) {
                                            ssubCatDataList.addAll(array_sub_categories.get(j).getSsubCatData());
                                            fragmentList = new ArrayList<>();
                                            fragmenTitle = new ArrayList<>();
                                            fragmentList.clear();
                                            fragmenTitle.clear();
                                            if (ssubCatDataList.isEmpty()) {
                                                viewPagerProducts.setVisibility(View.GONE);
                                                no_found.setText(" Data Not Found ");
                                               // Custom_Toast_Activity.makeText(Product_Activity.this, "no data found..,.......", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                                            } else {
                                                for (int i1 = 0; i1 < ssubCatDataList.size(); i1++) {
                                                    Bundle bundle = new Bundle();
                                                    bundle.putSerializable("product_id", ssubCatDataList.get(i1));
                                                    bundle.putString("name", ssubCatDataList.get(i1).getCatName());
                                                    Fragment fragment = new ProductsViewPager();
                                                    fragmenTitle.add(ssubCatDataList.get(i1).getCatName());
                                                    fragment.setArguments(bundle);
                                                    fragmentList.add(fragment);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ViewPagerAdapter adapter = new ViewPagerAdapter(Product_Activity.this, getSupportFragmentManager(), fragmentList, fragmenTitle);
                            viewPagerProducts.setAdapter(adapter);
                            tablayout.setupWithViewPager(viewPagerProducts);
                        }

                    } else {

                        Custom_Toast_Activity.makeText(Product_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(Product_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Categories_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    //ViewPager Adapter ----------------------------
    class ViewPagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public ViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
