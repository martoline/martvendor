package com.mart.martonlinevendor.Product_Sub_Cat_Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.martonlinevendor.AddNewProduct.AddNewProduct;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Fragment.ProductDetails;
import com.mart.martonlinevendor.Model.Delete_Product_Model;
import com.mart.martonlinevendor.Model.Out_Stock_Model;
import com.mart.martonlinevendor.Model.Product_ViewPager_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.Swipe_Custom_Class.SwipeRevealLayout;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Product_List extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private Button btn_addnewProduct;
    private RecyclerView recycler_products;
    List<Product_ViewPager_Model.Response> array_product_list;
    ProductsAdapter productsAdapter;
    UserSessionManager session;
    HashMap<String, String> user;
    String Size_id, size, Actual_Price, selling_price, Quantity;
    String quantity_count = "0", product_id = "0";
    int QUANTITY_COUNT = 18946;
    RelativeLayout no_data_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product__list);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        no_data_found = findViewById(R.id.no_data_found);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        btn_addnewProduct = findViewById(R.id.btn_addnewProduct);
        recycler_products = findViewById(R.id.recycler_products);
        btn_addnewProduct.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        map.put("method", "ProductList");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        //68
        //map.put("category_Id", "2");
        map.put("category_Id", getIntent().getStringExtra("catid"));
        GetProductList(map);
        btn_addnewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(Product_List.this, AddNewProduct.class)
//                        .putExtra("subcat_id", getIntent().getStringExtra("catid"))
//                        .putExtra("mainid", getIntent().getStringExtra("mainid"))
//                        .putExtra("tag", "new_product"));
                Intent intentDest = new Intent(Product_List.this, AddNewProduct.class)
                        .putExtra("subcat_id", getIntent().getStringExtra("catid"))
                        .putExtra("mainid", getIntent().getStringExtra("mainid"))
                        .putExtra("tag", "new_product");
                startActivityForResult(intentDest, QUANTITY_COUNT);
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Product_List.this, SearchProduct.class));
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Product_List.this, Notification_Screen.class));

            }
        });
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private void GetProductList(Map<String, String> map) {
        final Dialog dialog = new Dialog(Product_List.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_ViewPager_Model> call = Apis.getAPIService().getProductList(map);
        call.enqueue(new Callback<Product_ViewPager_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_ViewPager_Model> call, @NonNull Response<Product_ViewPager_Model> response) {
                dialog.dismiss();
                Product_ViewPager_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_data_found.setVisibility(View.GONE);
                        recycler_products.setVisibility(View.VISIBLE);
                        btn_addnewProduct.setVisibility(View.VISIBLE);
                        array_product_list = new ArrayList<>();
                        array_product_list.addAll(userdata.getResponse());
                        productsAdapter = new ProductsAdapter();
                        recycler_products.setAdapter(productsAdapter);
                        productsAdapter.notifyDataSetChanged();
                    } else {
                        // tvid.setText(userdata.getMsg());
                        no_data_found.setVisibility(View.VISIBLE);
                        recycler_products.setVisibility(View.GONE);
                        // btn_addnewProduct.setVisibility(View.GONE);
                        //   Custom_Toast_Activity.makeText(Product_List.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_ViewPager_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void DeleteProductList(Map<String, String> map) {
        final Dialog dialog = new Dialog(Product_List.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Delete_Product_Model> call = Apis.getAPIService().Delete_Product(map);
        call.enqueue(new Callback<Delete_Product_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Delete_Product_Model> call, @NonNull Response<Delete_Product_Model> response) {
                dialog.dismiss();
                Delete_Product_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Product_List.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "ProductList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("category_Id", getIntent().getStringExtra("catid"));
                        GetProductList(map);
                    } else {
                        Custom_Toast_Activity.makeText(Product_List.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Delete_Product_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Out_Stock_ProductList(Map<String, String> map) {
        final Dialog dialog = new Dialog(Product_List.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Out_Stock_Model> call = Apis.getAPIService().Out_Stock(map);
        call.enqueue(new Callback<Out_Stock_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Out_Stock_Model> call, @NonNull Response<Out_Stock_Model> response) {
                dialog.dismiss();
                Out_Stock_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Product_List.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "ProductList");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        map.put("category_Id", getIntent().getStringExtra("catid"));
                        GetProductList(map);
                    } else {
                        Custom_Toast_Activity.makeText(Product_List.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Out_Stock_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
        public int counter;
        public String Sizeid, Size_name, quantity,Size_value;
        List<Product_ViewPager_Model.Images> imagelist;
        List<Product_ViewPager_Model.Size_chart> sizelist;
        @Override
        public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_products, parent, false);
            ProductsAdapter.ViewHolder viewHolder = new ProductsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ProductsAdapter.ViewHolder holder, final int position) {
            holder.tv_name.setText(array_product_list.get(position).getProduct_Name());
            holder.tv_new_price.setText("" + array_product_list.get(position).getOfferprice());
            holder.tv_oldPrice.setText("Rs " + array_product_list.get(position).getMrp());
           // holder.tv_date.setText("Valid to " + array_product_list.get(position).getCreate_date());
            holder.tv_date.setText(array_product_list.get(position).getCountry());
            Double Actual_Ammount = Double.parseDouble(array_product_list.get(position).getMrp());
            Double Selling_Ammount = Double.parseDouble(array_product_list.get(position).getOfferprice());
            Double remaining = Actual_Ammount - Selling_Ammount;
            // Sizeid = sizelist.get(i).getSize_id();
            Double totaldis = (remaining / Actual_Ammount) * 100;
            String value = new DecimalFormat("##").format(totaldis);
            if (value.equals("0")) {
                holder.offer.setVisibility(View.GONE);
            } else {
                holder.offer.setVisibility(View.VISIBLE);
            }
            holder.tv_off.setText(value + "%");

            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            Product_ViewPager_Model.Response ddd = array_product_list.get(position);
            if (quantity_count == null) {
            } else {
                if (product_id.equals(array_product_list.get(position).getProduct_id())) {
                    if (quantity_count.equals("0")) {
                        holder.tv_add.setVisibility(View.VISIBLE);
                        holder.tv_remove_item.setVisibility(View.GONE);
                        holder.tv_item_count.setVisibility(View.GONE);
                    } else {
                        holder.tv_add.setVisibility(View.GONE);
                        holder.tv_remove_item.setVisibility(View.VISIBLE);
                        holder.tv_item_count.setVisibility(View.VISIBLE);
                        holder.tv_item_count.setText(quantity_count + "");
                    }

                }
            }
           // final List<Product_ViewPager_Model.Size_chart> sizelist = ddd.getSize_chart();  // list to carry cuisineList
             sizelist = array_product_list.get(position).getSize_chart();
            if (sizelist.size()>0){
                array_product_list.get(position).setSizevalue(sizelist.get(0).getSize_value());
                array_product_list.get(position).setSizeid(sizelist.get(0).getSize_id());
                Log.d("ddddddgggggggg",sizelist.get(0).getSize_id());
            }
            for (int i = 0; i < sizelist.size(); i++) {
                size = sizelist.get(i).getSize();
                Actual_Price = sizelist.get(i).getActual_Price();
                selling_price = sizelist.get(i).getSelling_price();
                quantity = sizelist.get(i).getQuantity();
            }
            if (sizelist.isEmpty()) {
                holder.tv_quantity.setVisibility(View.GONE);
            } else if (size.equals("")){
                holder.tv_quantity.setVisibility(View.GONE);
            }else {
                holder.tv_quantity.setVisibility(View.VISIBLE);
                //Toast.makeText(Product_List.this,size+"",Toast.LENGTH_LONG).show();
                holder.tv_quantity.setText(Html.fromHtml(size));
            }

//            final List<Product_ViewPager_Model.Images> images = ddd.getImages();  // list to carry cuisineList
//            for (int i = 0; i < images.size(); i++) {
//                Picasso.get().load(Apis.IMAGE_PATH + images.get(i).getProduct_Image())
//                        .error(R.drawable.no_image)
//                        .priority(Picasso.Priority.HIGH)
//                        .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
//
//            }
            Picasso.get().load(Apis.IMAGE_PATH + array_product_list.get(position).getMainimage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            if (array_product_list.get(position).getQty().equals("0")) {
                holder.out_stock.setVisibility(View.VISIBLE);
                Picasso.get().load(Apis.IMAGE_PATH + array_product_list.get(position).getMainimage())
                        .error(R.drawable.out_stock)
                        .priority(Picasso.Priority.HIGH)
                        .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            } else {
                holder.out_stock.setVisibility(View.GONE);
                Picasso.get().load(Apis.IMAGE_PATH + array_product_list.get(position).getMainimage())
                        .error(R.drawable.no_image)
                        .priority(Picasso.Priority.HIGH)
                        .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
            }
            Quntity_Adapter adapter = new Quntity_Adapter(Product_List.this,
                    R.layout.spinner_list_text, R.id.title, sizelist);
            holder.quatity_spinner.setAdapter(adapter);
            holder.quatity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
//                    holder.tv_new_price.setText("" + sizelist.get(i).getSelling_price());
//                    holder.tv_oldPrice.setText("Rs " + sizelist.get(i).getActual_Price());
                    holder.tv_new_price.setText("" + array_product_list.get(position).getOfferprice());
                    holder.tv_oldPrice.setText("Rs " + array_product_list.get(position).getMrp());
                    Double Actual_Ammount = Double.parseDouble(sizelist.get(i).getActual_Price());
                    Double Selling_Ammount = Double.parseDouble(sizelist.get(i).getSelling_price());

                    Size_id = sizelist.get(i).getSize_id();
                    Size_value = sizelist.get(i).getSize_value();
                    Double remaining = Actual_Ammount - Selling_Ammount;
                    Sizeid = sizelist.get(i).getSize_id();
                    Size_name = sizelist.get(i).getSize();
                    Double totaldis = (remaining / Actual_Ammount) * 100;
                    String value = new DecimalFormat("##").format(totaldis);
                    if (value.equals("0")) {
                        holder.offer.setVisibility(View.GONE);
                    } else {
                        holder.offer.setVisibility(View.VISIBLE);
                    }
                    holder.tv_off.setText(value + "%");
                    holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    adapterView.setSelection(i);
//                    holder.tv_item_count.addTextChangedListener(new TextWatcher() {
//
//                        public void afterTextChanged(Editable s) {
//                        }
//
//                        public void beforeTextChanged(CharSequence s, int start,
//                                                      int count, int after) {
//                        }
//                        public void onTextChanged(CharSequence s, int start,
//                                                  int before, int count) {
//
//                            if (quantity_count==null){
//                                Map<String, String> map = new HashMap<>();
//                                map.put("method", "AddToCart");
//                                map.put("userId", user.get(UserSessionManager.KEY_ID));
//                                map.put("Pid", array_product_list.get(position).getProduct_id());
//                                map.put("Qty", holder.tv_item_count.getText().toString());
//                                map.put("size", sizelist.get(i).getSize_id());
//                                //user.get(UserSessionManager.KEY_ID)
//                                //Add_Cart(map);
//                                if(holder.tv_item_count.getText().toString().equals("0")){
//                                    holder.tv_add.setVisibility(View.VISIBLE);
//                                    holder.tv_remove_item.setVisibility(View.GONE);
//                                    holder.tv_item_count.setVisibility(View.GONE);
//                                }
//                            }else {
//                                Map<String, String> map = new HashMap<>();
//                                map.put("method", "AddToCart");
//                                map.put("userId", user.get(UserSessionManager.KEY_ID));
//                                map.put("Pid", array_product_list.get(position).getProduct_id());
//                                map.put("Qty", holder.tv_item_count.getText().toString());
//                                map.put("size", sizelist.get(i).getSize_id());
//                                //user.get(UserSessionManager.KEY_ID)
//                                //Add_Cart(map);
//                                if(holder.tv_item_count.getText().toString().equals("0")){
//                                    holder.tv_add.setVisibility(View.VISIBLE);
//                                    holder.tv_remove_item.setVisibility(View.GONE);
//                                    holder.tv_item_count.setVisibility(View.GONE);
//                                }
//                            }
//
//
//                        }
//                    });
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    // DO Nothing here
                }
            });
//            holder.tv_new_price.setText("" + selling_price);
//            holder.tv_oldPrice.setText("Rs " + Actual_Price);
            holder.product_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentDest = new Intent(Product_List.this, ProductDetails.class)
                            .putExtra("product_id", array_product_list.get(position).getProduct_id())
                            .putExtra("quantity_count", holder.tv_item_count.getText().toString())
                            .putExtra("product_id", array_product_list.get(position).getProduct_id());
                    startActivityForResult(intentDest, QUANTITY_COUNT);

                }
            });
            holder.iv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentDest = new Intent(Product_List.this, ProductDetails.class)
                            .putExtra("product_id", array_product_list.get(position).getProduct_id())
                            .putExtra("quantity_count", holder.tv_item_count.getText().toString())
                            .putExtra("product_id", array_product_list.get(position).getProduct_id());
                    startActivityForResult(intentDest, QUANTITY_COUNT);

                }
            });

    holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(Product_List.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText("Do You Want to Edit this Product?");
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);

//                            startActivity(new Intent(Product_List.this, AddNewProduct.class)
//                                    .putExtra("subcat_id", getIntent().getStringExtra("catid"))
//                                    .putExtra("mainid", getIntent().getStringExtra("mainid"))
//                                    .putExtra("product_id", array_product_list.get(position).getProduct_id())
//                                    .putExtra("mrp", array_product_list.get(position).getMrp())
//                                    .putExtra("offerprize", array_product_list.get(position).getOfferprice())
//                                    .putExtra("quantity", quantity)
//                                    .putExtra("description", array_product_list.get(position).getDiscription())
//                                    .putExtra("product_title", array_product_list.get(position).getProduct_Name())
//                                    .putExtra("tag", "edit_product")
//                                    .putExtra("size_id", Size_id));
                            Intent intentDest = new Intent(Product_List.this, AddNewProduct.class)
                                    .putExtra("subcat_id", getIntent().getStringExtra("catid"))
                                    .putExtra("mainid", getIntent().getStringExtra("mainid"))
                                    .putExtra("product_id", array_product_list.get(position).getProduct_id())
                                    .putExtra("mrp", array_product_list.get(position).getMrp())
                                    .putExtra("offerprize", array_product_list.get(position).getOfferprice())
                                    .putExtra("quantity", quantity)
                                    .putExtra("description", array_product_list.get(position).getDiscription())
                                    .putExtra("product_title", array_product_list.get(position).getProduct_Name())
                                    .putExtra("tag", "edit_product")
                                    .putExtra("size_id", array_product_list.get(position).getSizeid())
                                    .putExtra("country", array_product_list.get(position).getCountry())
                                    .putExtra("Size_value", array_product_list.get(position).getSizevalue());
                              startActivityForResult(intentDest, QUANTITY_COUNT);
                        }
                    }).show();
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(Product_List.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText("Do You Want to Delete this Product?");
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "deleteproduct");
                            map.put("productId", array_product_list.get(position).getProduct_id());
                            DeleteProductList(map);
                        }
                    }).show();
                }
            });
            holder.out_of_stock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(Product_List.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText("Do You Want to Show  this Product Out Of Stock?");
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "stockstatusupdate");
                            map.put("productId", array_product_list.get(position).getProduct_id());
                            Out_Stock_ProductList(map);
                        }
                    }).show();
                }
            });

            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }


        @Override
        public int getItemCount() {
            return array_product_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout product_details;
            private ImageView iv_item;
            private TextView out_stock;
            private TextView tv_name;
            private TextView tv_oldPrice;
            private TextView tv_new_price;
            private Spinner quatity_spinner;
            private TextView tv_off, tv_quantity;

            private LinearLayout tv_remove_item;
            private LinearLayout offer;
            private TextView tv_item_count;
            private TextView tv_add, tv_date;
            private LinearLayout tv_add_item;
            private LinearLayout edit, delete, out_of_stock;
            private SwipeRevealLayout swipe_layout;

            public ViewHolder(View itemView) {

                super(itemView);
                this.swipe_layout = itemView.findViewById(R.id.swipe_layout);
                this.out_stock = itemView.findViewById(R.id.out_stock);
                this.edit = itemView.findViewById(R.id.edit);
                this.delete = itemView.findViewById(R.id.delete);
                this.out_of_stock = itemView.findViewById(R.id.out_of_stock);
                this.tv_date = itemView.findViewById(R.id.tv_date);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.tv_quantity = itemView.findViewById(R.id.tv_quantity);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.offer = itemView.findViewById(R.id.offer);
                this.tv_oldPrice = itemView.findViewById(R.id.tv_oldPrice);
                this.tv_new_price = itemView.findViewById(R.id.tv_new_price);
                this.quatity_spinner = itemView.findViewById(R.id.quatity_spinner);
                this.tv_off = itemView.findViewById(R.id.tv_off);
                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
                this.tv_add = itemView.findViewById(R.id.tv_add);
                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

    public class Quntity_Adapter extends ArrayAdapter<Product_ViewPager_Model.Size_chart> {

        LayoutInflater flater;

        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<Product_ViewPager_Model.Size_chart> list) {

            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return rowview(convertView, position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(convertView, position);
        }

        private View rowview(View convertView, int position) {

            final Product_ViewPager_Model.Size_chart rowItem = getItem(position);

            Quntity_Adapter.viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {
                holder = new Quntity_Adapter.viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
                holder.spinner_text = rowview.findViewById(R.id.spinner_text);
                rowview.setTag(holder);
            } else {
                holder = (Quntity_Adapter.viewHolder) rowview.getTag();
            }
            holder.spinner_text.setText(rowItem.getSize());

            return rowview;
        }

        private class viewHolder {
            TextView spinner_text;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addnewProduct:

                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == QUANTITY_COUNT) {
//                quantity_count = data.getStringExtra("catid");
//                product_id = data.getStringExtra("product_id");
                Map<String, String> map = new HashMap<>();
                map.put("method", "ProductList");
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                //68
                //map.put("category_Id", "2");
                map.put("category_Id", data.getStringExtra("catid"));
                GetProductList(map);
                //productsAdapter.notifyDataSetChanged();
            }
        }
    }
}
