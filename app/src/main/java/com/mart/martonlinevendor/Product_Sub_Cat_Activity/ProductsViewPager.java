package com.mart.martonlinevendor.Product_Sub_Cat_Activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Fragment.ProductDetails;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.Model.Product_ViewPager_Model;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductsViewPager extends Fragment  {

    private RecyclerView recycler_products;
    String id, name;
    List<Product_ViewPager_Model.Response> array_product_list;
    ProductsAdapter productsAdapter;
    //    private MerlinsBeard merlinsBeard;
    UserSessionManager session;
    HashMap<String, String> user;
    String Size_id, size, Actual_Price, selling_price, Quantity;
    private Categories_Model.SsubCatData ssubCatDataList;
    LinearLayout no_data_found;
    AlertDialog.Builder builder;
    int QUANTITY_COUNT = 18946;
    String quantity_count="0",product_id="0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_view_pager, container, false);
        session = new UserSessionManager(getActivity());

        user = session.getUserDetails();
        builder = new AlertDialog.Builder(getActivity());
        ssubCatDataList = (Categories_Model.SsubCatData) getArguments().getSerializable("product_id");
        name = getArguments().getString("name");
        Log.d("id1=", "kjgkljg" + ssubCatDataList.getCatId());
        recycler_products = view.findViewById(R.id.recycler_products);
        no_data_found = view.findViewById(R.id.no_data_found);
        Map<String, String> map = new HashMap<>();
        map.put("method", "ProductList");
        map.put("userId","2");
        //68
        //map.put("category_Id", ssubCatDataList.getCatId());
        map.put("category_Id", "45");
        GetProductList(map);
        return view;


    }
    public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

        public int counter;
        public String Sizeid;

        @Override
        public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_products, parent, false);
            ProductsAdapter.ViewHolder viewHolder = new ProductsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ProductsAdapter.ViewHolder holder, final int position) {
            holder.tv_name.setText(array_product_list.get(position).getProduct_Name());
            Product_ViewPager_Model.Response ddd = array_product_list.get(position);
            if (quantity_count == null) {
            } else {
                if (product_id.equals(array_product_list.get(position).getProduct_id())){
                    if (quantity_count.equals("0")){
                        holder.tv_add.setVisibility(View.VISIBLE);
                        holder.tv_remove_item.setVisibility(View.GONE);
                        holder.tv_item_count.setVisibility(View.GONE);
                    }else {
                        holder.tv_add.setVisibility(View.GONE);
                        holder.tv_remove_item.setVisibility(View.VISIBLE);
                        holder.tv_item_count.setVisibility(View.VISIBLE);
                        holder.tv_item_count.setText(quantity_count + "");
                    }

                }

            }

            final List<Product_ViewPager_Model.Size_chart> sizelist = ddd.getSize_chart();  // list to carry cuisineList
            for (int i = 0; i < sizelist.size(); i++) {
                Size_id = sizelist.get(i).getSize_id();
                size = sizelist.get(i).getSize();
                Actual_Price = sizelist.get(i).getActual_Price();
                selling_price = sizelist.get(i).getSelling_price();

            }
//            final List<Product_ViewPager_Model.Images> images = ddd.getImages();  // list to carry cuisineList
//            for (int i = 0; i < images.size(); i++) {
//                Picasso.get().load(Apis.IMAGE_PATH + images.get(i).getProduct_Image())
//                        .error(R.drawable.no_image)
//                        .priority(Picasso.Priority.HIGH)
//                        .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);
//
//            }
            Picasso.get().load(Apis.IMAGE_PATH + array_product_list.get(position).getMainimage())
                    .error(R.drawable.no_image)
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_item);

            Quntity_Adapter adapter = new Quntity_Adapter(getActivity(),
                    R.layout.spinner_list_text, R.id.title, sizelist);
            holder.quatity_spinner.setAdapter(adapter);


            holder.quatity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                    holder.tv_new_price.setText("" + sizelist.get(i).getSelling_price());
                    holder.tv_oldPrice.setText("Rs " + sizelist.get(i).getActual_Price());
                    Double Actual_Ammount = Double.parseDouble(sizelist.get(i).getActual_Price());
                    Double Selling_Ammount = Double.parseDouble(sizelist.get(i).getSelling_price());
                    Double remaining = Actual_Ammount - Selling_Ammount;
                    Sizeid = sizelist.get(i).getSize_id();
                    Double totaldis = (remaining / Actual_Ammount) * 100;
                    String value = new DecimalFormat("##").format(totaldis);
                    if (value.equals("0")){
                        holder.offer.setVisibility(View.GONE);
                    }else{
                        holder.offer.setVisibility(View.VISIBLE);
                    }

                    holder.tv_off.setText(value + "%");
                    holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    adapterView.setSelection(i);

//                    holder.tv_item_count.addTextChangedListener(new TextWatcher() {
//
//                        public void afterTextChanged(Editable s) {
//                        }
//
//                        public void beforeTextChanged(CharSequence s, int start,
//                                                      int count, int after) {
//                        }
//                        public void onTextChanged(CharSequence s, int start,
//                                                  int before, int count) {
//
//                            if (quantity_count==null){
//                                Map<String, String> map = new HashMap<>();
//                                map.put("method", "AddToCart");
//                                map.put("userId", user.get(UserSessionManager.KEY_ID));
//                                map.put("Pid", array_product_list.get(position).getProduct_id());
//                                map.put("Qty", holder.tv_item_count.getText().toString());
//                                map.put("size", sizelist.get(i).getSize_id());
//                                //user.get(UserSessionManager.KEY_ID)
//                                //Add_Cart(map);
//                                if(holder.tv_item_count.getText().toString().equals("0")){
//                                    holder.tv_add.setVisibility(View.VISIBLE);
//                                    holder.tv_remove_item.setVisibility(View.GONE);
//                                    holder.tv_item_count.setVisibility(View.GONE);
//                                }
//                            }else {
//                                Map<String, String> map = new HashMap<>();
//                                map.put("method", "AddToCart");
//                                map.put("userId", user.get(UserSessionManager.KEY_ID));
//                                map.put("Pid", array_product_list.get(position).getProduct_id());
//                                map.put("Qty", holder.tv_item_count.getText().toString());
//                                map.put("size", sizelist.get(i).getSize_id());
//                                //user.get(UserSessionManager.KEY_ID)
//                                //Add_Cart(map);
//                                if(holder.tv_item_count.getText().toString().equals("0")){
//                                    holder.tv_add.setVisibility(View.VISIBLE);
//                                    holder.tv_remove_item.setVisibility(View.GONE);
//                                    holder.tv_item_count.setVisibility(View.GONE);
//                                }
//                            }
//
//
//                        }
//                    });
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    // DO Nothing here
                }
            });
            holder.tv_new_price.setText("" + selling_price);
            holder.tv_oldPrice.setText("Rs " + Actual_Price);

            holder.product_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentDest = new Intent(getActivity(), ProductDetails.class)
                            .putExtra("product_id", array_product_list.get(position).getProduct_id())
                            .putExtra("quantity_count", holder.tv_item_count.getText().toString())
                            .putExtra("product_id",  array_product_list.get(position).getProduct_id());
                    startActivityForResult(intentDest, QUANTITY_COUNT);

                }
            });
            holder.iv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentDest = new Intent(getActivity(), ProductDetails.class)
                            .putExtra("product_id", array_product_list.get(position).getProduct_id())
                            .putExtra("quantity_count", holder.tv_item_count.getText().toString())
                            .putExtra("product_id",  array_product_list.get(position).getProduct_id());
                    startActivityForResult(intentDest, QUANTITY_COUNT);

                }
            });

//            holder.tv_remove_item.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    /*startActivity(new Intent(Notification.this, OtherProfile.class));*/
//                    // Toast.makeText(getActivity(), "You clicked on cart holder", Toast.LENGTH_LONG).show();
//                    if (user.get(UserSessionManager.KEY_ID).equals("0")) {
//                        builder.setMessage("You need to Register to use this feature?")
//                                .setCancelable(false)
//                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        startActivity(new Intent(getActivity(), Loginption.class));
//                                        getActivity().finish();
//                                    }
//                                })
//                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        //  Action for 'NO' Button
//                                        dialog.cancel();
//                                    }
//                                });
//                        //Creating dialog box
//                        AlertDialog alert = builder.create();
//                        //Setting the title manually
//                        alert.setTitle(R.string.app_name);
//                        alert.show();
//                    } else {
//                        counter = Integer.parseInt(holder.tv_item_count.getText().toString());
//                        if (counter > 0) {
//                            counter--;
//                            holder.tv_item_count.setText("" + counter);
//                        } else {
//                            holder.tv_add.setVisibility(View.VISIBLE);
//                            holder.tv_remove_item.setVisibility(View.GONE);
//                            holder.tv_item_count.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "You haven't select any of the item", Toast.LENGTH_LONG).show();
//
//
//                        }
//                    }
//                }
//            });


//            holder.tv_add_item.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (user.get(UserSessionManager.KEY_ID).equals("0")) {
//                        builder.setMessage("You need to Register to use this feature?")
//                                .setCancelable(false)
//                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        startActivity(new Intent(getActivity(), Loginption.class));
//                                        getActivity().finish();
//                                    }
//                                })
//                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        //  Action for 'NO' Button
//                                        dialog.cancel();
//                                    }
//                                });
//                        //Creating dialog box
//                        AlertDialog alert = builder.create();
//                        //Setting the title manually
//                        alert.setTitle(R.string.app_name);
//                        alert.show();
//                    } else {
//                        holder.tv_add.setVisibility(View.GONE);
//                        holder.tv_remove_item.setVisibility(View.VISIBLE);
//                        holder.tv_item_count.setVisibility(View.VISIBLE);
//                        counter = Integer.parseInt(holder.tv_item_count.getText().toString());
//                        counter++;
//                        holder.tv_item_count.setText("" + counter);
//                    }
//                }
//            });

            holder.tv_oldPrice.setPaintFlags(holder.tv_oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }


        @Override
        public int getItemCount() {
            return array_product_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout product_details;
            private ImageView iv_item;
            private TextView tv_name;
            private TextView tv_oldPrice;
            private TextView tv_new_price;
            private Spinner quatity_spinner;
            private TextView tv_off;

            private LinearLayout tv_remove_item;
            private LinearLayout offer;
            private TextView tv_item_count;
            private TextView tv_add;
            private LinearLayout tv_add_item;


            public ViewHolder(View itemView) {

                super(itemView);
                this.product_details = itemView.findViewById(R.id.product_details);
                this.iv_item = itemView.findViewById(R.id.iv_item);
                this.tv_name = itemView.findViewById(R.id.tv_name);
                this.offer = itemView.findViewById(R.id.offer);
                this.tv_oldPrice = itemView.findViewById(R.id.tv_oldPrice);
                this.tv_new_price = itemView.findViewById(R.id.tv_new_price);
                this.quatity_spinner = itemView.findViewById(R.id.quatity_spinner);
                this.tv_off = itemView.findViewById(R.id.tv_off);
                this.tv_remove_item = itemView.findViewById(R.id.tv_remove_item);
                this.tv_item_count = itemView.findViewById(R.id.tv_item_count);
                this.tv_add = itemView.findViewById(R.id.tv_add);
                this.tv_add_item = itemView.findViewById(R.id.tv_add_item);
            }
        }
    }

    public class Quntity_Adapter extends ArrayAdapter<Product_ViewPager_Model.Size_chart> {

        LayoutInflater flater;

        public Quntity_Adapter(Activity context, int resouceId, int textviewId, List<Product_ViewPager_Model.Size_chart> list) {

            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return rowview(convertView, position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(convertView, position);
        }

        private View rowview(View convertView, int position) {

            final Product_ViewPager_Model.Size_chart rowItem = getItem(position);

            Quntity_Adapter.viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new Quntity_Adapter.viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.spinner_list_text, null, false);
                holder.spinner_text = (TextView) rowview.findViewById(R.id.spinner_text);
                rowview.setTag(holder);
            } else {
                holder = (Quntity_Adapter.viewHolder) rowview.getTag();
            }
            holder.spinner_text.setText(rowItem.getSize());
            return rowview;
        }

        private class viewHolder {
            TextView spinner_text;
        }
    }

    private void GetProductList(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Product_ViewPager_Model> call = Apis.getAPIService().getProductList(map);
        call.enqueue(new Callback<Product_ViewPager_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Product_ViewPager_Model> call, @NonNull Response<Product_ViewPager_Model> response) {
                dialog.dismiss();
                Product_ViewPager_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        no_data_found.setVisibility(View.GONE);
                        array_product_list = new ArrayList<>();
                        array_product_list.addAll(userdata.getResponse());
                        productsAdapter = new ProductsAdapter();
                        recycler_products.setAdapter(productsAdapter);
                        productsAdapter.notifyDataSetChanged();
                    } else {
                        // tvid.setText(userdata.getMsg());
                        no_data_found.setVisibility(View.VISIBLE);
                        //  Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product_ViewPager_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

//    private void Add_Cart(Map<String, String> map) {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Add_Cart_Model> call = Apis.getAPIService().addCart(map);
//        call.enqueue(new Callback<Add_Cart_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull Call<Add_Cart_Model> call, @NonNull Response<Add_Cart_Model> response) {
//                dialog.dismiss();
//                Add_Cart_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//                    } else {
//                        Custom_Toast_Activity.makeText(getActivity(), userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<Add_Cart_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == QUANTITY_COUNT) {
                quantity_count = data.getStringExtra("quantity_count");
                product_id = data.getStringExtra("product_id");
                productsAdapter.notifyDataSetChanged();
            }
        }
    }
}
