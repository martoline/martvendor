package com.mart.martonlinevendor.Product_Sub_Cat_Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Categories_Model;
import com.mart.martonlinevendor.Model.Get_Sub_Category_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sub_Categories extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private RecyclerView sub_categories_list;
    private String subdata;
    RelativeLayout dialogView;
    List<Categories_Model.SubCatData> array_ssub_catgories_list;
    //List<Categories_Model.Response> array_categories_list;
    List<Get_Sub_Category_Model.Response> array_categories_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__categories);
        initView();
    }
    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        dialogView = findViewById(R.id.dialogView);
        sub_categories_list = findViewById(R.id.sub_categories_list);
        subdata = getIntent().getStringExtra("cat_id");
        Map<String, String> map = new HashMap<>();
        map.put("method", "getSubCategory");
        map.put("catId", subdata);
        GetCategories(map);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Sub_Categories.this, Notification_Screen.class));
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Sub_Categories.this, SearchProduct.class));

            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
    }

//    private void GetCategories(Map<String, String> map) {
//        final Dialog dialog = new Dialog(Sub_Categories.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Categories_Model> call = Apis.getAPIService().getCategories(map);
//        call.enqueue(new Callback<Categories_Model>() {
//            @SuppressLint("Assert")
//            @Override
//            public void onResponse(@NonNull Call<Categories_Model> call, @NonNull Response<Categories_Model> response) {
//                dialog.dismiss();
//                Categories_Model userdata = response.body();
//                if (userdata != null) {
//                    if (userdata.getStatus().equals("1")) {
//                        array_categories_list = new ArrayList<>();
//                        array_ssub_catgories_list=new ArrayList<>();
//                        array_categories_list.addAll(userdata.getResponse());
//                        for (int i = 0; i < array_categories_list.size(); i++) {
//                            if (array_categories_list.get(i).getCatId().equals(subdata)) {
//                                array_ssub_catgories_list.addAll(userdata.getResponse().get(i).getSubCatData());
//                            }
//                            if (array_ssub_catgories_list.isEmpty()){
//                             sub_categories_list.setVisibility(View.GONE);
//                            }else {
//                                sub_categories_list.setVisibility(View.VISIBLE);
//                                sub_categories_list.setAdapter(new CategoryAdapter());
//                            }
//                        }
//                    } else {
//
//                        Custom_Toast_Activity.makeText(Sub_Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
//
//
//                    }
//                } else {
//                    Custom_Toast_Activity.makeText(Sub_Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
//
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<Categories_Model> call, @NonNull Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "vv" + t.getMessage());
//            }
//        });
//    }

    private void GetCategories(Map<String, String> map) {
        final Dialog dialog = new Dialog(Sub_Categories.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Get_Sub_Category_Model> call = Apis.getAPIService().getSubcategory(map);
        call.enqueue(new Callback<Get_Sub_Category_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Get_Sub_Category_Model> call, @NonNull Response<Get_Sub_Category_Model> response) {
                dialog.dismiss();
                Get_Sub_Category_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        dialogView.setVisibility(View.GONE);
                        sub_categories_list.setVisibility(View.VISIBLE);
                        array_categories_list = new ArrayList<>();
                        // array_ssub_catgories_list=new ArrayList<>();
                        array_categories_list.addAll(userdata.getResponse());
                        sub_categories_list.setAdapter(new CategoryAdapter());
//                        for (int i = 0; i < array_categories_list.size(); i++) {
//                            if (array_categories_list.get(i).getCatId().equals(subdata)) {
//                                array_ssub_catgories_list.addAll(userdata.getResponse().get(i).getSubCatData());
//                            }
//
//                        }
                    } else {
                        dialogView.setVisibility(View.VISIBLE);
                        sub_categories_list.setVisibility(View.GONE);
                       // Custom_Toast_Activity.makeText(Sub_Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(Sub_Categories.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Get_Sub_Category_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


        public CategoryAdapter() {

        }

        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_shop_category, parent, false);
            CategoryAdapter.ViewHolder viewHolder = new CategoryAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
            holder.tv_categoryName.setText(array_categories_list.get(position).getCatName());
            holder.tv_categoryName.setSelected(true);
//            holder.tv_category_details.setText(array_categories_list.get(position).getOffertest());
            Glide.with(Sub_Categories.this)
                    .load(Apis.IMAGE_PATH + array_categories_list.get(position).getCatImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_category);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Sub_Categories.this, Product_List.class)
                            .putExtra("catid", array_categories_list.get(position).getCatId())
                            .putExtra("mainid", subdata));
                }
            });
        }


        @Override
        public int getItemCount() {
            return array_categories_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private RelativeLayout card_shop_category;
            private ImageView iv_category;
            private TextView tv_categoryName;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_shop_category = itemView.findViewById(R.id.card_shop_category);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            }
        }
    }
}
