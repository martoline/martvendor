package com.mart.martonlinevendor.Session_Manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.LoginRegister.SignIn;

import java.util.HashMap;



public class UserSessionManager {

    // Shared Preferences reference
    static SharedPreferences pref;

    // Editor reference for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "User Login Data";


    private static final String IS_LOGIN = "IsLoggedIn";
    // User name (make variable public to access from outside)
    public static final String KEY_ID = "userId";
    public static final String KEY_USER_NAME = "username";
    public static final String KEY_FIRST_NAME = "fname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_DOB = "dob";
    public static final String KEY_ACCESS_TOKEN = "token";


    // Constructor
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void createUserLoginSession(String id, String username, String fname , String email, String image,
                                       String phone, String gender, String dob,String token) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_ID, id);
        editor.putString(KEY_USER_NAME, username);
        editor.putString(KEY_FIRST_NAME, fname);

        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_GENDER, gender);
        editor.putString(KEY_DOB, dob);
        editor.putString(KEY_ACCESS_TOKEN, token);

        editor.apply();
        editor.commit();

    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_USER_NAME, pref.getString(KEY_USER_NAME, null));
        user.put(KEY_FIRST_NAME, pref.getString(KEY_FIRST_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, null));
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));
        user.put(KEY_GENDER, pref.getString(KEY_GENDER, null));
        user.put(KEY_DOB, pref.getString(KEY_DOB, null));
        user.put(KEY_ACCESS_TOKEN, pref.getString(KEY_ACCESS_TOKEN, null));


        // return user
        return user;
    }

    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, SignIn.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity

            _context.startActivity(i);
        } else {
            Intent i1 = new Intent(_context, Dashboard.class);
            i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i1);

        }
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        editor.putBoolean(IS_LOGIN, false);

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();
        editor.apply();
        Intent i = new Intent(_context, SignIn.class);
        // After logout redirect user to Login Activity

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        _context.startActivity(i);


    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public static String isLoginId() {
        return pref.getString(KEY_ID, "0");

    }

    public void iseditor() {
        editor.clear();
        editor.commit();

    }
}