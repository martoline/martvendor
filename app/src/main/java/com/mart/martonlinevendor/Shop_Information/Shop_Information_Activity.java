package com.mart.martonlinevendor.Shop_Information;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.DashBoard.Dashboard;
import com.mart.martonlinevendor.Google_Place_Search.PlacesAutoCompleteAdapter;
import com.mart.martonlinevendor.Model.Shop_Details_Update_Model;
import com.mart.martonlinevendor.Model.UserProfileModel;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Shop_Information_Activity extends AppCompatActivity implements View.OnClickListener, BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate, PlacesAutoCompleteAdapter.ClickListener {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private ImageView shopimage;
    private CardView shopimage_rel;
    private LinearLayout photo_rel;
    private EditText et_shop_name;
    private LinearLayout layout_shopname;
    private EditText et_times;
    private LinearLayout layout_time;
    private EditText et_address;
    private LinearLayout ll_address;
    private EditText et_pincode;
    private LinearLayout ll_pincode;
    private LinearLayout layout_address;
    private EditText et_dilivery_times;
    private LinearLayout layout_delivery_time;
    private EditText et_miniorder;
    private LinearLayout layout_miniorder;
    private Button btn_submit;
    int hour, minute;
    String aTime, aTime1;
    String tagg = "0";
    String shop_image = "";
    UserSessionManager session;
    HashMap<String, String> user;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    String lat, lng;
    Dialog dialog;
    String Delivery = "", PickUp = "";
    CheckBox pickup, delivery;
    LinearLayout layout_delivery_Charge;
    EditText et_Deliverycharge;

    private String isClicked = "0";
    private Boolean isClicked1 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop__information_);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        Places.initialize(this, getResources().getString(R.string.google_map_api));
        initView();
    }

    private void initView() {
        pickup = findViewById(R.id.pickup);
        delivery = findViewById(R.id.delivery);
        iv_menu = findViewById(R.id.iv_menu);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        shopimage = findViewById(R.id.shopimage);
        shopimage_rel = findViewById(R.id.shopimage_rel);
        photo_rel = findViewById(R.id.photo_rel);
        et_shop_name = findViewById(R.id.et_shop_name);
        layout_shopname = findViewById(R.id.layout_shopname);
        et_times = findViewById(R.id.et_times);
        layout_time = findViewById(R.id.layout_time);
        et_address = findViewById(R.id.et_address);
        ll_address = findViewById(R.id.ll_address);
        et_pincode = findViewById(R.id.et_pincode);
        ll_pincode = findViewById(R.id.ll_pincode);
        layout_address = findViewById(R.id.layout_address);
        et_dilivery_times = findViewById(R.id.et_dilivery_times);
        layout_delivery_time = findViewById(R.id.layout_delivery_time);
        et_miniorder = findViewById(R.id.et_miniorder);
        layout_miniorder = findViewById(R.id.layout_miniorder);
        btn_submit = findViewById(R.id.btn_submit);
        layout_delivery_Charge = findViewById(R.id.layout_delivery_Charge);
        et_Deliverycharge = findViewById(R.id.et_Deliverycharge);

        btn_submit.setOnClickListener(this);
        et_times.setOnClickListener(this);
        photo_rel.setOnClickListener(this);
        et_address.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        pickup.setOnClickListener(this);
        delivery.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        map.put("method", "Profile");
        map.put("UserId", user.get(UserSessionManager.KEY_ID));
        getProfile(map);
    }

    public void TimeDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        int time1;
        mTimePicker = new TimePickerDialog(Shop_Information_Activity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                updateTime(hour, minute);
                //time.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    public void TimeDialog1() {
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        int time1;
        mTimePicker = new TimePickerDialog(Shop_Information_Activity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                updateTime1(hour, minute);
                //time.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        TimeDialog1();

    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime1(int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;

        else
            minutes = String.valueOf(mins);
        // Append in a StringBuilder
        aTime1 = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        et_times.setText(aTime + "-" + aTime1);

    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                submit();
                break;
            case R.id.pickup:
                if (pickup.isChecked()){
                    PickUp = "Pickup";
                    Log.d("checkstatus",PickUp);
                }else {
                    PickUp = "";
                    Log.d("checkstatus",PickUp);
                }
                break;
            case R.id.delivery:
                //isClicked = isClicked ? false : true;
                if (delivery.isChecked()){
                    Delivery = "Delivery";
                    Log.d("checkstatus",Delivery);
                    layout_delivery_Charge.setVisibility(View.VISIBLE);
                }else {
                    Delivery = "";
                    et_Deliverycharge.setText("");
                    Log.d("checkstatus1",Delivery);
                    layout_delivery_Charge.setVisibility(View.GONE);
                }

                break;
            case R.id.iv_menu:
                onBackPressed();
                break;
            case R.id.et_times:
                TimeDialog();
                break;
            case R.id.et_address:
                Add_Time_Slot_dialog();
                break;
            case R.id.photo_rel:
                tagg = "1";
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.mart.martonlinevendor")
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
                break;
        }
    }

    private void submit() {
        // validate
        String name = et_shop_name.getText().toString().trim();


        String times = et_times.getText().toString().trim();

        String address = et_address.getText().toString().trim();

        if (TextUtils.isEmpty(address)) {
            Snackbar.make(btn_submit, "Enter Your Shop Address",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(Shop_Information_Activity.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Shop_Information_Activity.this,R.color.alertcolor))
                    .show();
            //  Custom_Toast_Activity.makeText(getApplicationContext(), "Enter Your Mobile Number", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.ERROR);
            return;
        }
        String pincode = et_pincode.getText().toString().trim();

        String deliverytimes = et_dilivery_times.getText().toString().trim();

        String miniorder = et_miniorder.getText().toString().trim();

        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("method", "UpdateShopDetail");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("open", aTime);
        map.put("close", aTime1);
        map.put("address", et_address.getText().toString());
        map.put("deliverytime", et_dilivery_times.getText().toString());
        map.put("pincode", et_pincode.getText().toString());
        map.put("latitude", lat);
        map.put("longitude", lng);
        map.put("Pickup", PickUp);
        map.put("Delivery", Delivery);
        map.put("deliverycharge", et_Deliverycharge.getText().toString());
        map.put("minorder", et_miniorder.getText().toString());
        map.put("shopname", et_shop_name.getText().toString());
        map.put("shop_image", shop_image);
        Update_Shop(map);

    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(Shop_Information_Activity.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        if (tagg.equals("1")) {
            // tv_passport_name.setText(uri.getPath());
            Log.d("image", uri.getPath());
            Bitmap thumbnail = (BitmapFactory.decodeFile(uri.getPath()));

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                shopimage_rel.setVisibility(View.VISIBLE);
                shopimage.setImageBitmap(selectedImage);
                shop_image = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void Update_Shop(final Map<String, String> map) {
        final Dialog dialog = new Dialog(Shop_Information_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Shop_Details_Update_Model> call = Apis.getAPIService().Shop_Update(map);
        call.enqueue(new Callback<Shop_Details_Update_Model>() {
            @Override
            public void onResponse(Call<Shop_Details_Update_Model> call, Response<Shop_Details_Update_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Shop_Details_Update_Model user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Shop_Information_Activity.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                        startActivity(new Intent(Shop_Information_Activity.this, Dashboard.class));
                    } else {
                        dialog.dismiss();
                        Custom_Toast_Activity.makeText(Shop_Information_Activity.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                } else {
                    dialog.dismiss();
                    Custom_Toast_Activity.makeText(Shop_Information_Activity.this, "Something wents Worng", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);


                }
            }

            @Override
            public void onFailure(Call<Shop_Details_Update_Model> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void getProfile(Map<String, String> map) {
        final Dialog dialog = new Dialog(Shop_Information_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UserProfileModel> call = Apis.getAPIService().getProfile(map);
        call.enqueue(new Callback<UserProfileModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<UserProfileModel> call, @NonNull Response<UserProfileModel> response) {
                dialog.dismiss();
                UserProfileModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        for (int i = 0; i < userdata.getResponse().size(); i++) {
                            et_shop_name.setText(userdata.getResponse().get(i).getShopname());
                            Log.d("gjh", Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getShop_image());
                            shopimage_rel.setVisibility(View.VISIBLE);
                            Picasso.get().load(Apis.USER_IMAGE_PATH + userdata.getResponse().get(i).getShop_image())
                                    .error(R.drawable.no_image)
                                    .priority(Picasso.Priority.HIGH)
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(shopimage);
                            et_address.setText(userdata.getResponse().get(i).getAddress());
                            et_pincode.setText(userdata.getResponse().get(i).getPincode());
                            et_times.setText(userdata.getResponse().get(i).getOpentime() + "-" + userdata.getResponse().get(i).getClosetime());
                            aTime = userdata.getResponse().get(i).getOpentime();
                            aTime1 = userdata.getResponse().get(i).getClosetime();
                            et_dilivery_times.setText(userdata.getResponse().get(i).getDeliverytime());
                            et_miniorder.setText(userdata.getResponse().get(i).getMinorder());
                            lat = userdata.getResponse().get(i).getLatitude();
                            lng = userdata.getResponse().get(i).getLongitude();

                            if (userdata.getResponse().get(i).getDeliverycharge().equals("")){

                            }else {
                                et_Deliverycharge.setText(userdata.getResponse().get(i).getDeliverycharge());
                                layout_delivery_Charge.setVisibility(View.VISIBLE);
                            }
                            if (userdata.getResponse().get(i).getPickup()==null||userdata.getResponse().get(i).getPickup().equals("")){

                            }else {
                                pickup.setChecked(true);
                                PickUp=userdata.getResponse().get(i).getPickup();
                            }
                            if (userdata.getResponse().get(i).getDelivery()==null||userdata.getResponse().get(i).getDelivery().equals("")){
                                isClicked="1";
                            }else {
                                et_Deliverycharge.setText(userdata.getResponse().get(i).getDeliverycharge());
                                layout_delivery_Charge.setVisibility(View.VISIBLE);
                                delivery.setChecked(true);
                                isClicked="0";
                                Delivery=userdata.getResponse().get(i).getDelivery();
                            }

                        }
                    } else {
                        Custom_Toast_Activity.makeText(Shop_Information_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Shop_Information_Activity.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserProfileModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public void Add_Time_Slot_dialog() {
        dialog = new Dialog(Shop_Information_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.search_address_layout);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        recyclerView = dialog.findViewById(R.id.places_recycler_view);
        TextWatcher filterTextWatcher = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                    if (recyclerView.getVisibility() == View.GONE) {
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (recyclerView.getVisibility() == View.VISIBLE) {
                        recyclerView.setVisibility(View.GONE);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };
        ((EditText) dialog.findViewById(R.id.place_search)).addTextChangedListener(filterTextWatcher);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();
        dialog.show();

    }

    @Override
    public void click(Place place) {
        lat = place.getLatLng().latitude + "";
        lng = place.getLatLng().longitude + "";
        et_address.setText(place.getAddress());
        Log.d("kldsfj", lat + lng + "");
        dialog.dismiss();
        Intent returnIntent = new Intent();
//        returnIntent.putExtra("Lat", place.getLatLng().latitude + "");
//        returnIntent.putExtra("Lng", place.getLatLng().longitude + "");
//        returnIntent.putExtra("address", place.getAddress() + "");
//        Log.d("kldsfj", place.getLatLng().latitude + "");
//        setResult(Activity.RESULT_OK, returnIntent);
//        finish();
    }
}
