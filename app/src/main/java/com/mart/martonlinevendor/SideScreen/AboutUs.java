package com.mart.martonlinevendor.SideScreen;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.About_Us_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUs extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        initView();
    }

    private void initView() {
        webView = findViewById(R.id.webView);
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (TextView) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        Map<String, String> map = new HashMap<>();
        map.put("method", "pages");
        aboutus(map);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("Status").equals("2")) {
                    Snackbar.make(webView, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    AboutUs.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(AboutUs.this, R.color.alertcolor)).show();
                } else {
                    startActivity(new Intent(AboutUs.this, Notification_Screen.class));

                }
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getIntent().getStringExtra("Status").equals("2")) {
                    Snackbar.make(webView, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    AboutUs.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(AboutUs.this, R.color.alertcolor)).show();
                } else {
                    startActivity(new Intent(AboutUs.this, SearchProduct.class));

                }

            }
        });
    }

    public void aboutus(final Map<String,String> map) {
        final Dialog dialog = new Dialog(AboutUs.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<About_Us_Model> call = Apis.getAPIService().aboutus(map);
        call.enqueue(new Callback<About_Us_Model>() {
            @Override
            public void onResponse(Call<About_Us_Model> call, Response<About_Us_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                About_Us_Model user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.loadUrl(Apis.URL+user.getAbout());
                     } else {
                        dialog.dismiss();
                        Custom_Toast_Activity.makeText(AboutUs.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                    }
                }
                else {
                    dialog.dismiss();
                    Custom_Toast_Activity.makeText(AboutUs.this, "Something wents Worng", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }
            @Override
            public void onFailure(Call<About_Us_Model> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
}
