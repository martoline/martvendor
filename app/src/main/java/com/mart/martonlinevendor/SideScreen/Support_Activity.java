package com.mart.martonlinevendor.SideScreen;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Get_Support_Subject_Model;
import com.mart.martonlinevendor.Model.Support_Submit_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Support_Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private EditText et_nationality;
    private EditText et_email;
    private Button btn_submit;
    private TextView iv_facebook;
    private RelativeLayout FrameLayout1;
    private Spinner choose_subject;
    List<Get_Support_Subject_Model.Category> array_subject_list;
    String category_id = "";
    UserSessionManager session;
    HashMap<String, String> user;
    String Phone="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        et_email = findViewById(R.id.et_email);
        btn_submit = findViewById(R.id.btn_submit);
        iv_facebook = findViewById(R.id.iv_facebook);
        FrameLayout1 = findViewById(R.id.FrameLayout1);
        choose_subject = findViewById(R.id.choose_subject);

        btn_submit.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        map.put("method", "getsupport");
        getSubject(map);
        iv_bellnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if (getIntent().getStringExtra("Status").equals("2")) {
                    Snackbar.make(btn_submit, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    Support_Activity.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(Support_Activity.this, R.color.alertcolor)).show();
                } else {
                     startActivity(new Intent(Support_Activity.this, Notification_Screen.class));

                 }
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getIntent().getStringExtra("Status").equals("2")) {
                    Snackbar.make(btn_submit, "Complete Your Profile and Verified By Admin",
                            Snackbar.LENGTH_SHORT)
                            .setActionTextColor(ContextCompat.getColor(
                                    Support_Activity.this, R.color.colorWhite))
                            .setBackgroundTint(ContextCompat.getColor(Support_Activity.this, R.color.alertcolor)).show();
                } else {
                    startActivity(new Intent(Support_Activity.this, SearchProduct.class));

                }

            }
        });
        choose_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String Company = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();

                category_id = array_subject_list.get(i).getId();

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        iv_facebook.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                submit();
                break;
                case R.id.iv_facebook:
                    Intent callIntent = new Intent(Intent.ACTION_VIEW);
                    callIntent. setData(Uri.parse("tel:" + Phone));
                    startActivity(callIntent);
                break;
        }
    }

    private void submit() {
        // validate
        if (category_id.equals("0")){
            Snackbar.make(btn_submit, "Please Select Your  Subject",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Support_Activity.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Support_Activity.this, R.color.alertcolor))
                    .show();
            return;
        }

        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
          //  Toast.makeText(this, "Description", Toast.LENGTH_SHORT).show();
            Snackbar.make(btn_submit, "Please Enter Your  Description ",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Support_Activity.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Support_Activity.this, R.color.alertcolor))
                    .show();
            return;
        }

        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("method", "supportrequest");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("categoryId", category_id);
        map.put("message", et_email.getText().toString());
        Support_Submit(map);

    }

    public void getSubject(final Map<String, String> map) {
        final Dialog pDialog = new Dialog(Support_Activity.this);
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDialog.setContentView(R.layout.dialog_login);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.show();
        Call<Get_Support_Subject_Model> call = Apis.getAPIService().getSupportSubject(map);

        call.enqueue(new Callback<Get_Support_Subject_Model>() {
            @Override
            public void onResponse(Call<Get_Support_Subject_Model> call, Response<Get_Support_Subject_Model> response) {
                pDialog.dismiss();
                final Get_Support_Subject_Model user = response.body();
                if (user.getStatus().equals("1")) {
                    iv_facebook.setText(user.getSupportnumber());
                    Phone=user.getSupportnumber();
                    array_subject_list = new ArrayList<>();
                    Get_Support_Subject_Model.Category model = new Get_Support_Subject_Model.Category();
                    model.setId("0");
                    model.setName("Select  Subject");
                    array_subject_list.addAll(user.getCategory());
                    array_subject_list.add(0, model);
                    Subject listAdapter = new Subject(Support_Activity.this, array_subject_list);
                    choose_subject.setAdapter(listAdapter);
                } else {

                }

            }

            @Override
            public void onFailure(Call<Get_Support_Subject_Model> call, Throwable t) {
                pDialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void Support_Submit(final Map<String, String> map) {
        final Dialog pDialog = new Dialog(Support_Activity.this);
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDialog.setContentView(R.layout.dialog_login);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.show();
        Call<Support_Submit_Model> call = Apis.getAPIService().supportsubmit(map);

        call.enqueue(new Callback<Support_Submit_Model>() {
            @Override
            public void onResponse(Call<Support_Submit_Model> call, Response<Support_Submit_Model> response) {
                pDialog.dismiss();
                final Support_Submit_Model user = response.body();
                if (user.getStatus().equals("1")) {
                    Custom_Toast_Activity.makeText(getApplicationContext(), user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);

                } else {
                    Custom_Toast_Activity.makeText(getApplicationContext(), user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }

            }

            @Override
            public void onFailure(Call<Support_Submit_Model> call, Throwable t) {
                pDialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public class Subject extends BaseAdapter {
        List<Get_Support_Subject_Model.Category> list;
        Context c;

        public Subject(Context c, List<Get_Support_Subject_Model.Category> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getName());


            return row;
        }
    }
}
