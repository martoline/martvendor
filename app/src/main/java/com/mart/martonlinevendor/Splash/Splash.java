package com.mart.martonlinevendor.Splash;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

public class Splash extends AppCompatActivity  {

    private ImageView imageView;
    private TextView textView;
    private Button button;
    HashMap<String, String> user;
    SharedPreferences.Editor editor;
    UserSessionManager session;
    public static final String USER_LOGIN_DATA = "User Login Data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        editor = getSharedPreferences(USER_LOGIN_DATA, MODE_PRIVATE).edit();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                if (user.get(UserSessionManager.KEY_ID) == null) {
                    session.checkLogin();
                    finish();
                } else if (user.get(UserSessionManager.KEY_ID).equals("0")) {
                    session.checkLogin();
                } else {
                    session.checkLogin();
                    finish();
                }

            }
        }, 3000);

    }


}
