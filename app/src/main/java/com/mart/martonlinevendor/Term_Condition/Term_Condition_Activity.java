package com.mart.martonlinevendor.Term_Condition;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mart.martonlinevendor.R;


public class Term_Condition_Activity extends AppCompatActivity {

    private ImageView iv_menu;
    private ImageView iv_send;
    private TextView et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term__condition_);
        initView();
    }

    private void initView() {
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        et_search = (TextView) findViewById(R.id.et_search);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        iv_bellnotification = (ImageView) findViewById(R.id.iv_bellnotification);
        header = (RelativeLayout) findViewById(R.id.header);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://stageofproject.com/marto/privacy.php");
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}