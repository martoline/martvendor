package com.mart.martonlinevendor.Vendor_Add_Category;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.mart.martonlinevendor.Api.Apis;
import com.mart.martonlinevendor.Model.Add_Category_Model;
import com.mart.martonlinevendor.Model.Add_Sub_Categorey_List_Model;
import com.mart.martonlinevendor.Model.Sub_Cat_Deactive_Model;
import com.mart.martonlinevendor.Model.Vendor_Category_Model;
import com.mart.martonlinevendor.NotificatinList.Notification_Screen;
import com.mart.martonlinevendor.R;
import com.mart.martonlinevendor.Search.SearchProduct;
import com.mart.martonlinevendor.Session_Manager.UserSessionManager;
import com.mart.martonlinevendor.Swipe_Custom_Class.SwipeRevealLayout;
import com.mart.martonlinevendor.custom_toast.Custom_Toast_Activity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Category extends AppCompatActivity implements View.OnClickListener, BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {

    private ImageView iv_menu;
    private ImageView iv_send;
    private EditText et_search;
    private ImageView iv_voice;
    private ImageView iv_bellnotification;
    private RelativeLayout header;
    private ImageView shopimage;
    private CardView shopimage_rel;
    private LinearLayout photo_rel;
    private Spinner choose_category;
    private LinearLayout spinner_rel;
    private EditText et_category_name;
    private Button btn_submit;

    String tagg = "0";
    String shop_image = "";
    UserSessionManager session;
    HashMap<String, String> user;
    String category_id;
    List<Vendor_Category_Model.Response> array_categories_list;
    RecyclerView subcate_list;
    List<Add_Sub_Categorey_List_Model.Response>array_sub_list;
    String status="";
    String message="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__category);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }
    private void initView() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_send = findViewById(R.id.iv_send);
        et_search = findViewById(R.id.et_search);
        iv_voice = findViewById(R.id.iv_voice);
        iv_bellnotification = findViewById(R.id.iv_bellnotification);
        header = findViewById(R.id.header);
        shopimage = findViewById(R.id.shopimage);
        shopimage_rel = findViewById(R.id.shopimage_rel);
        photo_rel = findViewById(R.id.photo_rel);
        choose_category = findViewById(R.id.choose_category);
        spinner_rel = findViewById(R.id.spinner_rel);
        et_category_name = findViewById(R.id.et_category_name);
        btn_submit = findViewById(R.id.btn_submit);
        subcate_list = findViewById(R.id.subcate_list);

        btn_submit.setOnClickListener(this);
        photo_rel.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        et_search.setOnClickListener(this);

        Map<String, String> map = new HashMap<>();
        map.put("method", "VendorCategory");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        GetCategories(map);
        choose_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String Company = ((TextView) view.findViewById(R.id.spinner_text)).getText().toString();

                category_id = array_categories_list.get(i).getCatId();
               // Toast.makeText(Add_Category.this,category_id+"",Toast.LENGTH_LONG).show();

            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        iv_bellnotification.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                  submit();
                break;
                case R.id.iv_bellnotification:
                    startActivity(new Intent(Add_Category.this, Notification_Screen.class));
                break;
            case R.id.photo_rel:
                tagg = "1";
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.mart.martonlinevendor")
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
                break;
            case R.id.iv_menu:
                onBackPressed();
                break;
            case R.id.et_search:
               startActivity(new Intent(Add_Category.this, SearchProduct.class));
                break;
        }
    }

    private void submit() {
        // validate
//        String search = et_search.getText().toString().trim();
//        if (TextUtils.isEmpty(search)) {
//            Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
//            return;
//        }
        if (shop_image.equals("")){
            Snackbar.make(btn_submit, "Select Category Shop Image",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Add_Category.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Add_Category.this,R.color.alertcolor))
                    .show();
            return;
        }
        if (category_id.equals("0")){
            Snackbar.make(btn_submit, "Select Category ",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Add_Category.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Add_Category.this,R.color.alertcolor))
                    .show();
            return;
        }
        String name = et_category_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Snackbar.make(btn_submit, "Enter Sub Category Name ",
                    Snackbar.LENGTH_SHORT)
                    .setActionTextColor(ContextCompat.getColor(
                            Add_Category.this, R.color.colorWhite))
                    .setBackgroundTint(ContextCompat.getColor(Add_Category.this,R.color.alertcolor))
                    .show();
            return;
        }
        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("method", "Add_category");
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("category_id", category_id);
        map.put("name", name);
        map.put("image", shop_image);
        Add_Category(map);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(Add_Category.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        if (tagg.equals("1")) {
            // tv_passport_name.setText(uri.getPath());
            Log.d("image", uri.getPath());
            Bitmap thumbnail = (BitmapFactory.decodeFile(uri.getPath()));

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                shopimage_rel.setVisibility(View.VISIBLE);
                shopimage.setImageBitmap(selectedImage);
                shop_image = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    public void Add_Category(final Map<String, String> map) {
        final Dialog dialog = new Dialog(Add_Category.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Category_Model> call = Apis.getAPIService().addCategory(map);
        call.enqueue(new Callback<Add_Category_Model>() {
            @Override
            public void onResponse(Call<Add_Category_Model> call, Response<Add_Category_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Add_Category_Model user = response.body();
                if (user != null) {
                    if (user.getStatus().equals("1")) {
                        Custom_Toast_Activity.makeText(Add_Category.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                          finish();
                    } else {
                        dialog.dismiss();
                        Custom_Toast_Activity.makeText(Add_Category.this, user.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);
                    }
                } else {
                    dialog.dismiss();
                    Custom_Toast_Activity.makeText(Add_Category.this, "Something wents Worng", Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);


                }
            }

            @Override
            public void onFailure(Call<Add_Category_Model> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    private void GetCategories(Map<String, String> map) {
        final Dialog dialog = new Dialog(Add_Category.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Vendor_Category_Model> call = Apis.getAPIService().getVendorCategory(map);
        call.enqueue(new Callback<Vendor_Category_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Vendor_Category_Model> call, @NonNull Response<Vendor_Category_Model> response) {
                dialog.dismiss();
                Vendor_Category_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        array_categories_list = new ArrayList<>();
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getsubcategory");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        Get_Sub_Categories(map);
                       // array_categories_list.addAll(userdata.getResponse());
                        Vendor_Category_Model.Response model = new Vendor_Category_Model.Response();
                        model.setCatId("0");
                        model.setCatName("Select  Category");
                        array_categories_list.addAll(userdata.getResponse());
                        array_categories_list.add(0, model);
                        Category listAdapter = new Category(Add_Category.this, array_categories_list);
                        choose_category.setAdapter(listAdapter);
                    } else {

                        Custom_Toast_Activity.makeText(Add_Category.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);


                    }
                } else {
                    Custom_Toast_Activity.makeText(Add_Category.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Vendor_Category_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public class Category extends BaseAdapter {
        List<Vendor_Category_Model.Response> list;
        Context c;

        public Category(Context c, List<Vendor_Category_Model.Response> list) {
            this.list = list;
            this.c = c;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row;
            LayoutInflater inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                row = inflater.inflate(R.layout.spinner_support_text, parent,
                        false);
            } else {
                row = convertView;
            }
            TextView spinner_text = row.findViewById(R.id.spinner_text);
            spinner_text.setText(list.get(position).getCatName());


            return row;
        }
    }
    private void Get_Sub_Categories(Map<String, String> map) {
        final Dialog dialog = new Dialog(Add_Category.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Add_Sub_Categorey_List_Model> call = Apis.getAPIService().getSubList(map);
        call.enqueue(new Callback<Add_Sub_Categorey_List_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Add_Sub_Categorey_List_Model> call, @NonNull Response<Add_Sub_Categorey_List_Model> response) {
                dialog.dismiss();
                Add_Sub_Categorey_List_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {

                        array_sub_list = new ArrayList<>();
                        // array_ssub_catgories_list=new ArrayList<>();
                        array_sub_list.addAll(userdata.getResponse());
                        subcate_list.setAdapter(new CategoryAdapter());
//                        for (int i = 0; i < array_categories_list.size(); i++) {
//                            if (array_categories_list.get(i).getCatId().equals(subdata)) {
//                                array_ssub_catgories_list.addAll(userdata.getResponse().get(i).getSubCatData());
//                            }
//
//                        }
                    }
                } else {
                    Custom_Toast_Activity.makeText(Add_Category.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Add_Sub_Categorey_List_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    private void Sub_Categories_Active(Map<String, String> map) {
        final Dialog dialog = new Dialog(Add_Category.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Sub_Cat_Deactive_Model> call = Apis.getAPIService().Sub_Cat_Active(map);
        call.enqueue(new Callback<Sub_Cat_Deactive_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(@NonNull Call<Sub_Cat_Deactive_Model> call, @NonNull Response<Sub_Cat_Deactive_Model> response) {
                dialog.dismiss();
                Sub_Cat_Deactive_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatus().equals("1")) {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "getsubcategory");
                        map.put("userId", user.get(UserSessionManager.KEY_ID));
                        Get_Sub_Categories(map);
                        Custom_Toast_Activity.makeText(Add_Category.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.SUCCESS);
                    }
                } else {
                    Custom_Toast_Activity.makeText(Add_Category.this, userdata.getMsg(), Custom_Toast_Activity.LENGTH_LONG, Custom_Toast_Activity.WARNING);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Sub_Cat_Deactive_Model> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


        public CategoryAdapter() {

        }

        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_get_sub_category, parent, false);
            CategoryAdapter.ViewHolder viewHolder = new CategoryAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
            holder.tv_categoryName.setText(array_sub_list.get(position).getName());
            holder.tv_categoryName.setSelected(true);
//            holder.tv_category_details.setText(array_categories_list.get(position).getOffertest());
            Glide.with(Add_Category.this)
                    .load(Apis.IMAGE_PATH + array_sub_list.get(position).getImage())
                    .error(R.drawable.no_image)
                    .into(holder.iv_category);
            if (array_sub_list.get(position).getStatus().equals("1")){
                holder.tv_activate.setText("Active");
                holder.tv_activate.setTextColor(Color.parseColor("#4CAF50"));
                holder.status_text.setText("Inactive");
            }else {
                holder.tv_activate.setTextColor(Color.parseColor("#F31F0F"));
                holder.tv_activate.setText("Inactive");
                holder.status_text.setText("Active");
            }

            holder.active_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.status_text.getText().toString().equals("Active")){
                        status="1";
                        message="Do You Active this Subcategory?";
                    }else {
                        status="2";
                        message="Do You Inactive this Subcategory?";
                    }
                    SweetAlertDialog pDialog = new SweetAlertDialog(Add_Category.this, SweetAlertDialog.SUCCESS_TYPE);
                    pDialog.setTitleText("Mart'Oline");
                    pDialog.setContentText(message);
                    pDialog.setConfirmText("Yes");
                    pDialog.setCancelText("No");
                    pDialog.setCancelable(false);
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                        }
                    });
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            holder.swipe_layout.close(true);
                            Map<String, String> map = new HashMap<>();
                            map.put("method", "updatecategorystatus");
                            map.put("id", array_sub_list.get(position).getId());
                            map.put("userId", user.get(UserSessionManager.KEY_ID));
                            map.put("status", status);
                            Sub_Categories_Active(map);
                        }
                    }).show();
                }
            });
        }


        @Override
        public int getItemCount() {
            return array_sub_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private RelativeLayout card_shop_category;
            private ImageView iv_category;
            private TextView tv_categoryName,tv_activate;
            private SwipeRevealLayout swipe_layout;
            private TextView status_text;
            private LinearLayout activate;
            private FrameLayout active_layout;
            public ViewHolder(View itemView) {
                super(itemView);
                this.card_shop_category = itemView.findViewById(R.id.card_shop_category);
                this.iv_category = itemView.findViewById(R.id.iv_category);
                this.tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
                this.tv_activate = itemView.findViewById(R.id.tv_activate);
                this.status_text = itemView.findViewById(R.id.status_text);
                this.activate = itemView.findViewById(R.id.activate);
                this.swipe_layout = itemView.findViewById(R.id.swipe_layout);
                this.active_layout = itemView.findViewById(R.id.active_layout);
            }
        }
    }

}
